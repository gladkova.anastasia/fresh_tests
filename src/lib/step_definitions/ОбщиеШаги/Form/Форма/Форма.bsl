﻿
///////////////////////////////////////////////////
// Служебные функции и процедуры
///////////////////////////////////////////////////

&НаКлиенте

// контекст фреймворка Vanessa-Automation
Перем Ванесса;

&НаКлиенте

// Структура, в которой хранится состояние сценария между выполнением шагов. Очищается перед выполнением каждого сценария.
Перем Контекст Экспорт;

&НаКлиенте

// Структура, в которой можно хранить служебные данные между запусками сценариев. Существует, пока открыта форма Vanessa-Automation.
Перем КонтекстСохраняемый Экспорт;

&НаКлиенте

// Функция экспортирует список шагов, которые реализованы в данной внешней обработке.
Функция ПолучитьСписокТестов(КонтекстФреймворкаBDD) Экспорт
	
	Ванесса = КонтекстФреймворкаBDD;
	
	ВсеТесты = Новый Массив;
	
	// описание параметров
	//Ванесса.ДобавитьШагВМассивТестов(ВсеТесты,Снипет,ИмяПроцедуры,ПредставлениеТеста,ОписаниеШага,ТипШага,Транзакция,Параметр);
	
	Ванесса.ДобавитьШагВМассивТестов(ВсеТесты,
									 "ЯВключаюВсеФункциональныеОпции()",
									 "ЯВключаюВсеФункциональныеОпции",
									 "Дано Я включаю все функциональные опции",
									 "",
									 "");
	Ванесса.ДобавитьШагВМассивТестов(ВсеТесты,
									 "ЯОтключаюВсеРегламентныеЗадания()",
									 "ЯОтключаюВсеРегламентныеЗадания",
									 "Дано Я отключаю все регламентные задания",
									 "",
									 "");
	
	Возврат ВсеТесты;
	
КонецФункции

&НаСервере

// Служебная функция.
Функция ПолучитьМакетСервер(ИмяМакета)
	
	ОбъектСервер = РеквизитФормыВЗначение("Объект");
	
	Возврат ОбъектСервер.ПолучитьМакет(ИмяМакета);

КонецФункции

&НаКлиенте

// Служебная функция для подключения библиотеки создания fixtures.
Функция ПолучитьМакетОбработки(ИмяМакета) Экспорт
	
	Возврат ПолучитьМакетСервер(ИмяМакета);

КонецФункции

///////////////////////////////////////////////////
// Работа со сценариями
///////////////////////////////////////////////////

&НаКлиенте

// Функция выполняется перед началом каждого сценария
Функция ПередНачаломСценария() Экспорт
	
КонецФункции

&НаКлиенте

// Функция выполняется перед окончанием каждого сценария
Функция ПередОкончаниемСценария() Экспорт
	
КонецФункции

///////////////////////////////////////////////////
// Реализация шагов
///////////////////////////////////////////////////

&НаКлиенте

// Дано Я включаю все функциональные опции
//@ЯВключаюВсеФункциональныеОпции()
Функция ЯВключаюВсеФункциональныеОпции() Экспорт
	
	ВключитьВсеФункциональныеОпции();
	
КонецФункции

&НаСервере
Процедура ВключитьВсеФункциональныеОпции()
	
	к2ЛицензированиеВызовСервераПолныеПрава.ОтключитьОтправкуСтатистики();
	
	мз = РегистрыСведений.к2НастройкиЛицензирования.СоздатьМенеджерЗаписи();
	
	Для каждого цРесурс Из Метаданные.РегистрыСведений.к2НастройкиЛицензирования.Ресурсы Цикл
		
		мз[цРесурс.Имя] = Истина;
		
	КонецЦикла;
	
	мз.Записать();
	
	Константы.ВыполнятьЗамерыПроизводительности.Установить(Истина);
	Константы.ИспользоватьАнкетирование.Установить(Истина);
	
	// Бизнес-процессы не настроены для работы
	Константы.ИспользоватьБизнесПроцессыИЗадачи.Установить(Ложь);
	Константы.ИспользоватьДатуИВремяВСрокахЗадач.Установить(Ложь);
	Константы.ИспользоватьДатуНачалаЗадач.Установить(Ложь);
	Константы.ИспользоватьПодчиненныеБизнесПроцессы.Установить(Ложь);
	
	Константы.ИспользоватьВерсионированиеОбъектов.Установить(Истина);
	Константы.ИспользоватьГруппыПользователей.Установить(Истина);
	Константы.ИспользоватьДатыЗапретаИзменения.Установить(Истина);
	Константы.ИспользоватьДополнительныеОтчетыИОбработки.Установить(Истина);
	Константы.ИспользоватьДополнительныеРеквизитыИСведения.Установить(Истина);
	Константы.ИспользоватьЗаметки.Установить(Истина);
	Константы.ИспользоватьНапоминанияПользователя.Установить(Истина);
	Константы.ИспользоватьНесколькоПроизводственныхКалендарей.Установить(Истина);
	
	Константы.ИспользоватьПолнотекстовыйПоиск.Установить(Истина);
	Константы.ИспользоватьПочтовыйКлиент.Установить(Истина);
	Константы.ИспользоватьПрочиеВзаимодействия.Установить(Истина);
	Константы.ИспользоватьУдалениеПомеченныхОбъектов.Установить(Истина);
	Константы.ИспользоватьУчетОригиналовПервичныхДокументов.Установить(Истина);
	Константы.ИспользоватьШаблоныСообщений.Установить(Истина);
	Константы.ИспользоватьШифрование.Установить(Истина);
	Константы.ИспользоватьЭлектроннуюПочтуВШаблонахСообщений.Установить(Истина);
	Константы.ИспользоватьЭлектронныеПодписи.Установить(Истина);
	Константы.к2АвтоматическиПерерабатыватьСырьеПослеПриемкиГИСМ.Установить(Истина);
	Константы.к2АвтоматическиФормироватьСчетаФактуры.Установить(Истина);
	Константы.к2ВестиУчетМаркируемойПродукцииИСМП.Установить(Истина);
	Константы.к2ВестиУчетПоСменам.Установить(Истина);
	Константы.к2ВестиУчетПоТаре.Установить(Истина);
	Константы.к2ВестиУчетТоваровОрганизации.Установить(Истина);
	Константы.к2ИспользоватьНеавтоматизированныеСклады.Установить(Истина);
	Константы.к2ИспользоватьПланыПотребленияПриПланированииПродаж.Установить(Истина);
	Константы.к2ИспользоватьПроверкиДокументов.Установить(Истина);
	Константы.к2ИспользоватьРасчетСырьевойСебестоимости.Установить(Истина);
	Константы.к2ИспользоватьСглаживаниеПериодовПланирования.Установить(Истина);
	Константы.к2ИспользоватьШтрихКодыСкладовДляПодтвержденияПеремещений.Установить(Истина);
	
КонецПроцедуры

&НаКлиенте

// Дано Я отключаю все регламентные задания
//@ЯОтключаюВсеРегламентныеЗадания()
Функция ЯОтключаюВсеРегламентныеЗадания() Экспорт
	
	ВключитьВсеФункциональныеОпции();
	
КонецФункции

&НаСервере
Процедура ОтключитьВсеРегламентныеЗадания()
	
	Для каждого цРеглЗадание Из Метаданные.РегламентныеЗадания Цикл
		
		РегламентныеЗаданияСервер.УстановитьИспользованиеПредопределенногоРегламентногоЗадания(цРеглЗадание, Ложь);
		
	КонецЦикла;
	
КонецПроцедуры
