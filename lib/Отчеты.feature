#language: ru

@ignore
@exportscenarios

Функционал: Общие сценарии работы с отчетами

Сценарий: Я в отчете устанавливаю период "ДатаНачала" "ДатаОкончания"

	И я жду доступности элемента с именем "Период1ДатаНачала" в течение 20 секунд
	И в поле с именем 'Период1ДатаНачала' я ввожу текст "ДатаНачала"
	И я жду доступности элемента с именем "Период1ДатаОкончания" в течение 20 секунд
	И в поле с именем 'Период1ДатаОкончания' я ввожу текст "ДатаОкончания"

Сценарий: Я в отчете устанавливаю период "ДатаНачала" по текущую дату

	И я жду доступности элемента с именем "Период1ДатаНачала" в течение 20 секунд
	И в поле с именем 'Период1ДатаНачала' я ввожу текст "ДатаНачала"
	И я жду доступности элемента с именем "Период1ДатаОкончания" в течение 20 секунд
	И в поле с именем 'Период1ДатаОкончания' я ввожу текущую дату

Сценарий: Я в отчете устанавливаю период дату "ДатаПериода"

	И я жду доступности элемента "Период" в течение 20 секунд
	И в поле 'Период' я ввожу текст "ДатаПериода"