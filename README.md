# Тесты для проекта Fresh

## Рекомендации по локальному запуску

В обработке Vanessaa Automation
- Закладка `Библиотеки`
    - Добавить путь к катологу библиотеки `.\lib`
- Закладка `Сервис - Основные`
    - В `Список исключаемых тегов` добавить:
         - `ignore` если установлен клиент OPC
         - `ignoreOPCauto` если opc клиент не установлен
- Закладка `Сервис - Выполнение сценариев`
    - Включить галку `Остановка при возникновении ошибки`
    - Включить галку `Приравнивать pending к failed`


## Запуск в Jenkins

Все джсоны:
```
/JSON/Smoke.json,/JSON/01_Init.json,/JSON/k2/01_pricing.json,/JSON/k2/02_checking_customer_orders.json,/JSON/k2/03_logistic.json,/JSON/k2/04_labeling.json,/JSON/k2/05_cutting.json,/JSON/k2/06_fzp.json,/JSON/k2/07_tma.json,/JSON/k2/08_purchase.json,/JSON/k2/09_CARC.json,/JSON/k2/10_scp.json,/JSON/k2/11_orders_confirmation.json,/JSON/k2/12_checking_transfer_orders.json,/JSON/k2/13_sales_of_products.json,/JSON/k2/14_moving_products.json,/JSON/k2/15_products_of_organizations.json,/JSON/k2/16_sales_of_products_RLS.json,/JSON/k2/17_moving_products_RLS.json,/JSON/k2/18_transport_logistics.json,/JSON/mes/milk_reception.json,/JSON/mes/02_milk_production_1.json,/JSON/mes/02_milk_production_2.json,/JSON/mes/quality_control.json,/JSON/mes/drivers_of_electric_car.json,/JSON/mes/OPC.json,/JSON/mes/cheese_maker.json,/JSON/mes/piece_rate_calculation.json,/JSON/mes/cost_price.json,/JSON/mes/milk_scenario.json
```

Общее:
```
/JSON/Smoke.json,/JSON/01_Init.json
```

k2:
```
/JSON/k2/01_pricing.json,/JSON/k2/02_checking_customer_orders.json,/JSON/k2/03_logistic.json,/JSON/k2/04_labeling.json,/JSON/k2/05_cutting.json,/JSON/k2/06_fzp.json,/JSON/k2/07_tma.json,/JSON/k2/08_purchase.json,/JSON/k2/09_CARC.json,/JSON/k2/10_scp.json,/JSON/k2/11_orders_confirmation.json,/JSON/k2/12_checking_transfer_orders.json,/JSON/k2/13_sales_of_products.json,/JSON/k2/14_moving_products.json,/JSON/k2/15_products_of_organizations.json,/JSON/k2/16_sales_of_products_RLS.json,/JSON/k2/17_moving_products_RLS.json,/JSON/k2/18_transport_logistics.json
```

mes:
```
/JSON/mes/milk_reception.json,/JSON/mes/02_milk_production_1.json,/JSON/mes/02_milk_production_2.json,/JSON/mes/quality_control.json,/JSON/mes/drivers_of_electric_car.json,/JSON/mes/OPC.json,/JSON/mes/cheese_maker.json,/JSON/mes/piece_rate_calculation.json,/JSON/mes/cost_price.json,/JSON/mes/milk_scenario.json
```
## Использование батников

### Описание

#### `scripts\cf\` Скрипты загрузки из cf

В настройках: `SRC_CF`

- **scripts\cf\PrepareTestIB__cf.bat** - Подготавливает базу в основной каталог на основе cf
- **scripts\cf\restore_update__cf.bat** - Восстанавливает основную базу из каталога-бекапа и обновляет cf-ником
- **scripts\cf\update__cf.bat** - Обновляет основную базу cf-ником

#### `scripts\cf_YDisk\` Скрипты скачивания cf с яндекс.диска и загрузки из него

В настройках: `YDISK_PUBLIC_HREF`, `YDISK_DIR`, `YDISK_CF`

- **scripts\cf_YDisk\PrepareTestIB__cf.bat** - Подготавливает базу в основной каталог на основе cf
- **scripts\cf_YDisk\restore_update__cf.bat** - Восстанавливает основную базу из каталога-бекапа и обновляет cf-ником
- **scripts\cf_YDisk\update__cf.bat** - Обновляет основную базу cf-ником

#### `scripts\demoDB\` Скрипты работы с демо-базой

- **scripts\demoDB\dumpToDT.bat** - Выгружает базу в ДТ. В настройках: `RESULT_DT`
- **scripts\demoDB\PrepareTestIB__cf_ydisk.bat** - Загружает базу из дт `MODEL_DT` и обновляет cf с яндекс диска `YDISK_CF`
- **scripts\demoDB\PrepareTestIB__cf.bat** - Загружает базу из дт `MODEL_DT` и обновляет cf `SRC_CF`
- **scripts\demoDB\PrepareTestIB__storage.bat** - Загружает базу из дт `MODEL_DT` и обновляет из хранилища `STORAGE_NAME`

#### `scripts\main\` Общие скрипты

- **scripts\main\backup_TestIB.bat** - Копирует базу в каталог-бекап
- **scripts\main\Open_VA.bat** - Открывает базу в режиме менеджера тестирования, запускает там VA и передает настройки
- **scripts\main\restoreTestIB.bat** - Восстанавливает основную базу из бекапа
- **scripts\main\run_designer.bat** - Запускает конфигуратор основной базы

#### `scripts\storage\` Скрипты загрузки из хранилища

- **scripts\storage\Prepare__ToBck__storage.bat** - Подготавливает базу в каталог-бекап на основе данных хранилища
- **scripts\storage\PrepareTestIB__storage.bat** - Подготавливает базу в основной каталог на основе данных хранилища
- **scripts\storage\restore_update__storage.bat** - Восстанавливает основную базу из каталога-бекапа и обновляет из хранилища
- **scripts\storage\update__storage.bat** - Обновляет основную базу из хранилища

### Подготовка настроек

1. Создать каталог в корне `.local`
2. Скопировать туда файлы из `examples`
3. В `.local\env.json` указать версию платфрмы, под которой будет все выполнятся
4. В `.local\init_env.bat` указать все значения параметров для текущего окружения
5. В `.local\VA.json` указать текущие настройки VA. Файл можно получить из самой VA

## Настройка окружения

```cmd
opm install vanessa-runner
opm install add
opm install vanessa-automation
opm install yadisk
```