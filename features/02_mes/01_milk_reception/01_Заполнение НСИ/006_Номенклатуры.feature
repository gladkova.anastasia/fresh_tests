﻿#language: ru

@milk_reception

Функционал: 01_milk_reception.01.006 Заполнение справочника Номенклатура + Упаковки номенклатуры

Сценарий: 01_milk_reception.01.006 Заполнение справочника Номенклатура. Потери
	Дано Я подключаюсь под "Администратор"
	И Я начинаю создавать Номенклатуру "milk_rec_Потери" с видом "milk_rec_Потери" и типом "Сырье"
	И Я заканчиваю создавать Номенклатуру

Сценарий: 01_milk_reception.01.006 Заполнение справочника Номенклатура. Сырье
	И Я начинаю создавать Номенклатуру "milk_rec_Молоко" с видом "milk_rec_Молочное сырье" и типом "Сырье"
	И Я заканчиваю создавать Номенклатуру