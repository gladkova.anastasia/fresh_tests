﻿#language: ru

@IIoT

Функционал: 05_IIoT.01.009 Заполнение справочника Теги ОПС

Сценарий: 05_IIoT.01.009 Заполнение справочника Теги число 250
	Дано Я подключаюсь под "Администратор"
	И В командном интерфейсе я выбираю 'Производство' 'Теги сервера ОПС'
	Тогда открылось окно 'Теги сервера ОПС'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Тег сервера ОПС (создание)'
	И в поле 'Наименование' я ввожу текст 'Тег число250'
	И я нажимаю кнопку выбора у поля "Сервер OPC"
	Тогда открылось окно 'Серверы ОПС'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'     |
		| 'Сервер Константа' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Тег сервера ОПС (создание) *'
	И из выпадающего списка "Тип значения тега" я выбираю точное значение 'Число'
	И в поле 'Идентификатор тега' я ввожу текст 'ns=4;s=Tank3Value'
	И я нажимаю на кнопку 'Записать'
	И я нажимаю на кнопку "Тест"
	И у элемента с именем "ЗначениеТегаСтрока" я жду значения "250" в течение 10 секунд
	И я закрываю окно 'Тег число250 (Тег сервера ОПС)'
	Тогда открылось окно 'Теги сервера ОПС'
	И Я закрываю окно 'Теги сервера ОПС'

Сценарий: 05_IIoT.01.009 Заполнение справочника Теги число 50
	И В командном интерфейсе я выбираю 'Производство' 'Теги сервера ОПС'
	Тогда открылось окно 'Теги сервера ОПС'	
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Тег сервера ОПС (создание)'
	И в поле 'Наименование' я ввожу текст 'Тег число50'
	И я нажимаю кнопку выбора у поля "Сервер OPC"
	Тогда открылось окно 'Серверы ОПС'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'     |
		| 'Сервер Константа' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Тег сервера ОПС (создание) *'
	И из выпадающего списка "Тип значения тега" я выбираю точное значение 'Число'
	И в поле 'Идентификатор тега' я ввожу текст 'ns=4;s=Tank1Value'
	И я нажимаю на кнопку 'Записать'
	И я нажимаю на кнопку "Тест"
	И у элемента с именем "ЗначениеТегаСтрока" я жду значения "50" в течение 10 секунд
	И я закрываю окно 'Тег число50 (Тег сервера ОПС)'
	Тогда открылось окно 'Теги сервера ОПС'
	И Я закрываю окно 'Теги сервера ОПС'

Сценарий: 05_IIoT.01.009 Заполнение справочника Теги число 100
	И В командном интерфейсе я выбираю 'Производство' 'Теги сервера ОПС'
	Тогда открылось окно 'Теги сервера ОПС'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Тег сервера ОПС (создание)'
	И в поле 'Наименование' я ввожу текст 'Тег число100'
	И я нажимаю кнопку выбора у поля "Сервер OPC"
	Тогда открылось окно 'Серверы ОПС'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'     |
		| 'Сервер Константа' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Тег сервера ОПС (создание) *'
	И из выпадающего списка "Тип значения тега" я выбираю точное значение 'Число'
	И в поле 'Идентификатор тега' я ввожу текст 'ns=4;s=Tank2Value'
	И я нажимаю на кнопку 'Записать'
	И я нажимаю на кнопку "Тест"
	И у элемента с именем "ЗначениеТегаСтрока" я жду значения "100" в течение 10 секунд
	И я закрываю окно 'Тег число100 (Тег сервера ОПС)'
	Тогда открылось окно 'Теги сервера ОПС'
	И Я закрываю окно 'Теги сервера ОПС'

Сценарий: 05_IIoT.01.009 Заполнение справочника Теги дата без смещения
	И В командном интерфейсе я выбираю 'Производство' 'Теги сервера ОПС'
	Тогда открылось окно 'Теги сервера ОПС'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Тег сервера ОПС (создание)'
	И в поле 'Наименование' я ввожу текст 'Тег дата без смещения'
	И я нажимаю кнопку выбора у поля "Сервер OPC"
	Тогда открылось окно 'Серверы ОПС'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'     |
		| 'Сервер Константа' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Тег сервера ОПС (создание) *'
	И из выпадающего списка "Тип значения тега" я выбираю точное значение 'Дата'
	И в поле 'Идентификатор тега' я ввожу текст 'ns=4;s=Tank4Time'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Тег сервера ОПС (создание) *' в течение 20 секунд
	Тогда открылось окно 'Теги сервера ОПС'
	И Я закрываю окно 'Теги сервера ОПС'

Сценарий: 05_IIoT.01.009 Заполнение справочника Теги дата со смещением
	И В командном интерфейсе я выбираю 'Производство' 'Теги сервера ОПС'
	Тогда открылось окно 'Теги сервера ОПС'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Тег сервера ОПС (создание)'
	И в поле 'Наименование' я ввожу текст 'Тег дата со смещением'
	И я нажимаю кнопку выбора у поля "Сервер OPC"
	Тогда открылось окно 'Серверы ОПС'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'     |
		| 'Сервер Константа' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Тег сервера ОПС (создание) *'
	И из выпадающего списка "Тип значения тега" я выбираю точное значение 'Дата'
	И в поле 'Идентификатор тега' я ввожу текст 'ns=4;s=Tank4Time'
	И я устанавливаю флаг 'Добавлять смещение часового пояса'
	И в поле 'UTC' я ввожу текст '3,00'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Тег сервера ОПС (создание) *' в течение 20 секунд
	Тогда открылось окно 'Теги сервера ОПС'
	И Я закрываю окно 'Теги сервера ОПС'

Сценарий: 05_IIoT.01.009 Заполнение справочника Теги строка
	И В командном интерфейсе я выбираю 'Производство' 'Теги сервера ОПС'
	Тогда открылось окно 'Теги сервера ОПС'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Тег сервера ОПС (создание)'
	И в поле 'Наименование' я ввожу текст 'Тег строка'
	И я нажимаю кнопку выбора у поля "Сервер OPC"
	Тогда открылось окно 'Серверы ОПС'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'     |
		| 'Сервер Константа' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Тег сервера ОПС (создание) *'
	И из выпадающего списка "Тип значения тега" я выбираю точное значение 'Строка'
	И в поле 'Идентификатор тега' я ввожу текст 'ns=4;s=Tank4Time'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Тег сервера ОПС (создание) *' в течение 20 секунд
	Тогда открылось окно 'Теги сервера ОПС'
	И Я закрываю окно 'Теги сервера ОПС'