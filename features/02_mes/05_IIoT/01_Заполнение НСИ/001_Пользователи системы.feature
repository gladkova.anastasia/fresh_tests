﻿#language: ru

@IIoT

Функционал: 05_IIoT.01.001 Настройка пользователей и прав

Сценарий: 05_IIoT.01.001 Создание групп пользователей
	Дано Я подключаюсь под "Администратор"
	И Я создаю группу доступа "Базовые права" с профилем "Базовый профиль"
	И Я создаю группу доступа "Киоски (меню учетных точек)" с профилем "Киоски(меню учетных точек)"
	И Я создаю группу доступа "Мастера сырцеха" с профилем "Мастера сырцеха"
	И Я создаю группу доступа "Чтение НСИ для внешних источников" с профилем "Чтение НСИ для внешних источников"
	И Я создаю группу доступа "Приемщик молока" с профилем "Приемка молока по количеству"

Сценарий: 05_IIoT.01.001 Создание пользователей
	И Я создаю пользователя "Киоск" с профилями "Базовые права;Киоски (меню учетных точек);Чтение НСИ для внешних источников"
	И Я создаю пользователя "Мастер сырцеха" с профилями "Базовые права;Мастера сырцеха;Чтение НСИ для внешних источников"
	И Я создаю пользователя "Приемщик молока" с профилями "Базовые права;Приемщик молока;Чтение НСИ для внешних источников"
