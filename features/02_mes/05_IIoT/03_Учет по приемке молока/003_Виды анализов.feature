﻿#language: ru

@OPC

Функционал: 05_OPC.03.003 Заполнение справочника Виды анализов номенклатуры с приемкой

Сценарий: 05_OPC.03.003 Создание вида анализа по МДЖ МДБ
	Дано Я подключаюсь под "Администратор"
	И В командном интерфейсе я выбираю 'Производство' 'Виды анализов номенклатуры'
	Тогда открылось окно 'Виды анализов номенклатуры'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Вид анализа номенклатуры (создание)'
	И в поле 'Наименование' я ввожу текст 'показатели МДЖ МДБ'
	И в таблице "Показатели" я нажимаю на кнопку 'Добавить'
	И в таблице "Показатели" я нажимаю кнопку выбора у реквизита "Показатель"
	Тогда открылось окно 'Показатели анализов номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'МДБ'          |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Вид анализа номенклатуры (создание) *'
	И в таблице "Показатели" я нажимаю на кнопку 'Добавить'
	И в таблице "Показатели" я нажимаю кнопку выбора у реквизита "Показатель"
	Тогда открылось окно 'Показатели анализов номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'МДЖ'          |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Вид анализа номенклатуры (создание) *'
	И в таблице "Показатели" я нажимаю на кнопку 'Добавить'
	И в таблице "Показатели" я нажимаю кнопку выбора у реквизита "Показатель"
	Тогда открылось окно 'Показатели анализов номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'      |
		| 'Плотность молока'  |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Вид анализа номенклатуры (создание) *'
	И в таблице "Показатели" я завершаю редактирование строки
	И я нажимаю на кнопку 'Записать'
	И в таблице "Показатели" я перехожу к строке:
		| 'N' | 'Показатель' |
		| '1' | 'МДБ'        |
	И в таблице "Показатели" в поле 'Значение max' я ввожу текст '6,000'
	И в таблице "Показатели" я перехожу к строке:
		| 'N' | 'Показатель' |
		| '2' | 'МДЖ'        |
	И в таблице "Показатели" в поле 'Значение max' я ввожу текст '50,000'
	И я нажимаю на кнопку 'Записать'
	И в таблице "Показатели" я перехожу к строке:
		| 'N' | 'Показатель' |
		| '1' | 'МДБ'        |
	И в таблице "Показатели" в поле 'Значение min' я ввожу текст '0,300'
	И в таблице "Показатели" я перехожу к строке:
		| 'N' | 'Показатель' |
		| '2' | 'МДЖ'        |
	И в таблице "Показатели" в поле 'Значение min' я ввожу текст '0,300'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Показатели МДЖ МДБ (Вид анализа номенклатуры) *' в течение 20 секунд
	Тогда открылось окно 'Виды анализов номенклатуры'
	И Я закрываю окно 'Виды анализов номенклатуры'