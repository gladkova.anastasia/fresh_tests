#language: ru

@OPC

Функционал: 05_OPC.03.999 Проверка на отсутствие ошибок в журнале регистрации с приемкой

Сценарий: 05_OPC.03.999 Обработка Журнал регистрации с приемкой
	Дано Я подключаюсь под "Администратор"
	И Я открываю журнал регистрации и нахожу "0" общих ошибок и "0" ошибок проведений документов по текущему блоку