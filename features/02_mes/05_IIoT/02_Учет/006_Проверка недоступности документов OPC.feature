﻿#language: ru

@IIoT

Функционал: 05_IIoT.02.006 Получение данных с датчика в Приемке и передаче

Сценарий: 05_IIoT.02.006 Проверка недоступности документов, созданных на основании данных ОПС
	Дано Я подключаюсь под "Мастер сырцеха"
	И В командном интерфейсе я выбираю 'Производство' 'Переработки'
	Тогда открылось окно 'Переработки'
	И в таблице "Список" я перехожу к строке:
		| 'Вид операции' | 'Способ создания'                          | 'Статус'    |
		| 'Переработка'  | 'Автоматически при обработке значений ОПС' | 'Выполнено' |
	И в таблице "Список" я активизирую поле "Способ создания"
	И я выбираю пункт контекстного меню с именем 'СписокКонтекстноеМенюПоискПоТекущемуЗначению' на элементе формы с именем "Список"
	И в таблице "Список" я перехожу к первой строке
	И в таблице "Список" я выбираю текущую строку
	Затем Проверяю шаги на Исключение:
		|'И в поле "Дата смены" я ввожу текст "22.11.20"'|
	И Я закрыл все окна клиентского приложения