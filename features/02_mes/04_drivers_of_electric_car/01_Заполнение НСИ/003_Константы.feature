﻿#language: ru

@drivers_of_electric_car

Функционал: 04_drivers.01.003 Заполнение констант

Сценарий: 04_drivers.01.003 Заполнение констант
	Дано Я подключаюсь под "Администратор"
	И В командном интерфейсе я выбираю 'Производство' 'Настройки параметров учета'
	Тогда открылось окно 'Параметры производства'
	И я нажимаю кнопку выбора у поля "Основной график работы предприятия"
	Тогда открылось окно 'Графики работы'
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Параметры производства *'
	И я устанавливаю флаг 'Вести учет по сменам'
	И я устанавливаю флаг 'Отключить использование стандартной формы ресурсных спецификаций'
	И я устанавливаю флаг 'Вести учет остатков на таре и местоположения тары'
	И в поле 'Префикс штрихкодов узла РИБ' я ввожу текст '2'
	И я нажимаю кнопку выбора у поля 'Склад пересчета'
	Тогда открылось окно 'Склады'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Склад ГП'    |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Параметры производства *'
	И я нажимаю кнопку выбора у поля "Единица измерения веса"
	Тогда открылось окно 'Выберите единицу измерения'
	И Я выбираю кг в первый раз
	Тогда открылось окно 'Параметры производства *'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Параметры производства *' в течение 20 секунд
	И я закрываю сеанс TESTCLIENT