﻿#language: ru

Функционал: Учет лаб.анализов созревшего сыра

Сценарий: Учет лаб.анализов и бальности сыра
	Дано Я подключаюсь под "Лаборант"
	И В командном интерфейсе я выбираю 'Производство' 'Мониторинг выполнения лабораторных анализов'
	Тогда открылось окно 'Заявки на анализы номенклатуры'
	И я разворачиваю группу с именем "Фильтры"
	И в таблице "СписокКомпоновщикНастроекПользовательскиеНастройки" я перехожу к строке:
		| 'Настройка'   |
		| 'Вид анализа' |
	И в таблице "СписокКомпоновщикНастроекПользовательскиеНастройки" я нажимаю кнопку выбора у реквизита с именем "СписокКомпоновщикНастроекПользовательскиеНастройкиЗначение"
	Тогда открылось окно 'Виды анализов номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'    |
		| 'Готовый продукт' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Заявки на анализы номенклатуры'
	И я сворачиваю группу с именем "Фильтры"
	И в таблице "Список" я разворачиваю строку с подчиненными:
		| 'Номенклатура'    |
		| 'Готовый продукт' |
	И в таблице "Список" я перехожу к строке:
		| 'Вид анализа'     | 'Номенклатура'   | 'Серия'        | 'Состояние' | 'Характеристика' |
		| 'Готовый продукт' | 'Российский 50%' | '12.03.2019/1' | 'Заявка'    | 'На созревании'  |
	И я нажимаю на кнопку с именем 'СписокКомандаАнализыНоменклатуры'
	Тогда открылось окно 'Анализы номенклатуры (создание)'
	И я перехожу к закладке "Результаты анализов"
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Показатель' |
		| 'Цвет'       |
	И в таблице "РезультатыАнализов" из выпадающего списка "Значение показателя" я выбираю точное значение 'Да'
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Показатель' |
		| 'Рисунок'    |
	И в таблице "РезультатыАнализов" из выпадающего списка "Значение показателя" я выбираю точное значение 'Да'
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Показатель' |
		| 'МДЖ'        |
	И в таблице "РезультатыАнализов" в поле 'Значение показателя' я ввожу текст '28,000'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Анализы номенклатуры (создание)' в течение 20 секунд
	Тогда открылось окно 'Заявки на анализы номенклатуры'
	И Я закрываю окно 'Заявки на анализы номенклатуры'

Сценарий: 10_milk_scenario.05.001 Закрытие клиента тестирования Лаборант
	И я закрываю сеанс TESTCLIENT