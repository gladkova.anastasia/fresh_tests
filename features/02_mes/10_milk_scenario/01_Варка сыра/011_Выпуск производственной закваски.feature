﻿#language: ru

Функционал: Выпуск производственной закваски

Сценарий: Выпуск производственной закваски
	Дано Я подключаюсь под "Мастер сырцеха"
	И В командном интерфейсе я выбираю 'Производство' 'Переработки'
	Тогда открылось окно 'Переработки'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Переработка (создание)'
	И в поле 'от' я ввожу текст '15.03.2019  0:00:00'
	И в поле 'Дата смены' я ввожу текст '15.03.2019'
	И я нажимаю кнопку выбора у поля "Рабочий центр"
	Тогда открылось окно 'Рабочие центры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'    |
		| 'Сырцех' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Переработка (создание) *'
	И я нажимаю кнопку выбора у поля с именем "СкладВыпуск"
	Тогда открылось окно 'Склады'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'    |
		| 'Сырцех' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Переработка (создание) *'
	И я перехожу к закладке "Выпуск"
	И в таблице "Выпуск" я нажимаю на кнопку с именем 'ВыпускДобавить'
	И в таблице "Выпуск" я нажимаю кнопку выбора у реквизита "Номенклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'                                          |
		| 'БАКТЕРИАЛЬНЫЙ КОНЦЕНТРАТ БК-УГЛИЧ-№4 производственная' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Переработка (создание) *'
	И в таблице "Выпуск" я нажимаю кнопку выбора у реквизита "Серия"
	Тогда открылось окно 'Серии номенклатуры'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	И я жду открытия окна 'Помощник регистрации новой серии' в течение 20 секунд
	И в поле 'Номер' я ввожу текст '52.524'
	И в поле 'Дата выпуска' я ввожу текст '15.03.2019'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Серии номенклатуры'
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Переработка (создание) *'
	И в таблице "Выпуск" в поле 'Количество' я ввожу текст '1 000,000'
	И я перехожу к закладке "Распределение материалов"
	И в таблице "РаспределениеМатериалов" я нажимаю на кнопку с именем 'РаспределениеМатериаловДобавить'
	И в таблице "РаспределениеМатериалов" я нажимаю кнопку выбора у реквизита "Номенклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'БК-УГЛИЧ-№4'  |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Переработка (создание) *'
	И в таблице "РаспределениеМатериалов" я нажимаю кнопку выбора у реквизита "Серия"
	Тогда открылось окно 'Серии номенклатуры'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	И я жду открытия окна 'Помощник регистрации новой серии' в течение 20 секунд
	И в поле 'Номер' я ввожу текст '1269'
	И в поле 'Дата выпуска' я ввожу текст '15.03.2019'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Серии номенклатуры'
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Переработка (создание) *'
	И в таблице "РаспределениеМатериалов" в поле 'Количество' я ввожу текст '2,000'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Переработка (создание) *' в течение 20 секунд
	Тогда открылось окно 'Переработки'
	И Я закрываю окно 'Переработки'
