﻿#language: ru

Функционал: Отчет Анализ остатков. На складе ПАЦ

Сценарий: Отчет Анализ остатков. На складе ПАЦ
	Дано Я подключаюсь под "Мастер сырцеха"
	И В командном интерфейсе я выбираю 'Производство' 'Отчеты'
	Тогда открылось окно 'Отчеты по производству'
	И я нажимаю на гиперссылку "Анализ остатков на складах"
	Тогда открылось окно 'Анализ остатков на складах'
	И я снимаю флаг 'Начало периода:'
	И я снимаю флаг 'Конец периода:'
	И я нажимаю на кнопку с именем 'ВсеНастройки'
	Тогда открылось окно 'Настройки отчета "Анализ остатков на складах"'
	И в таблице "Список1" я выбираю текущую строку
	И в таблице "Список1" я нажимаю кнопку выбора у реквизита "Значение"
	Тогда открылось окно 'Склады'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Танк №1'    |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Настройки отчета "Анализ остатков на складах"'
	И в таблице "Список1" я завершаю редактирование строки
	И я нажимаю на кнопку 'Закрыть и сформировать'
	Тогда открылось окно 'Анализ остатков на складах'
	И я снимаю флаг "Конец периода:"
	И я нажимаю на кнопку с именем 'СформироватьОтчет'
	И Ожидаем завершения фонового формирования отчета в течение 50 секунд
	Тогда табличный документ "ОтчетТабличныйДокумент" равен по шаблону:
		| 'Отбор:'                     | 'Склад В списке "Танк №1"'         | ''                  | ''          | ''          | ''                 | ''                  | ''       | ''       | ''                 | ''                  | ''       | ''       | ''                 |
		| ''                           | ''                                 | ''                  | ''          | ''          | ''                 | ''                  | ''       | ''       | ''                 | ''                  | ''       | ''       | ''                 |
		| 'Склад'                      | 'Ячейка'                           | 'Количество'        | ''          | ''          | ''                 | 'Количество 1'      | ''       | ''       | ''                 | 'Количество 2'      | ''       | ''       | ''                 |
		| 'Номенклатура'               | 'Характеристика'                   | 'Начальный остаток' | 'Приход'    | 'Расход'    | 'Конечный остаток' | 'Начальный остаток' | 'Приход' | 'Расход' | 'Конечный остаток' | 'Начальный остаток' | 'Приход' | 'Расход' | 'Конечный остаток' |
		| 'Серия'                      | ''                                 | ''                  | ''          | ''          | ''                 | ''                  | ''       | ''       | ''                 | ''                  | ''       | ''       | ''                 |
		| 'Танк №1'                    | '<пустая ячейка>'                  | ''                  | '1 270,000' | '1 250,000' | '20,000'           | ''                  | ''       | ''       | ''                 | ''                  | ''       | ''       | ''                 |
		| 'Молоко'                     | '<характеристики не используются>' | ''                  | '1 270,000' | '1 250,000' | '20,000'           | ''                  | ''       | ''       | ''                 | ''                  | ''       | ''       | ''                 |
		| '010000045345354345345 до *' | ''                                 | ''                  | '625,000'   | '625,000'   | ''                 | ''                  | ''       | ''       | ''                 | ''                  | ''       | ''       | ''                 |
		| '01000056969878767 до *'     | ''                                 | ''                  | '625,000'   | '625,000'   | ''                 | ''                  | ''       | ''       | ''                 | ''                  | ''       | ''       | ''                 |
		| 'Остаток до 12.03.2019 *'    | ''                                 | ''                  | '20,000'    | ''          | '20,000'           | ''                  | ''       | ''       | ''                 | ''                  | ''       | ''       | ''                 |
		| 'Итого'                      | ''                                 | ''                  | '1 270,000' | '1 250,000' | '20,000'           | ''                  | ''       | ''       | ''                 | ''                  | ''       | ''       | ''                 |
	И Я закрываю окно 'Анализ остатков на складах'
	Тогда открылось окно 'Отчеты по производству'
	И Я закрываю окно 'Отчеты по производству'