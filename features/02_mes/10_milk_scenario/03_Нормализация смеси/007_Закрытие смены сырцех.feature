﻿#language: ru

Функционал: Закрытие смены. Сырцех

Сценарий: Замена серии смеси в переработке
	Дано Я подключаюсь под "Мастер сырцеха"
	И В командном интерфейсе я выбираю 'Производство' 'Переработки'
	Тогда открылось окно 'Переработки'
	И в таблице "Список" я перехожу к строке:
		| 'Вид операции' | 'Дата смены' |  'Рабочий центр' |  'Способ создания'    | 'Статус'    |
		| 'Переработка'  | '12.03.2019' |  'Сырцех'        |  'Приемка и передача' | 'Выполнено' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Переработка *'
	И я перехожу к закладке "Распределение материалов"
	И в таблице "РаспределениеМатериалов" я перехожу к строке:
		|'Номенклатура'|
		|'Смесь 50%'|
	И в таблице "РаспределениеМатериалов" я нажимаю кнопку выбора у реквизита "Серия"
	Тогда открылось окно 'Серии номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Дата выпуска' |  'Номер' |
		| '12.03.2019'   |  '1'     |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Переработка *'
	И в таблице "РаспределениеМатериалов" я завершаю редактирование строки
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Переработка *' в течение 20 секунд
	Тогда открылось окно 'Переработки'
	И Я закрываю окно 'Переработки'

Сценарий: Заполнение ответственных в серии
	И В командном интерфейсе я выбираю 'Производство' 'Переработки'
	Тогда открылось окно 'Переработки'
	И в таблице "Список" я перехожу к строке:
		| 'Вид операции' | 'Дата смены' | 'Ответственный' | 'Рабочий центр' | 'Смена'   | 'Способ создания'    | 'Статус'    |
		| 'Переработка'  | '12.03.2019' | 'Киоск'         | 'Сырцех'        | 'Смена24' | 'Приемка и передача' | 'Выполнено' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Переработка *'
	И я перехожу к закладке "Выпуск"
	И В таблице "Выпуск" я нажимаю кнопку выбора у реквизита "Серия"
	Тогда открылось окно 'Серии номенклатуры'
	И я выбираю пункт контекстного меню с именем 'СписокКонтекстноеМенюИзменить' на элементе формы с именем "Список"
	Тогда открылось окно '12.03.2019/1 (Серия номенклатуры)'
	И я перехожу к закладке "Дополнительно"
	И я нажимаю кнопку выбора у поля "Сыродел"
	Тогда открылось окно 'Физические лица'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'      |
		| 'Карщик Невельская' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно '12.03.2019/1 (Серия номенклатуры) *'
	И я нажимаю кнопку выбора у поля "Солильщик"
	Тогда открылось окно 'Физические лица'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'      |
		| 'Укладчик Сидорова' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно '12.03.2019/1 (Серия номенклатуры) *'
	И я нажимаю кнопку выбора у поля "Мастер"
	Тогда открылось окно 'Физические лица'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'    |
		| 'Укладчик Петров' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно '12.03.2019/1 (Серия номенклатуры) *'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна '12.03.2019/1 (Серия номенклатуры) *' в течение 20 секунд
	Тогда открылось окно 'Серии номенклатуры'
	И я закрываю окно 'Серии номенклатуры'
	Тогда открылось окно 'Переработка *'
	И Я закрываю окно 'Переработка *'
	Тогда открылось окно 'Переработки'
	И Я закрываю окно 'Переработки'

Сценарий: Закрытие смены. Сырцех
	И В командном интерфейсе я выбираю 'Производство' 'Рабочее место мастера смены'
	Тогда открылось окно 'Рабочее место мастера смены*'
	И я перехожу к закладке "Настройки смены"
	И в поле 'Дата смены' я ввожу текст '12.03.2019'
	И я нажимаю кнопку выбора у поля "ОтборВидРабочегоЦентра"
	Тогда открылось окно 'Виды рабочих центров'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Сырцех'       |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Рабочее место мастера смены*'
	И в таблице "ДеревоРабочиеЦентры" я разворачиваю строку с подчиненными:
		| 'Вид РЦ / РЦ' |
		| 'Сырцех'      |
	И в таблице "ДеревоРабочиеЦентры" я перехожу к строке:
		| 'Вид РЦ / РЦ' | 'Статус'     |
		| 'Сырцех'      | 'Не закрыта' |
	И я перехожу к закладке "Выпуск"
	И я жду доступности элемента с именем 'КомандаПерераспределитьСырье' в течение 10 секунд
	И я нажимаю на кнопку 'Перераспределить сырье'
	И я перехожу к закладке "Баланс"
	И я нажимаю на кнопку 'Свести баланс'
	И я жду, что в таблице "ВыпускБаланс" количество строк будет "больше" 0 в течение 10 секунд
	И я нажимаю на кнопку 'Закрыть'
	И в таблице "ДеревоРабочиеЦентры" поле "Статус" имеет значение "Закрыта"
	И Я закрываю окно 'Рабочее место мастера смены*'

Сценарий: Проверка отсутствия документа Акт переработки
	И В командном интерфейсе я выбираю 'Производство' 'Закрытия смен'
	Тогда открылось окно 'Закрытия смен'
	И в таблице "Список" я перехожу к строке:
		| 'Дата смены' | 'Ответственный'  | 'Рабочий центр' | 'Смена'   | 'Способ создания' | 'Статус смены' |
		| '12.03.2019' | 'Мастер сырцеха' | 'Сырцех'        | 'Смена24' | 'Закрытие смены'  | 'Закрыта'      |
	И я нажимаю на кнопку 'Связанные документы'
	Тогда открылось окно 'Связанные документы*'
	И в табличном документе "ТаблицаОтчета" ячейка с адресом "R2C3" равна ''
	И Я закрываю окно 'Связанные документы*'
	Тогда открылось окно 'Закрытия смен'
	И я закрываю окно 'Закрытия смен'