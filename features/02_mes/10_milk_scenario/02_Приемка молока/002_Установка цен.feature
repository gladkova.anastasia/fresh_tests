#language: ru

Функционал: 10_milk_scenario.02.002 Создание документа Установка цен

Сценарий: 10_milk_scenario.02.002 Установка цены по поставщикам
	Дано Я подключаюсь под "Администратор"
	И В командном интерфейсе я выбираю 'Производство' 'История изменения цен'
	Тогда открылось окно 'История изменения цен'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Установка цен (создание)'
	И в таблице "ВидыЦен" я нажимаю на кнопку 'Добавить'
	И в таблице "ВидыЦен" я нажимаю кнопку выбора у реквизита "Вид цен"
	Тогда открылось окно 'Виды цен'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'  |
		| 'Поставщик 1'   |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Установка цен (создание) *'
	И в поле 'от' я ввожу текст '04.09.2017'
	И я нажимаю на кнопку 'Перейти к установке цен'
	И в таблице "ДеревоЦен" я нажимаю на кнопку 'Добавить'
	Тогда открылось окно 'Выберите номенклатуру'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Молоко'       |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Установка цен (создание) *'
	И в таблице "ДеревоЦен" я перехожу к строке:
		| 'Номенклатура/характеристика' |
		| 'Молоко'                      |
	И в таблице "ДеревоЦен" я активизирую поле "Новая цена"
	И в таблице "ДеревоЦен" я выбираю текущую строку
	И в таблице "ДеревоЦен" в поле 'Новая цена' я ввожу текст '26,00'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Установка цен (создание) *' в течение 20 секунд
	Тогда открылось окно 'История изменения цен'
	И Я закрываю окно 'История изменения цен'

Сценарий: 10_milk_scenario.02.002 Установка цены-себестоимости
	И В командном интерфейсе я выбираю 'Производство' 'История изменения цен'
	Тогда открылось окно 'История изменения цен'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Установка цен (создание)'
	И в таблице "ВидыЦен" я нажимаю на кнопку 'Добавить'
	И в таблице "ВидыЦен" я нажимаю кнопку выбора у реквизита "Вид цен"
	Тогда открылось окно 'Виды цен'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'  |
		| 'Себестоимость' |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Установка цен (создание) *'
	И в поле 'от' я ввожу текст '04.09.2017'
	И я нажимаю на кнопку 'Перейти к установке цен'
	И в таблице "ДеревоЦен" я нажимаю на кнопку 'Добавить'
	Тогда открылось окно 'Выберите номенклатуру'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Молоко'       |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Установка цен (создание) *'
	И в таблице "ДеревоЦен" я перехожу к строке:
		| 'Номенклатура/Характеристика' | 'Упаковка' |
		| 'Молоко'                      | 'кг'       |
	И в таблице "ДеревоЦен" в поле 'Новая цена' я ввожу текст '20,00'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Установка цен (создание) *' в течение 20 секунд
	Тогда открылось окно 'История изменения цен'
	И Я закрываю окно 'История изменения цен'

Сценарий: 10_milk_scenario.02.002 Установка цены для Поставщика2
	И В командном интерфейсе я выбираю 'Производство' 'История изменения цен'
	Тогда открылось окно 'История изменения цен'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Установка цен (создание)'
	И в таблице "ВидыЦен" я нажимаю на кнопку с именем 'ВидыЦенДобавить'
	И в таблице "ВидыЦен" я нажимаю кнопку выбора у реквизита "Вид цен"
	Тогда открылось окно 'Виды цен'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Поставщик 2'  |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Установка цен (создание) *'
	И в поле 'от' я ввожу текст '04.09.2017'
	И я нажимаю на кнопку 'Перейти к установке цен'
	И в таблице "ДеревоЦен" я нажимаю на кнопку с именем 'ДеревоЦенДобавитьНоменклатуру'
	Тогда открылось окно 'Выберите номенклатуру'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Молоко'       |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Установка цен (создание) *'
	И в таблице "ДеревоЦен" я перехожу к строке:
		| 'Номенклатура/характеристика' |
		| 'Молоко'                      |
	И в таблице "ДеревоЦен" в поле 'Новая цена' я ввожу текст '25,00'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Установка цен (создание) *' в течение 20 секунд
	Тогда открылось окно 'История изменения цен'
	И Я закрываю окно 'История изменения цен'