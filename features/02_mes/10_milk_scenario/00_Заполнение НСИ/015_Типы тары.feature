﻿#language: ru

Функционал: 10_milk_scenario. Заполнение справочника Типы тары

Сценарий: 10_milk_scenario. Создание типа Коробка
	Дано Я подключаюсь под "Администратор"
	И В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Типы тары'
	Тогда открылось окно 'Типы тары'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Тип тары (создание)'
	И в поле 'Наименование' я ввожу текст 'Коробка'
	И я устанавливаю флаг 'Используется для упаковочных листов'
	И в поле 'Цифра расширения SSCC' я ввожу текст '5'
	И в поле 'Вес' я ввожу текст '0,500'
	И я устанавливаю флаг 'Фиксированный вес'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Тип тары (создание)' в течение 20 секунд
	Тогда открылось окно 'Типы тары'
	И Я закрываю окно 'Типы тары'
	
Сценарий: 10_milk_scenario. Создание типа Рама
	И В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Типы тары'
	Тогда открылось окно 'Типы тары'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Тип тары (создание)'
	И в поле 'Наименование' я ввожу текст 'Рама'
	Когда я запоминаю значение выражения 'ОбщегоНазначенияКлиентСервер.РазложитьПолноеИмяФайла(Ванесса.ПолучитьСостояниеVanessaAutomation().ТекущаяФича.ПолныйПуть).Путь + "\"' в переменную "КаталогТеста"
	Когда я буду выбирать внешний файл "$КаталогТеста$""Паллета.jpg"
	Когда я нажимаю на гиперссылку с именем "ПолеКартинкиПрисоединенногоФайла1"
	Тогда открылось окно '1С:Предприятие'
	Когда я нажимаю на кнопку 'OK'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду открытия окна 'Типы тары' в течение 20 секунд
	И Я закрываю окно 'Типы тары'

Сценарий: 10_milk_scenario. Создание типа Китаянка
	И В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Типы тары'
	Тогда открылось окно 'Типы тары'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Тип тары (создание)'
	И в поле 'Наименование' я ввожу текст 'Китаянка'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Тип тары (создание)' в течение 20 секунд
	Тогда открылось окно 'Типы тары'
	И Я закрываю окно 'Типы тары'

Сценарий: 10_milk_scenario. Создание типа Полка
	И В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Типы тары'
	Тогда открылось окно 'Типы тары'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Тип тары (создание)'
	И в поле 'Наименование' я ввожу текст 'Полка'
	И в поле 'Вес' я ввожу текст '0,600'
	И я устанавливаю флаг 'Фиксированный вес'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Тип тары (создание)' в течение 20 секунд
	Тогда открылось окно 'Типы тары'
	И Я закрываю окно 'Типы тары'

Сценарий: 10_milk_scenario. Создание типа Палка
	И В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Типы тары'
	Тогда открылось окно 'Типы тары'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Тип тары (создание)'
	И в поле 'Наименование' я ввожу текст 'Палка'
	И я устанавливаю флаг 'Фиксированный вес'
	И в поле с именем 'Вес' я ввожу текст '0,200'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Тип тары (создание) *' в течение 20 секунд
	Тогда открылось окно 'Типы тары'
	И Я закрываю окно 'Типы тары'

Сценарий: 10_milk_scenario. Заполнение веса контейнера
	И В командном интерфейсе я выбираю 'Производство' 'Контейнеры'
	Тогда открылось окно 'Контейнеры'
	И я нажимаю на кнопку 'Создать'
	Затем я жду, что в сообщениях пользователю будет подстрока "не заполнено" в течение 10 секунд
	И Я закрываю окно 'Контейнеры'
	И В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Типы тары'
	Тогда открылось окно 'Типы тары'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Контейнер'    |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Контейнер (Тип тары)'
	И в поле с именем 'Вес' я ввожу текст '0,100'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Контейнер (Тип тары)' в течение 20 секунд
	Тогда открылось окно 'Типы тары'
	И Я закрываю окно 'Типы тары'