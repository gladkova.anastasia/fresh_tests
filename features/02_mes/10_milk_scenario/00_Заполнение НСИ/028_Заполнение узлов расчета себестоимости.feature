﻿#language: ru

Функционал: 10_milk_scenario. Заполнение плана обмена Расчет сырьевой себестоимости

Сценарий: 10_milk_scenario. Заполнение плана обмена Расчет сырьевой себестоимости
	Дано Я подключаюсь под "Администратор"
	И В командном интерфейсе я выбираю 'Производство' 'Расчет сырьевой себестоимости'
	Тогда открылось окно 'Расчет сырьевой себестоимости'
	И я нажимаю на кнопку 'Открыть план обмена "Расчет сырьевой себестоимости"'
	Тогда открылось окно 'Узлы регистрации к расчету сырьевой себестоимости'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Узел регистрации к расчету сырьевой себестоимости (создание)'
	И в поле 'Код' я ввожу текст '2'
	И в поле 'Наименование' я ввожу текст 'Переработка'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Узел регистрации к расчету сырьевой себестоимости (создание)' в течение 20 секунд
	Тогда открылось окно 'Узлы регистрации к расчету сырьевой себестоимости'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Узел регистрации к расчету сырьевой себестоимости (создание)'
	И в поле 'Код' я ввожу текст '3'
	И в поле 'Наименование' я ввожу текст 'Усушка'
	И из выпадающего списка "Статья затрат" я выбираю по строке 'Усушка'
	И я перехожу к следующему реквизиту
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Узел регистрации к расчету сырьевой себестоимости (создание)' в течение 20 секунд
	Тогда открылось окно 'Узлы регистрации к расчету сырьевой себестоимости'
	И Я закрываю окно 'Узлы регистрации к расчету сырьевой себестоимости'