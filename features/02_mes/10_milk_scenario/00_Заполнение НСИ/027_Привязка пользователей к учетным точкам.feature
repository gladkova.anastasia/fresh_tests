﻿#language: ru

Функционал: 10_milk_scenario. Привязка пользователей к учетным точкам

Сценарий: 10_milk_scenario. Привязка пользователей к учетным точкам
	Дано Я подключаюсь под "Администратор"
	И В командном интерфейсе я выбираю 'Администрирование' 'Учетные точки'
	Тогда открылось окно 'Учетные точки'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Сырцех'       |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Сырцех (Учетные точки)'
	И я перехожу к закладке "Пользователи"
	И в таблице "Пользователи" я нажимаю на кнопку с именем 'ПользователиДобавить'
	И в таблице "Пользователи" я нажимаю кнопку выбора у реквизита "Пользователь"
	Тогда открылось окно 'Выбор типа данных'
	И я нажимаю на кнопку 'ОК'
	Тогда открылось окно 'Выбор пользователя'
	И в таблице "ПользователиСписок" я перехожу к строке:
		| 'Полное имя' |
		| 'Киоск'      |
	И я нажимаю на кнопку с именем 'ВыбратьПользователя'
	Тогда открылось окно 'Сырцех (Учетные точки) *'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Сырцех (Учетные точки) *' в течение 20 секунд
	Тогда открылось окно 'Учетные точки'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'  |
		| 'Пост сыровара' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Пост сыровара (Учетные точки)'
	И я перехожу к закладке "Пользователи"
	И в таблице "Пользователи" я нажимаю на кнопку 'Добавить'
	И в таблице "Пользователи" я нажимаю кнопку выбора у реквизита "Пользователь"
	Тогда открылось окно 'Выбор типа данных'
	И я нажимаю на кнопку 'ОК'
	Тогда открылось окно 'Выбор пользователя'
	И в таблице "ПользователиСписок" я перехожу к строке:
		| 'Полное имя' |
		| 'Киоск'      |
	И я нажимаю на кнопку с именем 'ВыбратьПользователя'
	Тогда открылось окно 'Пост сыровара (Учетные точки) *'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Пост сыровара (Учетные точки) *' в течение 20 секунд
	Тогда открылось окно 'Учетные точки'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Посолка'      |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Посолка (Учетные точки)'
	И я перехожу к закладке "Пользователи"
	И в таблице "Пользователи" я нажимаю на кнопку 'Добавить'
	И в таблице "Пользователи" я нажимаю кнопку выбора у реквизита "Пользователь"
	Тогда открылось окно 'Выбор типа данных'
	И я нажимаю на кнопку 'ОК'
	Тогда открылось окно 'Выбор пользователя'
	И в таблице "ПользователиСписок" я перехожу к строке:
		| 'Полное имя' |
		| 'Киоск'      |
	И я нажимаю на кнопку с именем 'ВыбратьПользователя'
	Тогда открылось окно 'Посолка (Учетные точки) *'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Посолка (Учетные точки) *' в течение 20 секунд
	Тогда открылось окно 'Учетные точки'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Маркировка'   |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Маркировка (Учетные точки)'
	И я перехожу к закладке "Пользователи"
	И в таблице "Пользователи" я нажимаю на кнопку 'Добавить'
	И в таблице "Пользователи" я нажимаю кнопку выбора у реквизита "Пользователь"
	Тогда открылось окно 'Выбор типа данных'
	И я нажимаю на кнопку 'ОК'
	Тогда открылось окно 'Выбор пользователя'
	И в таблице "ПользователиСписок" я перехожу к строке:
		| 'Полное имя'              |
		| 'Бригада на комплектации' |
	И я нажимаю на кнопку с именем 'ВыбратьПользователя'
	Тогда открылось окно 'Маркировка (Учетные точки) *'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Маркировка (Учетные точки) *' в течение 20 секунд
	Тогда открылось окно 'Учетные точки'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'      |
		| 'Наборка по рецептуре' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Наборка по рецептуре (Учетные точки)'
	И я перехожу к закладке "Пользователи"
	И в таблице "Пользователи" я нажимаю на кнопку 'Добавить'
	И в таблице "Пользователи" я нажимаю кнопку выбора у реквизита "Пользователь"
	Тогда открылось окно 'Выбор типа данных'
	И я нажимаю на кнопку 'ОК'
	Тогда открылось окно 'Выбор пользователя'
	И в таблице "ПользователиСписок" я перехожу к строке:
		| 'Полное имя' |
		| 'Мастер сырцеха'      |
	И я нажимаю на кнопку с именем 'ВыбратьПользователя'
	Тогда открылось окно 'Наборка по рецептуре (Учетные точки) *'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Наборка по рецептуре (Учетные точки) *' в течение 20 секунд
	Тогда открылось окно 'Учетные точки'
	И Я закрываю окно 'Учетные точки'