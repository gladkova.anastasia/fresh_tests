﻿#language: ru

Функционал: 10_milk_scenario. Настройка пользователей и прав

Сценарий: 10_milk_scenario. Создание профиля лаборанта-микробиолога
	Дано Я подключаюсь под "Администратор"
	И В командном интерфейсе я выбираю 'Администрирование' 'Настройки пользователей и прав'
	Тогда открылось окно 'Настройки пользователей и прав'
	И я нажимаю на кнопку 'Профили групп доступа'
	Тогда открылось окно 'Профили групп доступа'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Профиль групп доступа (создание)'
	И в поле 'Наименование' я ввожу текст 'Лаборант-микробиолог'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'                                       |
		| 'Нет'     | 'Добавление изменение документ регистрация показателей поставщиков' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'                  |
		| 'Нет'     | 'Чтение справочник виды анализов номенклатуры' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'         |
		| 'Нет'     | 'Чтение справочник виды номенклатуры' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'   |
		| 'Нет'     | 'Чтение справочник контрагенты' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'    |
		| 'Нет'     | 'Чтение справочник номенклатура' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'                         |
		| 'Нет'     | 'Чтение справочник номенклатура присоединенные файлы' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'   |
		| 'Нет'     | 'Чтение справочник организации' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'                        |
		| 'Нет'     | 'Чтение справочник показатели анализов номенклатуры' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'          |
		| 'Нет'     | 'Чтение справочник показатели складов' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'     |
		| 'Нет'     | 'Чтение справочник пункты сборки' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'          |
		| 'Нет'     | 'Чтение справочник серии номенклатуры' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)' |
		| 'Нет'     | 'Раздел "Производство"'       |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Профиль групп доступа (создание) *' в течение 20 секунд
	Тогда открылось окно 'Профили групп доступа'
	И я закрываю окно 'Профили групп доступа'
	Тогда открылось окно 'Настройки пользователей и прав'
	И Я закрываю окно 'Настройки пользователей и прав'

Сценарий: 10_milk_scenario. Создание групп пользователей
	И Я создаю группу доступа "Киоски(комплектация)" с профилем "Киоски(комплектация через весы)"
	И Я создаю группу доступа "Базовые права" с профилем "Базовый профиль"
	И Я создаю группу доступа "Киоски (меню учетных точек)" с профилем "Киоски(меню учетных точек)"
	И Я создаю группу доступа "Лаборанты" с профилем "Лаборанты"
	И Я создаю группу доступа "Лаборанты приемки молока" с профилем "Лаборанты приемки молока"
	И Я создаю группу доступа "Мастера сырцеха" с профилем "Мастера сырцеха"
	И Я создаю группу доступа "Приемщик молока" с профилем "Приемка молока по количеству"
	И Я создаю группу доступа "Созревание сыра" с профилем "Созревание сыра"
	И Я создаю группу доступа "Сырцех" с профилем "Сыродельный цех"
	И Я создаю группу доступа "Технологи" с профилем "Технологи"
	И Я создаю группу доступа "Лаборант-микробиолог" с профилем "Лаборант-микробиолог"

Сценарий: 10_milk_scenario. Создание пользователей
	И Я создаю пользователя "Лаборант приемки молока" с профилями "Базовые права;Лаборанты приемки молока"
	И Я создаю пользователя "Бригада на комплектации" с профилями "Базовые права;Киоски(комплектация)"
	И Я создаю пользователя "Киоск" с профилями "Базовые права;Киоски (меню учетных точек)"
	И Я создаю пользователя "Лаборант" с профилями "Базовые права;Лаборанты"
	И Я создаю пользователя "Мастер сырцеха" с профилями "Базовые права;Мастера сырцеха"
	И Я создаю пользователя "Приемщик молока" с профилями "Базовые права;Приемщик молока"
	И Я создаю пользователя "Цех созревания сыра" с профилями "Базовые права;Созревание сыра"
	И Я создаю пользователя "Технолог" с профилями "Базовые права;Технологи"
	И Я создаю пользователя "Сырцех" с профилями "Базовые права;Сырцех"
	И Я создаю пользователя "Лаборант-микробиолог" с профилями "Базовые права;Лаборант-микробиолог"