﻿#language: ru

Функционал: 10_milk_scenario. Заполнение справочника Виды показателей складов

Сценарий: 10_milk_scenario. Создание вида показателей Солилка
	Дано Я подключаюсь под "Администратор"
	И В командном интерфейсе я выбираю 'Производство' 'Виды показателей складов'
	Тогда открылось окно 'Виды показателей складов'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Вид показателя складов (создание)'
	И в поле 'Наименование' я ввожу текст 'Солилка'
	И в таблице "Показатели" я нажимаю на кнопку с именем 'ПоказателиДобавить'
	И в таблице "Показатели" я нажимаю кнопку выбора у реквизита "Показатель"
	Тогда открылось окно 'Показатели складов'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Температура'  |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Вид показателя складов (создание) *'
	И в таблице "Показатели" я нажимаю на кнопку с именем 'ПоказателиДобавить'
	И в таблице "Показатели" я нажимаю кнопку выбора у реквизита "Показатель"
	Тогда открылось окно 'Показатели складов'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Номер пресса' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Вид показателя складов (создание) *'
	И в таблице "Показатели" я нажимаю на кнопку с именем 'ПоказателиДобавить'
	И в таблице "Показатели" я нажимаю кнопку выбора у реквизита "Показатель"
	Тогда открылось окно 'Показатели складов'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Давление'     |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Вид показателя складов (создание) *'
	И в таблице "Показатели" я нажимаю на кнопку с именем 'ПоказателиДобавить'
	И в таблице "Показатели" я нажимаю кнопку выбора у реквизита "Показатель"
	Тогда открылось окно 'Показатели складов'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Аварии'       |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Вид показателя складов (создание) *'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Вид показателя складов (создание) *' в течение 20 секунд
	Тогда открылось окно 'Виды показателей складов'
	И Я закрываю окно 'Виды показателей складов'