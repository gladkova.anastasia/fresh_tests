﻿#language: ru

Функционал: 10_milk_scenario. Заполнение справочника Рабочие центры + Виды РЦ

Сценарий: 10_milk_scenario. Заполнение справочника Виды РЦ
	Дано Я подключаюсь под "Администратор"
	И Я создаю вид рабочего центра "Сырцех"
	И Я создаю вид рабочего центра "ПАЦ"
	И Я создаю вид рабочего центра "Упаковка"
	И Я создаю вид рабочего центра "Маркировка"

Сценарий: 10_milk_scenario. Создание РЦ Сырцех
	И В командном интерфейсе я выбираю 'Производство' 'Рабочие центры'
	Тогда открылось окно 'Рабочие центры'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Рабочий центр (создание)'
	И я нажимаю кнопку выбора у поля "Вид рабочего центра"
	Тогда открылось окно 'Виды рабочих центров'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Сырцех'       |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Рабочий центр (создание) *'
	И в поле 'Наименование' я ввожу текст 'Сырцех'
	И я нажимаю кнопку выбора у поля "График работы"
	Тогда открылось окно 'Графики работы'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' 		|
		| 'Российская Федерация'|
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Рабочий центр (создание) *'
	И я перехожу к закладке "Склады"
	И я нажимаю кнопку выбора у поля "Склад"
	Тогда открылось окно 'Склады'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Сырцех'       |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Рабочий центр (создание) *'
	И я нажимаю кнопку выбора у поля "СкладВспомогательныхМатериалов"
	Тогда открылось окно 'Склады'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Сырцех'       |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Рабочий центр (создание) *'
	И я перехожу к закладке "Производство"
	И я устанавливаю флаг 'Контролировать баланс'
	И в поле 'Доступность, ч' я ввожу текст '24,00'
	И в поле 'Доступность, ч' я ввожу текст '23,00'
	И в поле 'Время переналадки, ч' я ввожу текст '1,00'
	И я устанавливаю флаг 'Требуется обеспечение'
	И из выпадающего списка "Вариант нумерации серий" я выбираю точное значение 'По номеру строки задания'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Рабочий центр (создание) *' в течение 20 секунд
	Тогда открылось окно 'Рабочие центры'
	И я закрываю окно 'Рабочие центры'
	
Сценарий: 10_milk_scenario. Создание РЦ ПАЦ
	И В командном интерфейсе я выбираю 'Производство' 'Рабочие центры'
	Тогда открылось окно 'Рабочие центры'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Рабочий центр (создание)'
	И я нажимаю кнопку выбора у поля "Вид рабочего центра"
	Тогда открылось окно 'Виды рабочих центров'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'ПАЦ'          |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Рабочий центр (создание) *'
	И в поле 'Наименование' я ввожу текст 'ПАЦ'
	И я перехожу к закладке "Склады"
	И я устанавливаю флаг 'Несколько складов'
	И в таблице "Склады" я нажимаю на кнопку 'Добавить'
	И в таблице "Склады" я нажимаю кнопку выбора у реквизита "Склад"
	Тогда открылось окно 'Склады'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Танк №2'      |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Рабочий центр (создание) *'
	И в таблице "Склады" я нажимаю на кнопку 'Добавить'
	И в таблице "Склады" я нажимаю кнопку выбора у реквизита "Склад"
	Тогда открылось окно 'Склады'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Танк №1'      |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Рабочий центр (создание) *'
	И в таблице "Склады" я перехожу к строке:
		| 'N' |
		| '1' |
	И в таблице "Склады" я удаляю строку
	И в таблице "Склады" я нажимаю на кнопку 'Добавить'
	И в таблице "Склады" я нажимаю кнопку выбора у реквизита "Склад"
	Тогда открылось окно 'Склады'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'ПАЦ'        |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Рабочий центр (создание) *'
	И я нажимаю кнопку выбора у поля "СкладВспомогательныхМатериалов"
	Тогда открылось окно 'Склады'
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Рабочий центр (создание) *'
	И я перехожу к закладке "Производство"
	И я устанавливаю флаг 'Контролировать баланс'
	И в поле 'Доступность, ч' я ввожу текст '21,00'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Рабочий центр (создание) *' в течение 20 секунд
	Тогда открылось окно 'Рабочие центры'
	И я закрываю окно 'Рабочие центры'

Сценарий: 10_milk_scenario. Создание РЦ Упаковка
	И В командном интерфейсе я выбираю 'Производство' 'Рабочие центры'
	Тогда открылось окно 'Рабочие центры'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Рабочий центр (создание)'
	И я нажимаю кнопку выбора у поля "Вид рабочего центра"
	Тогда открылось окно 'Виды рабочих центров'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Упаковка'     |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Рабочий центр (создание) *'
	И в поле 'Наименование' я ввожу текст 'Упаковка'
	И я перехожу к закладке "Склады"
	И я устанавливаю флаг 'Несколько складов'
	И в таблице "Склады" я нажимаю на кнопку 'Добавить'
	И в таблице "Склады" я нажимаю кнопку выбора у реквизита "Склад"
	Тогда открылось окно 'Склады'
	И в таблице "Список" я перехожу к строке:
		|'Наименование'|
		|'Бассейн соления'|
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Рабочий центр (создание) *'
	И в таблице "Склады" я перехожу к строке:
		| 'N' |
		| '1' |
	И в таблице "Склады" я удаляю строку
	И я нажимаю кнопку выбора у поля "СкладВспомогательныхМатериалов"
	Тогда открылось окно 'Склады'
	И в таблице "Список" я перехожу к строке:
		|'Наименование'|
		|'Упаковка'|
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Рабочий центр (создание) *'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Рабочий центр (создание) *' в течение 20 секунд
	Тогда открылось окно 'Рабочие центры'
	И я закрываю окно 'Рабочие центры'

Сценарий: 10_milk_scenario. Создание РЦ Маркировка
	И В командном интерфейсе я выбираю 'Производство' 'Рабочие центры'
	Тогда открылось окно 'Рабочие центры'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Рабочий центр (создание)'
	И я нажимаю кнопку выбора у поля "Вид рабочего центра"
	Тогда открылось окно 'Виды рабочих центров'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Маркировка'   |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Рабочий центр (создание) *'
	И в поле 'Наименование' я ввожу текст 'Маркировка'
	И я перехожу к закладке "Склады"
	И я устанавливаю флаг 'Несколько складов'
	И в таблице "Склады" я нажимаю на кнопку 'Добавить'
	И в таблице "Склады" я нажимаю кнопку выбора у реквизита "Склад"
	Тогда открылось окно 'Склады'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'        |
		| 'Камера созревания 1' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Рабочий центр (создание) *'
	И в таблице "Склады" я перехожу к строке:
		| 'N' |
		| '1' |
	И в таблице "Склады" я удаляю строку
	Тогда открылось окно 'Рабочий центр (создание) *'
	И я нажимаю кнопку выбора у поля "СкладВспомогательныхМатериалов"
	Тогда открылось окно 'Склады'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Упаковка'     |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Рабочий центр (создание) *'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Рабочий центр (создание) *' в течение 20 секунд
	Тогда открылось окно 'Рабочие центры'
	И я закрываю окно 'Рабочие центры'