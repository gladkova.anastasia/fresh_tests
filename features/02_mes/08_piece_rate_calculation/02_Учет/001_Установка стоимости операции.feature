﻿#language: ru

@piece_rate_calculation

Функционал: 08_piece_rate_calculation.02.001 Создание документа Установка стоимости операций

Сценарий: 08_piece_rate_calculation.02.001 Создание документа Установка стоимости операций
	Дано Я подключаюсь под "Бухгалтер"
	И В командном интерфейсе я выбираю 'Производство' 'Установки стоимостей операций'
	Тогда открылось окно 'Установки стоимостей операций'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Установка стоимости операций (создание)'
	И в поле 'от' я ввожу текст '01.01.2020 11:15:21'
	И я перехожу к закладке "Операции"
	И в таблице "Операции" я добавляю строку
	И в таблице "Операции" я активизирую поле "Операция"
	И в таблице "Операции" я нажимаю кнопку выбора у реквизита "Операция"
	Тогда открылось окно 'Технологические операции выработки'
	И в таблице "Список" я перехожу к строке:
		| 'Код'         | 'Наименование'     |
		| '00-00000001' | 'Прессование сыра' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Установка стоимости операций (создание) *'
	И в таблице "Операции" я активизирую поле "Стоимость"
	И в таблице "Операции" в поле 'Стоимость' я ввожу текст '2,000'
	И в таблице "Операции" я завершаю редактирование строки
	И в таблице "Операции" я нажимаю на кнопку с именем 'ОперацииДобавить'
	И в таблице "Операции" я нажимаю кнопку выбора у реквизита "Операция"
	Тогда открылось окно 'Технологические операции выработки'
	И в таблице "Список" я перехожу к строке:
		| 'Код'         | 'Наименование'      |
		| '00-00000006' | 'Поступление сырья' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Установка стоимости операций (создание) *'
	И в таблице "Операции" я активизирую поле "Стоимость"
	И в таблице "Операции" в поле 'Стоимость' я ввожу текст '1,200'
	И в таблице "Операции" я завершаю редактирование строки
	И в таблице "Операции" я нажимаю на кнопку с именем 'ОперацииДобавить'
	И в таблице "Операции" я нажимаю кнопку выбора у реквизита "Операция"
	Тогда открылось окно 'Технологические операции выработки'
	И в таблице "Список" я перехожу к строке:
		| 'Код'         | 'Наименование'           |
		| '00-00000005' | 'Поступление материалов' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Установка стоимости операций (создание) *'
	И в таблице "Операции" я активизирую поле "Стоимость"
	И в таблице "Операции" в поле 'Стоимость' я ввожу текст '1,500'
	И в таблице "Операции" я завершаю редактирование строки
	И в таблице "Операции" я нажимаю на кнопку с именем 'ОперацииДобавить'
	И в таблице "Операции" я нажимаю кнопку выбора у реквизита "Операция"
	Тогда открылось окно 'Технологические операции выработки'
	И в таблице "Список" я перехожу к строке:
		| 'Код'         | 'Наименование'     |
		| '00-00000003' | 'Перемещение сыра' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Установка стоимости операций (создание) *'
	И в таблице "Операции" я активизирую поле "Стоимость"
	И в таблице "Операции" в поле 'Стоимость' я ввожу текст '1,050'
	И в таблице "Операции" я завершаю редактирование строки
	И в таблице "Операции" я нажимаю на кнопку с именем 'ОперацииДобавить'
	И в таблице "Операции" я нажимаю кнопку выбора у реквизита "Операция"
	Тогда открылось окно 'Технологические операции выработки'
	И в таблице "Список" я перехожу к строке:
		| 'Код'         | 'Наименование' |
		| '00-00000004' | 'Зачистка'     |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Установка стоимости операций (создание) *'
	И в таблице "Операции" я активизирую поле "Стоимость"
	И в таблице "Операции" в поле 'Стоимость' я ввожу текст '3,000'
	И в таблице "Операции" я завершаю редактирование строки
	И в таблице "Операции" я нажимаю на кнопку с именем 'ОперацииДобавить'
	И в таблице "Операции" я нажимаю кнопку выбора у реквизита "Операция"
	Тогда открылось окно 'Технологические операции выработки'
	И в таблице "Список" я перехожу к строке:
		| 'Код'         | 'Наименование'    |
		| '00-00000002' | 'Выработка смеси' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Установка стоимости операций (создание) *'
	И в таблице "Операции" я активизирую поле "Стоимость"
	И в таблице "Операции" в поле 'Стоимость' я ввожу текст '0,870'
	И в таблице "Операции" я завершаю редактирование строки
	И в таблице "Операции" я нажимаю на кнопку с именем 'ОперацииДобавить'
	И в таблице "Операции" я нажимаю кнопку выбора у реквизита "Операция"
	Тогда открылось окно 'Технологические операции выработки'
	И в таблице "Список" я перехожу к строке:
		| 'Код'         | 'Наименование' |
		| '00-00000007' | 'Выпуск сыра'  |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Установка стоимости операций (создание) *'
	И в таблице "Операции" я активизирую поле "Стоимость"
	И в таблице "Операции" в поле 'Стоимость' я ввожу текст '1,300'
	И в таблице "Операции" я завершаю редактирование строки
	И в таблице "Операции" я перехожу к строке:
		| 'N' |
		| '1' |
	И я выбираю пункт контекстного меню с именем 'ОперацииКонтекстноеМенюУдалить' на элементе формы с именем "Операции"
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Установка стоимости операций (создание) *' в течение 20 секунд
	Тогда открылось окно 'Установки стоимостей операций'
	И Я закрываю окно 'Установки стоимостей операций'

Сценарий: 08_piece_rate_calculation.02.001 Закрытие клиента
	И я закрываю сеанс TESTCLIENT