﻿#language: ru

@piece_rate_calculation

Функционал: 08_piece_rate_calculation.02.008 Открытие отчетов

Сценарий: 08_piece_rate_calculation.02.008 Отчет Начисленная зарплата по операциям
	Дано Я переключаюсь на открытый клиент "Бухгалтер"
	И В командном интерфейсе я выбираю 'Производство' 'Отчеты'
	Тогда открылось окно 'Отчеты по производству'
	И я нажимаю на гиперссылку "Начисленная зарплата по операциям"
	Тогда открылось окно "Начисленная зарплата по операциям"
	И я нажимаю кнопку выбора у поля "Смена"
	Тогда открылось окно 'Смены'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'  |
		| 'Смена24' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'	
	Тогда открылось окно "Начисленная зарплата по операциям"
	И я нажимаю кнопку выбора у поля "Рабочий центр"
	Тогда открылось окно 'Рабочие центры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'  |
		| 'ПАЦ' 	  |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'		
	Тогда открылось окно "Начисленная зарплата по операциям"
	И из выпадающего списка "Дата смены" я выбираю точное значение 'Начало этого дня'
	И я нажимаю на кнопку 'Сформировать'
	И Ожидаем завершения фонового формирования отчета в течение "20" секунд
	Тогда табличный документ "ОтчетТабличныйДокумент" равен по шаблону:
		| 'Отбор:'            | 'Смена Равно "Смена24" И\nДата смены Равно * И\nРабочий центр Равно "ПАЦ"' 	      | ''          | ''                   | ''                  | ''              | ''       |
		| ''                  | ''                                                                                    | ''          | ''                   | ''                  | ''              | ''       |
		| 'Дата смены'        | 'Смена'                                                                               | 'Выработка' | 'Стоимость операции' | 'Сумма за операции' | 'Премиальный %' | 'Сумма'  |
		| 'Сотрудник'         | ''                                                                                    | ''          | ''                   | ''                  | ''              | ''       |
		| 'Рабочий центр'     | ''                                                                                    | ''          | ''                   | ''                  | ''              | ''       |
		| 'Операция'          | ''                                                                                    | ''          | ''                   | ''                  | ''              | ''       |
		| '*'        	      | 'Смена24'                                                                             | ''          | ''                   | '540,00'            | ''              | '675,00' |
		| 'Укладчик Петров'   | ''                                                                                    | ''          | ''                   | '270,00'            | '100%'          | '270,00' |
		| 'ПАЦ'               | ''                                                                                    | ''          | ''                   | '270,00'            | '100%'          | '270,00' |
		| 'Поступление сырья' | ''                                                                                    | '225,000'   | '1,200'              | '270,00'            | '100%'          | '270,00' |
		| 'Упаковщик Митяев'  | ''                                                                                    | ''          | ''                   | '270,00'            | '150%'          | '405,00' |
		| 'ПАЦ'               | ''                                                                                    | ''          | ''                   | '270,00'            | '150%'          | '405,00' |
		| 'Поступление сырья' | ''                                                                                    | '225,000'   | '1,200'              | '270,00'            | '150%'          | '405,00' |
		| 'Итого'             | ''                                                                                    | ''          | ''                   | '540,00'            | ''              | '675,00' |
	И я закрываю окно "Начисленная зарплата по операциям"	
	Тогда открылось окно 'Отчеты по производству'
	И я закрываю окно 'Отчеты по производству'

Сценарий: 08_piece_rate_calculation.02.008 Отчет Начисленная зарплата по операциям
	И В командном интерфейсе я выбираю 'Производство' 'Отчеты'
	Тогда открылось окно 'Отчеты по производству'
	И я нажимаю на гиперссылку "Распределение сдельной оплаты по времени работы"
	Тогда открылось окно 'Распределение сдельной оплаты по времени работы'
	И из выпадающего списка "Дата смены" я выбираю точное значение 'Начало этого дня'
	И я нажимаю кнопку выбора у поля "Смена"
	Тогда открылось окно 'Смены'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Смена24'      |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Распределение сдельной оплаты по времени работы'
	И я нажимаю кнопку выбора у поля "Рабочий центр"
	Тогда открылось окно 'Рабочие центры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Сырцех'       |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Распределение сдельной оплаты по времени работы'
	И я нажимаю на кнопку с именем 'СформироватьОтчет'
	И Ожидаем завершения фонового формирования отчета в течение "20" секунд
	Тогда табличный документ "ОтчетТабличныйДокумент" равен по шаблону:
		| 'Параметры:'          | 'Дата смены: *' 	   |
		| ''                    | 'Смена: Смена24'         |
		| ''                    | 'Рабочий центр: Сырцех'  |
		| ''                    | ''                       |
		| 'Сотрудник'           | 'Количество'             |
		| 'Операция'            | ''                       |
		| 'Дата'                | ''                       |
		| 'Карщик Невельская'   | '943,500'                |
		| 'Выработка смеси'     | '471,750'                |
		| '*'				    | '224,000'                |
		| '*'				    | '247,750'                |
		| 'Прессование сыра'    | '471,750'                |
		| '*'				    | '224,000'                |
		| '*'				    | '247,750'                |
		| 'Укладчик Петров'     | '943,500'                |
		| 'Выработка смеси'     | '471,750'                |
		| '*'				    | '224,000'                |
		| '*'				    | '247,750'                |
		| 'Прессование сыра'    | '471,750'                |
		| '*'				    | '224,000'                |
		| '*'				    | '247,750'                |
		| 'Укладчик Сидорова'   | '943,500'                |
		| 'Выработка смеси'     | '471,750'                |
		| '*'				    | '224,000'                |
		| '*'				    | '247,750'                |
		| 'Прессование сыра'    | '471,750'                |
		| '*'				    | '224,000'                |
		| '*'				    | '247,750'                |
		| 'Упаковщик Митяев'    | '943,500'                |
		| 'Выработка смеси'     | '471,750'                |
		| '*'				    | '224,000'                |
		| '*'				    | '247,750'                |
		| 'Прессование сыра'    | '471,750'                |
		| '*'				    | '224,000'                |
		| '*'				    | '247,750'                |
		| 'Итого'               | '3 774,000'              |
	И Я закрываю окно 'Распределение сдельной оплаты по времени работы'
	Тогда открылось окно 'Отчеты по производству'
	И Я закрываю окно 'Отчеты по производству'

Сценарий: 08_piece_rate_calculation.02.008 Закрытие клиента тестирования
	И я закрываю сеанс TESTCLIENT