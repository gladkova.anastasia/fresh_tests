﻿#language: ru
@integration

Функционал: 13_integration. Заполнение правил загрузки данных обмена

Сценарий: 13_integration. Правило для номенклатуры
	Дано Я подключаюсь под "Администратор"
	И В командном интерфейсе я выбираю 'Обмен данными ИБ' 'Правила загрузки'
	Тогда открылось окно 'Правила загрузки'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Правило загрузки (создание)'
	И я нажимаю кнопку выбора у поля с именем "Владелец"
	Тогда открылось окно 'Отправители'
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Правило загрузки (создание) *'
	И я нажимаю кнопку выбора у поля с именем "ТипПакета"
	Тогда открылось окно 'Форма выбора тип пакета'
	И в таблице "ТаблицаТиповПакета" я перехожу к строке:
		| 'Тип пакета'              |
		| 'Справочник.Номенклатура' |
	И в таблице "ТаблицаТиповПакета" я нажимаю на кнопку с именем 'ТаблицаТиповПакетаВыбрать'
	Тогда открылось окно 'Правило загрузки (создание) *'
	И я нажимаю кнопку выбора у поля с именем "ВидНоменклатуры"
	Тогда открылось окно 'Виды номенклатуры'
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Правило загрузки (создание) *'
	И я нажимаю на кнопку с именем 'ФормаЗаписатьИЗакрыть'
	И я жду закрытия окна 'Правило загрузки (создание) *' в течение 20 секунд
	Тогда открылось окно 'Правила загрузки'
	И Я закрываю окно 'Правила загрузки'

Сценарий: 13_integration. Правило для складов
	И В командном интерфейсе я выбираю 'Обмен данными ИБ' 'Правила загрузки'
	Тогда открылось окно 'Правила загрузки'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Правило загрузки (создание)'
	И из выпадающего списка с именем "Владелец" я выбираю точное значение 'erp'
	И я нажимаю кнопку выбора у поля с именем "Владелец"
	Тогда открылось окно 'Отправители'
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Правило загрузки (создание) *'
	И я нажимаю кнопку выбора у поля с именем "ТипПакета"
	Тогда открылось окно 'Форма выбора тип пакета'
	И в таблице "ТаблицаТиповПакета" я перехожу к строке:
		| 'Тип пакета'        |
		| 'Справочник.Склады' |
	И в таблице "ТаблицаТиповПакета" я нажимаю на кнопку с именем 'ТаблицаТиповПакетаВыбрать'
	Тогда открылось окно 'Правило загрузки (создание) *'
	И я нажимаю на кнопку с именем 'ФормаЗаписатьИЗакрыть'
	И я жду закрытия окна 'Правило загрузки (создание) *' в течение 20 секунд
	Тогда открылось окно 'Правила загрузки'
	И Я закрываю окно 'Правила загрузки'

Сценарий: 13_integration. Правило для перемещения товаров
	И В командном интерфейсе я выбираю 'Обмен данными ИБ' 'Правила загрузки'
	Тогда открылось окно 'Правила загрузки'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Правило загрузки (создание)'
	И я нажимаю кнопку выбора у поля с именем "Владелец"
	Тогда открылось окно 'Отправители'
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Правило загрузки (создание) *'
	И я нажимаю кнопку выбора у поля с именем "ТипПакета"
	Тогда открылось окно 'Форма выбора тип пакета'
	И в таблице "ТаблицаТиповПакета" я перехожу к строке:
		| 'Тип пакета'                  |
		| 'Документ.ПеремещениеТоваров' |
	И в таблице "ТаблицаТиповПакета" я нажимаю на кнопку с именем 'ТаблицаТиповПакетаВыбрать'
	Тогда открылось окно 'Правило загрузки (создание) *'
	И я нажимаю на кнопку с именем 'ФормаЗаписатьИЗакрыть'
	И я жду закрытия окна 'Правило загрузки (создание) *' в течение 20 секунд
	Тогда открылось окно 'Правила загрузки'
	И Я закрываю окно 'Правила загрузки'