﻿#language: ru
@integration

Функционал: 13_integration. Создание вида номенклатуры

Сценарий: 13_integration. Создание вида Сырье
	Дано Я подключаюсь под "Администратор"
	И Я начинаю создавать Вид Номенклатуры "interg_Сырье"
	И я нажимаю кнопку выбора у поля "Ед. хранения"
	И Я выбираю кг в первый раз
	И я перехожу к закладке "Производство"
	И из выпадающего списка "Тип воспроизводства" я выбираю точное значение 'Производство'
	И Я заканчиваю создавать Вид Номенклатуры