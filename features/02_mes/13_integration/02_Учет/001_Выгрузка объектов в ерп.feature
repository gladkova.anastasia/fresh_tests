﻿#language: ru
@integration

Функционал: 13_integration. Проверка выгрузки данных в базе ерп

Сценарий: 13_integration. Перезапись номенклатуры
	И Я подключаю клиент тестирования с параметрами:
		| 'Имя'           | 'Синоним'  | 'Тип клиента' |  'Порт' | 'Строка соединения'                       | 'Логин'                   | 'Пароль' | 'Запускаемая обработка' |  'Дополнительные параметры строки запуска'  |
		| 'ЕРПАдмин'      | 'ЕРПАдмин' | 'Тонкий'      |  '1538' | 'Srvr="srv-dev";Ref="erp_smirnova_test";' | 'Администратор (ОрловАВ)' | ''       | ''                      |  '/UC'                                      |
	И Я закрываю окно 'Подключение Интернет-поддержки'
	И В командном интерфейсе я выбираю 'НСИ и администрирование' 'Номенклатура'
	Тогда открылось окно 'Номенклатура'
	И я снимаю флаг 'ИспользоватьФильтры'
	И в таблице "СписокРасширенныйПоискНоменклатура" я перехожу к строке:
		| 'Наименование'         |
		| 'РБТ.100.02 Кронштейн' |
	И в таблице "СписокРасширенныйПоискНоменклатура" я выбираю текущую строку
	Тогда открылось окно 'РБТ.100.02 Кронштейн (Номенклатура)'
	И я нажимаю на кнопку с именем 'ФормаЗаписатьИЗакрыть'
	И я жду закрытия окна 'РБТ.100.02 Кронштейн (Номенклатура)' в течение 20 секунд
	Тогда открылось окно 'Номенклатура'
	И Я закрываю окно 'Номенклатура'

Сценарий: 13_integration. Проверка объектов к отправке, проверка очереди по номенклатуре
	И В командном интерфейсе я выбираю 'Обмен данными ИБ' 'Объекты к выгрузке обмен данными ИБ'
	Тогда открылось окно 'Объекты к выгрузке обмен данными ИБ'
	И в таблице "Список" количество строк "равно" 4
	И в таблице "Список" я перехожу к строке по шаблону:
		| 'Дата' | 'Объект ИБ'            | 'Пакет данных УИД'  | 'Тип пакета'              | 'Хеш объекта ИБ'  |
		| '*'    | 'РБТ.100.02 Кронштейн' | '*'                 | 'Справочник.Номенклатура' | '*'               |
	И в таблице "Список" я перехожу к строке по шаблону:
		| 'Дата' | 'Объект ИБ'           | 'Пакет данных УИД'  | 'Тип пакета'              | 'Хеш объекта ИБ'  |
		| '*'    | 'Полуфабрикаты: Реле' | '*'                 | 'Справочник.Номенклатура' | '*'               |
	И в таблице "Список" я перехожу к строке по шаблону:
		| 'Дата'  | 'Объект ИБ'            | 'Пакет данных УИД' | 'Тип пакета'              | 'Хеш объекта ИБ'  |
		| '*'     | 'РБТ.100.02 Кронштейн' | '*'                | 'Справочник.Номенклатура' | '*'               |
	И в таблице "Список" я перехожу к строке по шаблону:
		| 'Дата'  | 'Объект ИБ'                     | 'Пакет данных УИД' | 'Тип пакета'              | 'Хеш объекта ИБ' |
		| '*'     | 'Производственная деятельность' | '*'                | 'Справочник.Номенклатура' | '*'              |
	И я нажимаю на кнопку с именем 'ФормаКомандаДобавитьВОчередьВсе'
	И в таблице "Список" количество строк "равно" 0
	И Я закрываю окно 'Объекты к выгрузке обмен данными ИБ'

	И В командном интерфейсе я выбираю 'Обмен данными ИБ' 'Очередь исходящих пакетов обмен данными ИБ'
	Тогда открылось окно 'Очередь исходящих пакетов обмен данными ИБ'
	И в таблице "Список" количество строк "равно" 4
	И в таблице "Список" я перехожу к строке по шаблону:
		| 'Объект ИБ'            | 'Получатель' | 'Правило'                       | 'Статус'         | 'Тип пакета'              |
		| 'РБТ.100.02 Кронштейн' | 'E4ф'        | 'Справочник.Номенклатура (E4ф)' | 'Сформирован, *' | 'Справочник.Номенклатура' |
	И в таблице "Список" я перехожу к строке по шаблону:
		| 'Объект ИБ'     | 'Получатель' | 'Правило'                       | 'Статус'         | 'Тип пакета'              |
		| 'Полуфабрикаты' | 'E4ф'        | 'Справочник.Номенклатура (E4ф)' | 'Сформирован, *' | 'Справочник.Номенклатура' |
	И в таблице "Список" я перехожу к строке по шаблону:
		| 'Дата'  | 'Объект ИБ'                     | 'Пакет данных УИД' | 'Получатель' | 'Правило'                       | 'Статус'         | 'Тип пакета'              | 'Хеш объекта ИБ'  |
		| '*'     | 'Производственная деятельность' | '*'                | 'E4ф'        | 'Справочник.Номенклатура (E4ф)' | 'Сформирован, *' | 'Справочник.Номенклатура' | '*'               |
	И в таблице "Список" я перехожу к строке по шаблону:
		| 'Дата'   | 'Объект ИБ'           | 'Пакет данных УИД'  | 'Получатель' | 'Правило'                       | 'Статус'         | 'Тип пакета'              | 'Хеш объекта ИБ'   |
		| '*'      | 'Полуфабрикаты: Реле' | '*'                 | 'E4ф'        | 'Справочник.Номенклатура (E4ф)' | 'Сформирован, *' | 'Справочник.Номенклатура' | '*'                |
	И пауза 1
	И я нажимаю на кнопку с именем 'ФормаКомандаОтправитьВсе'
	И в таблице "Список" количество строк "равно" 0
	И Я закрываю окно 'Очередь исходящих пакетов обмен данными ИБ'
	И В панели разделов я выбираю 'Склад и доставка'

Сценарий: 13_integration. Перезапись склада
	И В командном интерфейсе я выбираю 'НСИ и администрирование' 'Склады и магазины'
	Тогда открылось окно 'Склады и магазины'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Производство' |
	И в таблице  "Список" я перехожу на один уровень вниз
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Склады'       |
	И в таблице  "Список" я перехожу на один уровень вниз
	И в таблице "Список" я перехожу к строке:
		| 'Должность'                               | 'Наименование'              | 'Склад / Магазин' |
		| 'Старший кладовщик, Иванова Нина Юрьевна' | 'Склад коммерческой службы' | 'Оптовый склад'   |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Склад коммерческой службы (Склад)'
	И я нажимаю на кнопку с именем 'ФормаЗаписатьИЗакрыть'
	И я жду закрытия окна 'Склад коммерческой службы (Склад)' в течение 20 секунд
	Тогда открылось окно 'Склады и магазины'
	И Я закрываю окно 'Склады и магазины'

Сценарий: 13_integration. Проверка объектов и очереди по складу
	И В командном интерфейсе я выбираю 'Обмен данными ИБ' 'Объекты к выгрузке обмен данными ИБ'
	Тогда открылось окно 'Объекты к выгрузке обмен данными ИБ'
	И в таблице "Список" количество строк "равно" 3
	И в таблице "Список" я перехожу к строке по шаблону:
		| 'Дата' | 'Объект ИБ' | 'Пакет данных УИД'  | 'Тип пакета'        | 'Хеш объекта ИБ'   |
		| '*'    | 'Склады'    | '*'                 | 'Справочник.Склады' | '*'                |
	И в таблице "Список" я перехожу к строке по шаблону:
		| 'Дата' | 'Объект ИБ'                 | 'Пакет данных УИД' | 'Тип пакета'        | 'Хеш объекта ИБ'  |
		| '*'    | 'Склад коммерческой службы' | '*'                | 'Справочник.Склады' | '*'               |
	И в таблице "Список" я перехожу к строке по шаблону:
		| 'Дата' | 'Объект ИБ'    | 'Пакет данных УИД'  | 'Тип пакета'        | 'Хеш объекта ИБ' |
		| '*'    | 'Производство' | '*'                 | 'Справочник.Склады' | '*'              |
	И я нажимаю на кнопку с именем 'ФормаКомандаДобавитьВОчередьВсе'
	И в таблице "Список" количество строк "равно" 0
	И Я закрываю окно 'Объекты к выгрузке обмен данными ИБ'

	И В командном интерфейсе я выбираю 'Обмен данными ИБ' 'Очередь исходящих пакетов обмен данными ИБ'
	Тогда открылось окно 'Очередь исходящих пакетов обмен данными ИБ'
	И в таблице "Список" количество строк "равно" 3
	И в таблице "Список" я перехожу к строке по шаблону:
		| 'Дата'  | 'Объект ИБ'                 | 'Пакет данных УИД' | 'Получатель' | 'Правило'                 | 'Статус'         | 'Тип пакета'        | 'Хеш объекта ИБ' |
		| '*'     | 'Склад коммерческой службы' | '*'                | 'E4ф'        | 'Справочник.Склады (E4ф)' | 'Сформирован, *' | 'Справочник.Склады' | '*'              |
	И в таблице "Список" я перехожу к строке по шаблону:
		| 'Дата'  | 'Объект ИБ'    | 'Пакет данных УИД' | 'Получатель' | 'Правило'                 | 'Статус'         | 'Тип пакета'        | 'Хеш объекта ИБ'   |
		| '*'     | 'Производство' | '*'                | 'E4ф'        | 'Справочник.Склады (E4ф)' | 'Сформирован, *' | 'Справочник.Склады' | '*'                |
	И в таблице "Список" я перехожу к строке по шаблону:
		| 'Дата'  | 'Объект ИБ' | 'Пакет данных УИД' | 'Получатель' | 'Правило'                 | 'Статус'         | 'Тип пакета'        | 'Хеш объекта ИБ'  |
		| '*'     | 'Склады'    | '*'                | 'E4ф'        | 'Справочник.Склады (E4ф)' | 'Сформирован, *' | 'Справочник.Склады' | '*'               |
	И пауза 1
	И я нажимаю на кнопку с именем 'ФормаКомандаОтправитьВсе'
	И в таблице "Список" количество строк "равно" 0
	И Я закрываю окно 'Очередь исходящих пакетов обмен данными ИБ'

Сценарий: 13_integration. Перепроведение документа перемещения
	И В командном интерфейсе я выбираю 'Склад и доставка' 'Внутренние документы (все)'
	Тогда открылось окно 'Внутренние документы (все)'
	И в таблице "СписокДокументыВнутреннегоТовародвижения" я перехожу к строке:
		| 'Вид документа'                            | 'Дата'       | 'Дополнительно'                              | 'Менеджер'                     | 'Номер'       | 'Организация'      | 'Склад'                   | 'Статус'  |
		| 'Перемещение товаров, Перемещение товаров' | '20.01.2019' | 'Перемещение на "Склад коммерческой службы"' | 'Орлов Александр Владимирович' | '0000-000001' | 'Андромеда Сервис' | 'Склад готовой продукции' | 'Принято' |
	И в таблице "СписокДокументыВнутреннегоТовародвижения" я активизирую поле с именем "СписокДокументыВнутреннегоТовародвиженияНомер"
	И в таблице "СписокДокументыВнутреннегоТовародвижения" я выбираю текущую строку
	Тогда открылось окно 'Перемещение товаров * от *'
	И я нажимаю на кнопку с именем 'ФормаПровестиИЗакрыть'
	И я жду закрытия окна 'Перемещение товаров * от *' в течение 20 секунд
	Тогда открылось окно 'Внутренние документы (все)'
	И Я закрываю окно 'Внутренние документы (все)'

Сценарий: 13_integration. Проверка объектов и очереди по перемещению
	И В командном интерфейсе я выбираю 'Обмен данными ИБ' 'Объекты к выгрузке обмен данными ИБ'
	Тогда открылось окно 'Объекты к выгрузке обмен данными ИБ'
	И в таблице "Список" количество строк "равно" 1
	И я нажимаю на кнопку с именем 'ФормаКомандаДобавитьВОчередьВсе'
	И в таблице "Список" количество строк "равно" 0
	И Я закрываю окно 'Объекты к выгрузке обмен данными ИБ'

	И В командном интерфейсе я выбираю 'Обмен данными ИБ' 'Очередь исходящих пакетов обмен данными ИБ'
	Тогда открылось окно 'Очередь исходящих пакетов обмен данными ИБ'
	И в таблице "Список" количество строк "равно" 1
	И пауза 1
	И я нажимаю на кнопку с именем 'ФормаКомандаОтправитьВсе'
	И в таблице "Список" количество строк "равно" 0
	И Я закрываю окно 'Очередь исходящих пакетов обмен данными ИБ'

Сценарий: 13_integration. Закрытие клиента тестирования
	И я закрываю сеанс текущего клиента тестирования