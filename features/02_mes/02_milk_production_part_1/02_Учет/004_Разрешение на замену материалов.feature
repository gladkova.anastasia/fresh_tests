﻿#language: ru

Функционал: Создание документа Разрешение на замену материалов

Сценарий: Создание документа Разрешение на замену материалов
	Дано Я подключаюсь под "milk_prod_Технолог"
	И В командном интерфейсе я выбираю 'Производство' 'Разрешения на замену материалов'
	Тогда открылось окно 'Разрешения на замену материалов'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Разрешение на замену материалов (создание)'
	И из выпадающего списка "Статус" я выбираю точное значение 'Утверждено'
	И в поле 'Указание по применению' я ввожу текст '02 67'
	И я нажимаю на гиперссылку "Действие"
	Тогда открылось окно 'Действие разрешения'
	И я нажимаю на кнопку 'ОК'
	Тогда открылось окно 'Разрешение на замену материалов (создание) *'
	И я перехожу к закладке "Замена"
	И в таблице "Материалы" я нажимаю на кнопку 'Добавить'
	И в таблице "Материалы" я нажимаю кнопку выбора у реквизита "Номенклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'milk_prod_Пакет '       |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Разрешение на замену материалов (создание) *'
	И в таблице "Материалы" я активизирую поле "Количество"
	И в таблице "Материалы" в поле 'Количество' я ввожу текст '1,000'
	И в таблице "Материалы" я завершаю редактирование строки
	И в таблице "Материалы" я нажимаю на кнопку 'Добавить'
	И в таблице "Материалы" я нажимаю кнопку выбора у реквизита "Номенклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'  |
		| 'milk_prod_Пакет 2'       |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Разрешение на замену материалов (создание) *'
	И в таблице "Материалы" я активизирую поле "Количество"
	И в таблице "Материалы" в поле 'Количество' я ввожу текст '1,000'
	И в таблице "Материалы" я завершаю редактирование строки
	И в таблице "Аналоги" я нажимаю на кнопку 'Добавить'
	И в таблице "Аналоги" я нажимаю кнопку выбора у реквизита "Номенклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'milk_prod_Пакет 2'       |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Разрешение на замену материалов (создание) *'
	И в таблице "Аналоги" я активизирую поле "Количество"
	И в таблице "Аналоги" в поле 'Количество' я ввожу текст '1,000'
	И в таблице "Аналоги" я завершаю редактирование строки
	И в таблице "Аналоги" я нажимаю на кнопку 'Добавить'
	И в таблице "Аналоги" я нажимаю кнопку выбора у реквизита "Номенклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'milk_prod_Пакет '       |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Разрешение на замену материалов (создание) *'
	И в таблице "Аналоги" я активизирую поле "Количество"
	И в таблице "Аналоги" в поле 'Количество' я ввожу текст '1,000'
	И в таблице "Аналоги" я завершаю редактирование строки
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Разрешение на замену материалов (создание) *' в течение 20 секунд
	Тогда открылось окно 'Разрешения на замену материалов'
	И Я закрываю окно 'Разрешения на замену материалов'

Сценарий: 02_milk_production.02.004 Закрытие клиента
	И я закрываю сеанс TESTCLIENT