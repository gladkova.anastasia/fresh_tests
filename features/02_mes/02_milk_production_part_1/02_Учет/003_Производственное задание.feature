﻿#language: ru

Функционал: Создание документа Производственное задание

Сценарий: Создание документа Производственное задание
	Дано Я переключаюсь на открытый клиент "milk_prod_Мастер сырцеха"
	И В командном интерфейсе я выбираю 'Производство' 'Производственные задания'
	Тогда открылось окно 'Производственные задания'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Производственное задание (создание)'
	И в поле 'Дата смены' я ввожу текст '01.11.2018'
	И в поле 'от' я ввожу текст '01.11.2018 09:12:32'
	И я нажимаю кнопку выбора у поля "Смена"
	Тогда открылось окно 'Смены'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Смена24'      |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И я нажимаю кнопку выбора у поля "Рабочий центр"
	Тогда открылось окно 'Рабочие центры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'milk_prod_Сырцех'       |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И я перехожу к закладке "Задание"
	И в таблице "Товары" я добавляю новую строку
	И в таблице "Товары" я активизирую поле "Номенклатура"
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита "Номенклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'   |
		| 'milk_prod_Российский 50%' |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" я активизирую поле "Характеристика"
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита "Характеристика"
	Тогда открылось окно 'Характеристики номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'	|
		| 'Варка'			|
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита "Серия"
	Тогда открылось окно 'Серии номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Номер' | 'Дата выпуска' |
		| '2'     | '01.11.2018'   |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" я перехожу к строке:
		| 'N' |
		| '1' |
	И в таблице 'Товары' я удаляю строку
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита "Номенклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'  |
		| 'milk_prod_Сливочный 45%' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита "Характеристика"
	Тогда открылось окно 'Характеристики номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Варка'        |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита "Номенклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'  |
		| 'milk_prod_Сливочный 45%' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита "Характеристика"
	Тогда открылось окно 'Характеристики номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Варка'        |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита "Номенклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'  |
		| 'milk_prod_Сливочный 45% ГП' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита "Номенклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'  |
		| 'milk_prod_Сливочный 45% ГП' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита с именем "ТоварыНоменклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Вид' 		  | 'Ед.' | 'Наименование'  		  |
		| 'milk_prod_Сыр' | 'кг'  | 'milk_prod_Сливочный 45%' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита с именем "ТоварыНоменклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Вид' 		  | 'Ед.' | 'Наименование'    			|
		| 'milk_prod_Сыр' | 'кг'  | 'milk_prod_Диетический 35%' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита с именем "ТоварыНоменклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Вид'           			| 'Ед.' | 'Наименование' 		   |
		| 'milk_prod_Полуфабрикаты' | 'кг'  | 'milk_prod_Сыворотка'    |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита "Спецификация"
	Тогда открылось окно 'Ресурсные спецификации'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'      |
		| 'milk_prod_Сливочный 45% milk_prod_Сыр' |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" в поле с именем 'ТоварыКоличество' я ввожу текст '100,000'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Производственное задание (создание) *' в течение 20 секунд
	Тогда открылось окно 'Производственные задания'
	И Я закрываю окно 'Производственные задания'

Сценарий: Задание на выпуск сыворотки без серий
	И В командном интерфейсе я выбираю 'Производство' 'Производственные задания'
	Тогда открылось окно 'Производственные задания'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Производственное задание (создание)'
	И в поле 'от' я ввожу текст '02.02.2020'
	И в поле 'Дата смены' я ввожу текст '02.02.2020'
	И я нажимаю кнопку выбора у поля "Рабочий центр"
	Тогда открылось окно 'Рабочие центры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'     |
		| 'milk_prod_Выпуск сыворотки' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И я перехожу к закладке "Задание"
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита с именем "ТоварыНоменклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'milk_prod_Сыворотка'    |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" я перехожу к строке:
		| 'N' |
		| '1' |
	И в таблице 'Товары' я удаляю строку
	И я нажимаю на кнопку 'Провести'
	И     таблица "Товары" стала равной:
		| 'N' | 'Номенклатура' 			 | 'Характеристика'    | 'Серия' | 'Ед. изм.' |
		| '1' | 'milk_prod_Сыворотка'    | '<не используются>' | ''      | 'кг'       |
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита с именем "ТоварыНоменклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'milk_prod_Сыворотка'    |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Производственное задание *'
	И я перехожу к закладке "Дополнительно"
	И в поле 'Комментарий' я ввожу текст 'для нескольких выпусков по одной строке задания'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Производственное задание *' в течение 20 секунд
	Тогда открылось окно 'Производственные задания'
	И Я закрываю окно 'Производственные задания'

Сценарий: Создание задания на выпуск сыра для использования нескольких партий смеси
	И В командном интерфейсе я выбираю 'Производство' 'Производственные задания'
	Тогда открылось окно 'Производственные задания'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Производственное задание (создание)'
	И в поле 'от' я ввожу текст '03.02.2020'
	И в поле 'Дата смены' я ввожу текст '03.02.2020'
	И я нажимаю кнопку выбора у поля "Рабочий центр"
	Тогда открылось окно 'Рабочие центры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'milk_prod_Сырцех'       |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И я перехожу к закладке "Задание"
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита с именем "ТоварыНоменклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'  |
		| 'milk_prod_Сливочный 45%' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" я перехожу к строке:
		| 'N' |
		| '1' |
	И в таблице 'Товары' я удаляю строку
	И я перехожу к закладке "Дополнительно"
	И в поле 'Комментарий' я ввожу текст 'для использования нескольких серий материалов'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Производственное задание (создание) *' в течение 20 секунд
	Тогда открылось окно 'Производственные задания'
	И Я закрываю окно 'Производственные задания'

Сценарий: Ичалки #15. Выпуск несколькими документами на разные тары - избавиться от дублирования строк материалов. Подготовка задания
	И В командном интерфейсе я выбираю 'Производство' 'Производственные задания'
	Тогда открылось окно 'Производственные задания'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Производственное задание (создание)'
	И я запоминаю значение поля "от" как "ДатаДокумента"
	И в поле 'Дата смены' ввожу значение переменной "ДатаДокумента"
	И я запоминаю значение поля 'Дата смены' как 'ДатаСмены'
	И я нажимаю кнопку выбора у поля "Рабочий центр"
	Тогда открылось окно 'Рабочие центры'
	И в таблице "Список" я перехожу к строке:
		| Наименование |
		| milk_prod_Сырцех       |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" я добавляю новую строку
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита "Номенклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| Наименование   |
		| 'milk_prod_Российский 50%' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита "Характеристика"
	Тогда открылось окно 'Характеристики номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| Наименование |
		| Варка        |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И я перехожу к закладке "Дополнительно"
	И в поле 'Комментарий' я ввожу текст 'Пригодится для ОПС'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Производственное задание (создание) *' в течение 20 секунд
	Тогда открылось окно 'Производственные задания'
	И Я закрываю окно 'Производственные задания'

Сценарий: Мес 69. Учет ингредиентов при нескольких основных складах без указания основного ингредиента. Подготовка. Создание задания
	И В командном интерфейсе я выбираю 'Производство' 'Производственные задания'
	Тогда открылось окно 'Производственные задания'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Производственное задание (создание)'
	И я запоминаю значение поля "от" как "ДатаДокумента"
	И в поле 'Дата смены' ввожу значение переменной "ДатаДокумента"
	И из выпадающего списка "Рабочий центр" я выбираю точное значение 'milk_prod_Сырцех'
	И я перехожу к закладке "Задание"
	И в таблице "Товары" я добавляю новую строку
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита "Номенклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'   |
		| 'milk_prod_Российский 50%' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" я нажимаю кнопку выбора у реквизита "Характеристика"
	Тогда открылось окно 'Характеристики номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Варка'    |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Производственное задание (создание) *'
	И в таблице "Товары" я перехожу к строке:
		|'N'|
		|'1'|
	И в таблице "Товары" я удаляю строку
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Производственное задание (создание) *' в течение 20 секунд
	Тогда открылось окно 'Производственные задания'
	И Я закрываю окно 'Производственные задания'

Сценарий: Создание задания на выпуск масла
	И В командном интерфейсе я выбираю 'Производство' 'Производственные задания'
	Тогда открылось окно 'Производственные задания'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Производственное задание (создание)'
	И в поле 'Дата смены' я ввожу текущую дату
	И из выпадающего списка "Рабочий центр" я выбираю по строке 'milk_prod_масло'
	И я перехожу к закладке "Задание"
	И в таблице "Товары" я добавляю строку
	И в таблице "Товары" из выпадающего списка с именем "ТоварыНоменклатура" я выбираю по строке 'milk_prod_масло'
	И в таблице "Товары" я перехожу к строке:
		| 'N' | 'Серия'             | 'Характеристика'    |
		| '1' | '<не используются>' | '<не используются>' |
	И в таблице 'Товары' я удаляю строку
	И     таблица "Товары" стала равной:
		| 'N' | 'Спецификация' 			 | 'Количество' | 'Номенклатура' 		   | 'Характеристика'    | 'Серия' | 'Ед. изм.' |
		| '1' | 'milk_prod_Масло'        | '1 000,000'  | 'milk_prod_Масло'        | '<не используются>' | ''      | 'кг'       |
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Производственное задание (создание) *' в течение 20 секунд
	Тогда открылось окно 'Производственные задания'
	И Я закрываю окно 'Производственные задания'