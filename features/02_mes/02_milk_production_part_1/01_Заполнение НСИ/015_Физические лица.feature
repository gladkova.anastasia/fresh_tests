#language: ru

Функционал: 02_milk_production. Заполнение справочников Физ.лица

Сценарий: 02_milk_production. Создание физ.лиц
	Дано Я подключаюсь под "Администратор"
	И Я создаю физическое лицо "milk_prod_Укладчик Сидорова"
	И Я создаю физическое лицо "milk_prod_Укладчик Петров"
	И Я создаю физическое лицо "milk_prod_Упаковщик Митяев"
	И Я создаю физическое лицо "milk_prod_Карщик Невельская"