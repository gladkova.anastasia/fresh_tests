﻿#language: ru

@quality_control

Функционал: 03_quality_control. Создание документа Анализы номенклатуры

Сценарий: 03_quality_control. Создание документа Анализы номенклатуры на основании заявки
	Дано Я подключаюсь под "qual_Лаборант"
	И В командном интерфейсе я выбираю 'Производство' 'Заявки на анализы номенклатуры'
	Тогда открылось окно 'Заявки на анализы номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Выполнена' | 'Способ создания' | 'Номенклатура' | 'Вид анализа'        |
		| 'Нет'       | 'Создан вручную'  | 'Смесь 50%'    | 'показатели МДЖ МДБ' |
	И я нажимаю на кнопку 'Анализы номенклатуры'
	Тогда открылось окно 'Анализы номенклатуры (создание)'
	И я перехожу к закладке "Результаты анализов"
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Показатель' | 'Соответствует нормативу' |
		| 'МДБ'        | 'Нет'                     |
	И в таблице "РезультатыАнализов" в поле 'Значение показателя' я ввожу текст '2,000'
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Вид результата анализа'    | 'Значение max' | 'Показатель' | 'Соответствует нормативу' |
		| 'Число дробное в диапазоне' | '50,000'       | 'МДЖ'        | 'Нет'                     |
	И в таблице "РезультатыАнализов" в поле 'Значение показателя' я ввожу текст '4,000'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Анализы номенклатуры (создание) *' в течение 20 секунд
	Тогда открылось окно 'Заявки на анализы номенклатуры'
	И я закрываю окно 'Заявки на анализы номенклатуры'

Сценарий: 03_quality_control. Создание документа Анализы номенклатуры как самостоятельного документа
	И В командном интерфейсе я выбираю 'Производство' 'Анализы номенклатуры'
	Тогда открылось окно 'Анализы номенклатуры'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Анализы номенклатуры (создание)'
	И я нажимаю кнопку выбора у поля "Вид анализа"
	Тогда открылось окно 'Виды анализов номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'       |
		| 'показатели МДЖ МДБ' |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Анализы номенклатуры (создание) *'
	И я нажимаю кнопку выбора у поля "Номенклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'   |
		| 'Российский 50%' |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Анализы номенклатуры (создание) *'
	И из выпадающего списка "Характеристика" я выбираю по строке 'вар'
	Тогда открылось окно 'Анализы номенклатуры (создание) *'
	И я нажимаю кнопку выбора у поля "Серия"
	Тогда открылось окно 'Серии номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Номер' | 'Дата выпуска' |
		| '1'     | '11.11.2018'   |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Анализы номенклатуры (создание) *'
	И я перехожу к закладке "Результаты анализов"
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Показатель' | 'Соответствует нормативу' |
		| 'МДБ'        | 'Нет'                     |
	И в таблице "РезультатыАнализов" я активизирую поле "Значение показателя"
	И в таблице "РезультатыАнализов" в поле 'Значение показателя' я ввожу текст '21,000'
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Вид результата анализа'    | 'Значение max' | 'Показатель' | 'Соответствует нормативу' |
		| 'Число дробное в диапазоне' | '50,000'       | 'МДЖ'        | 'Нет'                     |
	И в таблице "РезультатыАнализов" в поле 'Значение показателя' я ввожу текст '25,000'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Анализы номенклатуры (создание) *' в течение 20 секунд
	Тогда открылось окно 'Анализы номенклатуры'
	И Я закрываю окно 'Анализы номенклатуры'

Сценарий: 03_quality_control. Проверка нормативов при смене показателя
	И В командном интерфейсе я выбираю 'Производство' 'Анализы номенклатуры'
	Тогда открылось окно 'Анализы номенклатуры'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Анализы номенклатуры (создание)'
	И я нажимаю кнопку выбора у поля "Вид анализа"
	Тогда открылось окно 'Виды анализов номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'тест'         |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Анализы номенклатуры (создание) *'
	И я нажимаю кнопку выбора у поля "Номенклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Молоко'       |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Анализы номенклатуры (создание) *'
	И я нажимаю кнопку выбора у поля "Серия"
	Тогда открылось окно 'Серии номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Номер' | 'Дата выпуска' |
		| '2'     | '11.11.2018'   |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Анализы номенклатуры (создание) *'
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Показатель'       |
		| 'Расчет от других' |
	И в таблице "РезультатыАнализов" я нажимаю кнопку выбора у реквизита "Показатель"
	Тогда открылось окно 'Показатели анализов номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Признак'      |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Анализы номенклатуры (создание) *'
	И в таблице "РезультатыАнализов" поле "Значение min" имеет значение "Да"
	И Я закрываю окно 'Анализы номенклатуры (создание) *'
	Тогда открылось окно '1С:Предприятие'
	И я нажимаю на кнопку 'Нет'
	Тогда открылось окно 'Анализы номенклатуры'
	И Я закрываю окно 'Анализы номенклатуры'

Сценарий: 03_quality_control. Анализ по молоку от 06.06.20
	И В командном интерфейсе я выбираю 'Производство' 'Анализы номенклатуры'
	Тогда открылось окно 'Анализы номенклатуры'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Анализы номенклатуры (создание)'
	И из выпадающего списка "Вид анализа" я выбираю по строке 'пока'
	И из выпадающего списка "Номенклатура" я выбираю по строке 'моло'
	Тогда открылось окно 'Анализы номенклатуры (создание) *'
	И я нажимаю кнопку выбора у поля "Серия"
	Тогда открылось окно 'Серии номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Дата выпуска' | 'Номер'                  |
		| '06.06.2020'   | '0100324532253253253252' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Анализы номенклатуры (создание) *'
	И я перехожу к закладке "Результаты анализов"
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Показатель' | 'Соответствует нормативу' |
		| 'МДБ'        | 'Нет'                     |
	И в таблице "РезультатыАнализов" в поле 'Значение показателя' я ввожу текст '3,000'
	И в таблице "РезультатыАнализов" я завершаю редактирование строки
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Значение max' | 'Значение min' | 'Показатель' | 'Соответствует нормативу' |
		| '50,000'       | '0,300'        | 'МДЖ'        | 'Нет'                     |
	И в таблице "РезультатыАнализов" в поле 'Значение показателя' я ввожу текст '3,400'
	И в таблице "РезультатыАнализов" я завершаю редактирование строки
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Анализы номенклатуры (создание) *' в течение 20 секунд
	Тогда открылось окно 'Анализы номенклатуры'
	И Я закрываю окно 'Анализы номенклатуры'

Сценарий: 03_quality_control. Анализ по молоку от 07.06.20
	И В командном интерфейсе я выбираю 'Производство' 'Анализы номенклатуры'
	Тогда открылось окно 'Анализы номенклатуры'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Анализы номенклатуры (создание)'
	И из выпадающего списка "Вид анализа" я выбираю по строке 'пока'
	И из выпадающего списка "Номенклатура" я выбираю по строке 'моло'
	Тогда открылось окно 'Анализы номенклатуры (создание) *'
	И я нажимаю кнопку выбора у поля "Серия"
	Тогда открылось окно 'Серии номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Дата выпуска' | 'Номер'                  |
		| '07.06.2020'   | '0100324532253253253252' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Анализы номенклатуры (создание) *'
	И я перехожу к закладке "Результаты анализов"
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Показатель' | 'Соответствует нормативу' |
		| 'МДБ'        | 'Нет'                     |
	И в таблице "РезультатыАнализов" в поле 'Значение показателя' я ввожу текст '3,200'
	И в таблице "РезультатыАнализов" я завершаю редактирование строки
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Значение max' | 'Значение min' | 'Показатель' | 'Соответствует нормативу' |
		| '50,000'       | '0,300'        | 'МДЖ'        | 'Нет'                     |
	И в таблице "РезультатыАнализов" в поле 'Значение показателя' я ввожу текст '3,600'
	И в таблице "РезультатыАнализов" я завершаю редактирование строки
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Анализы номенклатуры (создание) *' в течение 20 секунд
	Тогда открылось окно 'Анализы номенклатуры'
	И Я закрываю окно 'Анализы номенклатуры'

Сценарий: 03_quality_control. Анализ по молоку от 08.06.20
	И В командном интерфейсе я выбираю 'Производство' 'Анализы номенклатуры'
	Тогда открылось окно 'Анализы номенклатуры'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Анализы номенклатуры (создание)'
	И из выпадающего списка "Вид анализа" я выбираю по строке 'пока'
	И из выпадающего списка "Номенклатура" я выбираю по строке 'моло'
	Тогда открылось окно 'Анализы номенклатуры (создание) *'
	И я нажимаю кнопку выбора у поля "Серия"
	Тогда открылось окно 'Серии номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Дата выпуска' | 'Номер'                  |
		| '08.06.2020'   | '0100324532253253253252' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Анализы номенклатуры (создание) *'
	И я перехожу к закладке "Результаты анализов"
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Показатель' | 'Соответствует нормативу' |
		| 'МДБ'        | 'Нет'                     |
	И в таблице "РезультатыАнализов" в поле 'Значение показателя' я ввожу текст '3,100'
	И в таблице "РезультатыАнализов" я завершаю редактирование строки
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Значение max' | 'Значение min' | 'Показатель' | 'Соответствует нормативу' |
		| '50,000'       | '0,300'        | 'МДЖ'        | 'Нет'                     |
	И в таблице "РезультатыАнализов" в поле 'Значение показателя' я ввожу текст '3,500'
	И в таблице "РезультатыАнализов" я завершаю редактирование строки
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Анализы номенклатуры (создание) *' в течение 20 секунд
	Тогда открылось окно 'Анализы номенклатуры'
	И Я закрываю окно 'Анализы номенклатуры'

Сценарий: 03_quality_control.02.003 Закрытие клиента
	И я закрываю сеанс TESTCLIENT