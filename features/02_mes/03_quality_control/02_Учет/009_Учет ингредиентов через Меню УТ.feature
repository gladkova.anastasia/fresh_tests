﻿#language: ru

@quality_control

Функционал: 03_quality_control. Запуск обработки Меню учетных точек: Работа с заданиями

Сценарий: 03_quality_control. Проверка предупреждений в Учете ингредиентов с пересчетом смеси в кг
	Дано Я подключаюсь под "qual_Киоск"
	И в поле с именем 'Дата' я ввожу текст '01.11.2018'
	И я нажимаю кнопку выбора у поля "Смена"
	Тогда открылось окно 'Смены'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Смена24'      |
	И я нажимаю на кнопку 'Выбрать'
	И я нажимаю кнопку выбора у поля "УчетнаяТочка"
	Тогда открылось окно 'Учетные точки'
	И в таблице "Список" я перехожу к строке:
		|'Наименование' |
		| 'Сырцех'       |
	И я нажимаю на кнопку 'Выбрать'
	И я нажимаю на кнопку 'Пересчет из литров в кг'
	Тогда открылось окно 'Пересчет из литров в кг *: Работа с заданиями'
	И в таблице "Задания" я перехожу к строке:
		| 'N' | 'Ед. изм.' | 'Задание'    | 'Номенклатура'                           | 'Состояние' |
		| '7' | 'кг'       | '950,000'    | 'Диетический 35% / Варка / 01.11.2018/7' | 'Не начато' |
	И я нажимаю на кнопку 'Ингредиенты'
	Тогда открылось окно 'Диетический 35% / Варка / 01.11.2018/7 [950 кг]: Учет расхода ингредиентов'
	И я нажимаю кнопку выбора у поля "СкладОтправитель"
	Тогда открылось окно 'Склады'
	И я нажимаю на кнопку 'ФормаВыбрать'
	Тогда открылось окно 'Диетический 35% / Варка / 01.11.2018/7 [950 кг]: Учет расхода ингредиентов'
	И я нажимаю на кнопку 'Указать серию'
	Тогда открылось окно 'Выбрать серию для Смесь 35%'
	И в таблице "Список" я перехожу к строке:
		| 'Серия'                   |
		| '7 до 01.11.2018 0:00:00' |
	И я нажимаю на кнопку 'КомандаВыбрать'
	Тогда открылось окно 'Диетический 35% / Варка / 01.11.2018/7 [950 кг]: Учет расхода ингредиентов *'
	И я нажимаю на кнопку '9'
	И я нажимаю на кнопку '8'
	И я нажимаю на кнопку '8'
	И я нажимаю на кнопку '0'
	И я нажимаю на кнопку 'Подтвердить'
	И я нажимаю на кнопку 'КомандаОК'
	Тогда открылось окно 'Диетический 35% / Варка / 01.11.2018/7 [950 кг]: Учет расхода ингредиентов *'
	И я нажимаю на кнопку 'КомандаЗакрыть'
	Тогда открылось окно 'Пересчет из литров в кг (01.11.18 Смена24) *: Работа с заданиями'
	И в таблице "Задания" я перехожу к строке:
		| 'N' | 'Ед. изм.' | 'Задание'    | 'Номенклатура'                         | 'Состояние' |
		| '6' | 'кг'       | '950,000'    | 'Сливочный 45% / Варка / 01.11.2018/6' | 'Не начато' |
	И я нажимаю на кнопку 'Ингредиенты'
	И я нажимаю на кнопку 'КомандаОК'
	Тогда открылось окно 'Пересчет из литров в кг (01.11.18 Смена24) *: Работа с заданиями'
	И я закрыл все окна клиентского приложения

Сценарий: 03_quality_control. Заполнение значения плотности для смеси
	Дано Я подключаюсь под "qual_Лаборант"
	И В командном интерфейсе я выбираю 'Производство' 'Анализы номенклатуры'
	Тогда открылось окно 'Анализы номенклатуры'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Анализы номенклатуры (создание)'
	И я нажимаю кнопку выбора у поля "Вид анализа"
	Тогда открылось окно 'Виды анализов номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'       |
		| 'показатели МДЖ МДБ' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Анализы номенклатуры (создание) *'
	И я нажимаю кнопку выбора у поля "Номенклатура"
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Вид'   | 'Ед.' | 'Наименование' | 'Наименование для печати' |
		| 'Смеси' | 'кг'  | 'Смесь 35%'    | 'Смесь 35%'               |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Анализы номенклатуры (создание) *'
	И я нажимаю кнопку выбора у поля "Серия"
	Тогда открылось окно 'Серии номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Дата выпуска' | 'Наименование'            | 'Номер' |
		| '01.11.2018'   | '7 до 01.11.2018 0:00:00' | '7'     |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Анализы номенклатуры (создание) *'
	И я перехожу к закладке "Результаты анализов"
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Вид результата анализа'    | 'Показатель' | 'Соответствует нормативу' |
		| 'Число дробное в диапазоне' | 'МДБ'        | 'Нет'                     |
	И в таблице "РезультатыАнализов" в поле 'Значение показателя' я ввожу текст '3,400'
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Вид результата анализа'    | 'Значение max' | 'Значение min' | 'Показатель' | 'Соответствует нормативу' |
		| 'Число дробное в диапазоне' | '50,000'       | '0,300'        | 'МДЖ'        | 'Нет'                     |
	И в таблице "РезультатыАнализов" в поле 'Значение показателя' я ввожу текст '2,400'
	И в таблице "РезультатыАнализов" я нажимаю на кнопку с именем 'РезультатыАнализовДобавить'
	И в таблице "РезультатыАнализов" я нажимаю кнопку выбора у реквизита "Показатель"
	Тогда открылось окно 'Показатели анализов номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Плотность'    |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Анализы номенклатуры (создание) *'
	И в таблице "РезультатыАнализов" в поле 'Значение показателя' я ввожу текст '1,210'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Анализы номенклатуры (создание) *' в течение 20 секунд
	Тогда открылось окно 'Анализы номенклатуры'
	И Я закрываю окно 'Анализы номенклатуры'

Сценарий: 03_quality_control. Проверка пересчета в кг в Учете ингредиентов
	Дано Я переключаюсь на открытый клиент "qual_Киоск"
	И в поле с именем 'Дата' я ввожу текст '01.11.2018'
	И я нажимаю кнопку выбора у поля "Смена"
	Тогда открылось окно 'Смены'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Смена24'      |
	И я нажимаю на кнопку 'Выбрать'
	И я нажимаю кнопку выбора у поля "УчетнаяТочка"
	Тогда открылось окно 'Учетные точки'
	И в таблице "Список" я перехожу к строке:
		|'Наименование' |
		| 'Сырцех'       |
	И я нажимаю на кнопку 'Выбрать'
	И я нажимаю на кнопку 'Пересчет из литров в кг'
	Тогда открылось окно 'Пересчет из литров в кг (01.11.18 Смена24) *: Работа с заданиями'
	И в таблице "Задания" я перехожу к строке:
		| 'N' | 'Ед. изм.' | 'Задание'    | 'Номенклатура'                           | 'Состояние' |
		| '7' | 'кг'       | '950,000'    | 'Диетический 35% / Варка / 01.11.2018/7' | 'Не начато' |
	И я нажимаю на кнопку 'Ингредиенты'
	Тогда открылось окно 'Диетический 35% / Варка / 01.11.2018/7 [950 кг]: Учет расхода ингредиентов'
	И     таблица "РаспределениеМатериалов" стала равной:
		| 'Номенклатура' | 'Норма'          | 'Серия' | 'План' | 'Факт' | 'Факт' | 'Факт (л)' |
		| 'Смесь 35%'    | '10 000,000, кг' | ''      | ''     | ''     | ''     | ''         |
	И я нажимаю кнопку выбора у поля "СкладОтправитель"
	Тогда открылось окно 'Склады'
	И я нажимаю на кнопку 'ФормаВыбрать'
	Тогда открылось окно 'Диетический 35% / Варка / 01.11.2018/7 [950 кг]: Учет расхода ингредиентов'
	И я нажимаю на кнопку 'Указать серию'
	Тогда открылось окно 'Выбрать серию для Смесь 35%'
	И в таблице "Список" я перехожу к строке:
		| 'Серия'                   |
		| '7 до 01.11.2018 0:00:00' |
	И я нажимаю на кнопку 'КомандаВыбрать'
	Тогда открылось окно 'Диетический 35% / Варка / 01.11.2018/7 [950 кг]: Учет расхода ингредиентов *'
	И я нажимаю на кнопку '9'
	И я нажимаю на кнопку '8'
	И я нажимаю на кнопку '8'
	И я нажимаю на кнопку '5'
	И я нажимаю на кнопку 'Подтвердить'
	И     таблица "РаспределениеМатериалов" стала равной по шаблону:
		| 'Номенклатура' | 'Норма'          | 'Серия'                   | 'План' | 'Факт'       | 'Факт (л)'  |
		| 'Смесь 35%'    | '10 000,000, кг' | '7 до 01.11.2018 0:00:00' | ''     | '11 960,850' | '9 885,000' |
	И я нажимаю на кнопку 'КомандаСохранить'
	Тогда открылось окно 'Пересчет из литров в кг *: Работа с заданиями'
	И Я закрываю окно 'Пересчет из литров в кг *: Работа с заданиями'

Сценарий: 03_quality_control.02.009 Закрытие клиента
	И я закрываю сеанс TESTCLIENT

Сценарий: 03_quality_control. Проверка документа с пересчетом
	Дано Я переключаюсь на открытый клиент "Администратор"
	И В командном интерфейсе я выбираю 'Производство' 'Переработки'
	Тогда открылось окно 'Переработки'
	И в таблице "Список" я перехожу к строке:
		| 'Вид операции' | 'Дата смены' | 'Ответственный' | 'Рабочий центр' | 'Смена'   | 'Способ создания'           | 'Статус' |
		| 'Переработка'  | '01.11.2018' | 'qual_Киоск'    | 'Сырцех'        | 'Смена24' | 'Учет расхода ингредиентов' | 'Начат'  |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Переработка *'
	И я перехожу к закладке "Распределение материалов"
	И в таблице "РаспределениеМатериалов" я перехожу к строке:
		| 'Распределить на выпуск'                 | 'Ед. изм. 2' | 'N' | 'Количество' | 'Номер пакета' | 'Тара' | 'Номенклатура' | 'Количество 1' | 'Характеристика'    | 'Серия'                   | 'Количество 2' | 'Ед. изм.' | 'Ед. изм. 1' |
		| 'Диетический 35% / Варка / 01.11.2018/7' | ''           | '1' | '11 960,850' | ''             | ''     | ''             | '9 885,000'    | '<не используются>' | '7 до 01.11.2018 0:00:00' | ''             | ''         | 'л (дм3)'    |
	И я закрыл все окна клиентского приложения