﻿#language: ru

@cheese_maker

Функционал: 07_cheeseMaker.01.005 Заполнение справочника Виды анализов номенклатуры

Сценарий: 07_cheeseMaker.01.005 Создание Вида показатели МДЖ МДБ
	Дано Я подключаюсь под "Администратор"
	И В командном интерфейсе я выбираю 'Производство' 'Виды анализов номенклатуры'
	Тогда открылось окно 'Виды анализов номенклатуры'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Вид анализа номенклатуры (создание)'
	И в поле 'Наименование' я ввожу текст 'показатели МДЖ МДБ'
	И в таблице "Показатели" я нажимаю на кнопку 'Добавить'
	И в таблице "Показатели" я нажимаю кнопку выбора у реквизита "Показатель"
	Тогда открылось окно 'Показатели анализов номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'МДБ'          |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Вид анализа номенклатуры (создание) *'
	И в таблице "Показатели" я завершаю редактирование строки
	И в таблице "Показатели" я нажимаю на кнопку 'Добавить'
	И в таблице "Показатели" я нажимаю кнопку выбора у реквизита "Показатель"
	Тогда открылось окно 'Показатели анализов номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'МДЖ'          |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Вид анализа номенклатуры (создание) *'
	И в таблице "Показатели" я завершаю редактирование строки
	И я нажимаю на кнопку 'Записать'
	И в таблице "Показатели" я перехожу к строке:
		| 'N' | 'Показатель' |
		| '1' | 'МДБ'        |
	И в таблице "Показатели" в поле 'Значение max' я ввожу текст '6,000'
	И в таблице "Показатели" я завершаю редактирование строки
	И в таблице "Показатели" я перехожу к строке:
		| 'N' | 'Показатель' |
		| '2' | 'МДЖ'        |
	И в таблице "Показатели" в поле 'Значение max' я ввожу текст '50,000'
	И в таблице "Показатели" я завершаю редактирование строки
	И я нажимаю на кнопку 'Записать'
	И в таблице "Показатели" я перехожу к строке:
		| 'N' | 'Показатель' |
		| '1' | 'МДБ'        |
	И в таблице "Показатели" в поле 'Значение min' я ввожу текст '0,300'
	И в таблице "Показатели" я завершаю редактирование строки
	И в таблице "Показатели" я перехожу к строке:
		| 'N' | 'Показатель' |
		| '2' | 'МДЖ'        |
	И в таблице "Показатели" в поле 'Значение min' я ввожу текст '0,300'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Показатели МДЖ МДБ (Вид анализа номенклатуры) *' в течение 20 секунд
	Тогда открылось окно 'Виды анализов номенклатуры'
	И Я закрываю окно 'Виды анализов номенклатуры'

Сценарий: 07_cheeseMaker.01.005 Создание Вида Этапы варки сыра - параметры т\п
	И В командном интерфейсе я выбираю 'Производство' 'Виды анализов номенклатуры'
	Тогда открылось окно 'Виды анализов номенклатуры'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Вид анализа номенклатуры (создание)'
	И в поле 'Наименование' я ввожу текст 'Этапы варки сыра'
	И в таблице "Показатели" я нажимаю на кнопку с именем 'ПоказателиДобавить'
	И в таблице "Показатели" я нажимаю кнопку выбора у реквизита "Показатель"
	Тогда открылось окно 'Показатели анализов номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'         |
		| 'Время наливки молока' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Вид анализа номенклатуры (создание) *'
	И в таблице "Показатели" я нажимаю на кнопку с именем 'ПоказателиДобавить'
	И в таблице "Показатели" я нажимаю кнопку выбора у реквизита "Показатель"
	Тогда открылось окно 'Показатели анализов номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'      |
		| 'Время прессования' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Вид анализа номенклатуры (создание) *'
	И я устанавливаю флаг 'Параметр тех. процесса'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Вид анализа номенклатуры (создание) *' в течение 20 секунд
	Тогда открылось окно 'Виды анализов номенклатуры'
	И Я закрываю окно 'Виды анализов номенклатуры'

Сценарий: 07_cheeseMaker.01.005 Привязка видов анализа к виду номенклатуры Сыр
	И В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Классификаторы номенклатуры'
	Тогда открылось окно 'Классификаторы номенклатуры'
	И я нажимаю на кнопку 'Виды номенклатуры'
	Тогда открылось окно 'Виды номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Сыр'          |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Сыр (Вид номенклатуры)'
	И я перехожу к закладке "Производство"
	И у поля "Виды анализов качественных показателей (0)" я нажимаю гиперссылку 'Виды анализов качественных показателей (0)'
	Тогда открылось окно 'Виды анализов вида номенклатуры Сыр'
	И в таблице "ВидыАнализов" я нажимаю на кнопку с именем 'ВидыАнализовДобавить'
	И в таблице "ВидыАнализов" я нажимаю кнопку выбора у реквизита "Вид анализа"
	Тогда открылось окно 'Виды анализов номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'     |
		| 'Этапы варки сыра' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Виды анализов вида номенклатуры Сыр *'
	И в таблице "ВидыАнализов" я нажимаю кнопку выбора у реквизита "Характеристика"
	Тогда открылось окно 'Характеристики номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'  |
		| 'Варка' 	  |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Виды анализов вида номенклатуры Сыр *'
	И я закрыл все окна клиентского приложения

Сценарий: 07_cheeseMaker.01.005 Привязка видов анализа к номенклатуре Российский 50%
	И В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Номенклатура'
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'   |
		| 'Российский 50%' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Российский 50% (Номенклатура)'
	И я перехожу к закладке "Дополнительно"
	И у поля "Виды анализов качественных показателей (0)" я нажимаю гиперссылку 'Виды анализов качественных показателей (0)'
	Тогда открылось окно 'Виды анализов позиции номенклатуры Российский 50%'
	И в таблице "ВидыАнализов" я нажимаю на кнопку с именем 'ВидыАнализовДобавить'
	И в таблице "ВидыАнализов" я нажимаю кнопку выбора у реквизита с именем "ВидыАнализовВидАнализа"
	Тогда открылось окно 'Виды анализов номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'     |
		| 'Этапы варки сыра' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Виды анализов позиции номенклатуры Российский 50% *'
	И в таблице "ВидыАнализов" я нажимаю кнопку выбора у реквизита "Характеристика"
	Тогда открылось окно 'Характеристики номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Варка'        |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Виды анализов позиции номенклатуры Российский 50% *'
	И Я закрываю окно 'Виды анализов позиции номенклатуры Российский 50% *'
	Тогда открылось окно 'Российский 50% (Номенклатура)'
	И Я закрываю окно 'Российский 50% (Номенклатура)'
	Тогда открылось окно 'Номенклатура'
	И я закрыл все окна клиентского приложения

Сценарий: 07_cheeseMaker.01.005 Привязка видов анализа к номенклатуре Сливочный 45%
	И В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Номенклатура'
	Тогда открылось окно 'Номенклатура'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'  |
		| 'Сливочный 45%' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Сливочный 45% (Номенклатура)'
	И я перехожу к закладке "Дополнительно"
	И у поля "Виды анализов качественных показателей (0)" я нажимаю гиперссылку 'Виды анализов качественных показателей (0)'
	Тогда открылось окно 'Виды анализов позиции номенклатуры Сливочный 45%'
	И в таблице "ВидыАнализов" я нажимаю на кнопку с именем 'ВидыАнализовДобавить'
	И в таблице "ВидыАнализов" я нажимаю кнопку выбора у реквизита с именем "ВидыАнализовВидАнализа"
	Тогда открылось окно 'Виды анализов номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'     |
		| 'Этапы варки сыра' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Виды анализов позиции номенклатуры Сливочный 45% *'
	И в таблице "ВидыАнализов" я нажимаю кнопку выбора у реквизита "Характеристика"
	Тогда открылось окно 'Характеристики номенклатуры'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'  |
		| 'Варка'	  |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Виды анализов позиции номенклатуры Сливочный 45% *'
	И Я закрываю окно 'Виды анализов позиции номенклатуры Сливочный 45% *'
	Тогда открылось окно 'Сливочный 45% (Номенклатура)'
	И Я закрываю окно 'Сливочный 45% (Номенклатура)'
	Тогда открылось окно 'Номенклатура'
	И я закрыл все окна клиентского приложения