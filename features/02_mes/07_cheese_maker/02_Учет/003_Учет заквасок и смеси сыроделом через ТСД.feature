﻿#language: ru

@cheese_maker

Функционал: 07_cheeseMaker.02.003 Учет расхода времени внесения ингредиентов сыроделом через ТСД

Сценарий: 07_cheeseMaker.02.003 Запоминание штрихкода переработки
	Дано Я переключаюсь на открытый клиент "Администратор"
	И В командном интерфейсе я выбираю 'Производство' 'Переработки'
	Тогда открылось окно 'Переработки'
	И в таблице "Список" я перехожу к строке:
		| 'Вид операции' | 'Дата смены' | 'Номер'       | 'Ответственный'  | 'Рабочий центр' | 'Смена'   | 'Способ создания'      | 'Статус' |
		| 'Переработка'  | '12.03.2019' | '0000-000001' | 'Мастер сырцеха' | 'Сырцех'        | 'Смена24' | 'Наборка по рецептуре' | 'Начат'  |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно "Переработка * от *"
	И я сохраняю навигационную ссылку текущего окна в переменную "СсылкаНаПереработку"
	И я запоминаю значение выражения 'Прав($СсылкаНаПереработку$, 8) +"-" + Сред($СсылкаНаПереработку$, 59 ,4) + "-" + Сред($СсылкаНаПереработку$, 55 ,4) + "-" + Сред($СсылкаНаПереработку$, 39 ,4) + "-" + Сред($СсылкаНаПереработку$, 43 ,12)' в переменную "$$ШтрихкодПереработки$$"
	И Я закрываю окно 'Переработка * от *'
	Тогда открылось окно 'Переработки'
	И Я закрываю окно 'Переработки'

Сценарий: 07_cheeseMaker.02.003 Создание кнопки учета операции
	Дано Я подключаюсь под "Сыродел ТСД"
	Когда я нажимаю на кнопку 'Настроить'
	Когда открылось окно 'Рабочий стол (Разделы панели пользователя)'
	И в таблице "СоставКоманд" я нажимаю на кнопку с именем 'СоставКомандДобавить'
	И в таблице "СоставКоманд" я нажимаю кнопку выбора у реквизита "Кнопка"
	Тогда открылось окно 'Кнопки панели пользователя'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Кнопка панели пользователя (создание)'
	И в поле 'Заголовок' я ввожу текст 'Варки сыра'
	И я нажимаю кнопку выбора у поля "Форма"
	Тогда открылось окно 'Выбор формы'
	И в таблице "ДеревоОбъектов" я разворачиваю строку:
		| 'Синоним объекта' |
		| 'Обработки'       |
	И в таблице "ДеревоОбъектов" я перехожу к строке:
		| 'Синоним объекта'    |
		| 'Меню учетных точек' |
	И в таблице "ДеревоОбъектов" я выбираю текущую строку
	Тогда открылось окно 'Кнопка панели пользователя (создание) *'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Кнопка панели пользователя (создание) *' в течение 20 секунд
	Тогда открылось окно 'Кнопки панели пользователя'
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Рабочий стол (Разделы панели пользователя) *'
	И в таблице "СоставКоманд" в поле 'Строка' я ввожу текст '1'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Рабочий стол (Разделы панели пользователя) *' в течение 20 секунд
	И я нажимаю на кнопку 'Открыть новое окно'

Сценарий: 07_cheeseMaker.02.003 Учет расхода времени внесения ингредиентов сыроделом через ТСД только ингредиентов
	Когда я нажимаю на кнопку 'Варки сыра'
	Тогда открылось окно '[Сыродел ТСД]: Меню учетных точек'
	И в поле с именем 'Дата' я ввожу текст '12.03.2019'
	И я нажимаю кнопку выбора у поля "Смена"
	Тогда открылось окно 'Смены'
	И я нажимаю на кнопку с именем 'Выбрать'
	Тогда открылось окно '[Сыродел ТСД]: Меню учетных точек'
	И я нажимаю кнопку выбора у поля "УчетнаяТочка"
	Тогда открылось окно 'Учетные точки'
	И я нажимаю на кнопку с именем 'Выбрать'
	Тогда открылось окно '[Сыродел ТСД]: Меню учетных точек'
	И я нажимаю на кнопку 'Учет только ингредиентов'
	Тогда открылось окно 'Учет только ингредиентов (12.03.19 Смена24) [Сыродел ТСД]: Работа с заданиями'
	И в таблице "Задания" я перехожу к строке:
		| 'Партия'                               | 'Период варки'                    |
		| 'Сливочный 45% / Варка / 12.03.2019/4' | 'с <не указано>\nпо <не указано>' |
	И я нажимаю на кнопку 'Ингредиенты'
	И я нажимаю на кнопку 'КомандаОК'
	Тогда открылось окно 'Учет только ингредиентов (12.03.19 Смена24) [Сыродел ТСД]: Работа с заданиями'
	И в таблице "Задания" я перехожу к строке:
		| 'Партия'                               | 'Период варки'                    |
		| 'Сливочный 45% / Варка / 12.03.2019/1' | 'с <не указано>\nпо <не указано>' |
	И я нажимаю на кнопку 'Начать варку'
	И я нажимаю на кнопку 'Ингредиенты'
	Тогда открылось окно 'Сливочный 45% / Варка / 12.03.2019/1 [950 кг]: Учет расхода ингредиентов'
	И я запоминаю значение выражения 'Строка($$ШтрихкодПереработки$$) + ";1"' в переменную "ШтрихкодПервыйПакет"
	И в поле с именем "ШтрихКод" я ввожу текст "$ШтрихкодПервыйПакет$"
	И я перехожу к следующему реквизиту
	Тогда таблица "РаспределениеМатериалов" стала равной по шаблону:
		| 'Пакет' | 'Номенклатура'                     | 'Дата расхода'   |
		| '1'     | 'Ароматизатор Масло 16256 (масло)' | '* *' 		  |
		| '2'     | 'Закваска Ceska L 100'             | ''               |
		| '2'     | 'Краситель Бета-каротин FS 30%'    | ''               |
	И я запоминаю значение выражения 'Строка($$ШтрихкодПереработки$$) + ";2"' в переменную "ШтрихкодВторойПакет"
	И в поле с именем "ШтрихКод" я ввожу текст "$ШтрихкодВторойПакет$"
	И я перехожу к следующему реквизиту
	Тогда таблица "РаспределениеМатериалов" стала равной по шаблону:
		| 'Пакет' | 'Номенклатура'                     | 'Дата расхода'   |
		| '1'     | 'Ароматизатор Масло 16256 (масло)' | '* *' 		  |
		| '2'     | 'Закваска Ceska L 100'             | '* *'            |
		| '2'     | 'Краситель Бета-каротин FS 30%'    | '* *'            |
	И я нажимаю на кнопку 'КомандаСохранить'
	Тогда открылось окно 'Учет только ингредиентов (12.03.19 Смена24) [Сыродел ТСД]: Работа с заданиями'
	И в таблице "Задания" я перехожу к строке по шаблону:
		| 'Партия'                               | 'Период варки'             |
		| 'Сливочный 45% / Варка / 12.03.2019/1' | 'с * *:*\nпо <не указано>' |
	И я нажимаю на кнопку 'Закончить варку'
	И я нажимаю на кнопку 'Закрыть'
	И я закрыл все окна клиентского приложения

Сценарий: 07_cheeseMaker.02.003 Учет расхода времени внесения ингредиентов сыроделом через ТСД смесь без выпуска и ингредиенты
	И я нажимаю на кнопку 'Открыть новое окно'
	Когда я нажимаю на кнопку 'Варки сыра'
	Тогда открылось окно '[Сыродел ТСД]: Меню учетных точек'
	И в поле с именем 'Дата' я ввожу текст '12.03.2019'
	И я нажимаю кнопку выбора у поля "Смена"
	Тогда открылось окно 'Смены'
	И я нажимаю на кнопку с именем 'Выбрать'
	Тогда открылось окно '[Сыродел ТСД]: Меню учетных точек'
	И я нажимаю кнопку выбора у поля "УчетнаяТочка"
	Тогда открылось окно 'Учетные точки'
	И я нажимаю на кнопку с именем 'Выбрать'
	Тогда открылось окно '[Сыродел ТСД]: Меню учетных точек'
	И я нажимаю на кнопку 'Учет вместе со смесью, без выпуска'
	Тогда открылось окно 'Учет вместе со смесью, без выпуска (12.03.19 Смена24) [Сыродел ТСД]: Работа с заданиями'
	И в таблице "Задания" я перехожу к строке:
		| 'Партия'                                | 'Период варки'                    |
		| 'Российский 50% / Варка / 12.03.2019/2' | 'с <не указано>\nпо <не указано>' |
	И я нажимаю на кнопку 'Начать варку'
	И я нажимаю на кнопку 'Ингредиенты'
	Тогда открылось окно 'Российский 50% / Варка / 12.03.2019/2 [950 кг]: Учет расхода ингредиентов'
	И я нажимаю на кнопку 'Указать смесь'
	И     элемент формы с именем "НоменклатураМатериалОсновной" стал равен 'Смесь 50%'
	И     элемент формы с именем "СерияМатериалОсновной" стал равен '2 до 12.03.2019 0:00:00'
	И     у элемента формы с именем "КоличествоМатериалОсновной" текст редактирования стал равен '0,000'
	И     элемент формы с именем "МатериалОсновнойЕдиницаИзмерения" стал равен 'кг'
	И я нажимаю кнопку выбора у поля "СкладМатериалОсновной"
	Тогда открылось окно 'Склады'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Сырцех'       |
	И я нажимаю на кнопку 'ФормаВыбрать'
	Тогда открылось окно 'Российский 50% / Варка / 12.03.2019/2 [950 кг]: Учет расхода ингредиентов *'
	И в поле 'КоличествоМатериалОсновной' я ввожу текст '9 820,000'
	И я нажимаю на кнопку 'КомандаСохранить'
	И я нажимаю на кнопку 'Указать смесь'
	И     элемент формы с именем "НоменклатураМатериалОсновной" стал равен 'Смесь 50%'
	И     элемент формы с именем "СерияМатериалОсновной" стал равен '2 до 12.03.2019 0:00:00'
	И     элемент формы с именем "СкладМатериалОсновной" стал равен 'Сырцех'
	И     у элемента формы с именем "КоличествоМатериалОсновной" текст редактирования стал равен '9 820,000'
	И я нажимаю на кнопку 'Ингредиенты'
	И в таблице "РаспределениеМатериалов" я перехожу к строке:
		| 'Номенклатура'                     			  | 'Пакет' |
		| 'БАКТЕРИАЛЬНЫЙ КОНЦЕНТРАТ БК-УГЛИЧ-№4 производственная' | ''      |
	И в таблице "РаспределениеМатериалов" в поле 'Дата расхода' я ввожу текущую дату
	И я нажимаю на кнопку 'КомандаСохранить'
	Тогда открылось окно 'Учет вместе со смесью, без выпуска (12.03.19 Смена24) [Сыродел ТСД]: Работа с заданиями'
	И в таблице "Задания" я перехожу к строке по шаблону:
		| 'Партия'                                | 'Период варки'             |
		| 'Российский 50% / Варка / 12.03.2019/2' | 'с * *:*\nпо <не указано>' |
	И я нажимаю на кнопку 'Закончить варку'
	И я нажимаю на кнопку 'Закрыть'
	И я закрыл все окна клиентского приложения

Сценарий: 07_cheeseMaker.02.003 Проверка открытия смеси, если не указаны ингредиенты
	И я нажимаю на кнопку 'Открыть новое окно'
	Когда я нажимаю на кнопку 'Варки сыра'
	Тогда открылось окно '[Сыродел ТСД]: Меню учетных точек'
	И в поле с именем 'Дата' я ввожу текст '12.03.2019'
	И я нажимаю кнопку выбора у поля "Смена"
	Тогда открылось окно 'Смены'
	И я нажимаю на кнопку с именем 'Выбрать'
	Тогда открылось окно '[Сыродел ТСД]: Меню учетных точек'
	И я нажимаю кнопку выбора у поля "УчетнаяТочка"
	Тогда открылось окно 'Учетные точки'
	И я нажимаю на кнопку с именем 'Выбрать'
	Тогда открылось окно '[Сыродел ТСД]: Меню учетных точек'
	И я нажимаю на кнопку 'Учет вместе со смесью, без выпуска'
	Тогда открылось окно 'Учет вместе со смесью, без выпуска (12.03.19 Смена24) [Сыродел ТСД]: Работа с заданиями'
	И в таблице "Задания" я перехожу к строке:
		| 'Партия'                               | 'Период варки'                    |
		| 'Сливочный 45% / Варка / 12.03.2019/4' | 'с <не указано>\nпо <не указано>' |
	И я нажимаю на кнопку 'Ингредиенты'
	Тогда в таблице "РаспределениеМатериалов" количество строк "равно" 0
	И     элемент формы с именем "НоменклатураМатериалОсновной" стал равен 'Смесь 45%'
	И     элемент формы с именем "СерияМатериалОсновной" стал равен '4 до 12.03.2019 0:00:00'
	И     у элемента формы с именем "КоличествоМатериалОсновной" текст редактирования стал равен '0,000'
	И я закрыл все окна клиентского приложения

Сценарий: 07_cheeseMaker.02.003 Учет расхода времени внесения ингредиентов сыроделом через ТСД смесь с выпуском и ингредиенты
	И я нажимаю на кнопку 'Открыть новое окно'
	Когда я нажимаю на кнопку 'Варки сыра'
	Тогда открылось окно '[Сыродел ТСД]: Меню учетных точек'
	И в поле с именем 'Дата' я ввожу текст '12.03.2019'
	И я нажимаю кнопку выбора у поля "Смена"
	Тогда открылось окно 'Смены'
	И я нажимаю на кнопку с именем 'Выбрать'
	Тогда открылось окно '[Сыродел ТСД]: Меню учетных точек'
	И я нажимаю кнопку выбора у поля "УчетнаяТочка"
	Тогда открылось окно 'Учетные точки'
	И я нажимаю на кнопку с именем 'Выбрать'
	Тогда открылось окно '[Сыродел ТСД]: Меню учетных точек'
	И я нажимаю на кнопку 'Учет со смесью с выпуском'
	Тогда открылось окно 'Учет со смесью с выпуском (12.03.19 Смена24) [Сыродел ТСД]: Работа с заданиями'
	И в таблице "Задания" я перехожу к строке:
		| 'Партия'                               | 'Период варки'                    |
		| 'Сливочный 45% / Варка / 12.03.2019/3' | 'с <не указано>\nпо <не указано>' |
	И я нажимаю на кнопку 'Начать варку'
	И я нажимаю на кнопку 'Ингредиенты'
	Тогда открылось окно 'Сливочный 45% / Варка / 12.03.2019/3 [950 кг]: Учет расхода ингредиентов'
	И я нажимаю на кнопку 'Указать смесь'
	И     элемент формы с именем "НоменклатураМатериалОсновной" стал равен 'Смесь 45%'
	И     элемент формы с именем "СерияМатериалОсновной" стал равен '3 до 12.03.2019 0:00:00'
	И     у элемента формы с именем "КоличествоМатериалОсновной" текст редактирования стал равен '0,000'
	И я нажимаю кнопку выбора у поля "СкладМатериалОсновной"
	Тогда открылось окно 'Склады'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Сырцех'       |
	И я нажимаю на кнопку 'ФормаВыбрать'
	Тогда открылось окно 'Сливочный 45% / Варка / 12.03.2019/3 [950 кг]: Учет расхода ингредиентов *'
	И в поле 'КоличествоМатериалОсновной' я ввожу текст '9 850,000'
	И я нажимаю на кнопку 'КомандаСохранить'
	И в таблице "РаспределениеМатериалов" я перехожу к строке:
		| 'Номенклатура'                     | 'Пакет' |
		| 'Ароматизатор Масло 16256 (масло)' | '1'     |
	И в таблице "РаспределениеМатериалов" в поле 'Дата расхода' я ввожу текущую дату
	И в таблице "РаспределениеМатериалов" я перехожу к строке:
		| 'Номенклатура'         | 'Пакет' |
		| 'Закваска Ceska L 100' | '2'     |
	И в таблице "РаспределениеМатериалов" в поле 'Дата расхода' я ввожу текущую дату
	И в таблице "РаспределениеМатериалов" я перехожу к строке:
		| 'Номенклатура'                  | 'Пакет' |
		| 'Краситель Бета-каротин FS 30%' | '2'     |
	И в таблице "РаспределениеМатериалов" в поле 'Дата расхода' я ввожу текущую дату
	И я нажимаю на кнопку 'КомандаСохранить'
	Тогда открылось окно 'Учет со смесью с выпуском (12.03.19 Смена24) [Сыродел ТСД]: Работа с заданиями'
	И в таблице "Задания" я перехожу к строке по шаблону:
		| 'Партия'                               | 'Период варки'             |
		| 'Сливочный 45% / Варка / 12.03.2019/3' | 'с * *:*\nпо <не указано>' |
	И я нажимаю на кнопку 'Закончить варку'
	И я нажимаю на кнопку 'Закрыть'
	И я закрыл все окна клиентского приложения

Сценарий: 07_cheeseMaker.02.003 Учет параметров через ТСД по 1 варке сыра
	И я нажимаю на кнопку 'Открыть новое окно'
	Когда я нажимаю на кнопку 'Варки сыра'
	Тогда открылось окно '[Сыродел ТСД]: Меню учетных точек'
	И в поле с именем 'Дата' я ввожу текст '12.03.2019'
	И я нажимаю кнопку выбора у поля "Смена"
	Тогда открылось окно 'Смены'
	И я нажимаю на кнопку с именем 'Выбрать'
	Тогда открылось окно '[Сыродел ТСД]: Меню учетных точек'
	И я нажимаю кнопку выбора у поля "УчетнаяТочка"
	Тогда открылось окно 'Учетные точки'
	И я нажимаю на кнопку с именем 'Выбрать'
	Тогда открылось окно '[Сыродел ТСД]: Меню учетных точек'
	И я нажимаю на кнопку 'Этапы варки'
	Когда открылось окно 'Этапы варки (12.03.19 Смена24) [Сыродел ТСД]: Работа с заданиями'
	И в таблице "Задания" я перехожу к строке по шаблону:
		| 'Партия'                               | 'Период варки'  |
		| 'Сливочный 45% / Варка / 12.03.2019/1' | 'с * *\nпо * *' |
	И я нажимаю на кнопку 'Параметры т/п'
	Тогда открылось окно 'Сливочный 45% / Варка / 12.03.2019/1 [Этапы варки сыра]: Учет лабораторных анализов'
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Показатель'           |
		| 'Время наливки молока' |
	И в таблице "РезультатыАнализов" в поле 'Значение' я ввожу текст '30,000'
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Показатель'        |
		| 'Время прессования' |
	И в таблице "РезультатыАнализов" в поле 'Значение' я ввожу текст '12,000'
	И в таблице "РезультатыАнализов" я завершаю редактирование строки
	И я нажимаю на кнопку 'КомандаСохранить'
	Тогда открылось окно 'Этапы варки (12.03.19 Смена24) [Сыродел ТСД]: Работа с заданиями'
	И я нажимаю на кнопку 'Закрыть'
	И я закрыл все окна клиентского приложения

Сценарий: 07_cheeseMaker.02.003 Учет параметров через ТСД по 2 варке сыра
	И я нажимаю на кнопку 'Открыть новое окно'
	Когда я нажимаю на кнопку 'Варки сыра'
	Тогда открылось окно '[Сыродел ТСД]: Меню учетных точек'
	И в поле с именем 'Дата' я ввожу текст '12.03.2019'
	И я нажимаю кнопку выбора у поля "Смена"
	Тогда открылось окно 'Смены'
	И я нажимаю на кнопку с именем 'Выбрать'
	Тогда открылось окно '[Сыродел ТСД]: Меню учетных точек'
	И я нажимаю кнопку выбора у поля "УчетнаяТочка"
	Тогда открылось окно 'Учетные точки'
	И я нажимаю на кнопку с именем 'Выбрать'
	Тогда открылось окно '[Сыродел ТСД]: Меню учетных точек'
	И я нажимаю на кнопку 'Этапы варки'
	Когда открылось окно 'Этапы варки (12.03.19 Смена24) [Сыродел ТСД]: Работа с заданиями'
	И в таблице "Задания" я перехожу к строке по шаблону:
		| 'Партия'                                | 'Период варки'  |
		| 'Российский 50% / Варка / 12.03.2019/2' | 'с * *\nпо * *' |
	И я нажимаю на кнопку 'Параметры т/п'
	Тогда открылось окно 'Российский 50% / Варка / 12.03.2019/2 [Этапы варки сыра]: Учет лабораторных анализов'
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Показатель'           |
		| 'Время наливки молока' |
	И в таблице "РезультатыАнализов" в поле 'Значение' я ввожу текст '25,000'
	И в таблице "РезультатыАнализов" я перехожу к строке:
		| 'Показатель'        |
		| 'Время прессования' |
	И в таблице "РезультатыАнализов" в поле 'Значение' я ввожу текст '15,000'
	И я нажимаю на кнопку 'КомандаСохранить'
	Тогда открылось окно 'Этапы варки (12.03.19 Смена24) [Сыродел ТСД]: Работа с заданиями'
	И я нажимаю на кнопку 'Параметры т/п'
	Тогда открылось окно 'Российский 50% / Варка / 12.03.2019/2 [Этапы варки сыра]: Учет лабораторных анализов'
	Тогда таблица "РезультатыАнализов" стала равной:
		| 'Показатель'           | 'Значение' |
		| 'Время наливки молока' | '25,000'   |
		| 'Время прессования'    | '15,000'   |
	И я нажимаю на кнопку 'КомандаЗакрыть'
	И я закрыл все окна клиентского приложения

Сценарий: 07_cheeseMaker.02.003 Закрытие клиента тестирования
	И я закрываю сеанс TESTCLIENT