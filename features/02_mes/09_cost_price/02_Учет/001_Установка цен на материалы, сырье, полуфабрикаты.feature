﻿#language: ru

@cost_price

Функционал: 09_cost_price.02.001 Установка цен на материалы, сырье, полуфабрикаты

Сценарий: 09_cost_price.02.001 Установка цен на сырье
	Дано Я подключаюсь под "Администратор"
	И В командном интерфейсе я выбираю 'Производство' 'История изменения цен'
	Тогда открылось окно 'История изменения цен'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Установка цен (создание)'
	И в поле с именем 'Дата' я ввожу текст '01.01.2018'
	И в таблице "ВидыЦен" я нажимаю на кнопку 'Добавить'
	И в таблице "ВидыЦен" я нажимаю кнопку выбора у реквизита "Вид цен"
	Тогда открылось окно 'Виды цен'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'  |
		| 'Себестоимость' |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Установка цен (создание) *'
	И в таблице "ВидыЦен" я завершаю редактирование строки
	И я нажимаю на кнопку 'Перейти к установке цен'
	И в таблице "ДеревоЦен" я нажимаю на кнопку 'Добавить'
	Тогда открылось окно 'Выберите номенклатуру'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Молоко'       |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Установка цен (создание) *'
	И в таблице "ДеревоЦен" я перехожу к строке:
		| 'Номенклатура/характеристика' |
		| 'Молоко'                      |
	И в таблице "ДеревоЦен" я активизирую поле "Ед."
	И в таблице "ДеревоЦен" я активизирую поле "Новая цена"
	И в таблице "ДеревоЦен" я выбираю текущую строку
	И в таблице "ДеревоЦен" в поле 'Новая цена' я ввожу текст '25,00'
	И в таблице "ДеревоЦен" я нажимаю на кнопку 'Добавить'
	Тогда открылось окно 'Выберите номенклатуру'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' | 'Наименование для печати' |
		| 'Обрат сырье'  | 'Обрат сырье'             |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Установка цен (создание) *'
	И в таблице "ДеревоЦен" я перехожу к строке:
		| 'Номенклатура/Характеристика' |
		| 'Обрат сырье'                 |
	И в таблице "ДеревоЦен" в поле 'Новая цена' я ввожу текст '12,00'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Установка цен (создание) *' в течение 20 секунд
	Тогда открылось окно 'История изменения цен'
	И Я закрываю окно 'История изменения цен'

Сценарий: 09_cost_price.02.001 Установка цен на материалы
	И В командном интерфейсе я выбираю 'Производство' 'История изменения цен'
	Тогда открылось окно 'История изменения цен'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Установка цен (создание)'
	И в поле 'от' я ввожу текст '01.01.2018'
	И в таблице "ВидыЦен" я нажимаю на кнопку 'Добавить'
	И в таблице "ВидыЦен" я нажимаю кнопку выбора у реквизита "Вид цен"
	Тогда открылось окно 'Виды цен'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'  |
		| 'Себестоимость' |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Установка цен (создание) *'
	И в таблице "ВидыЦен" я завершаю редактирование строки
	И я нажимаю на кнопку 'Перейти к установке цен'
	И в таблице "ДеревоЦен" я нажимаю на кнопку 'Добавить'
	Тогда открылось окно 'Выберите номенклатуру'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Пакет '       |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Установка цен (создание) *'
	И в таблице "ДеревоЦен" я нажимаю на кнопку 'Добавить'
	Тогда открылось окно 'Выберите номенклатуру'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Пакет 2'      |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Установка цен (создание) *'
	И в таблице "ДеревоЦен" я перехожу к строке:
		| 'Номенклатура/характеристика' |
		| 'Пакет '                      |
	И в таблице "ДеревоЦен" я активизирую поле "Новая цена"
	И в таблице "ДеревоЦен" я выбираю текущую строку
	И в таблице "ДеревоЦен" в поле 'Новая цена' я ввожу текст '29,00'
	И в таблице "ДеревоЦен" я завершаю редактирование строки
	И в таблице "ДеревоЦен" я перехожу к строке:
		| 'Номенклатура/характеристика' |
		| 'Пакет 2'                     |
	И в таблице "ДеревоЦен" я выбираю текущую строку
	И в таблице "ДеревоЦен" в поле 'Новая цена' я ввожу текст '15,00'
	И в таблице "ДеревоЦен" я завершаю редактирование строки
	И в таблице "ДеревоЦен" я нажимаю на кнопку 'Добавить'
	Тогда открылось окно 'Выберите номенклатуру'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Короб'        |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Установка цен (создание) *'
	И в таблице "ДеревоЦен" я перехожу к строке:
		| 'Номенклатура/характеристика' |
		| 'Короб'                       |
	И в таблице "ДеревоЦен" я выбираю текущую строку
	И в таблице "ДеревоЦен" в поле 'Новая цена' я ввожу текст '54,00'
	И в таблице "ДеревоЦен" я завершаю редактирование строки
	И в таблице "ДеревоЦен" я нажимаю на кнопку 'Добавить'
	Тогда открылось окно 'Выберите номенклатуру'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'   |
		| 'Короб 150х200'  |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Установка цен (создание) *'
	И в таблице "ДеревоЦен" я перехожу к строке:
		| 'Номенклатура/характеристика' |
		| 'Короб 150х200'               |
	И в таблице "ДеревоЦен" я выбираю текущую строку
	И в таблице "ДеревоЦен" в поле 'Новая цена' я ввожу текст '52,50'
	И в таблице "ДеревоЦен" я завершаю редактирование строки
	И в таблице "ДеревоЦен" я нажимаю на кнопку 'Добавить'
	Тогда открылось окно 'Выберите номенклатуру'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'         |
		| 'Закваска Ceska L 100' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Установка цен (создание) *'
	И в таблице "ДеревоЦен" я перехожу к строке:
		| 'Номенклатура/характеристика'  |
		| 'Закваска Ceska L 100' |
	И в таблице "ДеревоЦен" в поле 'Новая цена' я ввожу текст '2 000,00'
	И в таблице "ДеревоЦен" я нажимаю на кнопку 'Добавить'
	Тогда открылось окно 'Выберите номенклатуру'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'       | 'Наименование для печати' |
		| 'Ароматизатор 13456' | 'Ароматизатор 13456'      |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Установка цен (создание) *'
	И в таблице "ДеревоЦен" я перехожу к строке:
		| 'Номенклатура/характеристика' |
		| 'Ароматизатор 13456'          |
	И в таблице "ДеревоЦен" в поле 'Новая цена' я ввожу текст '600,00'
	И в таблице "ДеревоЦен" я нажимаю на кнопку 'Добавить'
	Тогда открылось окно 'Выберите номенклатуру'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'                  | 'Наименование для печати'       |
		| 'Краситель Бета-каротин FS 30%' | 'Краситель Бета-каротин FS 30%' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Установка цен (создание) *'
	И в таблице "ДеревоЦен" я перехожу к строке:
		| 'Номенклатура/характеристика'    |
		| 'Краситель Бета-каротин FS 30%'  |
	И в таблице "ДеревоЦен" в поле 'Новая цена' я ввожу текст '250,00'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Установка цен (создание) *' в течение 20 секунд
	Тогда открылось окно 'История изменения цен'
	И Я закрываю окно 'История изменения цен'

Сценарий: 09_cost_price.02.001 Установка стоимости молока для поставщика 1
	И В командном интерфейсе я выбираю 'Производство' 'История изменения цен'
	Тогда открылось окно 'История изменения цен'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Установка цен (создание)'
	И в таблице "ВидыЦен" я нажимаю на кнопку с именем 'ВидыЦенДобавить'
	И в таблице "ВидыЦен" я нажимаю кнопку выбора у реквизита "Вид цен"
	Тогда открылось окно 'Виды цен'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'             |
		| 'Поставщик 1' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Установка цен (создание) *'
	И я нажимаю на кнопку 'Перейти к установке цен'
	И в поле 'от' я ввожу текст '01.01.2018'
	И в таблице "ДеревоЦен" я нажимаю на кнопку с именем 'ДеревоЦенДобавитьНоменклатуру'
	Тогда открылось окно 'Выберите номенклатуру'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'  |
		| 'Молоко' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Установка цен (создание) *'
	И в таблице "ДеревоЦен" я перехожу к строке:
		| 'Номенклатура/характеристика' |
		| 'Молоко'               |
	И в таблице "ДеревоЦен" в поле 'Новая цена' я ввожу текст '24,00'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Установка цен (создание) *' в течение 20 секунд
	Тогда открылось окно 'История изменения цен'
	И Я закрываю окно 'История изменения цен'

Сценарий: 09_cost_price.02.001 Установка фикс цены на ПФ
	И В командном интерфейсе я выбираю 'Производство' 'История изменения цен'
	Тогда открылось окно 'История изменения цен'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Установка цен (создание)'
	И в таблице "ВидыЦен" я нажимаю на кнопку с именем 'ВидыЦенДобавить'
	И в таблице "ВидыЦен" я нажимаю кнопку выбора у реквизита "Вид цен"
	Тогда открылось окно 'Виды цен'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'  |
		| 'Себестоимость' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Установка цен (создание) *'
	И я нажимаю на кнопку 'Перейти к установке цен'
	И в поле 'от' я ввожу текст '01.04.2021'
	И в таблице "ДеревоЦен" я нажимаю на кнопку с именем 'ДеревоЦенДобавитьНоменклатуру'
	Тогда открылось окно 'Выберите номенклатуру'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Сыворотка'    |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Установка цен (создание) *'
	И в таблице "ДеревоЦен" я перехожу к строке:
		| 'Номенклатура/характеристика' |
		| 'Сыворотка'                   |
	И в таблице "ДеревоЦен" в поле 'Новая цена' я ввожу текст '15,00'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Установка цен (создание) *' в течение 20 секунд
	Тогда открылось окно 'История изменения цен'
	И Я закрываю окно 'История изменения цен'