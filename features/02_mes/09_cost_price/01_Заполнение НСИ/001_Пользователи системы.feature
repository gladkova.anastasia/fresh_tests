﻿#language: ru

@cost_price

Функционал: 09_cost_price.01.001 Настройка пользователей и прав

Сценарий: 09_cost_price.01.001 Создание профиля для ответственного за расчет себестоимости
	Дано Я подключаюсь под "Администратор"
	И В командном интерфейсе я выбираю 'Администрирование' 'Настройки пользователей и прав'
	Тогда открылось окно 'Настройки пользователей и прав'
	И я нажимаю на кнопку 'Профили групп доступа'
	Тогда открылось окно 'Профили групп доступа'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Профиль групп доступа (создание)'
	И в поле 'Наименование' я ввожу текст 'Расчет себестоимости'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'                                 |
		| 'Нет'     | 'Добавление изменение документ расчет сырьевой себестоимости' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'                        |
		| 'Нет'     | 'Добавление изменение документ плановая калькуляция' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'                            |
		| 'Нет'     | 'Добавление изменение справочник сырьевые статьи затрат' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'                              |
		| 'Нет'     | 'Использование обработки расчет нормативной себестоимости' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'                    |
		| 'Нет'     | 'Использование обработки "Расчет себестоимости"' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'      |
		| 'Нет'     | 'Просмотр отчет цены номенклатуры' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'                    |
		| 'Нет'     | 'Просмотр отчета "Ошибки расчета себестоимости"' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'                            |
		| 'Нет'     | 'Просмотр отчета "Нормативно-фактическая себестоимость"' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'        |
		| 'Нет'     | 'Просмотр отчета движения документа' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)' |
		| 'Нет'     | 'Раздел "Производство"'       |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'                  |
		| 'Нет'     | 'Редактирование порядка расчета себестоимости' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)' |
		| 'Нет'     | 'Чтение документ переработка' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'              |
		| 'Нет'     | 'Чтение документ распределение материалов' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)' |
		| 'Нет'     | 'Чтение справочник валюты'    |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'         |
		| 'Нет'     | 'Чтение справочник виды номенклатуры' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)' |
		| 'Нет'     | 'Чтение справочник виды цен'  |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'    |
		| 'Нет'     | 'Чтение справочник номенклатура' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'                         |
		| 'Нет'     | 'Чтение справочник номенклатура присоединенные файлы' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'   |
		| 'Нет'     | 'Чтение справочник организации' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'      |
		| 'Нет'     | 'Чтение справочник рабочие центры' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'          |
		| 'Нет'     | 'Чтение справочник серии номенклатуры' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)' |
		| 'Нет'     | 'Чтение справочник склады'    |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'        |
		| 'Нет'     | 'Чтение справочник складские ячейки' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)' |
		| 'Нет'     | 'Чтение справочник смены'     |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'                  |
		| 'Нет'     | 'Чтение справочник упаковки единицы измерения' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'                   |
		| 'Нет'     | 'Чтение справочник характеристики номенклатуры' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'          |
		| 'Нет'     | 'Чтение справочник этапы производства' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'               |
		| 'Нет'     | 'Чтение справочника "Виды рабочих центров"' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И в таблице "Роли" я перехожу к строке:
		| 'Пометка' | 'Разрешенное действие (роль)'                 |
		| 'Нет'     | 'Чтение справочника "Ресурсные спецификации"' |
	И в таблице "Роли" я устанавливаю флаг 'Пометка'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Профиль групп доступа (создание) *' в течение 20 секунд
	Тогда открылось окно 'Профили групп доступа'
	И Я закрываю окно 'Профили групп доступа'
	Тогда открылось окно 'Настройки пользователей и прав'
	И Я закрываю окно 'Настройки пользователей и прав'

Сценарий: 09_cost_price.01.001 Создание групп пользователей
	И Я создаю группу доступа "Базовые права" с профилем "Базовый профиль"
	И Я создаю группу доступа "Технологи" с профилем "Технологи"
	И Я создаю группу доступа "Расчет себестоимости" с профилем "Расчет себестоимости"

Сценарий: 09_cost_price.01.001 Создание пользователей
	И Я создаю пользователя "Технолог" с профилями "Базовые права;Технологи"
	И Я создаю пользователя "Ответственный за себестоимость" с профилями "Базовые права;Расчет себестоимости"
