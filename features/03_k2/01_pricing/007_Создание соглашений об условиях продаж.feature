#language: ru

@pricing

Функционал: 007. Pricing. Создание соглашений об условиях продаж

Сценарий: 007.1. Pricing. Создание соглашений об условиях продаж для Обычного покупателя, Ашана по 2 регионам, Перекрестка и 5ки

	Дано Я подключаюсь под "Создание заказов клиентов"
	Дано я устанавливаю текущему сеансу заголовок приложения "01.007. Цены"
	Дано я удаляю объекты "Справочники.к2СоглашенияСКлиентами" без контроля ссылок
	И В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Соглашения с контрагентами'
	Тогда открылось окно 'Соглашения с контрагентами'
	И Я начинаю создавать соглашение "Соглашение для Обычного покупателя" с контрагентом "Обычный покупатель" по организации "Организация"
	И из выпадающего списка "Ответственный" я выбираю по строке 'Администратор'
	И из выпадающего списка "Статус" я выбираю точное значение 'Действует'
	И в поле 'Действует с' я ввожу текст '09.05.2019'
	И в поле 'по' я ввожу текст '09.09.2025  0:00:00'
	И Я устанавливаю вид цены "Зависимый 1", налогообложение "Облагается (с НДС)" и цену на "На дату заказа" в соглашении с контрагетом
	И Я заканчиваю создавать соглашение с контрагентом
	И Я начинаю создавать соглашение "Соглашение для Ашана по региону без скидки" с контрагентом "Ашан" по организации "Организация"
	И из выпадающего списка "Регион" я выбираю по строке 'Бесскидочный регион'
	И из выпадающего списка "Ответственный" я выбираю по строке 'Администратор'
	И из выпадающего списка "Статус" я выбираю точное значение 'Действует'
	И в поле 'Действует с' я ввожу текст '09.05.2019'
	И в поле 'по' я ввожу текст '09.09.2025  0:00:00'
	И Я устанавливаю вид цены "Ашан", налогообложение "Облагается (с НДС)" и цену на "На дату заказа" в соглашении с контрагетом
	И Я заканчиваю создавать соглашение с контрагентом
	И Я начинаю создавать соглашение "Соглашение для Ашана по Скидочному региону" с контрагентом "Ашан" по организации "Организация"
	И из выпадающего списка "Регион" я выбираю по строке 'Скидочный регион'
	И из выпадающего списка "Ответственный" я выбираю по строке 'Администратор'
	И из выпадающего списка "Статус" я выбираю точное значение 'Действует'
	И в поле 'Действует с' я ввожу текст '09.05.2019'
	И в поле 'по' я ввожу текст '09.09.2025  0:00:00'
	И Я устанавливаю вид цены "Ашан", налогообложение "Облагается (с НДС)" и цену на "На дату заказа" в соглашении с контрагетом
	И Я добавляю в уточнении ценовых условий для товарной категории "Мягкие сыры" процент скидки "7,77"
	И Я добавляю в уточнении ценовых условий для товарной категории "Твердые сыры" процент скидки "7,77"
	И Я добавляю в уточнении ценовых условий для товарной категории "Полутвердые сыры" процент скидки "7,77"
	И Я добавляю в уточнении ценовых условий для товарной категории "Мясо птицы" процент скидки "7,77"
	И Я заканчиваю создавать соглашение с контрагентом
	И Я начинаю создавать соглашение "Соглашение для 5ка" с контрагентом "5ка" по организации "Организация"
	И из выпадающего списка "Ответственный" я выбираю по строке 'Администратор'
	И из выпадающего списка "Статус" я выбираю точное значение 'Действует'
	И в поле 'Действует с' я ввожу текст '09.05.2019'
	И в поле 'по' я ввожу текст '09.09.2025  0:00:00'
	И Я устанавливаю вид цены "5ка", налогообложение "Облагается (с НДС)" и цену на "На дату отгрузки" в соглашении с контрагетом
	И Я заканчиваю создавать соглашение с контрагентом
	И Я начинаю создавать соглашение "Соглашение для Перекресток" с контрагентом "Перекресток" по организации "Организация"
	И из выпадающего списка "Ответственный" я выбираю по строке 'Администратор'
	И из выпадающего списка "Статус" я выбираю точное значение 'Действует'
	И в поле 'Действует с' я ввожу текст '09.05.2019'
	И в поле 'по' я ввожу текст '09.09.2025  0:00:00'
	И Я устанавливаю вид цены "Перекресток", налогообложение "Не облагается (без НДС)" и цену на "На дату заказа" в соглашении с контрагетом
	И Я заканчиваю создавать соглашение с контрагентом
	И Я начинаю создавать соглашение "Соглашение для 5ка - цена из заказа" с контрагентом "5ка" по организации "Организация"
	И из выпадающего списка "Ответственный" я выбираю по строке 'Администратор'
	И из выпадающего списка "Статус" я выбираю точное значение 'Действует'
	И в поле 'Действует с' я ввожу текст '02.05.2019'
	И в поле 'по' я ввожу текст '08.05.2019  0:00:00'
	И Я устанавливаю вид цены "Пятерочка", налогообложение "Не облагается (без НДС)" и цену на "Из заказа" в соглашении с контрагетом
	И Я заканчиваю создавать соглашение с контрагентом
	И Я начинаю создавать соглашение "Соглашение с поставщиком - на дату заказа" с контрагентом "Поставщик (проверка цен)" по организации "Организация"
	И из выпадающего списка "Вид соглашения" я выбираю точное значение 'С поставщиком'
	И из выпадающего списка "Ответственный" я выбираю по строке 'Администратор'
	И из выпадающего списка "Статус" я выбираю точное значение 'Действует'
	И в поле 'Действует с' я ввожу текст '01.09.2019'
	И в поле 'по' я ввожу текст '30.09.2019  0:00:00'
	И Я устанавливаю вид цены "Поставщик (проверка цен)", налогообложение "Облагается (с НДС)" и цену на "На дату заказа" в соглашении с контрагетом
	И я перехожу к закладке "Условия поставки"
	И из выпадающего списка "Способ учета отклонений при приемке" я выбираю точное значение 'Формировать акт о расхождениях'
	И Я заканчиваю создавать соглашение с контрагентом
	И Я начинаю создавать соглашение "Соглашение с поставщиком - на дату отгрузки" с контрагентом "Поставщик (проверка цен)" по организации "Организация"
	И из выпадающего списка "Вид соглашения" я выбираю точное значение 'С поставщиком'
	И из выпадающего списка "Ответственный" я выбираю по строке 'Администратор'
	И из выпадающего списка "Статус" я выбираю точное значение 'Действует'
	И в поле 'Действует с' я ввожу текст '01.10.2019'
	И в поле 'по' я ввожу текст '30.10.2019  0:00:00'
	И Я устанавливаю вид цены "Поставщик (проверка цен)", налогообложение "Облагается (с НДС)" и цену на "На дату отгрузки" в соглашении с контрагетом
	И я перехожу к закладке "Условия поставки"
	И из выпадающего списка "Способ учета отклонений при приемке" я выбираю точное значение 'Формировать акт о расхождениях'
	И Я заканчиваю создавать соглашение с контрагентом
	И Я начинаю создавать соглашение "Соглашение с поставщиком - из заказа" с контрагентом "Поставщик (проверка цен)" по организации "Организация"
	И из выпадающего списка "Вид соглашения" я выбираю точное значение 'С поставщиком'
	И из выпадающего списка "Ответственный" я выбираю по строке 'Администратор'
	И из выпадающего списка "Статус" я выбираю точное значение 'Действует'
	И в поле 'Действует с' я ввожу текст '01.08.2019'
	И в поле 'по' я ввожу текст '30.08.2019  0:00:00'
	И Я устанавливаю вид цены "Поставщик (проверка цен)", налогообложение "Облагается (с НДС)" и цену на "Из заказа" в соглашении с контрагетом
	И я перехожу к закладке "Условия поставки"
	И из выпадающего списка "Способ учета отклонений при приемке" я выбираю точное значение 'Формировать акт о расхождениях'
	И Я заканчиваю создавать соглашение с контрагентом
	И Я закрываю окно 'Соглашения с контрагентами'

Сценарий: 007.2. Pricing. Проверка работы формы списка, открытие и закрытие правой панели, установка отборов для соглашений об условиях продаж

	И В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Соглашения с контрагентами'
	Тогда открылось окно 'Соглашения с контрагентами'
	И я нажимаю на кнопку 'свернуть'
	И я нажимаю на гиперссылку "Декорация развернуть панель отборов"
	И из выпадающего списка "ОтборПоСтатусу" я выбираю точное значение 'Действует'
	И из выпадающего списка "ОтборПоКонтрагенту" я выбираю по строке 'Ашан'
	И из выпадающего списка "ОтборПоРегиону" я выбираю по строке 'Бесскидочный регион'
	И Я закрываю окно 'Соглашения с контрагентами'

Сценарий: 007.3. Pricing. Завершение

	И Я закрыл все окна клиентского приложения