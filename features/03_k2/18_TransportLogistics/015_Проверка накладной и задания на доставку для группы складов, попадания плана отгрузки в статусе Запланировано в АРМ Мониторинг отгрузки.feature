
#language: ru

@TransportLogistics

Функционал: 015. Проверка накладной и задания на доставку для группы складов, попадания плана отгрузки в статусе Запланировано в АРМ Мониторинг отгрузки

Сценарий: 015.1. TransportLogistics. Проверка сформировавшихся документов для заданий на доставку для группы складов

	Дано Я подключаюсь под "Администратор"
	Дано я устанавливаю текущему сеансу заголовок приложения "18.015. Транспортная логистика"
	И В командном интерфейсе я выбираю 'Склад и доставка' 'Задания на доставку'
	Тогда открылось окно 'Задания на доставку'
	И в таблице "Список" я перехожу к строке:
		| '(План) Начало рейса ' | 'Водитель'             | 'ТС'            |
		| '07.04.2020 0:00:00'   | 'Водитель Петров Петр' | 'А524НВ Газель' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Задание на доставку * от *'
	И элемент формы "Склад" стал равен "Группа складов для отгрузки"
	И Я закрываю окно 'Задание на доставку * от *'
	Тогда открылось окно 'Задания на доставку'
	И Я закрываю окно 'Задания на доставку'
	И В командном интерфейсе я выбираю 'Заказы' 'Реализации товаров'
	Тогда открылось окно 'Реализации товаров'
	И я нажимаю кнопку выбора у поля "Задание на доставку"
	Тогда открылось окно 'Задания на доставку'
	И в таблице "Список" я перехожу к строке:
		| '(План) Начало рейса ' |
		| '07.04.2020 0:00:00'   |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Реализации товаров'
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Реализация товаров * от *'
	И я перехожу к закладке "Товары"
	И в таблице "Товары" поле "Склад" имеет значение "Куриная ГП"
	И Я закрываю окно 'Реализация товаров * от *'
	Тогда открылось окно 'Реализации товаров'
	И в таблице "Список" я перехожу на одну строку вверх
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Реализация товаров * от *'
	И я перехожу к закладке "Товары"
	И в таблице "Товары" поле "Склад" имеет значение "Сырная ГП"
	И Я закрываю окно 'Реализация товаров * от *'
	Тогда открылось окно 'Реализации товаров'
	И Я закрываю окно 'Реализации товаров'

Сценарий: 015.2. TransportLogistics. Проверка попадания планов отгрузки в статусе запланировано в АРМ Мониторинг отгрузки

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Мониторинг отгрузки'
	Тогда открылось окно 'Мониторинг отгрузки'
	И я разворачиваю группу "Отборы"
	И я перехожу к закладке "Отборы"
	И я нажимаю кнопку выбора у поля "Период"
	Тогда открылось окно 'Выберите период'
	И в поле 'DateBegin' я ввожу текст '01.05.2020'
	И в поле 'DateEnd' я ввожу текст '31.05.2020'
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Мониторинг отгрузки'
	И из выпадающего списка "Склад" я выбираю по строке 'Сырная ГП'
	И я перехожу к следующему реквизиту
	И я нажимаю на кнопку 'Обновить дерево документов'
	И в таблице "ДеревоДокументовОтгрузки" я разворачиваю строку:
		| 'Транспорт/Распоряжение' |
		| 'А524НВ Газель'          |
	И в таблице "ДеревоДокументовОтгрузки" я перехожу к строке:
		| 'План'   | 'Статус'        |
		| '10,000' | 'Запланировано' |
	И Я закрываю окно 'Мониторинг отгрузки'

Сценарий: 015.3. TransportLogistics. Завершение

	И Я закрыл все окна клиентского приложения