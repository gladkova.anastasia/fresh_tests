
#language: ru

@TransportLogistics

Функционал: 012. TransportLogistics. Проверка актуализации заданий на доставку

Сценарий: 012.1. TransportLogistics. Изменение плана отгрузки для проверки актуализации заданий на доставку

	Дано Я подключаюсь под "Проверка актуализации заданий на доставку"
	Дано я устанавливаю текущему сеансу заголовок приложения "18.012. Транспортная логистика"
	И В командном интерфейсе я выбираю 'Склад и доставка' 'Планы отгрузки'
	Тогда открылось окно 'Планы отгрузки'
	И в таблице "Список" я перехожу к строке:
		| 'Дата отгрузки' | 'Склад'     | 'Статус'        |
		| '07.04.2020'    | 'Сырная ГП' | 'Запланировано' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'План отгрузки * от *'
	И я перехожу к закладке "Товары"
	И в таблице "Товары" я выбираю текущую строку
	И в таблице "Товары" в поле 'Количество' я ввожу текст '15,000'
	И в таблице "Товары" я завершаю редактирование строки
	И в поле 'Комментарий' я ввожу текст 'План отгрузки для актуализации заданий на доставку'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'План отгрузки * от * *' в течение 20 секунд
	Тогда открылось окно 'Планы отгрузки'
	И Я закрываю окно 'Планы отгрузки'

Сценарий: 012.2. TransportLogistics. Задание на доставку для группы складов, проверка актуализации

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Рабочее место менеджера по доставке'
	Тогда открылось окно 'Формирование заданий на доставку на *'
	И из выпадающего списка с именем "Склад" я выбираю по строке 'Группа складов для отгрузки'
	И в поле 'Распоряжения на дату' я ввожу текст '07.04.20'
	И в таблице "РаспоряженияНаДоставку" я перехожу к строке:
		| 'Адрес'                                                                                | 'Вес, кг' | 'Мест'   | 'Получатель'             |
		| 'Фактическая Нижегородская область, город Нижний Новгород, проспект Гагарина, дом 223' | '51,25'   | '28,777' | 'Ашан. Скидочный регион' |
	И в таблице "ЗаданияНаДоставкуПланируемые" я нажимаю на кнопку 'Актуализировать'
	Тогда таблица "ЗаданияНаДоставкуПланируемые" стала равной по шаблону:
		| 'Транспорт / Адрес / План отгрузки'                                                    | 'Время отправления' | 'Склад'                       | 'Вес, кг' | 'Мест'    |
		| '*, А524НВ Газель, Водитель Петров Петр'                                               | '00:00'             | 'Группа складов для отгрузки' | '205'     | '115,108' |
		| 'Фактическая Нижегородская область, город Нижний Новгород, проспект Гагарина, дом 223' | ''                  | 'Группа складов для отгрузки' | '205'     | '115,108' |
		| 'План отгрузки * от *'                                                                 | ''                  | 'Сырная ГП'                   | '153,75'  | '86,331'  |
		| 'План отгрузки * от *'                                                                 | ''                  | 'Куриная ГП'                  | '51,25'   | '28,777'  |
		| '*, А524НВ Газель, Водитель Петров Петр'                                               | '13:00'             | 'Группа складов для отгрузки' | ''        | ''        |
		| '*, А524НВ Газель, Водитель Петров Петр'                                               | '15:00'             | 'Группа складов для отгрузки' | ''        | ''        |
	И Я закрываю окно 'Формирование заданий на доставку на *'

Сценарий: 012.3. TransportLogistics. Изменение плана отгрузки к начальному состоянию

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Планы отгрузки'
	Тогда открылось окно 'Планы отгрузки'
	И в таблице "Список" я перехожу к строке:
		| 'Дата отгрузки' | 'Склад'     | 'Статус'        |
		| '07.04.2020'    | 'Сырная ГП' | 'Запланировано' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'План отгрузки * от *'
	И я перехожу к закладке "Товары"
	И в таблице "Товары" я выбираю текущую строку
	И в таблице "Товары" в поле 'Количество' я ввожу текст '10,000'
	И в таблице "Товары" я завершаю редактирование строки
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'План отгрузки * от * *' в течение 20 секунд
	Тогда открылось окно 'Планы отгрузки'
	И Я закрываю окно 'Планы отгрузки'

Сценарий: 012.4. TransportLogistics. Задание на доставку для группы складов - установка статуса К Погрузке

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Рабочее место менеджера по доставке'
	Тогда открылось окно 'Формирование заданий на доставку на *'
	И из выпадающего списка с именем "Склад" я выбираю по строке 'Группа складов для отгрузки'
	И в поле 'Распоряжения на дату' я ввожу текст '07.04.20'
	Тогда таблица "РаспоряженияНаДоставку" стала равной:
		| 'Адрес'                                                                                | 'Получатель'             | 'Мест'    | 'Склад'     | 'Вес, кг' |
		| ''                                                                                     | ''                       | '-28,777' | ''          | '-51,25'  |
		| 'Фактическая Нижегородская область, город Нижний Новгород, проспект Гагарина, дом 223' | 'Ашан. Скидочный регион' | '-28,777' | ''          | '-51,25'  |
		| ''                                                                                     | 'Ашан. Скидочный регион' | '-28,777' | 'Сырная ГП' | '-51,25'  |
	И в таблице "ЗаданияНаДоставкуПланируемые" я перехожу к строке:
		| 'Вес, кг' | 'Мест'    | 'Склад'                       | 'Транспорт / Адрес / План отгрузки'                                                    |
		| '205'     | '115,108' | 'Группа складов для отгрузки' | 'Фактическая Нижегородская область, город Нижний Новгород, проспект Гагарина, дом 223' |
	И в таблице "ЗаданияНаДоставкуПланируемые" я перехожу к строке:
		| 'Вес, кг' | 'Мест'    | 'Склад'                       | 'Транспорт / Адрес / План отгрузки'      |
		| '205'     | '115,108' | 'Группа складов для отгрузки' | '7, А524НВ Газель, Водитель Петров Петр' |
	И я нажимаю на кнопку с именем 'ПеренестиСтрокиВРейс'
	Тогда таблица "ЗаданияНаДоставкуПланируемые" стала равной по шаблону:
		| 'Транспорт / Адрес / План отгрузки'                                                    | 'Время отправления' | 'Склад'                       | 'Вес, кг' | 'Мест'   |
		| '7, А524НВ Газель, Водитель Петров Петр'                                               | '00:00'             | 'Группа складов для отгрузки' | '153,75'  | '86,331' |
		| 'Фактическая Нижегородская область, город Нижний Новгород, проспект Гагарина, дом 223' | ''                  | 'Группа складов для отгрузки' | '153,75'  | '86,331' |
		| 'План отгрузки * от *'                                                                 | ''                  | 'Сырная ГП'                   | '102,5'   | '57,554' |
		| 'План отгрузки * от *'                                                                 | ''                  | 'Куриная ГП'                  | '51,25'   | '28,777' |
		| '8, А524НВ Газель, Водитель Петров Петр'                                               | '13:00'             | 'Группа складов для отгрузки' | ''        | ''       |
		| '9, А524НВ Газель, Водитель Петров Петр'                                               | '15:00'             | 'Группа складов для отгрузки' | ''        | ''       |
	И я перехожу к закладке "Задания на доставку"
	И в таблице "ЗаданияНаДоставкуВРаботе" я перехожу к строке:
		| 'Вес, кг' | 'Водитель'             | 'Мест'   | 'Склад'                       | 'Статус'      | 'Транспорт'     |
		| '153,75'  | 'Водитель Петров Петр' | '86,331' | 'Группа складов для отгрузки' | 'Формируется' | 'А524НВ Газель' |
	И в таблице "ЗаданияНаДоставкуВРаботе" я активизирую поле с именем "ЗаданияНаДоставкуВРаботеСтатус"
	И я нажимаю на кнопку с именем 'УстановитьСтатусКПогрузке'
	И Я закрываю окно 'Формирование заданий на доставку на *'

Сценарий: 012.5. TransportLogistics. Завершение

	И я закрываю сеанс TESTCLIENT