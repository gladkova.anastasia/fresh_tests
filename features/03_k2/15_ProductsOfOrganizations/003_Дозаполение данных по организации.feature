#language: ru

@ProductsOfOrganizations

Функционал: 003. ProductsOfOrganizations. Дозаполение данных по организации

Сценарий: 003.1. ProductsOfOrganizations. Создание валюты

	Дано Я подключаюсь под "Ведение организаций"
	Дано я устанавливаю текущему сеансу заголовок приложения "15.003. Товары организаций"
	И Я создаю валюту RUB

Сценарий: 003.2. ProductsOfOrganizations. Создание физических лиц

	Дано я удаляю объекты "Справочники.к2ФизическиеЛица" без контроля ссылок
	Когда В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Физические лица'
	Тогда я жду открытия окна 'Физические лица' в течение 20 секунд
	И Я создаю физическое лицо "Главный Руководитель Компании" с ФИО "Главный Руководитель Компании" и инициалами "Главный Р. К."
	И Я создаю физическое лицо "Главный Бухгалтер Компании" с ФИО "Главный Бухгалтер Компании" и инициалами "Главный Б. К."
	И Я создаю физическое лицо "Ответственный за склад Петров Петр" с ФИО "Петров Петр" и инициалами "Петров П."
	И Я создаю физическое лицо "Главный Руководитель Организации2" с ФИО "Главный Руководитель Организации2" и инициалами "Главный Р. О."
	И Я создаю физическое лицо "Главный Бухгалтер Организации2" с ФИО "Главный Бухгалтер Организации2" и инициалами "Главный Б. О."
	И я закрываю окно 'Физические лица'

Сценарий: 003.3. ProductsOfOrganizations. Заполнение ответсвенного за склад, графика доставки, условия приема заказов в складе
	
	Когда В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Склады'
	Тогда я жду открытия окна 'Склады' в течение 20 секунд
	* Заполнение ответсвенного для склада Сырная ГП 
		И в таблице "Список" я перехожу к строке:
			| 'Наименование'                |
			| 'Группа складов для отгрузки' |
		И в таблице  "Список" я перехожу на один уровень вниз
		И в таблице "Список" я перехожу к строке:
			| 'Наименование' |
			| 'Сырная ГП'    |
		И в таблице "Список" я выбираю текущую строку
		Тогда открылось окно 'Сырная ГП (Склад)'
		И из выпадающего списка "Ответственный" я выбираю по строке 'Ответственный за склад Петров Петр'
		И в поле 'Должность' я ввожу текст 'Кладовщик'
		И я нажимаю на кнопку 'Записать и закрыть'
		И я жду закрытия окна 'Сырная ГП (Склад) *' в течение 20 секунд
		Тогда открылось окно 'Склады'
	* Заполнение ответсвенного для склада Куриная ГП 
		И в таблице "Список" я перехожу к строке:
			| 'Наименование' |
			| 'Куриная ГП'   |
		И в таблице "Список" я выбираю текущую строку
		Тогда открылось окно 'Куриная ГП (Склад)'
		И из выпадающего списка "Ответственный" я выбираю по строке 'Ответственный за склад Петров Петр'
		И в поле 'Должность' я ввожу текст 'Кладовщик'
		И я нажимаю на кнопку 'Записать и закрыть'
		И я жду закрытия окна 'Куриная ГП (Склад) *' в течение 20 секунд
		Тогда открылось окно 'Склады'
	И Я закрываю окно 'Склады'
	
Сценарий: 003.4. ProductsOfOrganizations. Заполнение ответственных лиц организации и банковского счета

	Дано я удаляю объекты "Справочники.БанковскиеСчета" без контроля ссылок
	Дано я удаляю объекты "Справочники.к2ОтветственныеЛицаОрганизаций" без контроля ссылок
	Когда В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Организации'
	Тогда я жду открытия окна 'Организации' в течение 20 секунд
	* Заполнение ответственных лиц и банковского счета для организации
		И в таблице "Список" я перехожу к строке:
			| 'Наименование' |
			| 'Организация'  |
		И в таблице "Список" я выбираю текущую строку
		Тогда открылось окно 'Организация (Организация)'
		И я разворачиваю группу с именем "ОтветственныеЛица"
		И я нажимаю на кнопку 'Все лица с правом подписи'
		Тогда открылось окно 'Лицо с правом подписи'
		И Я начинаю создавать ответственное лицо "Главный Руководитель Компании" в должности "Руководитель" с наименованием "Главный Р. К."
		И в поле 'Период действия с' я ввожу текст '01.01.2000'
		И в поле 'Представление должности для печати' я ввожу текст 'Директор'
		И я заканчиваю создавать ответственное лицо
		И Я начинаю создавать ответственное лицо "Главный Бухгалтер Компании" в должности "Главный бухгалтер" с наименованием "Главный Б. К."
		И в поле 'Период действия с' я ввожу текст '01.02.2000'
		И я заканчиваю создавать ответственное лицо
		И Я закрываю окно 'Лицо с правом подписи'
		Тогда открылось окно 'Организация (Организация)'
		И элемент формы "Директор" стал равен "Главный Р. К."
		И элемент формы "Главный бухгалтер" стал равен "Главный Б. К."
		И я нажимаю на кнопку создать поля "Основной банковский счет"
		Тогда открылось окно 'Банковский счет (создание)'
		И в поле с именем 'Наименование' я ввожу текст 'Основной счет'
		И в поле 'Номер счета' я ввожу текст '40702810838110013654'
		И в поле "Валюта" я ввожу текст "RUB"
		И в поле с именем 'БИКБанка' я ввожу текст '044585250'
		И я перехожу к следующему реквизиту
		Тогда открылось окно 'Выбор банка из классификатора'
		И я нажимаю на кнопку 'Продолжить ввод'
		Тогда открылось окно 'Банковский счет (создание) *'
		И Я заканчиваю создавать банковский счет
		Тогда открылось окно 'Организация (Организация) *'
		И я нажимаю на кнопку 'Записать и закрыть'
		И я жду закрытия окна 'Организация (Организация) *' в течение 20 секунд
		Тогда открылось окно 'Организации'
	* Заполнение ответственных лиц и банковского счета для Организации2
		И в таблице "Список" я перехожу к строке:
			| 'Наименование' |
			| 'Организация2' |
		И в таблице "Список" я выбираю текущую строку
		Тогда открылось окно 'Организация2 (Организация)'
		И я разворачиваю группу с именем "ОтветственныеЛица"
		И я нажимаю на кнопку 'Все лица с правом подписи'
		Тогда открылось окно 'Лицо с правом подписи'
		И Я начинаю создавать ответственное лицо "Главный Руководитель Организации2" в должности "Руководитель" с наименованием "Главный Р. О."
		И в поле 'Период действия с' я ввожу текст '01.01.2000'
		И в поле 'Представление должности для печати' я ввожу текст 'Директор'
		И Я заканчиваю создавать ответственное лицо
		И Я начинаю создавать ответственное лицо "Главный Бухгалтер Организации2" в должности "Главный бухгалтер" с наименованием "Главный Б. О."
		И в поле 'Период действия с' я ввожу текст '01.02.2000'
		И Я заканчиваю создавать ответственное лицо
		И Я закрываю окно 'Лицо с правом подписи'
		Тогда открылось окно 'Организация2 (Организация)'
		И я нажимаю на кнопку создать поля "Основной банковский счет"
		Тогда открылось окно 'Банковский счет (создание)'
		И в поле с именем 'Наименование' я ввожу текст 'Основной счет(2)'
		И в поле 'Номер счета' я ввожу текст '40702810838110012215'
		И в поле "Валюта" я ввожу текст "RUB"
		И в поле с именем 'БИКБанка' я ввожу текст '044585261'
		И я перехожу к следующему реквизиту
		Тогда открылось окно 'Выбор банка из классификатора'
		И я нажимаю на кнопку 'Продолжить ввод'
		Тогда открылось окно 'Банковский счет (создание) *'
		И я нажимаю на кнопку 'Записать и закрыть'
		Тогда открылось окно 'Организация2 (Организация) *'
		И я нажимаю на кнопку 'Записать и закрыть'
		И я жду закрытия окна 'Организация2 (Организация) *' в течение 20 секунд
		Тогда открылось окно 'Организации'
	И Я закрываю окно 'Организации'

Сценарий: 003.5. ProductsOfOrganizations. Завершение

	И я закрываю сеанс TESTCLIENT