
#language: ru

@MovingProducts

Функционал: 005. MovingProducts. Установка цен и фиксация скидок

Сценарий: 005.1. MovingProducts. Установка цен

	Дано Я подключаюсь под "Ценообразование"
	Дано я устанавливаю текущему сеансу заголовок приложения "14.005. Перемещение товаров"
	Дано я удаляю объекты "Документы.к2УстановкаЦенНоменклатуры" без контроля ссылок
	И В командном интерфейсе я выбираю 'Заказы' 'История изменения цен'
	Тогда открылось окно 'История изменения цен'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Установка цен (создание)'
	И Я добавляю вид цены "Базовый" в документ установка цен
	И в поле 'от' я ввожу текст '01.08.2019'
	И в поле 'Комментарий' я ввожу текст 'Документ 1. Установка цен'
	И я нажимаю на кнопку 'Перейти к установке цен'
	И в таблице "ДеревоЦен" я нажимаю на кнопку 'Подобрать'
	Тогда открылось окно 'Подбор номенклатуры'
	И в таблице "СписокИерархия" я перехожу к строке:
		| 'Ссылка' |
		| 'Сыры'   |
	И Я подбираю номенклатуру "Сыр Адыгейский"
	И Я подбираю номенклатуру "Сыр Костромской"
	И Я подбираю номенклатуру "Сыр Пошехонский"
	И я нажимаю на кнопку "Перенести"
	Тогда открылось окно 'Установка цен (создание) *'
	И Я устанавливаю для номенклатуры "Сыр Адыгейский" и цены "ДеревоЦенНоваяЦена_4" цену "100,00"
	И Я устанавливаю для номенклатуры "Сыр Костромской" и цены "ДеревоЦенНоваяЦена_4" цену "50,00"
	И Я устанавливаю для номенклатуры "Сыр Пошехонский" и цены "ДеревоЦенНоваяЦена_4" цену "250,00"
	И в таблице "ДеревоЦен" я нажимаю на кнопку 'Добавить'
	Тогда открылось окно 'Выберите номенклатуру'
	И Я подбираю номенклатуру "Крылья куринные (подложка)"
	Тогда открылось окно 'Установка цен (создание) *'
	И Я устанавливаю для номенклатуры "Крылья куринные (подложка)" и цены "ДеревоЦенНоваяЦена_4" цену "200,00"
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Установка цен (создание) *' в течение 20 секунд
	Тогда открылось окно 'История изменения цен'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Установка цен (создание)' 
	И в поле 'от' я ввожу текст '1.07.2019'
	И Я добавляю вид цены "Базовый" в документ установка цен
	И я нажимаю на кнопку 'Перейти к установке цен'
	И в таблице "ДеревоЦен" я нажимаю на кнопку 'Добавить'
	Тогда открылось окно 'Выберите номенклатуру'
	И Я подбираю номенклатуру "Сыр Адыгейский"
	Тогда открылось окно 'Установка цен (создание) *'
	И в таблице "ДеревоЦен" я перехожу к строке:
		| 'Номенклатура/Характеристика' |
		| 'Сыр Адыгейский'              |
	И в таблице "ДеревоЦен" в поле 'Новая цена' я ввожу текст '291,28'
	И в таблице "ДеревоЦен" я завершаю редактирование строки
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Установка цен (создание) *' в течение 20 секунд
	Тогда открылось окно 'История изменения цен'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Установка цен (создание)' 
	И в поле 'от' я ввожу текст '1.07.2019'
	И Я добавляю вид цены "Для проверки суммы в перемещении - цена без НДС" в документ установка цен
	И я нажимаю на кнопку 'Перейти к установке цен'
	И в таблице "ДеревоЦен" я нажимаю на кнопку 'Добавить'
	Тогда открылось окно 'Выберите номенклатуру'
	И Я подбираю номенклатуру "Сыр Адыгейский"
	Тогда открылось окно 'Установка цен (создание) *'
	И в таблице "ДеревоЦен" я перехожу к строке:
		| 'Номенклатура/Характеристика' |
		| 'Сыр Адыгейский'              |
	И в таблице "ДеревоЦен" в поле 'Новая цена' я ввожу текст '291,88'
	И в таблице "ДеревоЦен" я завершаю редактирование строки
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Установка цен (создание) *' в течение 20 секунд
	Тогда открылось окно 'История изменения цен'
	И Я закрываю окно 'История изменения цен'

Сценарий: 005.2. MovingProducts. Завершение

	И я закрываю сеанс TESTCLIENT