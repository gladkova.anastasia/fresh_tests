
#language: ru

@SalesOfProducts

Функционал: 011. SalesOfProducts. Создание заданий на доставку

Сценарий: 011.1. SalesOfProducts. Создание заданий на доставку с помощью АРМа Рабочее место менеджера - по одному заказу клиента

	Дано Я подключаюсь под "Создание заданий на доставку"
	Дано я устанавливаю текущему сеансу заголовок приложения "13.011. Реализация товаров"
	Дано я удаляю объекты "Документы.к2ЗаданиеНаДоставку" без контроля ссылок
	И В командном интерфейсе я выбираю 'Склад и доставка' 'Рабочее место менеджера по доставке'
	Тогда открылось окно 'Формирование заданий на доставку на *'
	И из выпадающего списка с именем "Склад" я выбираю по строке 'Сырная ГП'
	И в поле 'Распоряжения на дату' я ввожу текст '11.10.19'
	И в таблице "ЗаданияНаДоставкуПланируемые" я нажимаю на кнопку с именем 'ЗаданияНаДоставкуПланируемыеСоздатьЗаданияНаДоставку'
	Тогда открылось окно 'Форма выбора транспорта'
	И в таблице "ТранспортныеСредства" я перехожу к строке:
		| 'Наименование'  |
		| 'А524НВ Газель' |
	И я нажимаю на кнопку '>'
	И я нажимаю на кнопку 'Сформировать задания'
	Тогда открылось окно 'Формирование заданий на доставку на *'
	И в таблице "РаспоряженияНаДоставку" я перехожу к строке:
		| 'Вес, кг' |
		| '10'      | 
	И я нажимаю на кнопку с именем 'ПеренестиСтрокиВРейс'
	И я перехожу к закладке "Задания на доставку"
	И в таблице "ЗаданияНаДоставкуВРаботе" я перехожу к строке:
		| 'Водитель'             | 'Склад'     | 'Статус'      | 'Транспорт'     |
		| 'Водитель Петров Петр' | 'Сырная ГП' | 'Формируется' | 'А524НВ Газель' |
	И в таблице "ЗаданияНаДоставкуВРаботе" я активизирую поле с именем "ЗаданияНаДоставкуВРаботеСтатус"
	И я нажимаю на кнопку с именем 'УстановитьСтатусКПогрузке'
	И Я закрываю окно 'Формирование заданий на доставку на *'

Сценарий: 011.2. SalesOfProducts. Проверка задания на доставку созданного с помощью АРМа Рабочее место менеджера по одному заказу клиента

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Задания на доставку'
	Тогда открылось окно 'Задания на доставку'
	И в таблице "Список" я перехожу к строке:
		| '(План) Начало рейса ' | 'Водитель'             | 'Ответственный'                | 'Статус'     | 'ТС'            |
		| '11.10.2019 0:00:00'   | 'Водитель Петров Петр' | 'Создание заданий на доставку' | 'К погрузке' | 'А524НВ Газель' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Задание на доставку * от *'
	* Проверка по способу формирования
		И элемент формы "Способ создания" стал равен 'АРМ \"Рабочее место менеджера по доставке\"'
	И элемент формы "Статус" стал равен 'К погрузке'
	И элемент формы "Вес" стал равен '10'
	И элемент формы "ПроцентЗагруженностиПоВесу" стал равен '0,11'
	И элемент формы "КоличествоМест" стал равен '11,23'
	И элемент формы "ПроцентЗагруженностиПоКоличествуМест" стал равен '32,09'
	И элемент формы "Перевозчик" стал равен 'Перевозчик(Газель)'
	И элемент формы "Транспортное средство" стал равен 'А524НВ Газель'
	И элемент формы "Водитель" стал равен 'Водитель Петров Петр'
	И элемент формы "Склад" стал равен 'Сырная ГП'
	И элемент формы "Ответственный" стал равен 'Создание заданий на доставку'
	И элемент формы с именем 'ГосНомерПрицепа' стал равен ''
	И элемент формы "Марка" стал равен ''
	И элемент формы 'Начало рейса план' стал равен '11.10.2019  0:00:00'
	И элемент формы 'Окончание рейса план' стал равен '  .  .       :  :  '
	И элемент формы 'Начало рейса факт' стал равен '  .  .       :  :  '
	И элемент формы 'Окончание рейса факт' стал равен '  .  .       :  :  '
	И таблица "ДеревоМаршрута" стала равной:
		| 'Количество мест' | 'Адрес / Распоряжение'                                                      | 'Вес'   |
		| '11,23'           | 'Нижегородская область, город Нижний Новгород, ул. Верхнепечерская, дом 21' | '10,00' |
	И Я закрываю окно 'Задание на доставку * от *'
	Тогда открылось окно 'Задания на доставку'
	И Я закрываю окно 'Задания на доставку'

Сценарий: 011.3. SalesOfProducts. Создание заданий на доставку с помощью АРМа Рабочее место менеджера - по двум заказам клиента

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Рабочее место менеджера по доставке'
	Тогда открылось окно 'Формирование заданий на доставку на *'
	И я перехожу к закладке "Формирование"
	И из выпадающего списка с именем "Склад" я выбираю по строке 'Сырная ГП'
	И в поле 'Распоряжения на дату' я ввожу текст '15.10.19'
	И в таблице "ЗаданияНаДоставкуПланируемые" я нажимаю на кнопку с именем 'ЗаданияНаДоставкуПланируемыеСоздатьЗаданияНаДоставку'
	Тогда открылось окно 'Форма выбора транспорта'
	И в таблице "ТранспортныеСредства" я перехожу к строке:
		| 'Наименование'  |
		| 'А524НВ Газель' |
	И я нажимаю на кнопку '>'
	И я нажимаю на кнопку 'Сформировать задания'
	Тогда открылось окно 'Формирование заданий на доставку на *'
	И в таблице "РаспоряженияНаДоставку" я перехожу к строке:
		| 'Вес, кг' |
		| '82'      |
	И я нажимаю на кнопку 'ПеренестиСтрокиВРейс'
	Тогда открылось окно '1С:Предприятие'
	И я нажимаю на кнопку с именем 'Button0'
	Тогда открылось окно 'Формирование заданий на доставку на *'
	И я перехожу к закладке "Задания на доставку"
	И в таблице "ЗаданияНаДоставкуВРаботе" я перехожу к строке:
		| 'Водитель'             | 'Склад'     | 'Статус'      | 'Транспорт'     |
		| 'Водитель Петров Петр' | 'Сырная ГП' | 'Формируется' | 'А524НВ Газель' |
	И в таблице "ЗаданияНаДоставкуВРаботе" я активизирую поле с именем "ЗаданияНаДоставкуВРаботеСтатус"
	И я нажимаю на кнопку с именем 'УстановитьСтатусКПогрузке'
	И Я закрываю окно 'Формирование заданий на доставку на *'

Сценарий: 011.4. SalesOfProducts. Создание заданий на доставку с помощью АРМа Рабочее место менеджера - по двум заказам, количество в паллете больше чем в заказах

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Рабочее место менеджера по доставке'
	Тогда открылось окно 'Формирование заданий на доставку на *'
	И я перехожу к закладке "Формирование"
	И из выпадающего списка с именем "Склад" я выбираю по строке 'Сырная ГП'
	И в поле 'Распоряжения на дату' я ввожу текст '16.10.19'
	И в таблице "ЗаданияНаДоставкуПланируемые" я нажимаю на кнопку с именем 'ЗаданияНаДоставкуПланируемыеСоздатьЗаданияНаДоставку'
	Тогда открылось окно 'Форма выбора транспорта'
	И в таблице "ТранспортныеСредства" я перехожу к строке:
		| 'Наименование'  |
		| 'А524НВ Газель' |
	И я нажимаю на кнопку '>'
	И я нажимаю на кнопку 'Сформировать задания'
	Тогда открылось окно 'Формирование заданий на доставку на *'
	И в таблице "РаспоряженияНаДоставку" я перехожу к строке:
		| 'Вес, кг' |
		| '82'      | 
	И я нажимаю на кнопку с именем 'ПеренестиСтрокиВРейс'
	Тогда открылось окно '1С:Предприятие'
	И я нажимаю на кнопку с именем 'Button0'
	И я перехожу к закладке "Задания на доставку"
	И в таблице "ЗаданияНаДоставкуВРаботе" я перехожу к строке:
		| 'Водитель'             | 'Склад'     | 'Статус'      | 'Транспорт'     |
		| 'Водитель Петров Петр' | 'Сырная ГП' | 'Формируется' | 'А524НВ Газель' |
	И в таблице "ЗаданияНаДоставкуВРаботе" я активизирую поле с именем "ЗаданияНаДоставкуВРаботеСтатус"
	И я нажимаю на кнопку с именем 'УстановитьСтатусКПогрузке'
	И Я закрываю окно 'Формирование заданий на доставку на *'

Сценарий: 011.5. SalesOfProducts. Создание заданий на доставку с помощью АРМа Рабочее место менеджера - по двум заказам, количество в паллете меньше чем в заказах

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Рабочее место менеджера по доставке'
	Тогда открылось окно 'Формирование заданий на доставку на *'
	И я перехожу к закладке "Формирование"
	И из выпадающего списка с именем "Склад" я выбираю по строке 'Сырная ГП'
	И в поле 'Распоряжения на дату' я ввожу текст '17.10.19'
	И в таблице "ЗаданияНаДоставкуПланируемые" я нажимаю на кнопку с именем 'ЗаданияНаДоставкуПланируемыеСоздатьЗаданияНаДоставку'
	Тогда открылось окно 'Форма выбора транспорта'
	И в таблице "ТранспортныеСредства" я перехожу к строке:
		| 'Наименование'  |
		| 'А524НВ Газель' | 
	И я нажимаю на кнопку '>'
	И я нажимаю на кнопку 'Сформировать задания'
	Тогда открылось окно 'Формирование заданий на доставку на *'
	И в таблице "РаспоряженияНаДоставку" я перехожу к строке:
		| 'Вес, кг' |
		| '82'      | 
	И я нажимаю на кнопку с именем 'ПеренестиСтрокиВРейс'
	Тогда открылось окно '1С:Предприятие'
	И я нажимаю на кнопку с именем 'Button0'
	И я перехожу к закладке "Задания на доставку"
	И в таблице "ЗаданияНаДоставкуВРаботе" я перехожу к строке:
		| 'Водитель'             | 'Склад'     | 'Статус'      | 'Транспорт'     |
		| 'Водитель Петров Петр' | 'Сырная ГП' | 'Формируется' | 'А524НВ Газель' |
	И в таблице "ЗаданияНаДоставкуВРаботе" я активизирую поле с именем "ЗаданияНаДоставкуВРаботеСтатус"
	И я нажимаю на кнопку с именем 'УстановитьСтатусКПогрузке'
	И Я закрываю окно 'Формирование заданий на доставку на *'

Сценарий: 011.6. SalesOfProducts. Создание заданий на доставку с помощью АРМа Рабочее место менеджера для группы складов - реализация по складам

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Рабочее место менеджера по доставке'
	Тогда открылось окно 'Формирование заданий на доставку на *'
	И я перехожу к закладке "Формирование"
	И из выпадающего списка с именем "Склад" я выбираю по строке 'Группа складов для отгрузки'
	И в поле 'Распоряжения на дату' я ввожу текст '28.01.20'
	И в таблице "ЗаданияНаДоставкуПланируемые" я нажимаю на кнопку с именем 'ЗаданияНаДоставкуПланируемыеСоздатьЗаданияНаДоставку'
	Тогда открылось окно 'Форма выбора транспорта'
	И в таблице "ТранспортныеСредства" я перехожу к строке:
		| 'Наименование'  |
		| 'А524НВ Газель' |
	И я нажимаю на кнопку '>'
	И я нажимаю на кнопку 'Сформировать задания'
	Тогда открылось окно 'Формирование заданий на доставку на *'
	И в таблице "РаспоряженияНаДоставку" я перехожу к строке:
		| 'Вес, кг' | 'Мест'    |
		| '358,75'  | '201,439' |
	И я нажимаю на кнопку с именем 'ПеренестиСтрокиВРейс'
	И я перехожу к закладке "Задания на доставку"
	И в таблице "ЗаданияНаДоставкуВРаботе" я перехожу к строке:
		| 'Вес, кг' | 'Водитель'             | 'Склад'                       | 'Статус'      | 'Транспорт'     |
		| '358,75'  | 'Водитель Петров Петр' | 'Группа складов для отгрузки' | 'Формируется' | 'А524НВ Газель' |
	И в таблице "ЗаданияНаДоставкуВРаботе" я активизирую поле с именем "ЗаданияНаДоставкуВРаботеСтатус"
	И я нажимаю на кнопку с именем 'УстановитьСтатусКПогрузке'
	И Я закрываю окно 'Формирование заданий на доставку на *'

Сценарий: 011.7. SalesOfProducts. Задание на доставку на основании плана отгрузки

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Планы отгрузки'
	Тогда открылось окно 'Планы отгрузки'
	И в таблице "Список" я перехожу к строке:
		| 'Комментарий'                                                                        |
		| 'Использование неавтоматизированных складов при оформлении накладных. План отгрузки' |
	И я нажимаю на кнопку 'Задание на доставку'
	Тогда открылось окно 'Задание на доставку (создание)'
	И из выпадающего списка с именем "Статус" я выбираю точное значение 'К погрузке'
	И из выпадающего списка "Перевозчик" я выбираю по строке 'Перевозчик(Газель)'
	И из выпадающего списка "Транспортное средство" я выбираю точное значение 'А524НВ Газель'
	И в поле с именем 'Дата' я ввожу текст '22.03.2020  0:00:00'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Задание на доставку (создание) *' в течение 20 секунд
	Тогда открылось окно 'Планы отгрузки'
	И я закрываю окно 'Планы отгрузки'

Сценарий: 011.8. SalesOfProducts. Завершение

	И я закрываю сеанс TESTCLIENT