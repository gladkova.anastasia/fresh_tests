#language: ru

@SalesOfProducts

Функционал: 004. SalesOfProducts. Зона доставки и транспортное средство

Сценарий: 004.1. SalesOfProducts. Создание зоны доставки

	Дано Я подключаюсь под "Ведение НСИ транспортной логистики"
	Дано я устанавливаю текущему сеансу заголовок приложения "13.004. Реализация товаров"
	Дано я удаляю объекты "Справочники.к2ЗоныДоставки" без контроля ссылок
	Когда В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Зоны доставки'
	Тогда я жду открытия окна 'Зоны доставки' в течение 20 секунд
	Тогда открылось окно 'Зоны доставки'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Зоны доставки (создание)'
	И в поле 'Наименование' я ввожу текст 'Зона доставки'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Зоны доставки (создание) *' в течение 20 секунд
	Тогда открылось окно 'Зоны доставки'
	И Я закрываю окно 'Зоны доставки'

Сценарий: 004.2. SalesOfProducts. Создание физического лица

	Когда В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Физические лица'
	Тогда я жду открытия окна 'Физические лица' в течение 20 секунд
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Физическое лицо (создание)'
	И в поле 'Наименование' я ввожу текст 'Водитель Петров Петр'
	И в поле 'ФИО' я ввожу текст 'Петров Петр'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Физическое лицо (создание)' в течение 20 секунд
	Тогда открылось окно 'Физические лица'
	И Я закрываю окно 'Физические лица'

Сценарий: 004.3. SalesOfProducts. Создание типа транспортного средства

	Дано я удаляю объекты "Справочники.к2ТипыТранспортныхСредств" без контроля ссылок
	Когда В командном интерфейсе я выбираю 'Склад и доставка' 'Типы транспортных средств'
	Тогда я жду открытия окна 'Типы транспортных средств' в течение 20 секунд
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Типы транспортных средств (создание)'
	И в поле 'Наименование' я ввожу текст 'Газель'
	И в поле 'Вместимость' я ввожу текст '35,000'
	И я перехожу к следующему реквизиту
	И в поле 'Грузоподъемность' я ввожу текст '9,000'
	И я перехожу к следующему реквизиту
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Транспортные средства (создание) *' в течение 20 секунд
	И Я закрываю окно 'Типы транспортных средств'

Сценарий: 004.4. SalesOfProducts. Создание транспортного средства

	Дано я удаляю объекты "Справочники.к2ТранспортныеСредства" без контроля ссылок
	Когда В командном интерфейсе я выбираю 'Склад и доставка' 'Транспортные средства'
	Тогда я жду открытия окна 'Транспортные средства' в течение 20 секунд
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Транспортные средства (создание)'
	И в поле 'Государственный номер' я ввожу текст 'А524НВ'
	И в поле 'Марка' я ввожу текст 'Газель'
	И из выпадающего списка "Тип" я выбираю по строке 'Газель'
	И из выпадающего списка "Основной водитель" я выбираю по строке 'Водитель Петров Петр'
	И я устанавливаю флаг 'Постоянно наемное ТС'
	И из выпадающего списка "Транспортная компания" я выбираю по строке 'Перевозчик(Газель)'
	И в таблице "СкладыОтгрузки" я нажимаю на кнопку с именем 'СкладыОтгрузкиДобавить'
	И в таблице "СкладыОтгрузки" из выпадающего списка "Склад отгрузки" я выбираю по строке 'Склад готовой продукции'
	И в таблице "СкладыОтгрузки" я завершаю редактирование строки
	И в таблице "СкладыОтгрузки" я нажимаю на кнопку с именем 'СкладыОтгрузкиДобавить'
	И в таблице "СкладыОтгрузки" из выпадающего списка "Склад отгрузки" я выбираю по строке 'Сырная ГП'
	И в таблице "СкладыОтгрузки" я завершаю редактирование строки
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Транспортные средства (создание) *' в течение 20 секунд
	Тогда открылось окно 'Транспортные средства'
	И Я закрываю окно 'Транспортные средства'

Сценарий: 004.5. SalesOfProducts. Завершение

	И я закрываю сеанс TESTCLIENT