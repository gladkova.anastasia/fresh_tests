#language: ru

@cutting

Функционал: 011. Разруб. Проверка АРМа.


Контекст: 
	Дано Я подключаюсь под "Планирование разруба"

# 2.11. Проверяем, что у всех используемых ресурсов проставлено подразделение "Цех индеек №1" ожидаем тот результат разруба, как без проставления подразделений
Сценарий: 011.1. Разруб. Планирование 1

	И В командном интерфейсе я выбираю 'Планирование' 'Планирование разруба'
	Тогда я жду открытия окна 'Планирование разруба' в течение 20 секунд
	И я нажимаю кнопку очистить у поля "Сценарий"
	И я перехожу к закладке с именем "Страница_Сводка"
	И я перехожу к закладке "Настройки"
	И я меняю значение переключателя 'Вариант балансировки по волнам' на 'Одноэтапный'
	И в поле 'Дата разруба' я ввожу текст '02.11.2019'
	И я перехожу к закладке "Ресурсы"
	И я перехожу к закладке "Доступные ресурсы разруба"
	И в таблице "РесурсыРазруба" я перехожу к строке:
		| 'Волна' | 'Ресурс'          |
		| '1. До 12' | 'Разделка(Тушка)' |
	И в таблице "РесурсыРазруба" поле с именем "РесурсыРазрубаПодразделение" имеет значение "Цех индеек №1"
	И в таблице "РесурсыРазруба" я перехожу к строке:
		| 'Волна' | 'Ресурс'             |
		| '1. До 12' | 'Разделка(Окорочок)' |
	И в таблице "РесурсыРазруба" поле с именем "РесурсыРазрубаПодразделение" имеет значение "Цех индеек №1"
	И в таблице "РесурсыРазруба" я перехожу к строке:
		| 'Волна' | 'Ресурс'             |
		| '1. До 12' | 'Убой(Индейки)' |
	И в таблице "РесурсыРазруба" поле с именем "РесурсыРазрубаПодразделение" имеет значение "Цех индеек №1"
	И в таблице "РесурсыРазруба" я перехожу к строке:
		| 'Волна' | 'Ресурс'          |
		| '2. До 15' | 'Разделка(Тушка)' |
	И в таблице "РесурсыРазруба" поле с именем "РесурсыРазрубаПодразделение" имеет значение "Цех индеек №1"
	И в таблице "РесурсыРазруба" я перехожу к строке:
		| 'Волна' | 'Ресурс'             |
		| '2. До 15' | 'Разделка(Окорочок)' |
	И в таблице "РесурсыРазруба" поле с именем "РесурсыРазрубаПодразделение" имеет значение "Цех индеек №1"
	И в таблице "РесурсыРазруба" я перехожу к строке:
		| 'Волна' | 'Ресурс'             |
		| '2. До 15' | 'Убой(Индейки)' |
	И в таблице "РесурсыРазруба" поле с именем "РесурсыРазрубаПодразделение" имеет значение "Цех индеек №1"
	И в таблице "РесурсыРазруба" я перехожу к строке:
		| 'Волна' | 'Ресурс'          |
		| '3. До 17' | 'Разделка(Тушка)' |
	И в таблице "РесурсыРазруба" поле с именем "РесурсыРазрубаПодразделение" имеет значение "Цех индеек №1"
	И в таблице "РесурсыРазруба" я перехожу к строке:
		| 'Волна' | 'Ресурс'             |
		| '3. До 17' | 'Разделка(Окорочок)' |
	И в таблице "РесурсыРазруба" поле с именем "РесурсыРазрубаПодразделение" имеет значение "Цех индеек №1"
	И в таблице "РесурсыРазруба" я перехожу к строке:
		| 'Волна' | 'Ресурс'             |
		| '3. До 17' | 'Убой(Индейки)' |
	И в таблице "РесурсыРазруба" поле с именем "РесурсыРазрубаПодразделение" имеет значение "Цех индеек №1"
	И я нажимаю на кнопку 'Выполнить балансировку'	
	И я перехожу к закладке "Результат планирования"
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'     | 'План'  | 'Подразделение' | 'Продукция'             | 'Штраф' |
		| '1 736'  | '2'     | 'Крылышки индейки' | '1 736' | 'Цех индеек №1' | 'Крылышки индейки охл.' | '200'   |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'  | 'План'   | 'Подразделение' | 'Продукция'          | 'Штраф' |
		| '10 000' | '2'     | 'Тушка индейки' | '10 000' | 'Цех индеек №1' | 'Тушка индейки охл.' | '3 000' |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'  | 'План'  | 'Подразделение' | 'Продукция'          | 'Штраф' |
		| '1 824'  | '2'     | 'Бедро индейки' | '1 824' | 'Цех индеек №1' | 'Бедро индейки охл.' | '3 000' |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'     | 'План'  | 'Подразделение' | 'Продукция'             | 'Штраф' |
		| '2 806'  | '2'     | 'Крылышки индейки' | '2 806' | 'Цех индеек №1' | 'Крылышки индейки охл.' | '2 000' |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'  | 'План'  | 'Подразделение' | 'Продукция'          | 'Сумма штрафа' | 'Штраф' | 'Штраф за волну' |
		| '2 944'  | '2'     | 'Бедро индейки' | '2 944' | 'Цех индеек №1' | 'Бедро индейки охл.' | '4 416 000'    | '3 000' | '1 500'          |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'     | 'План' | 'Подразделение' | 'Продукция'             | 'Сумма штрафа' | 'Штраф' | 'Штраф за волну' |
		| '458'    | '2'     | 'Крылышки индейки' | '458'  | 'Цех индеек №1' | 'Крылышки индейки охл.' | '458 000'      | '2 000' | '1 000'          |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'  | 'План'  | 'Подразделение' | 'Продукция'          | 'Сумма штрафа' | 'Штраф' | 'Штраф за волну' |
		| '1 964'  | '2'     | 'Бедро индейки' | '1 964' | 'Цех индеек №1' | 'Бедро индейки охл.' | '2 946 000'    | '3 000' | '1 500'          |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'     | 'Подразделение' | 'Продукция'             | 'Сумма штрафа'  | 'Штраф' | 'Штраф за волну' |
		| '11 000' | '2'     | 'Окорочок индейки' | 'Цех индеек №1' | 'Окорочок индейки охл.' | '11 000 000'    | '1 000' | '500'            |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'  | 'Подразделение' | 'Продукция'          | 'Сумма штрафа' | 'Штраф' | 'Штраф за волну' |
		| '268'    | '2'     | 'Бедро индейки' | 'Цех индеек №1' | 'Бедро индейки охл.' | '804 000'      | '3 000' | '1 500'          |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'        | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Субпродукты индейки' | 'Цех индеек №1' | '7 425'        | '1'     |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'  | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Тушка индейки' | 'Цех индеек №1' | '13,85'        | '1'     |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'   | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Грудка индейки' | 'Цех индеек №1' | '14 427,51'    | '1'     |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'     | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Крылышки индейки' | 'Цех индеек №1' | '1 412,22'     | '1'     |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'  | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Бедро индейки' | 'Цех индеек №1' | '0,83'         | '1'     |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'   | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Голень индейки' | 'Цех индеек №1' | '4 488,56'     | '1'     |
	И Я закрываю окно 'Планирование разруба'


# 3.11 У нас достаточно ресурсов чтобы убить весь план убоя(30 тонн) за 3 часа,
# мы проверяем после балансировки сколько было использовано ресурсов. Ресурсы распределяются пропорционально длинам волн.
Сценарий: 011.2. Разруб. Планирование 2

	И В командном интерфейсе я выбираю 'Планирование' 'Планирование разруба'
	Тогда я жду открытия окна 'Планирование разруба' в течение 20 секунд
	И я нажимаю кнопку очистить у поля "Сценарий"
	И я перехожу к закладке "Настройки"
	И я меняю значение переключателя 'Вариант балансировки по волнам' на 'Одноэтапный'
	И в поле 'Дата разруба' я ввожу текст '03.11.2019'
	И я нажимаю на кнопку 'Выполнить балансировку'
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'     | 'План'  | 'Подразделение' | 'Продукция'             | 'Штраф' |
		| '1 862'  | '2'     | 'Крылышки индейки' | '1 862' | 'Цех индеек №1' | 'Крылышки индейки охл.' | '100'   |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'     | 'План'  | 'Подразделение' | 'Продукция'             | 'Штраф' |
		| '2 000'  | '2'     | 'Окорочок индейки' | '2 000' | 'Цех индеек №1' | 'Окорочок индейки охл.' | '100'   |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'   | 'План'  | 'Подразделение' | 'Продукция'           | 'Штраф' |
		| '2 000'  | '2'     | 'Грудка индейки' | '2 000' | 'Цех индеек №1' | 'Грудка индейки охл.' | '100'   |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'  | 'План'  | 'Подразделение' | 'Продукция'          | 'Штраф' |
		| '2 000'  | '2'     | 'Тушка индейки' | '2 000' | 'Цех индеек №1' | 'Тушка индейки охл.' | '100'   |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'  | 'План' | 'Подразделение' | 'Продукция'          | 'Штраф' |
		| '754'    | '2'     | 'Бедро индейки' | '754'  | 'Цех индеек №1' | 'Бедро индейки охл.' | '100'   |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'     | 'План' | 'Подразделение' | 'Продукция'             | 'Штраф' |
		| '138'    | '2'     | 'Крылышки индейки' | '138'  | 'Цех индеек №1' | 'Крылышки индейки охл.' | '100'   |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'  | 'План'  | 'Подразделение' | 'Продукция'          | 'Штраф' |
		| '1 246'  | '2'     | 'Бедро индейки' | '1 246' | 'Цех индеек №1' | 'Бедро индейки охл.' | '100'   |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'        | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Субпродукты индейки' | 'Цех индеек №1' | '4 499,55'     | '1'     |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'  | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Тушка индейки' | 'Цех индеек №1' | '8 212,75'     | '1'     |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'   | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Грудка индейки' | 'Цех индеек №1' | '4 878,11'     | '1'     |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'     | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Крылышки индейки' | 'Цех индеек №1' | '1 056,94'     | '1'     |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'     | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Окорочок индейки' | 'Цех индеек №1' | '2,77'         | '1'     |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'  | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Бедро индейки' | 'Цех индеек №1' | '8,12'         | '1'     |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'   | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Голень индейки' | 'Цех индеек №1' | '1 338,75'     | '1'     |
	И я перехожу к закладке "Ресурсы"
	И я перехожу к закладке "Доступные ресурсы разруба"
	И в таблице "РесурсыРазруба" я перехожу к строке:
		| 'Волна'    | 'Доступно, ч.' | 'Подразделение' | 'Ресурс'        |
		| '1. До 12' | '4'            | 'Цех индеек №1' | 'Убой(Индейки)' |
	И в таблице "РесурсыРазруба" поле с именем "РесурсыРазрубаИспользовано" имеет значение  "1,3332"
	И в таблице "РесурсыРазруба" я перехожу к строке:
		| 'Волна'    | 'Доступно, ч.' | 'Подразделение' | 'Ресурс'        |
		| '2. До 15' | '3'            | 'Цех индеек №1' | 'Убой(Индейки)' |
	И в таблице "РесурсыРазруба" поле с именем "РесурсыРазрубаИспользовано" имеет значение  "0,9999"
	И в таблице "РесурсыРазруба" я перехожу к строке:
		| 'Волна'    | 'Доступно, ч.' | 'Подразделение' | 'Ресурс'        |
		| '3. До 17' | '2'            | 'Цех индеек №1' | 'Убой(Индейки)' |
	И в таблице "РесурсыРазруба" поле с именем "РесурсыРазрубаИспользовано" имеет значение  "0,6666"
	И Я закрываю окно 'Планирование разруба'


Сценарий: 011.3. Разруб. Проверка дерева разруба

	И В командном интерфейсе я выбираю 'Планирование' 'Планирование разруба'
	Тогда я жду открытия окна 'Планирование разруба' в течение 20 секунд
	И в поле 'Дата разруба' я ввожу текст '03.11.2019'
	И я нажимаю на кнопку 'Выполнить балансировку'
	И из выпадающего списка "ОтборПоПодразделению" я выбираю по строке 'Цех индеек №1'
	И я нажимаю кнопку очистить у поля "ОтборПоПодразделению"
	И из выпадающего списка "ОтборПоВолне" я выбираю по строке '1. До 12'
	И из выпадающего списка "ОтборПоВолне" я выбираю по строке '2. До 15'
	И из выпадающего списка "ОтборПоВолне" я выбираю по строке '3. До 17'
	И я нажимаю кнопку очистить у поля "ОтборПоВолне"
	И в таблице "РезультатПланирования" я нажимаю на кнопку 'Создать перезаписать документ плана'
	И я нажимаю на кнопку 'Прочитать из документа'
	Тогда я жду открытия окна 'Планы разруба' в течение 20 секунд
	И в таблице "Список" я выбираю текущую строку
	Тогда я жду открытия окна 'Планирование разруба' в течение 20 секунд
	И я перехожу к закладке с именем "Страница_Сводка"
	И в таблице "ДеревьяРазруба" я выбираю текущую строку
	Тогда я жду открытия окна 'Ручное изменение дерева разруба' в течение 20 секунд
	И в таблице "ДеревоРазруба" я разворачиваю строку с подчиненными:
		| 'Абс. %' | 'Выход по, кг' | 'Выход с, кг' | 'Выход, кг' | 'Исп., кг' | 'Представление' |
		| '100'    | '0'            | '0'           | '0'         | '29 997'   | 'Индейка'       |
	И в таблице "ДеревоРазруба" я разворачиваю строку с подчиненными:
		| 'Исп., кг' | 'Представление'    |
		| '29 997'   | '1. Убой индейки'  |
	И в таблице "ДеревоРазруба" я разворачиваю строку с подчиненными:
		| 'Абс. %' | 'Выход по, кг' | 'Выход с, кг' | 'Выход, %' | 'Выход, кг' | 'Исп., кг' | 'Представление' |
		| '85'     | '10 213'       | '0'           | '34,05'    | '10 213'    | '15 285'   | 'Тушка индейки' |
	И в таблице "ДеревоРазруба" я разворачиваю строку с подчиненными:
		| 'Исп., кг' | 'Представление'     |
		| '15 285'   | '2. Разруб индейки' |
	И в таблице "ДеревоРазруба" я разворачиваю строку с подчиненными:
		| 'Абс. %' | 'Выход по, кг' | 'Выход с, кг' | 'Выход, %' | 'Выход, кг' | 'Исп., кг' | 'Представление'    | 'Своб., кг' |
		| '29,75'  | '5 577'        | '0'           | '6,68'     | '2 003'     | '3 347'    | 'Окорочок индейки' | '2 874'     |
	И в таблице "ДеревоРазруба" я разворачиваю строку с подчиненными:
		| 'Исп., кг' | 'Представление'     |
		| '3 347'    | '3. Делим окорочок' |
	И я нажимаю на кнопку '1 -> 1,000'
	И я нажимаю на кнопку '1 -> 1,000'
	И я нажимаю на кнопку 'Обновлять'
	И в таблице "ДеревоРазруба" я перехожу к строке:
		| 'Абс. %' | 'Выход по, кг' | 'Выход с, кг' | 'Выход, %' | 'Выход, кг' | 'Представление'       |
		| '15'     | '4 500'        | '2 697'       | '15,00'    | '4 500'     | 'Субпродукты индейки' |
	И в таблице "ДеревоРазруба" я активизирую поле "Выход с, кг"
	И в таблице "ДеревоРазруба" я выбираю текущую строку
	И в таблице "ДеревоРазруба" я выбираю текущую строку
	И в таблице "ДеревоРазруба" я перехожу к строке:
		| 'Абс. %' | 'Выход по, кг' | 'Выход с, кг' | 'Выход, %' | 'Выход, кг' | 'Представление'    | 'Своб., кг' |
		| '17'     | '5 099'        | '1 913'       | '10,19'    | '3 057'     | 'Крылышки индейки' | '1 642'     |
	И в таблице "ДеревоРазруба" я активизирую поле "Выход с, кг"
	И в таблице "ДеревоРазруба" я выбираю текущую строку
	И в таблице "ДеревоРазруба" я выбираю текущую строку
	И в таблице "ДеревоРазруба" я активизирую поле "Выход, кг"
	И в таблице "Выход" я перехожу к строке:
		| 'В заявке, кг' | 'Вес, кг' | 'Макс, кг' | 'Номенклатура'   | 'Отклонение, %' | 'Отклонение, кг' |
		| '2 000'        | '11 474'  | '11 474'   | 'Грудка индейки' | '473,69'        | '9 474'          |
	И в таблице "Выход" я активизирую поле с именем "ВыходВес"
	И в таблице "Выход" я выбираю текущую строку
	И в таблице "Выход" в поле с именем 'ВыходВес' я ввожу текст '10 000,000'
	И в таблице "Выход" я завершаю редактирование строки
	И в таблице "ДеревоРазруба" я перехожу к строке:
		| 'Исп., кг' | 'Представление'     |
		| '22 223'   | '2. Разруб индейки' |
	И в таблице "ДеревоРазруба" я активизирую поле "Исп., кг"
	И в таблице "ДеревоРазруба" я выбираю текущую строку
	И в таблице "ДеревоРазруба" в поле 'Исп., кг' я ввожу текст '20 000,000'
	И в таблице "ДеревоРазруба" я завершаю редактирование строки
	И в таблице "ДеревоРазруба" я перехожу к строке:
		| 'Абс. %' | 'Выход по, кг' | 'Выход с, кг' | 'Выход, %' | 'Выход, кг' | 'Представление'  | 'Своб., кг' |
		| '38,25'  | '11 474'       | '4 303'       | '30,01'    | '9 001'     | 'Грудка индейки' | '1 572'     |
	И в таблице "ДеревоРазруба" я активизирую поле "Выход, кг"
	И в таблице "ДеревоРазруба" я выбираю текущую строку
	И в таблице "ДеревоРазруба" в поле 'Выход, кг' я ввожу текст '8 000,000'
	И в таблице "ДеревоРазруба" я завершаю редактирование строки
	И в таблице "ДеревоРазруба" я перехожу к строке:
		| 'Абс. %' | 'Выход по, кг' | 'Выход с, кг' | 'Выход, %' | 'Выход, кг' | 'Исп., кг' | 'Представление'    | 'Своб., кг' |
		| '29,75'  | '5 577'        | '0'           | '9,59'     | '2 876'     | '3 347'    | 'Окорочок индейки' | '2 001'     |
	И в таблице "ДеревоРазруба" я активизирую поле "Выход, %"
	И в таблице "ДеревоРазруба" я выбираю текущую строку
	И в таблице "ДеревоРазруба" в поле 'Выход, %' я ввожу текст '8,00'
	И в таблице "ДеревоРазруба" я завершаю редактирование строки
	И в таблице "ДеревоРазруба" я перехожу к строке:
		| 'Абс. %' | 'Выход по, кг' | 'Выход с, кг' | 'Выход, %' | 'Выход, кг' | 'Представление' | 'Своб., кг' |
		| '17,85'  | '3 448'        | '0'           | '6,69'     | '2 008'     | 'Бедро индейки' | '240'       |
	И в таблице "ДеревоРазруба" я активизирую поле "↑"
	И в таблице "ДеревоРазруба" я выбираю текущую строку
	И в таблице "ДеревоРазруба" в поле '↑' я ввожу текст '3,000'
	И в таблице "ДеревоРазруба" я завершаю редактирование строки
	И в таблице "ДеревоРазруба" я перехожу к строке:
		| 'Абс. %' | 'Выход по, кг' | 'Выход с, кг' | 'Выход, %' | 'Выход, кг' | 'Исп., кг' | 'Представление'    | 'Своб., кг' |
		| '29,75'  | '5 582'        | '0'           | '8,02'     | '2 404'     | '3 342'    | 'Окорочок индейки' | '2 477'     |
	И в таблице "ДеревоРазруба" я активизирую поле "↓"
	И в таблице "ДеревоРазруба" я выбираю текущую строку
	И в таблице "ДеревоРазруба" в поле '↓' я ввожу текст '404,000'
	И в таблице "ДеревоРазруба" я завершаю редактирование строки
	И в таблице "Выход" я перехожу к строке:
		| 'В заявке, кг' | 'Вес, кг' | 'Макс, кг' | 'Номенклатура'  | 'Отклонение, %' | 'Отклонение, кг' |
		| '2 000'        | '9 078'   | '25 497'   | 'Тушка индейки' | '353,90'        | '7 078'          |
	И в таблице "Выход" я выбираю текущую строку
	И в таблице "Выход" в поле с именем 'ВыходВес' я ввожу текст '100 000,000'
	И я перехожу к закладке "Сырье и спецификации"
	И я перехожу к закладке "Ресурсы"
	И я перехожу к закладке "Дерево"
	И в таблице "Выход" я выбираю текущую строку
	И в таблице "Выход" в поле с именем 'ВыходВес' я ввожу текст '10 000 000,000'
	И я перехожу к закладке "Ресурсы"
	И в таблице "РесурсыРазруба" я активизирую поле "Доступно, ч."
	И в таблице "РесурсыРазруба" я выбираю текущую строку
	И в таблице "РесурсыРазруба" в поле 'Доступно, ч.' я ввожу текст '1,0000000'
	И в таблице "РесурсыРазруба" я завершаю редактирование строки
	И в таблице "РесурсыРазруба" я перехожу к строке:
		| 'Ресурс'          | 
		| 'Разделка(Тушка)' |
	И в таблице "РесурсыРазруба" я выбираю текущую строку
	И в таблице "РесурсыРазруба" в поле 'Доступно, ч.' я ввожу текст '1,0000000'
	И в таблице "РесурсыРазруба" я завершаю редактирование строки
	И в таблице "РесурсыРазруба" я перехожу к строке:
		| 'Ресурс'             |
		| 'Разделка(Окорочок)' |
	И в таблице "РесурсыРазруба" я выбираю текущую строку
	И в таблице "РесурсыРазруба" в поле 'Доступно, ч.' я ввожу текст '1,0000000'
	И в таблице "РесурсыРазруба" я завершаю редактирование строки
	И я нажимаю на кнопку 'Обновить результат'
	И я перехожу к закладке "Сырье и спецификации"
	И я перехожу к закладке "Дерево"
	И я нажимаю на кнопку 'Отменить последнее действие'
	И я нажимаю на кнопку 'Отменить последнее действие'
	И я нажимаю на кнопку 'Отменить последнее действие'
	И я нажимаю на кнопку 'Отменить последнее действие'
	И я нажимаю на кнопку 'Отменить последнее действие'
	И я нажимаю на кнопку 'Отменить последнее действие'
	И я нажимаю на кнопку 'Отменить последнее действие'
	И я нажимаю на кнопку 'Отменить последнее действие'
	И я нажимаю на кнопку 'Отменить последнее действие'
	И я нажимаю на кнопку 'Отменить последнее действие'
	И я нажимаю на кнопку 'Повторить отмененное действие'
	И я нажимаю на кнопку 'Отменить последнее действие'
	И я нажимаю на кнопку 'Передать результат'
	Тогда я жду открытия окна 'Планирование разруба' в течение 20 секунд
	И я нажимаю на кнопку 'Выполнить балансировку'
	И я нажимаю на кнопку с именем 'ОчиститьРучнойПланИПерепланировать_КоманднаяПанель'
	И Я закрываю окно 'Планирование разруба'

# 1.11. У нас достаточно ресурсов чтобы убить весь план убоя(1,575 тонн) за 0,1575 часа, мы проверяем после балансировки сколько было использовано ресурсов.
# Ресурсы распределяются пропорционально длинам волн.
Сценарий: 011.4. Разруб. Планирование 3

	И В командном интерфейсе я выбираю 'Планирование' 'Планирование разруба'
	Тогда я жду открытия окна 'Планирование разруба' в течение 20 секунд
	И я перехожу к закладке "Настройки"
	И я меняю значение переключателя 'Вариант балансировки по волнам' на 'Одноэтапный'
	И в поле 'Дата разруба' я ввожу текст '01.11.2019'
	И я нажимаю на кнопку 'Выполнить балансировку'
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'   | 'План' | 'Подразделение' | 'Продукция'           | 'Штраф' |
		| '100'    | '2'     | 'Грудка индейки' | '100'  | 'Цех индеек №1' | 'Грудка индейки охл.' | '3 000' |
	И в таблице "РезультатПланирования" я активизирую поле с именем "РезультатПланированияВес"
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'   | 'План' | 'Подразделение' | 'Продукция'           | 'Штраф' |
		| '100'    | '2'     | 'Грудка индейки' | '100'  | 'Цех индеек №1' | 'Грудка индейки охл.' | '4 000' |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'  | 'План' | 'Подразделение' | 'Продукция'          | 'Штраф' |
		| '124'    | '2'     | 'Бедро индейки' | '124'  | 'Цех индеек №1' | 'Бедро индейки охл.' | '4 000' |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'  | 'План' | 'Подразделение' | 'Продукция'          | 'Штраф' |
		| '170'    | '2'     | 'Тушка индейки' | '170'  | 'Цех индеек №1' | 'Тушка индейки охл.' | '2 000' |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'  | 'План' | 'Подразделение' | 'Продукция'          | 'Штраф' |
		| '170'    | '2'     | 'Тушка индейки' | '170'  | 'Цех индеек №1' | 'Тушка индейки охл.' | '3 000' |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'  | 'План' | 'Подразделение' | 'Продукция'          | 'Сумма штрафа' | 'Штраф' | 'Штраф за волну' |
		| '22'     | '2'     | 'Бедро индейки' | '22'   | 'Цех индеек №1' | 'Бедро индейки охл.' | '44 000'       | '4 000' | '2 000'          |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'  | 'План' | 'Подразделение' | 'Продукция'          | 'Сумма штрафа' | 'Штраф' | 'Штраф за волну' |
		| '58'     | '2'     | 'Бедро индейки' | '58'   | 'Цех индеек №1' | 'Бедро индейки охл.' | '87 000'       | '3 000' | '1 500'          |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'  | 'План' | 'Подразделение' | 'Продукция'          | 'Сумма штрафа' | 'Штраф' | 'Штраф за волну' |
		| '4'      | '2'     | 'Бедро индейки' | '4'    | 'Цех индеек №1' | 'Бедро индейки охл.' | '8 000'        | '4 000' | '2 000'          |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Заявка' | 'Квант' | 'Номенклатура'  | 'Подразделение' | 'Продукция'          | 'Сумма штрафа' | 'Штраф' | 'Штраф за волну' |
		| '92'     | '2'     | 'Бедро индейки' | 'Цех индеек №1' | 'Бедро индейки охл.' | '276 000'      | '3 000' | '1 500'          |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'        | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Субпродукты индейки' | 'Цех индеек №1' | '236,25'       | '1'     |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'  | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Тушка индейки' | 'Цех индеек №1' | '4,25'         | '1'     |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'   | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Грудка индейки' | 'Цех индеек №1' | '247,52'       | '1'     |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'     | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Крылышки индейки' | 'Цех индеек №1' | '198,9'        | '1'     |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'  | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Бедро индейки' | 'Цех индеек №1' | '0,84'         | '1'     |
	И в таблице "РезультатПланирования" я перехожу к строке:
		| 'Номенклатура'   | 'Подразделение' | 'Сумма штрафа' | 'Штраф' |
		| 'Голень индейки' | 'Цех индеек №1' | '139,23'       | '1'     |
	И я перехожу к закладке "Ресурсы"
	И я перехожу к закладке "Доступные ресурсы разруба"
	И в таблице "РесурсыРазруба" я перехожу к строке:
		| 'Волна'    | 'Доступно, ч.' | 'Подразделение' | 'Ресурс'        |
		| '1. До 12' | '4'            | 'Цех индеек №1' | 'Убой(Индейки)' |
	И в таблице "РесурсыРазруба" поле с именем "РесурсыРазрубаИспользовано" имеет значение  "0,07005"
	И в таблице "РесурсыРазруба" я перехожу к строке:
		| 'Волна'    | 'Доступно, ч.' | 'Подразделение' | 'Ресурс'        |
		| '2. До 15' | '3'            | 'Цех индеек №1' | 'Убой(Индейки)' |
	И в таблице "РесурсыРазруба" поле с именем "РесурсыРазрубаИспользовано" имеет значение  "0,0525"
	И в таблице "РесурсыРазруба" я перехожу к строке:
		| 'Волна'    | 'Доступно, ч.' | 'Подразделение' | 'Ресурс'        |
		| '3. До 17' | '2'            | 'Цех индеек №1' | 'Убой(Индейки)' |
	И в таблице "РесурсыРазруба" поле с именем "РесурсыРазрубаИспользовано" имеет значение  "0,03495"
	И Я закрываю окно 'Планирование разруба'

Сценарий: 011.5. Разруб. Завершение

	И Я закрыл все окна клиентского приложения