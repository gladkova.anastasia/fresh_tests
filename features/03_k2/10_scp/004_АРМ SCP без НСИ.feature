#language: ru

@scp

Функционал: 004. SCP. АРМ планирования цепочек поставок

Сценарий: 004.1. SCP. АРМ планирования цепочек поставок без локальной НСИ

	Дано Я подключаюсь под "Планирование цепочек поставок"
	Когда В командном интерфейсе я выбираю 'Планирование' 'Планирование цепочек поставок'
	Тогда я жду открытия окна 'Планирование цепочек поставок [за все время]' в течение 20 секунд
	* Создание сценария
		И я нажимаю на кнопку создать поля "Сценарий"
		Тогда я жду открытия окна 'Сценарий планирования цепочек поставок (создание)' в течение 20 секунд
		И в поле 'Наименование' я ввожу текст 'Сценарий SCP'
		И я нажимаю на кнопку создать поля "План с"
		* Создание варианта начала
			Тогда я жду открытия окна 'Вариант начала периода (создание)' в течение 20 секунд
			И в поле 'Наименование' я ввожу текст '19.08.2020'
			И в поле 'Дата начала' я ввожу текст '19.08.2020  0:00:00'
			И я перехожу к следующему реквизиту
			Тогда элемент формы с именем "Наименование" стал равен '19.08.2020'
			И у элемента формы с именем "СтандартнаяДатаНачала" текст редактирования стал равен '19.08.2020  0:00:00'
			И элемент формы с именем "ПолучатьНачалоПериода" стал равен ''
			И элемент формы с именем "ПериодовСдвига" стал равен '0'
			И элемент формы с именем "Периодичность" стал равен ''
			И элемент формы с именем "ДатаРезультат" стал равен '19.08.2020 0:00:00'
			И я нажимаю на кнопку 'Записать и закрыть'
			И я жду закрытия окна 'Вариант начала периода (создание) *' в течение 20 секунд
		Тогда я жду открытия окна 'Сценарий планирования цепочек поставок (создание) *' в течение 20 секунд
		И в поле 'Периодов' я ввожу текст '8'
		И я перехожу к следующему реквизиту
		* Проверка сценария
			Тогда элемент формы с именем "Наименование" стал равен 'Сценарий SCP'
			И элемент формы с именем "Комментарий" стал равен ''
			И элемент формы с именем "ВариантНачала" стал равен '19.08.2020'
			И у элемента формы с именем "ПериодовПланирования" текст редактирования стал равен '8'
			И элемент формы с именем "Периодичность" стал равен 'День'
			И элемент формы с именем "ПримерПериода" стал равен '19.08.2020 - 26.08.2020'
		И я нажимаю на кнопку 'Записать и закрыть'
		И я жду закрытия окна 'Сценарий SCP (Сценарий планирования цепочек поставок)' в течение 20 секунд
	Тогда я жду открытия окна 'Планирование цепочек поставок [19.08.2020 - 26.08.2020]' в течение 20 секунд
	* Проверка АРМа после выбора сценария
		Тогда элемент формы с именем "Сценарий" стал равен 'Сценарий SCP'
		И элемент формы с именем "ВариантНачалаПериода" стал равен '19.08.2020'
		И элемент формы с именем "ПериодовПланирования" стал равен '8'
		И элемент формы с именем "ПериодичностьПланирования" стал равен 'День'
	* Вариант заполнения: Заполнить все потребности за период
		И из выпадающего списка "Потребности_ВариантЗаполнения" я выбираю точное значение 'Заполнить все потребности за период'
		И я нажимаю на кнопку 'Заполнить'
		И я нажимаю на кнопку с именем 'ФормаЗаполнить'
		Тогда таблица "Потребности" стала равной:
			| 'N'  | 'Номенклатура'                                | 'Характеристика' | 'План' | 'Начало периода' | 'Склад'              | 'Период'        | 'Волна'         |
			| '1'  | 'Ассорти из мяса цыплят-бройлеров /подложка/' | 'Ашан ООО, 01'   | '300'  | '24.08.20 00:00' | 'Основной склад ООО' | '24.08.20 (Пн)' | '1 [7:00-9:00]' |
			| '2'  | 'Ассорти из мяса цыплят-бройлеров /подложка/' | 'Ашан ООО, 01'   | '450'  | '25.08.20 00:00' | 'Основной склад ООО' | '25.08.20 (Вт)' | '1 [7:00-9:00]' |
			| '3'  | 'Ассорти из мяса цыплят-бройлеров /подложка/' | 'Ашан ООО, 01'   | '450'  | '26.08.20 00:00' | 'Основной склад ООО' | '26.08.20 (Ср)' | '1 [7:00-9:00]' |
			| '4'  | 'Цыплята 1 кат /подложка/'                    | 'Ашан ООО, 01'   | '300'  | '24.08.20 00:00' | 'Основной склад ООО' | '24.08.20 (Пн)' | '1 [7:00-9:00]' |
			| '5'  | 'Цыплята 1 кат /подложка/'                    | 'Ашан ООО, 01'   | '450'  | '25.08.20 00:00' | 'Основной склад ООО' | '25.08.20 (Вт)' | '1 [7:00-9:00]' |
			| '6'  | 'Цыплята 1 кат /подложка/'                    | 'Ашан ООО, 01'   | '450'  | '26.08.20 00:00' | 'Основной склад ООО' | '26.08.20 (Ср)' | '1 [7:00-9:00]' |
			| '7'  | 'Колбаски для жарки  "С сыром" /подл./'       | 'Ашан ООО, 01'   | '300'  | '24.08.20 00:00' | 'Основной склад ООО' | '24.08.20 (Пн)' | '1 [7:00-9:00]' |
			| '8'  | 'Колбаски для жарки  "С сыром" /подл./'       | 'Ашан ООО, 01'   | '450'  | '25.08.20 00:00' | 'Основной склад ООО' | '25.08.20 (Вт)' | '1 [7:00-9:00]' |
			| '9'  | 'Колбаски для жарки  "С сыром" /подл./'       | 'Ашан ООО, 01'   | '450'  | '26.08.20 00:00' | 'Основной склад ООО' | '26.08.20 (Ср)' | '1 [7:00-9:00]' |
			| '10' | 'Фарш куриный натуральный / подложка/'        | 'Ашан ООО, 01'   | '300'  | '24.08.20 00:00' | 'Основной склад ООО' | '24.08.20 (Пн)' | '1 [7:00-9:00]' |
			| '11' | 'Фарш куриный натуральный / подложка/'        | 'Ашан ООО, 01'   | '450'  | '25.08.20 00:00' | 'Основной склад ООО' | '25.08.20 (Вт)' | '1 [7:00-9:00]' |
			| '12' | 'Фарш куриный натуральный / подложка/'        | 'Ашан ООО, 01'   | '450'  | '26.08.20 00:00' | 'Основной склад ООО' | '26.08.20 (Ср)' | '1 [7:00-9:00]' |
		И таблица "ДвиженияТоваров" стала равной:
			| 'Номенклатура'                                | 'Ед.' | 'Характеристика' | 'Период'         | 'Склад'              | 'Приход' | 'На начало' | 'Расход' | 'На конец' | 'Источник'    | 'мин.' | 'рек.' | 'макс.' | 'Доступных схем обеспечения' |
			| 'Ассорти из мяса цыплят-бройлеров /подложка/' | 'кг'  | 'Ашан ООО, 01'   | '24.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '300'    | '-300'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Ассорти из мяса цыплят-бройлеров /подложка/' | 'кг'  | 'Ашан ООО, 01'   | '25.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '450'    | '-450'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Ассорти из мяса цыплят-бройлеров /подложка/' | 'кг'  | 'Ашан ООО, 01'   | '26.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '450'    | '-450'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Колбаски для жарки  "С сыром" /подл./'       | 'кг'  | 'Ашан ООО, 01'   | '24.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '300'    | '-300'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Колбаски для жарки  "С сыром" /подл./'       | 'кг'  | 'Ашан ООО, 01'   | '25.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '450'    | '-450'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Колбаски для жарки  "С сыром" /подл./'       | 'кг'  | 'Ашан ООО, 01'   | '26.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '450'    | '-450'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Фарш куриный натуральный / подложка/'        | 'кг'  | 'Ашан ООО, 01'   | '24.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '300'    | '-300'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Фарш куриный натуральный / подложка/'        | 'кг'  | 'Ашан ООО, 01'   | '25.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '450'    | '-450'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Фарш куриный натуральный / подложка/'        | 'кг'  | 'Ашан ООО, 01'   | '26.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '450'    | '-450'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Цыплята 1 кат /подложка/'                    | 'кг'  | 'Ашан ООО, 01'   | '24.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '300'    | '-300'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Цыплята 1 кат /подложка/'                    | 'кг'  | 'Ашан ООО, 01'   | '25.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '450'    | '-450'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Цыплята 1 кат /подложка/'                    | 'кг'  | 'Ашан ООО, 01'   | '26.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '450'    | '-450'     | 'Потребность' | ''     | ''     | ''      | ''                           |

	* Вариант заполнения: Заполнить потребности по оптимистичному сценарию
		И из выпадающего списка "Потребности_ВариантЗаполнения" я выбираю точное значение 'Заполнить потребности по сценарию'
		И из выпадающего списка "Потребности_ОтборПоСценарию" я выбираю по строке 'Оптимистичный сценарий'
		И я перехожу к следующему реквизиту
		И я нажимаю на кнопку с именем 'ПотребностиПотребности_Заполнить'
		И я нажимаю на кнопку с именем 'ФормаЗаполнить'
		Тогда у элемента формы с именем "Потребности_ВариантЗаполнения" текст редактирования стал равен 'Заполнить потребности по сценарию'
		Тогда элемент формы с именем "Потребности_ОтборПоСценарию" стал равен 'Оптимистичный сценарий'
		Тогда элемент формы с именем "Потребности_ОтборПоСценарию" присутствует на форме
		Тогда элемент формы с именем "Потребности_ОтборПоСценарию" доступен
		Тогда таблица формы с именем "Потребности" стала равной:
			| 'План' |
			| '200'  |
			| '300'  |
			| '300'  |
			| '200'  |
			| '300'  |
			| '300'  |
			| '200'  |
			| '300'  |
			| '300'  |
			| '200'  |
			| '300'  |
			| '300'  |
		Тогда таблица формы с именем "ДвиженияТоваров" стала равной:
			| 'Расход' | 'На конец' |
			| '200'    | '-200'     |
			| '300'    | '-300'     |
			| '300'    | '-300'     |
			| '200'    | '-200'     |
			| '300'    | '-300'     |
			| '300'    | '-300'     |
			| '200'    | '-200'     |
			| '300'    | '-300'     |
			| '300'    | '-300'     |
			| '200'    | '-200'     |
			| '300'    | '-300'     |
			| '300'    | '-300'     |
	* Вариант заполнения: Заполнить потребности по реалистичному сценарию
		И из выпадающего списка "Потребности_ВариантЗаполнения" я выбираю точное значение 'Заполнить потребности по сценарию'
		И из выпадающего списка "Потребности_ОтборПоСценарию" я выбираю по строке 'Реалистичный сценарий'
		И я перехожу к следующему реквизиту
		И я нажимаю на кнопку с именем 'ПотребностиПотребности_Заполнить'
		И я нажимаю на кнопку с именем 'ФормаЗаполнить'
		Тогда таблица формы с именем "Потребности" стала равной:
			| 'План' |
			| '100'  |
			| '150'  |
			| '150'  |
			| '100'  |
			| '150'  |
			| '150'  |
			| '100'  |
			| '150'  |
			| '150'  |
			| '100'  |
			| '150'  |
			| '150'  |
		Тогда элемент формы с именем "Потребности_ОтборПоСценарию" стал равен 'Реалистичный сценарий'
		Тогда таблица формы с именем "ДвиженияТоваров" стала равной:
			| 'Расход' | 'На конец' |
			| '100'    | '-100'     |
			| '150'    | '-150'     |
			| '150'    | '-150'     |
			| '100'    | '-100'     |
			| '150'    | '-150'     |
			| '150'    | '-150'     |
			| '100'    | '-100'     |
			| '150'    | '-150'     |
			| '150'    | '-150'     |
			| '100'    | '-100'     |
			| '150'    | '-150'     |
			| '150'    | '-150'     |

	* Вариант заполнения: По документу
		И из выпадающего списка "Потребности_ВариантЗаполнения" я выбираю точное значение 'Заполнить потребности по документу'
		И я нажимаю кнопку выбора у поля "Потребности_ОтборПоДокументу"
		Тогда я жду открытия окна 'Планы потребностей в отгрузке' в течение 20 секунд
		И в таблице "Список" я перехожу к строке:
			| 'Автор'                     | 'Комментарий'                 | 'План по'    | 'План с'     | 'Сценарий'               |
			| 'Планирование потребностей' | 'SCP. Оптимистичный сценарий' | '27.08.2020' | '24.08.2020' | 'Оптимистичный сценарий' |
		И я нажимаю на кнопку с именем 'ФормаВыбрать'
		Тогда я жду открытия окна 'Планирование цепочек поставок [19.08.2020 - 26.08.2020]' в течение 20 секунд
		И я нажимаю на кнопку с именем 'ПотребностиПотребности_Заполнить'
		И я нажимаю на кнопку с именем 'ФормаЗаполнить'
		Тогда таблица формы с именем "Потребности" стала равной:
			| 'План' |
			| '200'  |
			| '300'  |
			| '300'  |
			| '200'  |
			| '300'  |
			| '300'  |
			| '200'  |
			| '300'  |
			| '300'  |
			| '200'  |
			| '300'  |
			| '300'  |
		Тогда у элемента формы с именем "Потребности_ВариантЗаполнения" текст редактирования стал равен 'Заполнить потребности по документу'
		Тогда элемент формы с именем "Потребности_ОтборПоСценарию" отсутствует на форме
		Тогда элемент формы с именем "Потребности_ОтборПоСценарию" не доступен
		Тогда элемент формы с именем "Потребности_ОтборПоДокументу" присутствует на форме
		Тогда элемент формы с именем "Потребности_ОтборПоДокументу" доступен
		Тогда таблица формы с именем "ДвиженияТоваров" стала равной:
			| 'Расход' | 'На конец' |
			| '200'    | '-200'     |
			| '300'    | '-300'     |
			| '300'    | '-300'     |
			| '200'    | '-200'     |
			| '300'    | '-300'     |
			| '300'    | '-300'     |
			| '200'    | '-200'     |
			| '300'    | '-300'     |
			| '300'    | '-300'     |
			| '200'    | '-200'     |
			| '300'    | '-300'     |
			| '300'    | '-300'     |
	* Вариант заполнения: по нескольким сценариям
		И из выпадающего списка "Потребности_ВариантЗаполнения" я выбираю точное значение 'Заполнить потребности по нескольким сценариям'
		И я нажимаю кнопку выбора у поля "Потребности_ОтборПоНесколькимСценариям"
		Тогда я жду открытия окна 'Выбор сценариев' в течение 20 секунд
		И я нажимаю на кнопку с именем 'СписокУстановитьФлажки'
		И я нажимаю на кнопку 'ОК'
		Тогда я жду открытия окна 'Планирование цепочек поставок [19.08.2020 - 26.08.2020]' в течение 20 секунд
		И я нажимаю на кнопку с именем 'ПотребностиПотребности_Заполнить'
		И я нажимаю на кнопку с именем 'ФормаЗаполнить'
		Тогда таблица формы с именем "Потребности" стала равной:
			| 'План' |
			| '300'  |
			| '450'  |
			| '450'  |
			| '300'  |
			| '450'  |
			| '450'  |
			| '300'  |
			| '450'  |
			| '450'  |
			| '300'  |
			| '450'  |
			| '450'  |
		Тогда у элемента формы с именем "Потребности_ВариантЗаполнения" текст редактирования стал равен 'Заполнить потребности по нескольким сценариям'
		Тогда элемент формы с именем "Потребности_ОтборПоНесколькимСценариям" стал равен 'Оптимистичный сценарий; Реалистичный сценарий'
		Тогда элемент формы с именем "Потребности_ОтборПоНесколькимСценариям" присутствует на форме
		Тогда элемент формы с именем "Потребности_ОтборПоНесколькимСценариям" доступен
		Тогда элемент формы с именем "Потребности_ОтборПоДокументу" отсутствует на форме
		Тогда элемент формы с именем "Потребности_ОтборПоДокументу" не доступен
		Тогда таблица формы с именем "ДвиженияТоваров" стала равной:
			| 'Расход' | 'На конец' |
			| '300'    | '-300'     |
			| '450'    | '-450'     |
			| '450'    | '-450'     |
			| '300'    | '-300'     |
			| '450'    | '-450'     |
			| '450'    | '-450'     |
			| '300'    | '-300'     |
			| '450'    | '-450'     |
			| '450'    | '-450'     |
			| '300'    | '-300'     |
			| '450'    | '-450'     |
			| '450'    | '-450'     |

	* Вариант заполнения: по заказам
		И из выпадающего списка "Потребности_ВариантЗаполнения" я выбираю точное значение 'Заполнить по заказам'
		И я нажимаю на кнопку с именем 'ПотребностиПотребности_Заполнить'
		И я нажимаю на кнопку с именем 'ФормаЗаполнить'
		Тогда элемент формы с именем "Сценарий" стал равен 'Сценарий SCP'
		И элемент формы с именем "ВариантНачалаПериода" стал равен '19.08.2020'
		И элемент формы с именем "ПериодовПланирования" стал равен '8'
		И элемент формы с именем "ПериодичностьПланирования" стал равен 'День'
		И таблица "Потребности" стала равной:
			| 'N'  | 'Номенклатура'                                | 'Характеристика' | 'План' | 'Начало периода' | 'Склад'              | 'Период'        | 'Волна'         |
			| '1'  | 'Ассорти из мяса цыплят-бройлеров /подложка/' | 'Ашан ООО, 01'   | '100'  | '24.08.20 00:00' | 'Основной склад ООО' | '24.08.20 (Пн)' | '1 [7:00-9:00]' |
			| '2'  | 'Ассорти из мяса цыплят-бройлеров /подложка/' | 'Ашан ООО, 01'   | '150'  | '25.08.20 00:00' | 'Основной склад ООО' | '25.08.20 (Вт)' | '1 [7:00-9:00]' |
			| '3'  | 'Ассорти из мяса цыплят-бройлеров /подложка/' | 'Ашан ООО, 01'   | '150'  | '26.08.20 00:00' | 'Основной склад ООО' | '26.08.20 (Ср)' | '1 [7:00-9:00]' |
			| '4'  | 'Цыплята 1 кат /подложка/'                    | 'Ашан ООО, 01'   | '100'  | '24.08.20 00:00' | 'Основной склад ООО' | '24.08.20 (Пн)' | '1 [7:00-9:00]' |
			| '5'  | 'Цыплята 1 кат /подложка/'                    | 'Ашан ООО, 01'   | '150'  | '25.08.20 00:00' | 'Основной склад ООО' | '25.08.20 (Вт)' | '1 [7:00-9:00]' |
			| '6'  | 'Цыплята 1 кат /подложка/'                    | 'Ашан ООО, 01'   | '150'  | '26.08.20 00:00' | 'Основной склад ООО' | '26.08.20 (Ср)' | '1 [7:00-9:00]' |
			| '7'  | 'Колбаски для жарки  "С сыром" /подл./'       | 'Ашан ООО, 01'   | '100'  | '24.08.20 00:00' | 'Основной склад ООО' | '24.08.20 (Пн)' | '1 [7:00-9:00]' |
			| '8'  | 'Колбаски для жарки  "С сыром" /подл./'       | 'Ашан ООО, 01'   | '150'  | '25.08.20 00:00' | 'Основной склад ООО' | '25.08.20 (Вт)' | '1 [7:00-9:00]' |
			| '9'  | 'Колбаски для жарки  "С сыром" /подл./'       | 'Ашан ООО, 01'   | '150'  | '26.08.20 00:00' | 'Основной склад ООО' | '26.08.20 (Ср)' | '1 [7:00-9:00]' |
			| '10' | 'Фарш куриный натуральный / подложка/'        | 'Ашан ООО, 01'   | '100'  | '24.08.20 00:00' | 'Основной склад ООО' | '24.08.20 (Пн)' | '1 [7:00-9:00]' |
			| '11' | 'Фарш куриный натуральный / подложка/'        | 'Ашан ООО, 01'   | '150'  | '25.08.20 00:00' | 'Основной склад ООО' | '25.08.20 (Вт)' | '1 [7:00-9:00]' |
			| '12' | 'Фарш куриный натуральный / подложка/'        | 'Ашан ООО, 01'   | '150'  | '26.08.20 00:00' | 'Основной склад ООО' | '26.08.20 (Ср)' | '1 [7:00-9:00]' |

		И у элемента формы с именем "Потребности_ВариантЗаполнения" текст редактирования стал равен 'Заполнить по заказам'
		И таблица "ДвиженияТоваров" стала равной:
			| 'Номенклатура'                                | 'Ед.' | 'Характеристика' | 'Период'         | 'Склад'              | 'Приход' | 'На начало' | 'Расход' | 'На конец' | 'Источник'    | 'мин.' | 'рек.' | 'макс.' | 'Доступных схем обеспечения' |
			| 'Ассорти из мяса цыплят-бройлеров /подложка/' | 'кг'  | 'Ашан ООО, 01'   | '24.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '100'    | '-100'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Ассорти из мяса цыплят-бройлеров /подложка/' | 'кг'  | 'Ашан ООО, 01'   | '25.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '150'    | '-150'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Ассорти из мяса цыплят-бройлеров /подложка/' | 'кг'  | 'Ашан ООО, 01'   | '26.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '150'    | '-150'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Колбаски для жарки  "С сыром" /подл./'       | 'кг'  | 'Ашан ООО, 01'   | '24.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '100'    | '-100'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Колбаски для жарки  "С сыром" /подл./'       | 'кг'  | 'Ашан ООО, 01'   | '25.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '150'    | '-150'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Колбаски для жарки  "С сыром" /подл./'       | 'кг'  | 'Ашан ООО, 01'   | '26.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '150'    | '-150'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Фарш куриный натуральный / подложка/'        | 'кг'  | 'Ашан ООО, 01'   | '24.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '100'    | '-100'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Фарш куриный натуральный / подложка/'        | 'кг'  | 'Ашан ООО, 01'   | '25.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '150'    | '-150'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Фарш куриный натуральный / подложка/'        | 'кг'  | 'Ашан ООО, 01'   | '26.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '150'    | '-150'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Цыплята 1 кат /подложка/'                    | 'кг'  | 'Ашан ООО, 01'   | '24.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '100'    | '-100'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Цыплята 1 кат /подложка/'                    | 'кг'  | 'Ашан ООО, 01'   | '25.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '150'    | '-150'     | 'Потребность' | ''     | ''     | ''      | ''                           |
			| 'Цыплята 1 кат /подложка/'                    | 'кг'  | 'Ашан ООО, 01'   | '26.08.20 07:00' | 'Основной склад ООО' | ''       | ''          | '150'    | '-150'     | 'Потребность' | ''     | ''     | ''      | ''                           |

		Тогда в таблице "ПлановыеДвижения" количество строк "равно" 0
		Тогда в таблице "Лог" количество строк "равно" 0
