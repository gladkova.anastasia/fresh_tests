﻿#language: ru

@OrdersConfirmation

Функционал: 003. OrdersConfirmation. Создание настроек формирования планов отгрузки

Сценарий: 003.1. OrdersConfirmation. Создание настроек формирования планов отгрузки - без этапов

	Дано Я подключаюсь под "Администратор"
	Дано я удаляю объекты "Справочники.к2НастройкиФормированияПлановОтгрузки" без контроля ссылок
	И Я начинаю создавать настройку формирования планов отгрузки "Настройки с обрезкой без этапов"
	И Я создаю в поле "Дата отгрузки" вариант начала на "11.10.2020"
	И Я создаю в поле "Дата работы" вариант начала на "09.10.2020"
	И Я создаю в поле "Дата заявки" вариант начала на "10.10.2020"
	И я устанавливаю флаг с именем 'ПравилоОкругления'
	И Я добавляю склад отгрузки "Сырная ГП" в настройку формирования планов отгрузки
	И Я добавляю точку доставки "Ашан. Скидочный регион" в настройку формирования планов отгрузки
	И Я добавляю склад получатель "Куриная ГП" в настройку формирования планов отгрузки
	И Я добавляю товарную категорию "Мягкие сыры" в настройку формирования планов отгрузки
	И Я добавляю товарную категорию "Мясо птицы" в настройку формирования планов отгрузки
	И Я добавляю товарную категорию "Полутвердые сыры" в настройку формирования планов отгрузки
	* Установка сценария заявки на производство
		И из выпадающего списка с именем "СценарийЗаявки" я выбираю по строке 'Сценарий планирования заявок на производство 1'
	И Я заканчиваю создавать настройку формирования планов отгрузки

Сценарий: 003.2. OrdersConfirmation. Создание настроек формирования планов отгрузки - без этапов (для проверки заполнения АРМа без заявки с выбранным сценарием)

	И Я начинаю создавать настройку формирования планов отгрузки "Настройки с обрезкой без этапов - без заявки"
	И Я создаю в поле "Дата отгрузки" вариант начала на "11.10.2020"
	И Я создаю в поле "Дата работы" вариант начала на "09.10.2020"
	И Я создаю в поле "Дата заявки" вариант начала на "10.10.2020"
	И Я добавляю склад отгрузки "Сырная ГП" в настройку формирования планов отгрузки
	И Я добавляю точку доставки "Ашан. Скидочный регион" в настройку формирования планов отгрузки
	И Я добавляю склад получатель "Куриная ГП" в настройку формирования планов отгрузки
	И Я добавляю товарную категорию "Мягкие сыры" в настройку формирования планов отгрузки
	И Я добавляю товарную категорию "Мясо птицы" в настройку формирования планов отгрузки
	И Я добавляю товарную категорию "Полутвердые сыры" в настройку формирования планов отгрузки
	* Установка сценария заявки на производство
		И из выпадающего списка с именем "СценарийЗаявки" я выбираю по строке 'Сценарий планирования заявок на производство 3'
	И Я заканчиваю создавать настройку формирования планов отгрузки

Сценарий: 003.3. OrdersConfirmation. Создание настроек формирования планов отгрузки - с этапами

	И Я начинаю создавать настройку формирования планов отгрузки "Настройки с обрезкой по этапам"
	И Я создаю в поле "Дата отгрузки" вариант начала на "18.10.2020"
	И Я создаю в поле "Дата работы" вариант начала на "16.10.2020"
	И Я создаю в поле "Дата заявки" вариант начала на "17.10.2020"
	* Установка этапа для чтения заявки
		И из выпадающего списка с именем "ЭтапЗаявкиДляЧтения" я выбираю точное значение 'Подтвержденный - Заявка на производство'
	* Установка этапов для чтения и записи для заказа клиента
		И из выпадающего списка с именем "ЭтапЗаказаКлиентаДляЧтения" я выбираю точное значение 'Исходный - Заказ клиента'
		И из выпадающего списка с именем "ЭтапЗаказаКлиентаДляЗаписи" я выбираю точное значение 'Подтвержденный - Заказ клиента'
	* Установка состояния для заказа клиента
		И из выпадающего списка с именем "СостояниеЗаказаКлиента" я выбираю точное значение 'Заказ клиента подтвержден'
	* Установка этапов для чтения и записи для заказа на перемещение
		И из выпадающего списка с именем 'ЭтапЗаказаНаПеремещениеДляЧтения' я выбираю точное значение 'Исходный - Заказ на перемещение'
		И из выпадающего списка с именем "ЭтапЗаказаНаПеремещениеДляЗаписи" я выбираю точное значение 'Подтвержденный - Заказ на перемещение'
	* Установка состояния для заказа на перемещение
		И из выпадающего списка с именем "СостояниеЗаказаНаПеремещение" я выбираю точное значение 'Подтвержден - перемещение'
	* Установка сценария заявки на производство
		И из выпадающего списка с именем "СценарийЗаявки" я выбираю по строке 'Сценарий планирования заявок на производство 2'
	И Я добавляю склад отгрузки "Сырная ГП" в настройку формирования планов отгрузки
	И Я добавляю точку доставки "Ашан. Скидочный регион" в настройку формирования планов отгрузки
	И Я добавляю склад получатель "Куриная ГП" в настройку формирования планов отгрузки
	И Я добавляю товарную категорию "Мягкие сыры" в настройку формирования планов отгрузки
	И Я добавляю товарную категорию "Мясо птицы" в настройку формирования планов отгрузки
	И Я добавляю товарную категорию "Полутвердые сыры" в настройку формирования планов отгрузки
	И Я заканчиваю создавать настройку формирования планов отгрузки

Сценарий: 003.4. OrdersConfirmation. Завершение

	И Я закрыл все окна клиентского приложения