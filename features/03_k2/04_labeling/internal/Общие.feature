﻿#language: ru

@ignore
@labeling
@exportscenarios

Функционал: Экспортные сценарии

Сценарий: Я фиксирую шаблон этикетки упаковки "ИмяШаблона" для номенклатуры "ИмяНоменклатуры" с характеристикой "ИмяХарактеристики" и упаковкой "ИмяУпаковки"
	
	И в таблице "Список" я перехожу к строке:
		| 'Номенклатура'    | 'Характеристика'    | 'Упаковка'    |
		| 'ИмяНоменклатуры' | 'ИмяХарактеристики' | 'ИмяУпаковки' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Упаковки характеристик'
	И из выпадающего списка "Шаблон этикетки упаковки" я выбираю по строке 'ИмяШаблона'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Упаковки характеристик *' в течение 20 секунд
	Тогда открылось окно 'Упаковки характеристик'