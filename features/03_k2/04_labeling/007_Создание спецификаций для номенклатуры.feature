#language: ru

@labeling

Функционал: 007. Labeling. Создание спецификаций для номенклатуры

Сценарий: 007.1. Labeling. Создание спецификации для номенклатуры

	Дано Я подключаюсь под "Администратор"
	Дано я удаляю объекты "Справочники.к2РесурсныеСпецификации" без контроля ссылок
	Дано я удаляю все элементы таблицы "РегистрСведений.к2ЯчейкиХраненияПодпиткиНоменклатурыПоУмолчанию"
	Дано я удаляю все элементы таблицы "РегистрСведений.к2ОсновныеСпецификации"
	Когда В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Номенклатура'
	Тогда я жду открытия окна 'Номенклатура' в течение 20 секунд
	И в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Сыр Сулугуни' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Сыр Сулугуни (Номенклатура)'
	И я нажимаю на кнопку 'Основные спецификации'
	Тогда открылось окно 'Основные спецификации'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Основная спецификация (создание)'
	И из выпадающего списка "Характеристика" я выбираю по строке 'Ашан'
	И я нажимаю кнопку выбора у поля "Спецификация"
	Тогда открылось окно 'Ресурсные спецификации'
	И я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Выберите форму для открытия:'
	И я нажимаю на кнопку 'Стандартная'
	И я жду открытия окна 'Ресурсные спецификации (создание)' в течение 20 секунд
	И я активизирую окно "Ресурсные спецификации (создание)"
	И в поле 'Наименование' я ввожу текст 'Сыр Сулугуни(Ашан)'
	И в таблице "ВыходныеИзделия" я нажимаю на кнопку с именем 'ВыходныеИзделияДобавить'
	И в таблице "ВыходныеИзделия" я изменяю флаг с именем 'ВыходныеИзделияОсновное'
	И в таблице "ВыходныеИзделия" из выпадающего списка с именем "ВыходныеИзделияНоменклатура" я выбираю по строке 'Сыр Сулугуни'
	И в таблице "ВыходныеИзделия" из выпадающего списка с именем "ВыходныеИзделияХарактеристика" я выбираю по строке 'Ашан'
	И в таблице "ВыходныеИзделия" в поле с именем 'ВыходныеИзделияКоличество' я ввожу текст '1,000'
	И я перехожу к закладке "Материалы и работы"
	И в таблице "МатериалыИУслуги" я нажимаю на кнопку с именем 'МатериалыИУслугиДобавить'
	И в таблице "МатериалыИУслуги" я изменяю флаг с именем 'МатериалыИУслугиОсновное'
	И в таблице "МатериалыИУслуги" из выпадающего списка с именем "МатериалыИУслугиНоменклатура" я выбираю по строке 'Сыр Сулугуни'
	И в таблице "МатериалыИУслуги" из выпадающего списка с именем "МатериалыИУслугиХарактеристика" я выбираю по строке '<Без маркировки>'
	И в таблице "МатериалыИУслуги" в поле с именем 'МатериалыИУслугиКоличество' я ввожу текст '1,000'
	И в таблице "МатериалыИУслуги" я завершаю редактирование строки
	И я перехожу к закладке "Производственный процесс"
	И я нажимаю на кнопку 'Записать'
	И в таблице "ЭтапыСписок" я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Этапы производства (создание)'
	И в поле 'Наименование' я ввожу текст 'Маркировка'
	И из выпадающего списка "Вид РЦ" я выбираю по строке 'Маркировка(склад)'
	И в таблице "Операции" я нажимаю на кнопку с именем 'ОперацииДобавить'
	И в таблице "Операции" я нажимаю кнопку выбора у реквизита с именем "ОперацииОперация"
	И я жду открытия окна 'Технологические операции' в течение 20 секунд
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Технологическая операция (создание)'
	И в поле 'Наименование' я ввожу текст 'Упаковка'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду открытия окна 'Технологические операции' в течение 20 секунд
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Этапы производства (создание) *'
	И я перехожу к закладке "Рабочие центры"
	И в таблице "РабочиеЦентры" я нажимаю на кнопку с именем 'РабочиеЦентрыДобавить'
	И в таблице "РабочиеЦентры" из выпадающего списка "Рабочий центр" я выбираю по строке 'Маркировка №1'
	И в таблице "РабочиеЦентры" я нажимаю на кнопку с именем 'РабочиеЦентрыДобавить'
	И в таблице "РабочиеЦентры" из выпадающего списка "Рабочий центр" я выбираю по строке 'Маркировка №2'
	И в поле 'N след. этапа' я ввожу текст '0'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Этапы производства (создание) *' в течение 20 секунд
	Тогда открылось окно 'Сыр Сулугуни(Ашан) (Ресурсные спецификации)'
	И я перехожу к закладке "Материалы и работы"
	И в таблице "МатериалыИУслуги" я выбираю текущую строку
	И в таблице "МатериалыИУслуги" из выпадающего списка с именем "МатериалыИУслугиЭтап" я выбираю по строке 'Маркировка'
	И в таблице "МатериалыИУслуги" я завершаю редактирование строки
	И я перехожу к закладке "Выходные изделия"
	И в таблице "ВыходныеИзделия" я выбираю текущую строку
	И в таблице "ВыходныеИзделия" из выпадающего списка с именем "ВыходныеИзделияЭтап" я выбираю по строке 'Маркировка'
	И в таблице "ВыходныеИзделия" я завершаю редактирование строки
	И я жду доступности элемента "Действует с" в течение 20 секунд
	И в поле "Действует с" я ввожу текст '18.10.2019'
	Тогда элемент формы "Действует с" стал равен '18.10.2019'
	И я нажимаю на кнопку 'Действует'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Сыр Сулугуни (Ресурсные спецификации)' в течение 20 секунд
	Тогда открылось окно 'Ресурсные спецификации'
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Основная спецификация (создание) *'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Основная спецификация (создание) *' в течение 20 секунд
	Тогда открылось окно 'Основные спецификации'
	И Я закрываю окно 'Основные спецификации'
	Тогда открылось окно 'Сыр Сулугуни (Номенклатура)'
	И я нажимаю на кнопку 'Ячейки хранения подпитки номенклатуры (по умолчанию)'
	Тогда открылось окно 'Ячейки хранения подпитки номенклатуры (по умолчанию)'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Ячейки хранения подпитки номенклатуры (по умолчанию) (создание)'
	И из выпадающего списка "Характеристика" я выбираю по строке '<Без маркировки>'
	И я нажимаю кнопку выбора у поля "Ячейка"
	Тогда открылось окно 'Складские ячейки'
	И в таблице "Список" я перехожу к строке:
		| 'Адрес'   | 'Описание'   | 'Склад'                           |
		| '1010011' | '1-01-001-1' | 'Склад немаркированной продукции' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Ячейки хранения подпитки номенклатуры (по умолчанию) (создание) *'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Ячейки хранения подпитки номенклатуры (по умолчанию) (создание) *' в течение 20 секунд
	Тогда открылось окно 'Ячейки хранения подпитки номенклатуры (по умолчанию)'
	И Я закрываю окно 'Ячейки хранения подпитки номенклатуры (по умолчанию)'
	Тогда открылось окно 'Сыр Сулугуни (Номенклатура)'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Сыр Сулугуни (Номенклатура)' в течение 20 секунд
	Тогда открылось окно 'Номенклатура'
	И Я закрываю окно 'Номенклатура'

Сценарий: 007.2. Labeling. Завершение

	И Я закрыл все окна клиентского приложения