﻿#language: ru

@labeling

Функционал: 010. Labeling. Установка функциональных опций Маркировка

Сценарий: 010.1. Labeling. Установка функциональных опций Маркировка

	Дано Я подключаюсь под "Администратор"
	И Я читаю лицензию "Маркировка.xml"
	И флаг "Анализ продаж доступен" равен "Нет"
	И флаг "Анализ сервисов предприятия" равен "Нет"
	И флаг "Маркировка" равен "Да"
	И флаг "Мобильное приложение" равен "Нет"
	И флаг "Обработка заказов клиента" равен "Нет"
	И флаг "Планирование переработки" равен "Нет"
	И флаг "Планирование продаж" равен "Нет"
	И флаг "Планирование разруба" равен "Нет"
	И флаг "Прогнозирование отгрузок" равен "Нет"
	И флаг "Приемка сырья" равен "Нет"
	И флаг "Приемка сырья (молочное)" равен "Нет"
	И флаг "Производство" равен "Нет"
	И флаг "Производство (молочное)" равен "Нет"
	И флаг "Производство (мясное)" равен "Нет"
	И флаг "Складская логистика" равен "Нет"
	И флаг "Транспортная логистика" равен "Нет"
	И флаг "Трейд-маркетинг" равен "Нет"
	И флаг "Управление продажами" равен "Нет"
	И флаг "Ценообразование" равен "Нет"
	И флаг "Интеграция с ГИС Меркурий" равен "Нет"
	И флаг "Управление процессами" равен "Нет"
	И Я закрываю окно 'Лицензирование'

Сценарий: 010.2. Labeling. Завершение

	И Я закрыл все окна клиентского приложения