﻿#language: ru

@labeling

Функционал: 006. Labeling. Создание учетных точек

Сценарий: 006.1. Labeling. Создание учетной точки Маркировка №1

	Дано Я подключаюсь под "Администратор"
	Дано я удаляю объекты "Справочники.к2УчетныеТочки" без контроля ссылок
	Дано я удаляю объекты "Справочники.к2КнопкиУчетныхТочек" без контроля ссылок
	И В командном интерфейсе я выбираю 'Администрирование' 'Учетные точки'
	Тогда открылось окно 'Учетные точки'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Учетные точки (создание)'
	И в поле с именем 'Наименование' я ввожу текст 'Маркировка №1'
	И я нажимаю на кнопку 'Записать'
	Тогда открылось окно 'Маркировка №1 (Учетные точки)'
	И в таблице "СписокКнопки" я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Кнопка учетной точки (создание)'
	И в поле 'Наименование' я ввожу текст 'Возврат излишков маркировка'
	И из выпадающего списка "Учетная точка" я выбираю по строке 'Маркировка №1'
	И я нажимаю кнопку выбора у поля "Обработка"
	Тогда открылось окно 'Форма выбора обработок'
	И в таблице "Список" я перехожу к строке:
		| 'Обработка'             |
		| 'Терминал сбора данных' |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Кнопка учетной точки (создание) *'
	И из выпадающего списка "Операция" я выбираю точное значение 'Возврат излишков (Маркировка)'
	И я устанавливаю флаг 'Отображать задания в виде дерева'
	И я устанавливаю флаг 'Проверить соответствие плана и факта'
	И из выпадающего списка "Рабочий центр" я выбираю по строке 'Маркировка №1'
	И из выпадающего списка "Организация" я выбираю по строке 'Организация'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Кнопка учетной точки (создание) *' в течение 20 секунд
	Тогда открылось окно 'Маркировка №1 (Учетные точки)'
	И в таблице "СписокКнопки" я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Кнопка учетной точки (создание)'
	И в поле 'Наименование' я ввожу текст 'Выпуск маркировка'
	И из выпадающего списка "Учетная точка" я выбираю по строке 'Маркировка №1'
	И я нажимаю кнопку выбора у поля "Обработка"
	Тогда открылось окно 'Форма выбора обработок'
	И в таблице "Список" я перехожу к строке:
		| 'Обработка'             |
		| 'Терминал сбора данных' |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Кнопка учетной точки (создание) *'
	И из выпадающего списка "Операция" я выбираю точное значение 'Выпуск (Маркировка)'
	И я устанавливаю флаг 'Проверить сканируемую номенклатуру'
	И я устанавливаю флаг 'Отображать задания в виде дерева'
	И я устанавливаю флаг 'Проверить соответствие плана и факта'
	И из выпадающего списка "Рабочий центр" я выбираю по строке 'Маркировка №1'
	И из выпадающего списка "Организация" я выбираю по строке 'Организация'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Кнопка учетной точки (создание) *' в течение 20 секунд
	Тогда открылось окно 'Маркировка №1 (Учетные точки)'
	И в таблице "СписокКнопки" я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Кнопка учетной точки (создание)'
	И в поле 'Наименование' я ввожу текст 'Палетирование'
	И из выпадающего списка "Учетная точка" я выбираю по строке 'Маркировка №1'
	И я нажимаю кнопку выбора у поля "Обработка"
	Тогда открылось окно 'Форма выбора обработок'
	И в таблице "Список" я перехожу к строке:
		| 'Обработка'             |
		| 'Терминал сбора данных' |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Кнопка учетной точки (создание) *'
	И из выпадающего списка "Операция" я выбираю точное значение 'Палетирование'
	И из выпадающего списка "Рабочий центр" я выбираю по строке 'Маркировка №1'
	И из выпадающего списка "Склад" я выбираю по строке 'Склад маркировки'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Кнопка учетной точки (создание) *' в течение 20 секунд
	Тогда открылось окно 'Маркировка №1 (Учетные точки)'
	И в таблице "СписокКнопки" я нажимаю на кнопку 'Создать'
	Тогда открылось окно 'Кнопка учетной точки (создание)'
	И в поле 'Наименование' я ввожу текст 'Приемка на маркировку'
	И из выпадающего списка "Учетная точка" я выбираю по строке 'Маркировка №1'
	И я нажимаю кнопку выбора у поля "Обработка"
	Тогда открылось окно 'Форма выбора обработок'
	И в таблице "Список" я перехожу к строке:
		| 'Обработка'             |
		| 'Терминал сбора данных' |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Кнопка учетной точки (создание) *'
	И из выпадающего списка "Операция" я выбираю точное значение 'Приемка (Маркировка)'
	И я устанавливаю флаг 'Проверить соответствие плана и факта'
	И я устанавливаю флаг 'Отображать задания в виде дерева'
	И из выпадающего списка "Рабочий центр" я выбираю по строке 'Маркировка №1'
	И из выпадающего списка "Организация" я выбираю по строке 'Организация'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Кнопка учетной точки (создание) *' в течение 20 секунд
	Тогда открылось окно 'Маркировка №1 (Учетные точки)'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Маркировка (Учетные точки)' в течение 20 секунд
	Тогда открылось окно 'Учетные точки'
	И Я закрываю окно 'Учетные точки'

Сценарий: 006.2. Labeling. Создание учетной точки Маркировка №2
	
	И В командном интерфейсе я выбираю 'Администрирование' 'Учетные точки'
	Тогда открылось окно 'Учетные точки'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Учетные точки (создание)'
	И в поле с именем 'Наименование' я ввожу текст 'Маркировка №2'
	И я нажимаю на кнопку 'Записать'
	Тогда открылось окно 'Маркировка №2 (Учетные точки)'
	И в таблице "СписокКнопки" я нажимаю на кнопку с именем 'СписокКнопкиСоздать'
	Тогда открылось окно 'Кнопка учетной точки (создание)'
	И в поле 'Наименование' я ввожу текст 'Стационарная маркировка'
	И я нажимаю кнопку выбора у поля "Обработка"
	Тогда открылось окно 'Форма выбора обработок'
	И в таблице "Список" я перехожу к строке:
		| 'Обработка'               |
		| 'Стационарная маркировка' |
	И я нажимаю на кнопку 'Выбрать'
	Тогда открылось окно 'Кнопка учетной точки (создание) *'
	И я перехожу к закладке "Настройки"
	И из выпадающего списка "Рабочий центр" я выбираю по строке 'Маркировка №2'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Кнопка учетной точки (создание) *' в течение 20 секунд
	Тогда открылось окно 'Маркировка №2 (Учетные точки)'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Маркировка (Учетные точки)' в течение 20 секунд
	Тогда открылось окно 'Учетные точки'
	И Я закрываю окно 'Учетные точки'

Сценарий: 006.3. Labeling. Завершение

	И Я закрыл все окна клиентского приложения	