#language: ru

@logistic

Функционал: 012. Logistic. Создание документов вручную для склада с ячейками 1

Сценарий: 012.1. Logistic. Создание распоряжения на приемку для склада с ячейками 1 (документ) (проверка работы всех кнопок)
	
	Дано Я подключаюсь под "Администратор"
	# Удаление документов списания и оприходования для корректировки остатков на адресном складе
	Дано я удаляю объекты "Документы.к2ОприходованиеТоваров" без контроля ссылок
	Дано я удаляю объекты "Документы.к2СписаниеТоваров" без контроля ссылок
	И В командном интерфейсе я выбираю 'Склад и доставка' 'Распоряжения на приемку'
	Тогда открылось окно 'Распоряжения на приемку'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Распоряжение на приемку (создание)'
	И из выпадающего списка "Склад" я выбираю по строке 'Склад(ячейки и контроль)'
	И из выпадающего списка "Ответственный" я выбираю точное значение 'Администратор'
	И в поле с именем 'Дата' я ввожу текст '23.09.2019  0:00:00'
	И я перехожу к закладке "Товары"
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" из выпадающего списка "Номенклатура" я выбираю по строке 'Сыр Адыгейский'
	И в таблице "Товары" из выпадающего списка "Характеристика" я выбираю по строке 'Ашан'
	И в таблице "Товары" из выпадающего списка "Серия" я выбираю по строке '000001 до 27.10.2020 0:00:00'
	И в таблице "Товары" в поле 'Количество' я ввожу текст '10,000'
	И в таблице "Товары" из выпадающего списка "Ячейка" я выбираю по строке '1020011'
	И в таблице "Товары" я завершаю редактирование строки
	Тогда таблица "Товары" стала равной:
		| 'Количество' | 'Ед.'                | 'Номенклатура'   | 'Характеристика' | 'Серия'                        | 'В ед. хранения' | 'Ячейка'  |
		| '10,000'     | 'упак (5 кг) (Ашан)' | 'Сыр Адыгейский' | 'Ашан'           | '000001 до 27.10.2020 0:00:00' | '50,000, кг'     | '1020011' |
	И в таблице "Товары" из выпадающего списка "Ед." я выбираю по строке 'упак (2.5 кг) 5ка'
	И в таблице "Товары" я завершаю редактирование строки
	Тогда таблица "Товары" стала равной:
		| 'Количество' | 'Ед.'               | 'Номенклатура'   | 'Характеристика' | 'Серия'                        | 'В ед. хранения' | 'Ячейка'  |
		| '10,000'     | 'упак (2.5 кг) 5ка' | 'Сыр Адыгейский' | 'Ашан'           | '000001 до 27.10.2020 0:00:00' | '25,000, кг'     | '1020011' |
	И в таблице "Товары" я активизирую поле "Номенклатура"
	И в таблице "Товары" я нажимаю на кнопку 'Копировать строки'
	И я выбираю пункт контекстного меню с именем 'ТоварыКонтекстноеМенюУдалить' на элементе формы с именем "Товары"
	И в таблице "Товары" я нажимаю на кнопку 'Вставить строки'
	И в таблице "Товары" я завершаю редактирование строки
	Тогда таблица "Товары" стала равной:
		| 'Количество' | 'Ед.'               | 'Номенклатура'   | 'Характеристика' | 'Серия'                        | 'В ед. хранения' | 'Ячейка'  |
		| '10,000'     | 'упак (2.5 кг) 5ка' | 'Сыр Адыгейский' | 'Ашан'           | '000001 до 27.10.2020 0:00:00' | '25,000, кг'     | '1020011' |
	И в таблице "Товары" я нажимаю на кнопку 'Подобрать'
	Тогда открылось окно 'Подбор номенклатуры'
	И я нажимаю на кнопку 'Иерархия'
	Если в таблице "Список" нет строк Тогда
		| 'Наименование'               |
		| 'Крылья куринные (подложка)' |
		| 'Сыр Адыгейский'             |
		| 'Сыр Костромской'            |
		| 'Сыр Пошехонский'            |
		И я нажимаю на кнопку 'Иерархия'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'    |
		| 'Сыр Пошехонский' |
	И в таблице "Список" я выбираю текущую строку
	И в таблице "ПодобраннаяНоменклатура" я перехожу к строке:
		| 'Номенклатура'    |
		| 'Сыр Пошехонский' |
	И в таблице "ПодобраннаяНоменклатура" из выпадающего списка "Характеристика" я выбираю по строке 'Перекресток'
	И в таблице "ПодобраннаяНоменклатура" из выпадающего списка "Упаковка" я выбираю по строке 'упак (1 кг)'
	И в таблице "ПодобраннаяНоменклатура" я завершаю редактирование строки
	Тогда таблица "ПодобраннаяНоменклатура" стала равной:
		| 'Номенклатура'    | 'Характеристика' | 'Упаковка'          |
		| 'Сыр Адыгейский'  | 'Ашан'           | 'упак (2.5 кг) 5ка' |
		| 'Сыр Пошехонский' | 'Перекресток'    | 'упак (1 кг)'       |
	И я нажимаю на кнопку "Перенести"
	Тогда открылось окно 'Распоряжение на приемку (создание) *'
	И в таблице "Товары" я перехожу к строке:
		| 'Номенклатура'    |
		| 'Сыр Пошехонский' |
	И в таблице "Товары" из выпадающего списка "Серия" я выбираю по строке '000001 до 27.10.2020 0:00:00'
	И в таблице "Товары" в поле 'Количество' я ввожу текст '20,000'
	И в таблице "Товары" из выпадающего списка "Ячейка" я выбираю по строке '1020011'
	И в таблице "Товары" я завершаю редактирование строки
	Тогда таблица "Товары" стала равной:
		| 'Количество' | 'Ед.'               | 'Номенклатура'    | 'Характеристика' | 'Серия'                        | 'В ед. хранения' | 'Ячейка'  |
		| '10,000'     | 'упак (2.5 кг) 5ка' | 'Сыр Адыгейский'  | 'Ашан'           | '000001 до 27.10.2020 0:00:00' | '25,000, кг'     | '1020011' |
		| '20,000'     | 'упак (1 кг)'       | 'Сыр Пошехонский' | 'Перекресток'    | '000001 до 27.10.2020 0:00:00' | '20,000, кг'     | '1020011' |
	И в таблице "Товары" я перехожу к строке:
		| 'Номенклатура'    |
		| 'Сыр Пошехонский' |
	И в таблице "Товары" я нажимаю на кнопку 'Разбить строку'
	Тогда открылось окно 'Введите количество товара в новой строке'
	И в поле 'InputFld' я ввожу текст '15,000'
	И я нажимаю на кнопку 'OK'
	Тогда таблица "Товары" стала равной:
		| 'Количество' | 'Ед.'               | 'Номенклатура'    | 'Характеристика' | 'Серия'                        | 'В ед. хранения' | 'Ячейка'  |
		| '10,000'     | 'упак (2.5 кг) 5ка' | 'Сыр Адыгейский'  | 'Ашан'           | '000001 до 27.10.2020 0:00:00' | '25,000, кг'     | '1020011' |
		| '5,000'      | 'упак (1 кг)'       | 'Сыр Пошехонский' | 'Перекресток'    | '000001 до 27.10.2020 0:00:00' | '5,000, кг'      | '1020011' |
		| '15,000'     | 'упак (1 кг)'       | 'Сыр Пошехонский' | 'Перекресток'    | '000001 до 27.10.2020 0:00:00' | '15,000, кг'     | '1020011' |
	И я удаляю все строки таблицы "Товары"
	Тогда в таблице "Товары" количество строк "равно" 0
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" из выпадающего списка "Номенклатура" я выбираю по строке 'Сыр Сулугуни'
	И в таблице "Товары" из выпадающего списка "Характеристика" я выбираю по строке '<Без маркировки>'
	И в таблице "Товары" из выпадающего списка "Серия" я выбираю по строке '000001 до 20.10.2019 0:00:00'
	И в таблице "Товары" в поле 'Количество' я ввожу текст '10,000'
	И в таблице "Товары" из выпадающего списка "Ячейка" я выбираю по строке '1020011'
	И в поле 'Комментарий' я ввожу текст 'Создание распоряжения вручную, склад с ячейками'
	И я нажимаю на кнопку 'Провести и закрыть'
	Тогда открылось окно 'Распоряжения на приемку'
	И Я закрываю окно 'Распоряжения на приемку'

Сценарий: 012.2. Logistic. Создание распоряжения на перемещение с типом размещение для склада с ячейками 1 (документ) (проверка работы всех кнопок)

	Дано я удаляю объекты "Документы.к2РаспоряжениеНаПеремещение" без контроля ссылок
	И В командном интерфейсе я выбираю 'Склад и доставка' 'Распоряжения на перемещение'
	Тогда открылось окно 'Распоряжения на перемещение'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Распоряжение на перемещение (создание)'
	И в поле 'от' я ввожу текст '25.09.2019  0:00:00'
	И я нажимаю кнопку выбора у поля "Склад отправитель"
	Тогда открылось окно 'Склады'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'             |
		| 'Склад(ячейки и контроль)' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Распоряжение на перемещение (создание) *'
	Тогда элемент формы "Склад отправитель" стал равен 'Склад(ячейки и контроль)'
	И из выпадающего списка "Склад отправитель" я выбираю по строке 'Склад(ячейки и контроль)'
	Тогда элемент формы "Склад отправитель" стал равен 'Склад(ячейки и контроль)'
	И я нажимаю кнопку выбора у поля "Склад получатель"
	Тогда открылось окно 'Склады'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'             |
		| 'Склад(ячейки и контроль)' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Распоряжение на перемещение (создание) *'
	Тогда элемент формы "Склад получатель" стал равен 'Склад(ячейки и контроль)'
	И из выпадающего списка "Склад получатель" я выбираю по строке 'Склад(ячейки и контроль)'
	Тогда элемент формы "Склад получатель" стал равен 'Склад(ячейки и контроль)'
	И из выпадающего списка "Тип перемещения" я выбираю точное значение 'Размещение'
	И в поле 'Фактическая дата перемещения' я ввожу текст '25.09.2019  0:00:00'
	И я перехожу к закладке "План"
	И в таблице "ТоварыПлан" я нажимаю на кнопку с именем 'ТоварыПланДобавить'
	И в таблице "ТоварыПлан" из выпадающего списка "Номенклатура" я выбираю по строке 'Сыр Адыгейский'
	И в таблице "ТоварыПлан" из выпадающего списка "Характеристика" я выбираю по строке 'Ашан'
	И в таблице "ТоварыПлан" из выпадающего списка "Серия" я выбираю по строке '000001 до 27.10.2020 0:00:00'
	И в таблице "ТоварыПлан" в поле 'Количество' я ввожу текст '10,000'
	И в таблице "ТоварыПлан" из выпадающего списка "Ячейка отправитель" я выбираю по строке '1020011'
	И в таблице "ТоварыПлан" из выпадающего списка "Ячейка получатель" я выбираю по строке '1040011'
	И в таблице "ТоварыПлан" я завершаю редактирование строки
	Тогда таблица "ТоварыПлан" стала равной:
		| 'Количество' | 'Ед.'                | 'Номенклатура'   | 'Характеристика' | 'Серия'                        | 'В ед. хранения' | 'Ячейка отправитель' | 'Ячейка получатель' |
		| '10,000'     | 'упак (5 кг) (Ашан)' | 'Сыр Адыгейский' | 'Ашан'           | '000001 до 27.10.2020 0:00:00' | '50,000, кг'     | '1020011'            | '1040011'           |
	И в таблице "ТоварыПлан" из выпадающего списка "Ед." я выбираю по строке 'упак (2.5 кг) 5ка'
	И в таблице "ТоварыПлан" я завершаю редактирование строки
	Тогда таблица "ТоварыПлан" стала равной:
		| 'Количество' | 'Ед.'               | 'Номенклатура'   | 'Характеристика' | 'Серия'                        | 'В ед. хранения' | 'Ячейка отправитель' | 'Ячейка получатель' |
		| '10,000'     | 'упак (2.5 кг) 5ка' | 'Сыр Адыгейский' | 'Ашан'           | '000001 до 27.10.2020 0:00:00' | '25,000, кг'     | '1020011'            | '1040011'           |
	И в таблице "ТоварыПлан" я активизирую поле "Номенклатура"
	И в таблице "ТоварыПлан" я нажимаю на кнопку 'Копировать строки'
	И я выбираю пункт контекстного меню с именем 'ТоварыПланКонтекстноеМенюУдалить' на элементе формы с именем "ТоварыПлан"
	И в таблице "ТоварыПлан" я нажимаю на кнопку 'Вставить строки'
	И в таблице "ТоварыПлан" я завершаю редактирование строки
	Тогда таблица "ТоварыПлан" стала равной:
		| 'Количество' | 'Ед.'               | 'Номенклатура'   | 'Характеристика' | 'Серия'                        | 'В ед. хранения' | 'Ячейка отправитель' | 'Ячейка получатель' |
		| '10,000'     | 'упак (2.5 кг) 5ка' | 'Сыр Адыгейский' | 'Ашан'           | '000001 до 27.10.2020 0:00:00' | '25,000, кг'     | '1020011'            | '1040011'           |
	И в таблице "ТоварыПлан" я перехожу к строке:
		| 'Номенклатура'   |
		| 'Сыр Адыгейский' |
	И в таблице "ТоварыПлан" я нажимаю на кнопку 'Разбить строку'
	Тогда открылось окно 'Введите количество товара в новой строке'
	И в поле 'InputFld' я ввожу текст '7,000'
	И я нажимаю на кнопку 'OK'
	Тогда таблица "ТоварыПлан" стала равной:
		| 'Количество' | 'Ед.'               | 'Номенклатура'    | 'Характеристика' | 'Серия'                        | 'В ед. хранения' | 'Ячейка отправитель' | 'Ячейка получатель' |
		| '3,000'      | 'упак (2.5 кг) 5ка' | 'Сыр Адыгейский'  | 'Ашан'           | '000001 до 27.10.2020 0:00:00' | '7,500, кг'      | '1020011'            | '1040011'           |
		| '7,000'      | 'упак (2.5 кг) 5ка' | 'Сыр Адыгейский'  | 'Ашан'           | '000001 до 27.10.2020 0:00:00' | '17,500, кг'     | '1020011'            | '1040011'           |
	И я удаляю все строки таблицы "ТоварыПлан"
	Тогда в таблице "ТоварыПлан" количество строк "равно" 0
	И в таблице "ТоварыПлан" я нажимаю на кнопку с именем 'ТоварыПланДобавить'
	И в таблице "ТоварыПлан" из выпадающего списка с именем "ТоварыПланНоменклатура" я выбираю по строке 'Сыр Сулугуни'
	И в таблице "ТоварыПлан" из выпадающего списка с именем "ТоварыПланХарактеристика" я выбираю по строке '<Без маркировки>'
	И в таблице "ТоварыПлан" из выпадающего списка "Серия" я выбираю по строке '000001 до 20.10.2019 0:00:00'
	И в таблице "ТоварыПлан" в поле с именем 'ТоварыПланКоличествоУпаковок' я ввожу текст '10,000'
	И в таблице "ТоварыПлан" из выпадающего списка с именем "ТоварыПланЯчейкаОтправитель" я выбираю по строке '1020011'
	И в таблице "ТоварыПлан" из выпадающего списка с именем "ТоварыПланЯчейкаПолучатель" я выбираю по строке '1040011'
	И я перехожу к закладке "Факт"
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" из выпадающего списка "Номенклатура" я выбираю по строке 'Сыр Адыгейский'
	И в таблице "Товары" из выпадающего списка "Характеристика" я выбираю по строке 'Ашан'
	И в таблице "Товары" из выпадающего списка "Серия" я выбираю по строке '000001 до 27.10.2020 0:00:00'
	И в таблице "Товары" в поле 'Количество' я ввожу текст '10,000'
	И в таблице "Товары" из выпадающего списка "Ячейка отправитель" я выбираю по строке '1020011'
	И в таблице "Товары" из выпадающего списка "Ячейка получатель" я выбираю по строке '1040011'
	И в таблице "Товары" я завершаю редактирование строки
	Тогда таблица "Товары" стала равной:
		| 'Количество' | 'Ед.'                | 'Номенклатура'   | 'Характеристика' | 'Серия'                        | 'В ед. хранения' | 'Ячейка отправитель' | 'Ячейка получатель' |
		| '10,000'     | 'упак (5 кг) (Ашан)' | 'Сыр Адыгейский' | 'Ашан'           | '000001 до 27.10.2020 0:00:00' | '50,000, кг'     | '1020011'            | '1040011'           |
	И в таблице "Товары" из выпадающего списка "Ед." я выбираю по строке 'упак (2.5 кг) 5ка'
	И в таблице "Товары" я завершаю редактирование строки
	Тогда таблица "Товары" стала равной:
		| 'Количество' | 'Ед.'               | 'Номенклатура'   | 'Характеристика' | 'Серия'                        | 'В ед. хранения' | 'Ячейка отправитель' | 'Ячейка получатель' |
		| '10,000'     | 'упак (2.5 кг) 5ка' | 'Сыр Адыгейский' | 'Ашан'           | '000001 до 27.10.2020 0:00:00' | '25,000, кг'     | '1020011'            | '1040011'           |
	И в таблице "Товары" я активизирую поле "Номенклатура"
	И в таблице "Товары" я нажимаю на кнопку 'Копировать строки'
	И я выбираю пункт контекстного меню с именем 'ТоварыКонтекстноеМенюУдалить' на элементе формы с именем "Товары"
	И в таблице "Товары" я нажимаю на кнопку 'Вставить строки'
	И в таблице "Товары" я завершаю редактирование строки
	Тогда таблица "Товары" стала равной:
		| 'Количество' | 'Ед.'               | 'Номенклатура'   | 'Характеристика' | 'Серия'                        | 'В ед. хранения' | 'Ячейка отправитель' | 'Ячейка получатель' |
		| '10,000'     | 'упак (2.5 кг) 5ка' | 'Сыр Адыгейский' | 'Ашан'           | '000001 до 27.10.2020 0:00:00' | '25,000, кг'     | '1020011'            | '1040011'           |
	И в таблице "Товары" я нажимаю на кнопку 'Подобрать'
	Тогда открылось окно 'Подбор номенклатуры'
	И я нажимаю на кнопку 'Иерархия'
	Если в таблице "Список" нет строк Тогда
		| 'Наименование'               |
		| 'Крылья куринные (подложка)' |
		| 'Сыр Адыгейский'             |
		| 'Сыр Костромской'            |
		| 'Сыр Пошехонский'            |
		И я нажимаю на кнопку 'Иерархия'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'    |
		| 'Сыр Пошехонский' |
	И в таблице "Список" я выбираю текущую строку
	И в таблице "ПодобраннаяНоменклатура" я перехожу к строке:
		| 'Номенклатура'    |
		| 'Сыр Пошехонский' |
	И в таблице "ПодобраннаяНоменклатура" из выпадающего списка "Характеристика" я выбираю по строке 'Перекресток'
	И в таблице "ПодобраннаяНоменклатура" из выпадающего списка "Упаковка" я выбираю по строке 'упак (1 кг)'
	И в таблице "ПодобраннаяНоменклатура" я завершаю редактирование строки
	Тогда таблица "ПодобраннаяНоменклатура" стала равной:
		| 'Номенклатура'    | 'Характеристика' | 'Упаковка'          |
		| 'Сыр Адыгейский'  | 'Ашан'           | 'упак (2.5 кг) 5ка' |
		| 'Сыр Пошехонский' | 'Перекресток'    | 'упак (1 кг)'       |
	И я нажимаю на кнопку "Перенести"
	Тогда открылось окно 'Распоряжение на перемещение (создание) *'
	И в таблице "Товары" я перехожу к строке:
		| 'Номенклатура'    |
		| 'Сыр Пошехонский' |
	И в таблице "Товары" из выпадающего списка "Серия" я выбираю по строке '000001 до 27.10.2020 0:00:00'
	И в таблице "Товары" в поле 'Количество' я ввожу текст '20,000'
	И в таблице "Товары" из выпадающего списка "Ячейка отправитель" я выбираю по строке '1020011'
	И в таблице "Товары" из выпадающего списка "Ячейка получатель" я выбираю по строке '1040011'
	И в таблице "Товары" я завершаю редактирование строки
	Тогда таблица "Товары" стала равной:
		| 'Количество' | 'Ед.'               | 'Номенклатура'    | 'Характеристика' | 'Серия'                        | 'В ед. хранения' | 'Ячейка отправитель' | 'Ячейка получатель' |
		| '10,000'     | 'упак (2.5 кг) 5ка' | 'Сыр Адыгейский'  | 'Ашан'           | '000001 до 27.10.2020 0:00:00' | '25,000, кг'     | '1020011'            | '1040011'           |
		| '20,000'     | 'упак (1 кг)'       | 'Сыр Пошехонский' | 'Перекресток'    | '000001 до 27.10.2020 0:00:00' | '20,000, кг'     | '1020011'            | '1040011'           |
	И в таблице "Товары" я перехожу к строке:
		| 'Номенклатура'    |
		| 'Сыр Пошехонский' |
	И в таблице "Товары" я нажимаю на кнопку 'Разбить строку'
	Тогда открылось окно 'Введите количество товара в новой строке'
	И в поле 'InputFld' я ввожу текст '15,000'
	И я нажимаю на кнопку 'OK'
	Тогда таблица "Товары" стала равной:
		| 'Количество' | 'Ед.'               | 'Номенклатура'    | 'Характеристика' | 'Серия'                        | 'В ед. хранения' | 'Ячейка отправитель' | 'Ячейка получатель' |
		| '10,000'     | 'упак (2.5 кг) 5ка' | 'Сыр Адыгейский'  | 'Ашан'           | '000001 до 27.10.2020 0:00:00' | '25,000, кг'     | '1020011'            | '1040011'           |
		| '5,000'      | 'упак (1 кг)'       | 'Сыр Пошехонский' | 'Перекресток'    | '000001 до 27.10.2020 0:00:00' | '5,000, кг'      | '1020011'            | '1040011'           |
		| '15,000'     | 'упак (1 кг)'       | 'Сыр Пошехонский' | 'Перекресток'    | '000001 до 27.10.2020 0:00:00' | '15,000, кг'     | '1020011'            | '1040011'           |
	И я удаляю все строки таблицы "Товары"
	Тогда в таблице "Товары" количество строк "равно" 0
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" из выпадающего списка с именем "ТоварыНоменклатура" я выбираю по строке 'Сыр Сулугуни'
	И в таблице "Товары" из выпадающего списка с именем "ТоварыХарактеристика" я выбираю по строке '<Без маркировки>'
	И в таблице "Товары" из выпадающего списка "Серия" я выбираю по строке '000001 до 20.10.2019 0:00:00'
	И в таблице "Товары" в поле с именем 'ТоварыКоличествоУпаковок' я ввожу текст '10,000'
	И в таблице "Товары" из выпадающего списка с именем "ТоварыЯчейкаОтправитель" я выбираю по строке '1020011'
	И в таблице "Товары" из выпадающего списка с именем "ТоварыЯчейкаПолучатель" я выбираю по строке '1040011'
	И я перехожу к закладке "Дополнительно"
	И я нажимаю кнопку выбора у поля "Ответственный"
	Тогда открылось окно 'Выбор пользователя'
	И в таблице "ПользователиСписок" я перехожу к строке:
		| 'Полное имя'    |
		| 'Администратор' |
	И я нажимаю на кнопку с именем 'ВыбратьПользователя'
	Тогда открылось окно 'Распоряжение на перемещение (создание) *'
	Тогда элемент формы с именем "Ответственный" стал равен 'Администратор'
	И из выпадающего списка "Ответственный" я выбираю по строке 'Администратор'
	Тогда элемент формы с именем "Ответственный" стал равен 'Администратор'
	И в поле 'Комментарий' я ввожу текст 'Распоряжение на перемещение 1 для склада с ячейками из ячейки поступления в новую'
	И из выпадающего списка "Статус" я выбираю точное значение 'Запланировано'
	И я нажимаю на кнопку 'Записать'
	Тогда открылось окно 'Распоряжение на перемещение * от *'
	И я нажимаю на кнопку 'Провести'
	И из выпадающего списка "Статус" я выбираю точное значение 'К выполнению'
	И я нажимаю на кнопку 'Записать'
	Тогда открылось окно 'Распоряжение на перемещение * от *'
	И я нажимаю на кнопку 'Провести'
	И из выпадающего списка "Статус" я выбираю точное значение 'Выполняется'
	И я нажимаю на кнопку 'Записать'
	Тогда открылось окно 'Распоряжение на перемещение * от *'
	И я нажимаю на кнопку 'Провести'
	И из выпадающего списка "Статус" я выбираю точное значение 'Выполнено'
	И я нажимаю на кнопку 'Записать'
	Тогда открылось окно 'Распоряжение на перемещение * от *'
	И я нажимаю на кнопку 'Провести'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Распоряжение на перемещение * от *' в течение 20 секунд
	Тогда открылось окно 'Распоряжения на перемещение'
	* Проверка отборов на форме списка распоряжений на перемещение
		И я нажимаю на гиперссылку "Период"
		Тогда открылось окно 'Выберите период'
		И в поле 'DateBegin' я ввожу текст '24.09.2019'
		И в поле 'DateEnd' я ввожу текст '24.09.2019'
		И я нажимаю на кнопку 'Выбрать'
		Тогда открылось окно 'Распоряжения на перемещение'
		Тогда в таблице "Список" количество строк "равно" 0
		И я нажимаю на гиперссылку "Период"
		Тогда открылось окно 'Выберите период'
		И в поле 'DateBegin' я ввожу текст '25.09.2019'
		И в поле 'DateEnd' я ввожу текст '25.09.2019'
		И я нажимаю на кнопку 'Выбрать'
		Когда открылось окно 'Распоряжения на перемещение'
		Тогда таблица "Список" стала равной:
			| 'Дата'       | 'Способ создания' | 'Статус'    | 'Ответственный' | 'Тип перемещения' | 'Склад отправитель'        | 'Дата перемещения' | 'Склад получатель'         | 'Комментарий'                                                                       |
			| '25.09.2019' | 'Создан вручную'  | 'Выполнено' | 'Администратор' | 'Размещение'      | 'Склад(ячейки и контроль)' | '25.09.2019'       | 'Склад(ячейки и контроль)' | 'Распоряжение на перемещение 1 для склада с ячейками из ячейки поступления в новую' |
		И я нажимаю на гиперссылку "Дата перемещения"
		Тогда открылось окно 'Выберите период'
		И в поле 'DateBegin' я ввожу текст '24.09.2019'
		И в поле 'DateEnd' я ввожу текст '24.09.2019'
		И я нажимаю на кнопку 'Выбрать'
		Тогда открылось окно 'Распоряжения на перемещение'
		Тогда в таблице "Список" количество строк "равно" 0
		И я нажимаю на гиперссылку "Дата перемещения"
		Тогда открылось окно 'Выберите период'
		И в поле 'DateBegin' я ввожу текст '25.09.2019'
		И в поле 'DateEnd' я ввожу текст '25.09.2019'
		И я нажимаю на кнопку 'Выбрать'
		Когда открылось окно 'Распоряжения на перемещение'
		Тогда таблица "Список" стала равной:
			| 'Дата'       | 'Способ создания' | 'Статус'    | 'Ответственный' | 'Тип перемещения' | 'Склад отправитель'        | 'Дата перемещения' | 'Склад получатель'         | 'Комментарий'                                                                       |
			| '25.09.2019' | 'Создан вручную'  | 'Выполнено' | 'Администратор' | 'Размещение'      | 'Склад(ячейки и контроль)' | '25.09.2019'       | 'Склад(ячейки и контроль)' | 'Распоряжение на перемещение 1 для склада с ячейками из ячейки поступления в новую' |
		И из выпадающего списка "Тип перемещения" я выбираю точное значение 'Перемещение'
		Тогда в таблице "Список" количество строк "равно" 0
		И из выпадающего списка "Тип перемещения" я выбираю точное значение 'Размещение'
		Тогда таблица "Список" стала равной:
			| 'Дата'       | 'Способ создания' | 'Статус'    | 'Ответственный' | 'Тип перемещения' | 'Склад отправитель'        | 'Дата перемещения' | 'Склад получатель'         | 'Комментарий'                                                                       |
			| '25.09.2019' | 'Создан вручную'  | 'Выполнено' | 'Администратор' | 'Размещение'      | 'Склад(ячейки и контроль)' | '25.09.2019'       | 'Склад(ячейки и контроль)' | 'Распоряжение на перемещение 1 для склада с ячейками из ячейки поступления в новую' |
		И из выпадающего списка "Склад отправитель" я выбираю по строке 'Сырная ГП'
		Тогда в таблице "Список" количество строк "равно" 0
		И из выпадающего списка "Склад отправитель" я выбираю по строке 'Склад(ячейки и контроль)'
		Тогда таблица "Список" стала равной:
			| 'Дата'       | 'Способ создания' | 'Статус'    | 'Ответственный' | 'Тип перемещения' | 'Склад отправитель'        | 'Дата перемещения' | 'Склад получатель'         | 'Комментарий'                                                                       |
			| '25.09.2019' | 'Создан вручную'  | 'Выполнено' | 'Администратор' | 'Размещение'      | 'Склад(ячейки и контроль)' | '25.09.2019'       | 'Склад(ячейки и контроль)' | 'Распоряжение на перемещение 1 для склада с ячейками из ячейки поступления в новую' |
		И из выпадающего списка "Склад получатель" я выбираю по строке 'Сырная ГП'
		Тогда в таблице "Список" количество строк "равно" 0
		И из выпадающего списка "Склад получатель" я выбираю по строке 'Склад(ячейки и контроль)'
		Тогда таблица "Список" стала равной:
			| 'Дата'       | 'Способ создания' | 'Статус'    | 'Ответственный' | 'Тип перемещения' | 'Склад отправитель'        | 'Дата перемещения' | 'Склад получатель'         | 'Комментарий'                                                                       |
			| '25.09.2019' | 'Создан вручную'  | 'Выполнено' | 'Администратор' | 'Размещение'      | 'Склад(ячейки и контроль)' | '25.09.2019'       | 'Склад(ячейки и контроль)' | 'Распоряжение на перемещение 1 для склада с ячейками из ячейки поступления в новую' |
		И я нажимаю кнопку выбора у поля "Основание"
		Тогда открылось окно 'Выбор типа данных'
		И в таблице "" я перехожу к строке:
			| ''               |
			| 'Инвентаризация' |
		И я нажимаю на кнопку 'ОК'
		Тогда открылось окно 'Инвентаризация'
		И Я закрываю окно 'Инвентаризация'
		Тогда открылось окно 'Распоряжения на перемещение'
		И я нажимаю кнопку выбора у поля "Основание"
		Тогда открылось окно 'Выбор типа данных'
		И в таблице "" я перехожу к строке:
			| ''              |
			| 'План отгрузки' |
		И я нажимаю на кнопку 'ОК'
		Тогда открылось окно 'Планы отгрузки'
		И Я закрываю окно 'Планы отгрузки'
		Тогда открылось окно 'Распоряжения на перемещение'
		И из выпадающего списка "Статус" я выбираю точное значение 'Запланировано'
		Тогда в таблице "Список" количество строк "равно" 0
		И из выпадающего списка "Статус" я выбираю точное значение 'Выполнено'
		Тогда таблица "Список" стала равной:
			| 'Дата'       | 'Способ создания' | 'Статус'    | 'Ответственный' | 'Тип перемещения' | 'Склад отправитель'        | 'Дата перемещения' | 'Склад получатель'         | 'Комментарий'                                                                       |
			| '25.09.2019' | 'Создан вручную'  | 'Выполнено' | 'Администратор' | 'Размещение'      | 'Склад(ячейки и контроль)' | '25.09.2019'       | 'Склад(ячейки и контроль)' | 'Распоряжение на перемещение 1 для склада с ячейками из ячейки поступления в новую' |
		И из выпадающего списка "Номенклатура" я выбираю по строке 'Сыр Адыгейский'
		Тогда в таблице "Список" количество строк "равно" 0
		И из выпадающего списка "Номенклатура" я выбираю по строке 'Сыр Сулугуни'
		Тогда таблица "Список" стала равной:
			| 'Дата'       | 'Способ создания' | 'Статус'    | 'Ответственный' | 'Тип перемещения' | 'Склад отправитель'        | 'Дата перемещения' | 'Склад получатель'         | 'Комментарий'                                                                       |
			| '25.09.2019' | 'Создан вручную'  | 'Выполнено' | 'Администратор' | 'Размещение'      | 'Склад(ячейки и контроль)' | '25.09.2019'       | 'Склад(ячейки и контроль)' | 'Распоряжение на перемещение 1 для склада с ячейками из ячейки поступления в новую' |
		И из выпадающего списка "Ответственный" я выбираю по строке 'Пользователь'
		Тогда в таблице "Список" количество строк "равно" 0
		И из выпадающего списка "Ответственный" я выбираю по строке 'Администратор'
		Тогда таблица "Список" стала равной:
			| 'Дата'       | 'Способ создания' | 'Статус'    | 'Ответственный' | 'Тип перемещения' | 'Склад отправитель'        | 'Дата перемещения' | 'Склад получатель'         | 'Комментарий'                                                                       |
			| '25.09.2019' | 'Создан вручную'  | 'Выполнено' | 'Администратор' | 'Размещение'      | 'Склад(ячейки и контроль)' | '25.09.2019'       | 'Склад(ячейки и контроль)' | 'Распоряжение на перемещение 1 для склада с ячейками из ячейки поступления в новую' |
		И я нажимаю кнопку очистить у поля "Ответственный"
		И я нажимаю кнопку очистить у поля "Номенклатура"
		И я нажимаю кнопку очистить у поля "Статус"
		И я нажимаю кнопку очистить у поля "Основание"
		И я нажимаю кнопку очистить у поля "Склад получатель"
		И я нажимаю кнопку очистить у поля "Склад отправитель"
		И я нажимаю кнопку очистить у поля "Тип перемещения"
		И я нажимаю на гиперссылку "Дата перемещения"
		Тогда открылось окно 'Выберите период'
		И я нажимаю на кнопку 'Очистить период'
		И я нажимаю на кнопку 'Выбрать'
		Тогда открылось окно 'Распоряжения на перемещение'
		И я нажимаю на гиперссылку "Период"
		Тогда открылось окно 'Выберите период'
		И я нажимаю на кнопку 'Очистить период'
		И я нажимаю на кнопку 'Выбрать'
		Тогда открылось окно 'Распоряжения на перемещение'
	И Я закрываю окно 'Распоряжения на перемещение'

Сценарий: 012.3. Logistic. Создание плана отгрузки (документ), п/ф плана отгрузки для склада с ячейками 1 (документ)
	
	И В командном интерфейсе я выбираю 'Склад и доставка' 'Планы отгрузки'
	Тогда открылось окно 'Планы отгрузки'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'План отгрузки (создание)'
	И в поле 'Дата отгрузки' я ввожу текст '24.09.2019'
	И из выпадающего списка "Склад" я выбираю по строке 'Склад(ячейки и контроль)'
	И из выпадающего списка "Статус" я выбираю точное значение 'В наборку'
	И я перехожу к закладке "Товары"
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" из выпадающего списка "Номенклатура" я выбираю по строке 'Сыр Сулугуни'
	И в таблице "Товары" из выпадающего списка "Характеристика" я выбираю по строке '<Без маркировки>'
	И в таблице "Товары" из выпадающего списка "Серия" я выбираю по строке '000001 до 20.10.2019 0:00:00'
	И в таблице "Товары" в поле 'Количество' я ввожу текст '10,000'
	И в таблице "Товары" в поле 'Вес, кг' я ввожу текст '10,00'
	И в таблице "Товары" в поле 'Мест, м3' я ввожу текст '10,00'
	И в таблице "Товары" я завершаю редактирование строки
	И в поле 'Комментарий' я ввожу текст 'План отгрузки 3 для склада c ячейками 1 (документ)'
	И я нажимаю на кнопку 'Провести'
	И я нажимаю на кнопку 'План отгрузки'
	Тогда Табличный документ "ТекущаяПечатнаяФорма" равен макету "План отгрузки для склада ячейками1 (документы)" по шаблону
	И Я закрываю окно 'План отгрузки *'
	Тогда открылось окно 'План отгрузки * от *'
	И из выпадающего списка "Статус" я выбираю точное значение 'Погружено'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'План отгрузки * от *' в течение 20 секунд
	Тогда открылось окно 'Планы отгрузки'
	И Я закрываю окно 'Планы отгрузки'

Сценарий: 012.4. Logistic. Создание распоряжения на перемещение c типом отбор в ячейку отгрузки для склада с ячейками 1 (документ)
	
	И В командном интерфейсе я выбираю 'Склад и доставка' 'Распоряжения на перемещение'
	Тогда открылось окно 'Распоряжения на перемещение'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Распоряжение на перемещение (создание)'
	И из выпадающего списка "Статус" я выбираю точное значение 'Выполнено'
	И из выпадающего списка "Склад отправитель" я выбираю по строке 'Склад(ячейки и контроль)'
	И из выпадающего списка "Склад получатель" я выбираю по строке 'Склад(ячейки и контроль)'
	И из выпадающего списка "Тип перемещения" я выбираю точное значение 'Отбор'
	И в поле 'Фактическая дата перемещения' я ввожу текст '25.09.2019  0:00:00'
	И я перехожу к закладке "Факт"
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" из выпадающего списка с именем "ТоварыНоменклатура" я выбираю по строке 'Сыр Сулугуни'
	И в таблице "Товары" из выпадающего списка с именем "ТоварыХарактеристика" я выбираю по строке '<Без маркировки>'
	И в таблице "Товары" из выпадающего списка "Серия" я выбираю по строке '000001 до 20.10.2019 0:00:00'
	И в таблице "Товары" в поле с именем 'ТоварыКоличествоУпаковок' я ввожу текст '10,000'
	И в таблице "Товары" из выпадающего списка с именем "ТоварыЯчейкаОтправитель" я выбираю по строке '1040011'
	И в таблице "Товары" из выпадающего списка с именем "ТоварыЯчейкаПолучатель" я выбираю по строке '1030011'
	И в поле 'Комментарий' я ввожу текст 'Распоряжение на перемещение 2 для склада с ячейками из ячейки в ячейку отгрузки'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Распоряжение на перемещение (создание) *' в течение 20 секунд
	Тогда открылось окно 'Распоряжения на перемещение'
	И Я закрываю окно 'Распоряжения на перемещение'

Сценарий: 012.5. Logistic. Создание распоряжения на отгрузку для склада с ячейками 1 (документ) на основании плана отгрузки
	
	И В командном интерфейсе я выбираю 'Склад и доставка' 'Планы отгрузки'
	Тогда открылось окно 'Планы отгрузки'
	И в таблице "Список" я перехожу к строке:
		| 'Комментарий'                                        |
		| 'План отгрузки 3 для склада c ячейками 1 (документ)' |
	И я нажимаю на кнопку 'Распоряжение на отгрузку(Факт наборки)'
	Тогда открылось окно 'Распоряжение на отгрузку (создание)'
	И элемент формы "Склад" стал равен "Склад(ячейки и контроль)"
	И элемент формы "Ответственный" стал равен "Администратор"
	И я перехожу к закладке "Товары"
	И в таблице "Товары" из выпадающего списка "Номенклатура" я выбираю по строке 'Сыр Сулугуни'
	И в таблице "Товары" из выпадающего списка "Характеристика" я выбираю по строке '<Без маркировки>'
	И в таблице "Товары" из выпадающего списка "Серия" я выбираю по строке '000001 до 20.10.2019 0:00:00'
	И в таблице "Товары" в поле 'Количество' я ввожу текст '10,000'
	И в таблице "Товары" из выпадающего списка "Ячейка" я выбираю по строке '1030011'
	И в таблице "Товары" я завершаю редактирование строки
	Тогда таблица "Товары" стала равной:
		| 'Количество' | 'Ед.' | 'Номенклатура' | 'Характеристика'   | 'Серия'                        | 'В ед. хранения' | 'Вес, кг' | 'Ячейка'  |
		| '10,000'     | 'кг'  | 'Сыр Сулугуни' | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | '10,000, кг'     | '10'      | '1030011' |
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Распоряжение на отгрузку (создание) *' в течение 20 секунд
	Тогда открылось окно 'Планы отгрузки'
	И Я закрываю окно 'Планы отгрузки'

Сценарий: 012.6. Logistic. Завершение

	И Я закрыл все окна клиентского приложения