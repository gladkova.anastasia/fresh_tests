#language: ru

@logistic

Функционал: 019. Logistic. Инвентаризация на складе с ячейками и контролем

Сценарий: 019.1. Logistic. Создание оприходования товаров для склада с ячейками

	Дано Я подключаюсь под "Администратор"
	И В командном интерфейсе я выбираю 'Склад и доставка' 'Оприходования товаров'
	Тогда открылось окно 'Оприходования товаров'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Оприходование товаров (создание)'
	И из выпадающего списка "Склад" я выбираю по строке 'Склад(ячейки и контроль)'
	И я перехожу к закладке "Товары"
	И я удаляю все строки таблицы "Товары"
	И в таблице "Товары" я добавляю строку
	И в таблице "Товары" из выпадающего списка "Номенклатура" я выбираю по строке 'Сыр Сулугуни'
	И в таблице "Товары" из выпадающего списка "Характеристика" я выбираю по строке '<Без маркировки>'
	И в таблице "Товары" из выпадающего списка "Серия" я выбираю по строке '000001 до 20.10.2019'
	И в таблице "Товары" из выпадающего списка "Ячейка" я выбираю по строке '1020011'
	И в таблице "Товары" в поле с именем 'ТоварыКоличество' я ввожу текст '10,000'
	И в таблице "Товары" я завершаю редактирование строки
	И в таблице "Товары" я добавляю строку
	И в таблице "Товары" я активизирую поле "Номенклатура"
	И в таблице "Товары" из выпадающего списка "Номенклатура" я выбираю по строке 'Сыр Сулугуни'
	И в таблице "Товары" из выпадающего списка "Характеристика" я выбираю по строке '<Без маркировки>'
	И в таблице "Товары" из выпадающего списка "Серия" я выбираю по строке '000001 до 20.10.2019 0:00:00'
	И в таблице "Товары" из выпадающего списка "Ячейка" я выбираю по строке '1040011'
	И в таблице "Товары" в поле с именем 'ТоварыКоличество' я ввожу текст '20,000'
	И в таблице "Товары" я завершаю редактирование строки
	И в поле 'Комментарий' я ввожу текст 'Оприходование на склад с ячейками'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Оприходование товаров (создание) *' в течение 20 секунд
	Тогда открылось окно 'Оприходования товаров'
	И Я закрываю окно 'Оприходования товаров'

Сценарий: 019.2. Logistic. Создание оприходования товаров организации на основании оприходования для склада с ячейками
	
	И В командном интерфейсе я выбираю 'Склад и доставка' 'Оприходования товаров'
	И в таблице "Список" я перехожу к строке:
		| 'Комментарий'                       |
		| 'Оприходование на склад с ячейками' |
	И я нажимаю на кнопку 'Оприходование товаров организации'
	Тогда открылось окно 'Оприходование товаров организации (создание)'
	И из выпадающего списка "Организация" я выбираю по строке 'Организация'
	И из выпадающего списка "Вид цен" я выбираю по строке 'Цена для оприходования/списания'
	И я перехожу к закладке "Товары"
	И в таблице "Товары" я перехожу к строке:
		| 'Количество' | 'Номенклатура' | 'Серия'                        | 'Характеристика'   |
		| '10,000'     | 'Сыр Сулугуни' | '000001 до 20.10.2019 0:00:00' | '<Без маркировки>' |
	И в таблице "Товары" в поле 'Цена, RUB' я ввожу текст '100,00'
	И в таблице "Товары" я завершаю редактирование строки
	И в таблице "Товары" я перехожу к строке:
		| 'Количество' | 'Номенклатура' | 'Серия'                        | 'Характеристика'   |
		| '20,000'     | 'Сыр Сулугуни' | '000001 до 20.10.2019 0:00:00' | '<Без маркировки>' |
	И в таблице "Товары" в поле 'Цена, RUB' я ввожу текст '100,00'
	И в таблице "Товары" я завершаю редактирование строки
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Оприходование товаров организации (создание) *' в течение 20 секунд
	Тогда открылось окно 'Оприходования товаров'
	И Я закрываю окно 'Оприходования товаров'

Сценарий: 019.3. Logistic. Cоздание пересчета для склада с ячейками

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Пересчеты'
	Тогда открылось окно 'Пересчеты'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Пересчет (создание)'
	И из выпадающего списка "Склад" я выбираю по строке 'Склад(ячейки и контроль)'
	И я перехожу к закладке "Товары"
	И в таблице "Товары" из выпадающего списка "Номенклатура" я выбираю по строке 'Сыр Сулугуни'
	И в таблице "Товары" из выпадающего списка "Характеристика" я выбираю по строке '<Без маркировки>'
	И в таблице "Товары" из выпадающего списка "Серия" я выбираю по строке '000001 до 20.10.2019 0:00:00'
	И в таблице "Товары" из выпадающего списка "Ячейка" я выбираю по строке '1020011'
	И в таблице "Товары" в поле с именем 'ТоварыКоличество' я ввожу текст '5,000'
	И в таблице "Товары" я завершаю редактирование строки
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" из выпадающего списка "Номенклатура" я выбираю по строке 'Сыр Сулугуни'
	И в таблице "Товары" из выпадающего списка "Характеристика" я выбираю по строке '<Без маркировки>'
	И в таблице "Товары" из выпадающего списка "Серия" я выбираю по строке '000001 до 20.10.2019 0:00:00'
	И в таблице "Товары" из выпадающего списка "Ячейка" я выбираю по строке '1040011'
	И в таблице "Товары" в поле с именем 'ТоварыКоличество' я ввожу текст '10,000'
	И я перехожу к закладке "Дополнительно"
	И в поле 'Комментарий' я ввожу текст 'Пересчет для склада с ячеками'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Пересчет (создание) *' в течение 20 секунд
	Тогда открылось окно 'Пересчеты'
	И Я закрываю окно 'Пересчеты'

Сценарий: 019.4. Logistic. Инвентаризация на складе с ячейками: заполнение инвентаризации по учету и по пересчету, перемещение на ячейку пересчета

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Инвентаризация'
	Тогда открылось окно 'Инвентаризация'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Инвентаризация (создание)'
	И из выпадающего списка "Статус" я выбираю точное значение 'В работе'
	И из выпадающего списка "Склад" я выбираю по строке 'Склад(ячейки и контроль)'
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыКомандаТоварыЗаполнитьПоУчету'
	Тогда открылось окно 'Складские ячейки'
	И в таблице "Список" я перехожу к строке:
		| 'Описание'   |
		| '1-01-001-1' |
	И в таблице "Список" я выбираю текущую строку
	И в таблице "Список" я перехожу к строке:
		| 'Описание'   |
		| '1-02-001-1' |
	И в таблице "Список" я выбираю текущую строку
	И в таблице "Список" я перехожу к строке:
		| 'Описание'   |
		| '1-03-001-1' |
	И в таблице "Список" я выбираю текущую строку
	И в таблице "Список" я перехожу к строке:
		| 'Описание'   |
		| '1-04-001-1' |
	И в таблице "Список" я выбираю текущую строку
	И я нажимаю на кнопку 'Перенести в документ'
	Тогда таблица "Товары" стала равной:
		| 'Ячейка'  | 'Количество факт' | 'Номенклатура' | 'Количество учет' | 'Характеристика'   | 'Серия'                        | 'Количество разница' | 'Ед. изм.' | 'Ответственный' |
		| '1020011' | ''                | 'Сыр Сулугуни' | '10,000'          | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | '-10,000'            | 'кг'       | ''              |
		| '1040011' | ''                | 'Сыр Сулугуни' | '20,000'          | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | '-20,000'            | 'кг'       | ''              |
	И в таблице "Товары" я нажимаю на кнопку 'Заполнить по пересчету'
	Тогда открылось окно 'Пересчеты'
	И в таблице "Список" я перехожу к строке:
		| 'Комментарий'                   |
		| 'Пересчет для склада с ячеками' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда таблица "Товары" стала равной:
		| 'Ячейка'  | 'Количество факт' | 'Номенклатура' | 'Количество учет' | 'Характеристика'   | 'Серия'                        | 'Количество разница' | 'Ед. изм.' | 'Ответственный' |
		| '1020011' | '5,000'           | 'Сыр Сулугуни' | '10,000'          | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | '-5,000'             | 'кг'       | 'Администратор' |
		| '1040011' | '10,000'          | 'Сыр Сулугуни' | '20,000'          | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | '-10,000'            | 'кг'       | 'Администратор' |
	И я нажимаю на кнопку 'Записать'
	И я нажимаю на кнопку с именем "ТоварыПереместитьОстаткиНаЯчейкуПересчета"
	Тогда таблица "Товары" стала равной:
		| 'Ячейка'  | 'Количество факт' | 'Номенклатура' | 'Количество учет' | 'Характеристика'   | 'Серия'                        | 'Количество разница' | 'Ед. изм.' | 'Ответственный' |
		| '1010011' | ''                | 'Сыр Сулугуни' | '15,000'          | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | '-15,000'            | 'кг'       | ''              |
		| '1020011' | '5,000'           | 'Сыр Сулугуни' | '5,000'           | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | ''                   | 'кг'       | 'Администратор' |
		| '1040011' | '10,000'          | 'Сыр Сулугуни' | '10,000'          | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | ''                   | 'кг'       | 'Администратор' |
	И в поле 'Комментарий' я ввожу текст 'Инвертаризация для склада с ячейками 1'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Инвентаризация *' в течение 20 секунд
	Тогда открылось окно 'Инвентаризация'
	И Я закрываю окно 'Инвентаризация'

Сценарий: 019.5. Logistic. Создание на основании инвентаризации списания товаров для склада с ячейками

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Инвентаризация'
	Тогда открылось окно 'Инвентаризация'
	И в таблице "Список" я перехожу к строке:
		| 'Статус'   | 'Ответственный' | 'Комментарий'                            | 'Склад'                    |
		| 'В работе' | 'Администратор' | 'Инвертаризация для склада с ячейками 1' | 'Склад(ячейки и контроль)' |
	И я нажимаю на кнопку 'Списание товаров'
	Тогда открылось окно 'Списание товаров (создание) *'
	Тогда таблица "Товары" стала равной:
		| 'Номенклатура' | 'Характеристика'   | 'Серия'                        | 'Ячейка'  | 'Количество' | 'Ед.' |
		| 'Сыр Сулугуни' | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | '1010011' | '15,000'     | 'кг'  |
	И в таблице "Товары" я нажимаю на кнопку 'Перезаполнить по основанию'
	Тогда открылось окно '1С:Предприятие'
	И я нажимаю на кнопку 'Да'
	Тогда таблица "Товары" стала равной:
		| 'Номенклатура' | 'Характеристика'   | 'Серия'                        | 'Ячейка'  | 'Количество' | 'Ед.' |
		| 'Сыр Сулугуни' | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | '1010011' | '15,000'     | 'кг'  |
	И в поле 'Комментарий' я ввожу текст 'Списание товаров для склада с ячейками'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Списание товаров (создание) *' в течение 20 секунд
	Тогда открылось окно 'Инвентаризация'
	И Я закрываю окно 'Инвентаризация'

Сценарий: 019.6. Logistic. Создание на основании списания товаров - списания товаров организации для склада с ячейками

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Списания товаров'
	Тогда открылось окно 'Списания товаров'
	И в таблице "Список" я перехожу к строке:
		| 'Комментарий'                            |
		| 'Списание товаров для склада с ячейками' |
	И я нажимаю на кнопку 'Списание товаров организации'
	Тогда открылось окно 'Списание товаров организации (создание)'
	И из выпадающего списка "Организация" я выбираю по строке 'Организация'
	И из выпадающего списка "Вид цен" я выбираю по строке 'Цена для оприходования/списания'
	И я перехожу к закладке "Товары"
	И в таблице "Товары" я перехожу к строке:
		| 'Количество' | 'Номенклатура' | 'Серия'                        | 'Характеристика'   |
		| '15,000'     | 'Сыр Сулугуни' | '000001 до 20.10.2019 0:00:00' | '<Без маркировки>' |
	И в таблице "Товары" в поле 'Цена, RUB' я ввожу текст '100,00'
	И в таблице "Товары" я завершаю редактирование строки
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Списание товаров организации (создание) *' в течение 20 секунд
	Тогда открылось окно 'Списания товаров'
	И Я закрываю окно 'Списания товаров'

Сценарий: 019.7. Logistic. Установка статуса Выполнено для инвентаризации 1 для склада с ячейками

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Инвентаризация'
	Тогда открылось окно 'Инвентаризация'
	И в таблице "Список" я перехожу к строке:
		| 'Комментарий'                            |
		| 'Инвертаризация для склада с ячейками 1' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Инвентаризация * от *'
	И из выпадающего списка "Статус" я выбираю точное значение 'Выполнено'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Инвентаризация * от * *' в течение 20 секунд
	Тогда открылось окно 'Инвентаризация'
	И Я закрываю окно 'Инвентаризация'
	
Сценарий: 019.8. Logistic. Инвентаризация на складе с ячейками: заполнение инвентаризации по учету, заполнение факта руками, перемещение на ячейку пересчета

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Инвентаризация'
	Тогда открылось окно 'Инвентаризация'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Инвентаризация (создание)'
	И из выпадающего списка "Статус" я выбираю точное значение 'В работе'
	И из выпадающего списка "Склад" я выбираю по строке 'Склад(ячейки и контроль)'
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыКомандаТоварыЗаполнитьПоУчету'
	Тогда открылось окно 'Складские ячейки'
	И в таблице "Список" я перехожу к строке:
		| 'Описание'   |
		| '1-01-001-1' |
	И в таблице "Список" я выбираю текущую строку
	И в таблице "Список" я перехожу к строке:
		| 'Описание'   |
		| '1-02-001-1' |
	И в таблице "Список" я выбираю текущую строку
	И в таблице "Список" я перехожу к строке:
		| 'Описание'   |
		| '1-03-001-1' |
	И в таблице "Список" я выбираю текущую строку
	И в таблице "Список" я перехожу к строке:
		| 'Описание'   |
		| '1-04-001-1' |
	И в таблице "Список" я выбираю текущую строку
	И я нажимаю на кнопку 'Перенести в документ'
	Тогда таблица "Товары" стала равной:
		| 'Ячейка'  | 'Количество факт' | 'Номенклатура' | 'Количество учет' | 'Характеристика'   | 'Серия'                        | 'Количество разница' | 'Ед. изм.' | 'Ответственный' |
		| '1020011' | ''                | 'Сыр Сулугуни' | '5,000'           | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | '-5,000'             | 'кг'       | ''              |
		| '1040011' | ''                | 'Сыр Сулугуни' | '10,000'          | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | '-10,000'            | 'кг'       | ''              |
	И в таблице "Товары" я перехожу к строке:
		| 'Номенклатура' | 'Серия'                        | 'Характеристика'   | 'Ячейка'  |
		| 'Сыр Сулугуни' | '000001 до 20.10.2019 0:00:00' | '<Без маркировки>' | '1020011' |
	И в таблице "Товары" в поле с именем 'ТоварыКоличествоФакт' я ввожу текст '10,000'
	И в таблице "Товары" я завершаю редактирование строки
	И в таблице "Товары" я перехожу к строке:
		| 'Номенклатура' | 'Серия'                        | 'Характеристика'   | 'Ячейка'  |
		| 'Сыр Сулугуни' | '000001 до 20.10.2019 0:00:00' | '<Без маркировки>' | '1040011' |
	И в таблице "Товары" в поле с именем 'ТоварыКоличествоФакт' я ввожу текст '15,000'
	И в таблице "Товары" я завершаю редактирование строки
	Тогда таблица "Товары" стала равной:
		| 'Ячейка'  | 'Количество факт' | 'Номенклатура' | 'Количество учет' | 'Характеристика'   | 'Серия'                        | 'Количество разница' | 'Ед. изм.' | 'Ответственный' |
		| '1020011' | '10,000'          | 'Сыр Сулугуни' | '5,000'           | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | '5,000'              | 'кг'       | 'Администратор' |
		| '1040011' | '15,000'          | 'Сыр Сулугуни' | '10,000'          | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | '5,000'              | 'кг'       | 'Администратор' |
	И я нажимаю на кнопку 'Записать'
	И я нажимаю на кнопку с именем "ТоварыПереместитьОстаткиНаЯчейкуПересчета"
	Тогда таблица "Товары" стала равной:
		| 'Ячейка'  | 'Количество факт' | 'Номенклатура' | 'Количество учет' | 'Характеристика'   | 'Серия'                        | 'Количество разница' | 'Ед. изм.' | 'Ответственный' |
		| '1010011' | ''                | 'Сыр Сулугуни' | '-10,000'         | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | '10,000'             | 'кг'       | ''              |
		| '1020011' | '10,000'          | 'Сыр Сулугуни' | '10,000'          | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | ''                   | 'кг'       | 'Администратор' |
		| '1040011' | '15,000'          | 'Сыр Сулугуни' | '15,000'          | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | ''                   | 'кг'       | 'Администратор' |
	И в поле 'Комментарий' я ввожу текст 'Инвертаризация для склада с ячейками 2'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Инвентаризация *' в течение 20 секунд
	Тогда открылось окно 'Инвентаризация'
	И Я закрываю окно 'Инвентаризация'

Сценарий: 019.9. Logistic. Создание на основании инвентаризации для склада с ячейками оприходования товаров

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Инвентаризация'
	Тогда открылось окно 'Инвентаризация'
	И в таблице "Список" я перехожу к строке:
		| 'Статус'   | 'Ответственный' | 'Комментарий'                            | 'Склад'                    |
		| 'В работе' | 'Администратор' | 'Инвертаризация для склада с ячейками 2' | 'Склад(ячейки и контроль)' |
	И я нажимаю на кнопку 'Оприходование товаров'
	Тогда открылось окно 'Оприходование товаров (создание) *'
	Тогда таблица "Товары" стала равной:
		| 'Номенклатура' | 'Характеристика'   | 'Серия'                        | 'Ячейка'  | 'Количество' | 'Ед.' |
		| 'Сыр Сулугуни' | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | '1010011' | '10,000'     | 'кг'  |
	И в таблице "Товары" я нажимаю на кнопку 'Перезаполнить по основанию'
	Тогда открылось окно '1С:Предприятие'
	И я нажимаю на кнопку 'Да'
	Тогда таблица "Товары" стала равной:
		| 'Номенклатура' | 'Характеристика'   | 'Серия'                        | 'Ячейка'  | 'Количество' | 'Ед.' |
		| 'Сыр Сулугуни' | '<Без маркировки>' | '000001 до 20.10.2019 0:00:00' | '1010011' | '10,000'     | 'кг'  |
	И в поле 'Комментарий' я ввожу текст 'Оприходование товаров для склада с ячейками'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Оприходование товаров (создание) *' в течение 20 секунд
	Тогда открылось окно 'Инвентаризация'
	И Я закрываю окно 'Инвентаризация'

Сценарий: 019.10. Logistic. Создание на основании оприходования товаров - оприходования товаров организации для склада с ячейками

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Оприходования товаров'
	Тогда открылось окно 'Оприходования товаров'
	И в таблице "Список" я перехожу к строке:
		| 'Комментарий'                                 |
		| 'Оприходование товаров для склада с ячейками' |
	И я нажимаю на кнопку 'Оприходование товаров организации'
	Тогда открылось окно 'Оприходование товаров организации (создание)'
	И из выпадающего списка "Организация" я выбираю по строке 'Организация'
	И из выпадающего списка "Вид цен" я выбираю по строке 'Цена для оприходования/списания'
	И я перехожу к закладке "Товары"
	И в таблице "Товары" я перехожу к строке:
		| 'Количество' | 'Номенклатура' | 'Серия'                        | 'Характеристика'   |
		| '10,000'     | 'Сыр Сулугуни' | '000001 до 20.10.2019 0:00:00' | '<Без маркировки>' |
	И в таблице "Товары" в поле 'Цена, RUB' я ввожу текст '100,00'
	И в таблице "Товары" я завершаю редактирование строки
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Оприходование товаров организации (создание) *' в течение 20 секунд
	Тогда открылось окно 'Оприходования товаров'
	И Я закрываю окно 'Оприходования товаров'

Сценарий: 019.11. Logistic. Установка статуса Выполнено для инвентаризации 2 для склада с ячейками

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Инвентаризация'
	Тогда открылось окно 'Инвентаризация'
	И в таблице "Список" я перехожу к строке:
		| 'Комментарий'                            |
		| 'Инвертаризация для склада с ячейками 2' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Инвентаризация * от *'
	И из выпадающего списка "Статус" я выбираю точное значение 'Выполнено'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Инвентаризация * от * *' в течение 20 секунд
	Тогда открылось окно 'Инвентаризация'
	И Я закрываю окно 'Инвентаризация'

Сценарий: 019.12. Logistic. Завершение

	И Я закрыл все окна клиентского приложения