#language: ru

@logistic

Функционал: 008. Logistic. Создание ячеек и помещений через обработку Генерация топологии склада 

Сценарий: 008.1. Logistic. Создание ячеек и помещений через обработку Генерация топологии склада для Склада (ячейки, помещения)
	
	Дано Я подключаюсь под "Администратор"
	И В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Генерация топологии склада'
	Тогда открылось окно 'Генерация топологии склада'
	И из выпадающего списка "Склад" я выбираю по строке 'Склад (ячейки, помещения)'
	И в таблице "ПомещенияИЯчейки" я нажимаю на кнопку с именем 'ПомещенияИЯчейкиДобавить'
	И в таблице "ПомещенияИЯчейки" в поле 'Помещение' я ввожу текст 'Помещение1'
	И в таблице "ПомещенияИЯчейки" я изменяю флаг 'Ячейки'
	И в таблице "ПомещенияИЯчейки" я завершаю редактирование строки
	И в таблице "ПомещенияИЯчейки" я нажимаю на кнопку с именем 'ПомещенияИЯчейкиДобавить'
	И в таблице "ПомещенияИЯчейки" в поле 'Помещение' я ввожу текст 'Помещение2'
	И в таблице "ПомещенияИЯчейки" я завершаю редактирование строки
	И в таблице "ПомещенияИЯчейки" я нажимаю на кнопку 'Перейти к созданию ячеек'
	И я нажимаю на кнопку 'Записать'
	И в таблице "ПомещенияИЯчейки" я перехожу к строке:
		| 'Помещение'  | 
		| 'Помещение2' |
	И в таблице "ПомещенияИЯчейки" поле "Ячейки" имеет значение "Нет"
	И в таблице "ПомещенияИЯчейки" поле "Количество ячеек" имеет значение "1"
	И в таблице "ПомещенияИЯчейки" я нажимаю на кнопку 'Перейти к созданию ячеек'
	И в таблице "ПомещенияИЯчейки" я перехожу к строке:
		| 'Помещение'  |
		| 'Помещение1' |
	И в таблице "ПомещенияИЯчейки" поле "Ячейки" имеет значение "Да"
	И в таблице "ПомещенияИЯчейки" поле "Количество ячеек" имеет значение ""
	И в таблице "ПомещенияИЯчейки" я нажимаю на кнопку 'Перейти к созданию ячеек'
	Тогда открылось окно 'Генерация топологии склада'
	И элемент формы "Склад" стал равен "Склад (ячейки, помещения)"
	И элемент формы "Помещение" стал равен "Помещение1"
	И я изменяю флаг с именем 'ОбновлятьТипоразмер'
	И я нажимаю кнопку выбора у поля с именем "Типоразмер"
	Тогда открылось окно 'Типоразмеры ячеек'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Типоразмеры ячеек (создание)'
	И в поле 'Наименование' я ввожу текст 'Неограниченый'
	И я устанавливаю флаг с именем 'НеОграниченаПоРазмерам'
	И я устанавливаю флаг с именем 'НеОграниченаПоГрузоподъемности'
	И я нажимаю на кнопку 'Записать и закрыть'
	И я жду закрытия окна 'Типоразмеры ячеек (создание) *' в течение 20 секунд
	Тогда открылось окно 'Типоразмеры ячеек'
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Генерация топологии склада'
	И я изменяю флаг с именем 'ОбновлятьШаблонЭтикетки'
	И я нажимаю кнопку выбора у поля с именем "ШаблонЭтикетки"
	Тогда открылось окно 'Шаблоны этикеток и ценников'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Помощник создания этикетки'
	И в поле с именем 'Наименование' я ввожу текст 'Основная'
	И я нажимаю на кнопку 'Сохранить'
	Тогда открылось окно 'Шаблоны этикеток и ценников'
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Генерация топологии склада'
	И я изменяю флаг 'Вместимость в местах'
	И в поле 'ВместимостьВМестах' я ввожу текст '1'
	И я изменяю флаг 'Обновлять существующие'
	И я изменяю флаг 'Ряд'
	И я изменяю флаг 'Стеллажи'
	И я изменяю флаг 'Ярусы'
	И в поле 'ДиапазонЧиселОтРяд' я ввожу текст '2'
	И в поле 'ДиапазонЧиселДоРяд' я ввожу текст '2'
	И в поле 'ДиапазонЧиселОтСтеллажи' я ввожу текст '2'
	И в поле 'ДиапазонЧиселДоСтеллажи' я ввожу текст '2'
	И в поле 'ДиапазонЧиселОтЯрусы' я ввожу текст '2'
	И в поле 'ДиапазонЧиселДоЯрусы' я ввожу текст '3'
	И я перехожу к следующему реквизиту
	И элемент формы "Количество ячеек" стал равен "2"
	И элемент формы "Всего ярусов" стал равен "2"
	И элемент формы "Всего рядов" стал равен "1"
	И элемент формы "Всего стеллажей" стал равен "1"
	И элемент формы "Образец" стал равен "02-002-2"
	И я нажимаю на кнопку 'Предварительный просмотр'
	Тогда открылось окно 'Предварительный просмотр структуры склада'
	Тогда Табличный документ "" равен макету "Пред просмотр структуры склада, ячейки 1020022(3)"
	И Я закрываю окно 'Предварительный просмотр структуры склада'
	Тогда открылось окно 'Генерация топологии склада'
	И я нажимаю на кнопку 'Создать структуру'
	Тогда открылось окно '1С:Предприятие'
	И я нажимаю на кнопку 'Да'
	Тогда открылось окно 'Генерация топологии склада'
	И я изменяю флаг 'Обновлять существующие'
	И я нажимаю на кнопку 'Создать структуру'
	Тогда открылось окно '1С:Предприятие'
	И я нажимаю на кнопку 'Да'
	Тогда открылось окно 'Генерация топологии склада'
	И я изменяю флаг 'Обновлять существующие'
	И я нажимаю на кнопку 'Создать структуру'
	Тогда открылось окно '1С:Предприятие'
	И я нажимаю на кнопку 'Да'
	Тогда открылось окно 'Генерация топологии склада'
	И Я закрываю окно 'Генерация топологии склада'
	Тогда открылось окно 'Генерация топологии склада'
	И в таблице "ПомещенияИЯчейки" я перехожу к строке:
		| 'Помещение'  |
		| 'Помещение1' |
	И в таблице "ПомещенияИЯчейки" поле "Ячейки" имеет значение "Да"
	И в таблице "ПомещенияИЯчейки" поле "Количество ячеек" имеет значение "2"
	И я меняю значение переключателя 'Создавать' на 'Ячейки'
	И элемент формы "Количество ячеек" стал равен "3"
	И в таблице "ПомещенияИЯчейки" я нажимаю на кнопку 'Перейти к созданию ячеек'
	Тогда открылось окно 'Генерация топологии склада'
	И элемент формы "Склад" стал равен "Склад (ячейки, помещения)"
	И элемент формы "Помещение" стал равен ""
	И я изменяю флаг с именем 'ОбновлятьТипоразмер'
	И из выпадающего списка 'Типоразмер' я выбираю по строке 'Неограниченый'
	И я изменяю флаг с именем 'ОбновлятьШаблонЭтикетки'
	И из выпадающего списка с именем "ШаблонЭтикетки" я выбираю по строке 'Основная'
	И я изменяю флаг 'Вместимость в местах'
	И в поле 'ВместимостьВМестах' я ввожу текст '2'
	И я изменяю флаг 'Обновлять существующие'
	И я изменяю флаг 'Ряд'
	И я изменяю флаг 'Стеллажи'
	И я изменяю флаг 'Ярусы'
	И в поле 'ДиапазонЧиселОтРяд' я ввожу текст '3'
	И в поле 'ДиапазонЧиселДоРяд' я ввожу текст '3'
	И в поле 'ДиапазонЧиселОтСтеллажи' я ввожу текст '3'
	И в поле 'ДиапазонЧиселДоСтеллажи' я ввожу текст '4'
	И в поле 'ДиапазонЧиселОтЯрусы' я ввожу текст '3'
	И в поле 'ДиапазонЧиселДоЯрусы' я ввожу текст '3'
	И я перехожу к следующему реквизиту
	И элемент формы "Количество ячеек" стал равен "2"
	И элемент формы "Всего ярусов" стал равен "1"
	И элемент формы "Всего рядов" стал равен "1"
	И элемент формы "Всего стеллажей" стал равен "2"
	И элемент формы "Образец" стал равен "03-003-3"
	И я нажимаю на кнопку 'Предварительный просмотр'
	Тогда открылось окно 'Предварительный просмотр структуры склада'
	Тогда Табличный документ "" равен макету "Пред просмотр структуры склада, ячейки 03003(4)3"
	И Я закрываю окно 'Предварительный просмотр структуры склада'
	Тогда открылось окно 'Генерация топологии склада'
	И я изменяю флаг 'Ряд'
	И элемент формы "Количество ячеек" стал равен "2"
	И элемент формы "Всего ярусов" стал равен "1"
	И элемент формы "Всего стеллажей" стал равен "2"
	И элемент формы "Образец" стал равен "003-3"
	И я нажимаю на кнопку 'Предварительный просмотр'
	Тогда открылось окно 'Предварительный просмотр структуры склада'
	Тогда Табличный документ "" равен макету "Пред просмотр структуры склада, ячейки 003(4)3"
	И Я закрываю окно 'Предварительный просмотр структуры склада'
	Тогда открылось окно 'Генерация топологии склада'
	И я изменяю флаг 'Стеллажи'
	И элемент формы "Количество ячеек" стал равен "1"
	И элемент формы "Всего ярусов" стал равен "1"
	И элемент формы "Образец" стал равен "3"
	И я нажимаю на кнопку 'Предварительный просмотр'
	Тогда открылось окно 'Предварительный просмотр структуры склада'
	Тогда Табличный документ "" равен макету "Пред просмотр структуры склада, ячейки 3"
	И Я закрываю окно 'Предварительный просмотр структуры склада'
	Тогда открылось окно 'Генерация топологии склада'
	И я изменяю флаг 'Ряд'
	И элемент формы "Количество ячеек" стал равен "1"
	И элемент формы "Всего ярусов" стал равен "1"
	И элемент формы "Всего рядов" стал равен "1"
	И элемент формы "Образец" стал равен "03-3"
	И я нажимаю на кнопку 'Предварительный просмотр'
	Тогда открылось окно 'Предварительный просмотр структуры склада'
	Тогда Табличный документ "" равен макету "Пред просмотр структуры склада, ячейки 033"
	И Я закрываю окно 'Предварительный просмотр структуры склада'
	Тогда открылось окно 'Генерация топологии склада'
	И я изменяю флаг 'Ярусы'
	И элемент формы "Количество ячеек" стал равен "1"
	И элемент формы "Всего рядов" стал равен "1"
	И элемент формы "Образец" стал равен "03"
	И я нажимаю на кнопку 'Предварительный просмотр'
	Тогда открылось окно 'Предварительный просмотр структуры склада'
	Тогда Табличный документ "" равен макету "Пред просмотр структуры склада, ячейки 03"
	И Я закрываю окно 'Предварительный просмотр структуры склада'
	Тогда открылось окно 'Генерация топологии склада'
	И я изменяю флаг 'Стеллажи'
	И элемент формы "Количество ячеек" стал равен "2"
	И элемент формы "Всего рядов" стал равен "1"
	И элемент формы "Всего стеллажей" стал равен "2"
	И элемент формы "Образец" стал равен "03-003"
	И я нажимаю на кнопку 'Предварительный просмотр'
	Тогда открылось окно 'Предварительный просмотр структуры склада'
	Тогда Табличный документ "" равен макету "Пред просмотр структуры склада, ячейки 03003(4)"
	И Я закрываю окно 'Предварительный просмотр структуры склада'
	Тогда открылось окно 'Генерация топологии склада'
	И я изменяю флаг 'Ряд'
	И элемент формы "Количество ячеек" стал равен "2"
	И элемент формы "Всего стеллажей" стал равен "2"
	И элемент формы "Образец" стал равен "003"
	И я нажимаю на кнопку 'Предварительный просмотр'
	Тогда открылось окно 'Предварительный просмотр структуры склада'
	Тогда Табличный документ "" равен макету "Пред просмотр структуры склада, ячейки 003(4)"
	И Я закрываю окно 'Предварительный просмотр структуры склада'
	Тогда открылось окно 'Генерация топологии склада'
	И я изменяю флаг 'Ряд'
	И я изменяю флаг 'Ярусы'
	И я нажимаю на кнопку 'Создать структуру'
	Тогда открылось окно '1С:Предприятие'
	И я нажимаю на кнопку 'Да'
	Тогда открылось окно 'Генерация топологии склада'
	И Я закрываю окно 'Генерация топологии склада'
	Тогда открылось окно 'Генерация топологии склада *'
	И элемент формы "Количество ячеек" стал равен "5"
	И я нажимаю на кнопку открытия поля "Склад"
	Тогда открылось окно 'Склад (ячейки, помещения) (Склад)'
	И В текущем окне я нажимаю кнопку командного интерфейса 'Складские помещения'
	И в таблице "Список" я перехожу к строке:
		| 'Владелец'                  | 'Код'       | 'Наименование' | 'Номер помещения' |
		| 'Склад (ячейки, помещения)' | '000000001' | 'Помещение1'   | '1'               |
	И в таблице "Список" я перехожу к строке:
		| 'Владелец'                  | 'Код'       | 'Наименование' | 'Номер помещения' |
		| 'Склад (ячейки, помещения)' | '000000002' | 'Помещение2'   | '2'               |
	И В текущем окне я нажимаю кнопку командного интерфейса 'Складские ячейки'
	И в таблице "Список" я перехожу к строке:
		| 'Описание' | 
		| '03-003-3' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно '03-003-3 (Складская ячейка)'
	И элемент формы "Склад" стал равен "Склад (ячейки, помещения)"
	И элемент формы "Складское помещение" стал равен ""
	И элемент формы "Адрес" стал равен "03-003-3"
	И элемент формы "Помещение" стал равен "0"
	И элемент формы "Ряд" стал равен "03"
	И элемент формы "Стеллаж" стал равен "003"
	И элемент формы "Ярус" стал равен "3"
	И элемент формы "Описание" стал равен "03-003-3"
	И элемент формы "Типоразмер" стал равен "Неограниченый"
	И элемент формы "Вместимость в местах" стал равен "2"
	И элемент формы "Шаблон этикетки" стал равен "Основная"
	И Я закрываю окно '03-003-3 (Складская ячейка)'
	Тогда открылось окно 'Склад (ячейки, помещения) (Склад)'
	И в таблице "Список" я перехожу к строке:
		| 'Описание' |
		| '03-004-3' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно '03-004-3 (Складская ячейка)'
	И элемент формы "Склад" стал равен "Склад (ячейки, помещения)"
	И элемент формы "Складское помещение" стал равен ""
	И элемент формы "Адрес" стал равен "03-004-3"
	И элемент формы "Помещение" стал равен "0"
	И элемент формы "Ряд" стал равен "03"
	И элемент формы "Стеллаж" стал равен "004"
	И элемент формы "Ярус" стал равен "3"
	И элемент формы "Описание" стал равен "03-004-3"
	И элемент формы "Типоразмер" стал равен "Неограниченый"
	И элемент формы "Вместимость в местах" стал равен "2"
	И элемент формы "Шаблон этикетки" стал равен "Основная"
	И Я закрываю окно '03-004-3 (Складская ячейка)'
	Тогда открылось окно 'Склад (ячейки, помещения) (Склад)'
	И в таблице "Список" я перехожу к строке:
		| 'Описание'   |
		| '1-02-002-2' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно '1-02-002-2 (Складская ячейка)'
	И элемент формы "Склад" стал равен "Склад (ячейки, помещения)"
	И элемент формы "Складское помещение" стал равен "Помещение1"
	И элемент формы "Адрес" стал равен "1-02-002-2"
	И элемент формы "Помещение" стал равен "1"
	И элемент формы "Ряд" стал равен "02"
	И элемент формы "Стеллаж" стал равен "002"
	И элемент формы "Ярус" стал равен "2"
	И элемент формы "Описание" стал равен "1-02-002-2"
	И элемент формы "Типоразмер" стал равен "Неограниченый"
	И элемент формы "Вместимость в местах" стал равен "1"
	И элемент формы "Шаблон этикетки" стал равен "Основная"
	И Я закрываю окно '1-02-002-2 (Складская ячейка)'
	Тогда открылось окно 'Склад (ячейки, помещения) (Склад)'
	И в таблице "Список" я перехожу к строке:
		| 'Описание'   |
		| '1-02-002-3' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно '1-02-002-3 (Складская ячейка)'
	И элемент формы "Склад" стал равен "Склад (ячейки, помещения)"
	И элемент формы "Складское помещение" стал равен "Помещение1"
	И элемент формы "Адрес" стал равен "1-02-002-3"
	И элемент формы "Помещение" стал равен "1"
	И элемент формы "Ряд" стал равен "02"
	И элемент формы "Стеллаж" стал равен "002"
	И элемент формы "Ярус" стал равен "3"
	И элемент формы "Описание" стал равен "1-02-002-3"
	И элемент формы "Типоразмер" стал равен "Неограниченый"
	И элемент формы "Вместимость в местах" стал равен "1"
	И элемент формы "Шаблон этикетки" стал равен "Основная"
	И Я закрываю окно '1-02-002-3 (Складская ячейка)'
	Тогда открылось окно 'Склад (ячейки, помещения) (Склад)'
	И в таблице "Список" я перехожу к строке:
		| 'Описание'   |
		| 'Помещение2' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно '2000000 (Складская ячейка)'
	И элемент формы "Склад" стал равен "Склад (ячейки, помещения)"
	И элемент формы "Складское помещение" стал равен "Помещение2"
	И элемент формы "Адрес" стал равен "2000000"
	И элемент формы "Помещение" стал равен "2"
	И элемент формы "Ряд" стал равен "00"
	И элемент формы "Стеллаж" стал равен "000"
	И элемент формы "Ярус" стал равен "0"
	И элемент формы "Описание" стал равен "Помещение2"
	И элемент формы "Типоразмер" стал равен ""
	И элемент формы "Вместимость в местах" стал равен "0"
	И элемент формы "Шаблон этикетки" стал равен ""
	И Я закрываю окно '2000000 (Складская ячейка)'
	Тогда открылось окно 'Склад (ячейки, помещения) (Склад)'
	И Я закрываю окно 'Склад (ячейки, помещения) (Склад)'
	Тогда открылось окно 'Генерация топологии склада *'
	И Я закрываю окно 'Генерация топологии склада *'

Сценарий: 008.2. Logistic. Завершение

	И Я закрыл все окна клиентского приложения