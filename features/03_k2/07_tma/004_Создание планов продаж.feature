#language: ru

@tma

Функционал: 004. Tma. Создание планов продаж

Сценарий: 004.1. Tma. Создание сценариев товарного планирования

	Дано Я подключаюсь под "Администратор"
	Дано я удаляю объекты "Справочники.к2СценарииТоварногоПланирования" без контроля ссылок
	И Я создаю сценарий товарного планирования "Сценарий для годового плана продаж" с периодичностью "Месяц"
	И Я создаю сценарий товарного планирования "Сценарий плана продаж (месяц)" с периодичностью "Месяц" и годовым сценарием "Сценарий для годового плана продаж"
	
Сценарий: 004.2. Tma. Создание годового плана продаж

	Дано я удаляю объекты "Документы.к2ГодовойПланПродаж" без контроля ссылок
	И В командном интерфейсе я выбираю 'Маркетинг и продажи' 'Годовой план продаж'
	Тогда открылось окно 'Годовой план продаж'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Годовой план продаж (создание)'
	И в поле 'Год' я ввожу текст '2 019'
	И в поле 'от' я ввожу текст '10.01.2019  0:00:00'
	И из выпадающего списка "Сценарий" я выбираю по строке 'Сценарий для годового плана продаж'
	И я перехожу к закладке "План"
	И я заполняю таблицу "ДанныеПлана" данными
		| 'Группа партнеров' | 'Товарная категория' | '4 месяц'     | '1 месяц'     | '2 месяц'     | '3 месяц'     | '5 месяц'     | '6 месяц'     | '7 месяц'     | '8 месяц'     | '9 месяц'     | '10 месяц'    | '11 месяц'    | '12 месяц'    |
		| 'Лента'            | 'Рассольные сыры'    | '120 000,000' | '120 000,000' | '120 000,000' | '120 000,000' | '120 000,000' | '120 000,000' | '120 000,000' | '120 000,000' | '120 000,000' | '120 000,000' | '120 000,000' | '120 000,000' |
		| 'Магнит'           | 'Плавленые сыры'     | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' |
		| 'Магнит'           | 'Рассольные сыры'    | '100 000,000' | '100 000,000' | '100 000,000' | '100 000,000' | '100 000,000' | '100 000,000' | '100 000,000' | '100 000,000' | '100 000,000' | '100 000,000' | '100 000,000' | '100 000,000' |
		| 'Лента'            | 'Плавленые сыры'     | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' |
	Тогда таблица "ДанныеПлана" стала равной:
		| 'Группа партнеров' | 'Товарная категория' | '4 месяц'     | '1 месяц'     | '2 месяц'     | '3 месяц'     | '5 месяц'     | '6 месяц'     | '7 месяц'     | '8 месяц'     | '9 месяц'     | '10 месяц'    | '11 месяц'    | '12 месяц'    |
		| 'Лента'            | 'Рассольные сыры'    | '120 000,000' | '120 000,000' | '120 000,000' | '120 000,000' | '120 000,000' | '120 000,000' | '120 000,000' | '120 000,000' | '120 000,000' | '120 000,000' | '120 000,000' | '120 000,000' |
		| 'Магнит'           | 'Плавленые сыры'     | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' |
		| 'Магнит'           | 'Рассольные сыры'    | '100 000,000' | '100 000,000' | '100 000,000' | '100 000,000' | '100 000,000' | '100 000,000' | '100 000,000' | '100 000,000' | '100 000,000' | '100 000,000' | '100 000,000' | '100 000,000' |
		| 'Лента'            | 'Плавленые сыры'     | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' | '150 000,000' |
	И я нажимаю на кнопку 'Провести'
	Тогда открылось окно 'Годовой план продаж * от *'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Годовой план продаж * от *' в течение 20 секунд
	Тогда открылось окно 'Годовой план продаж'
	И Я закрываю окно 'Годовой план продаж'

Сценарий: 004.3. Tma. Заполнение периодов планирования продаж

	Дано я удаляю объекты "Справочники.к2ПериодыПланированияПродаж" без контроля ссылок
	И В командном интерфейсе я выбираю 'Маркетинг и продажи' 'Периоды планирования продаж'
	Тогда открылось окно 'Периоды планирования продаж'
	И я нажимаю на кнопку 'Заполнить периоды'
	Тогда открылось окно 'Форма заполнения периодов'
	И в поле 'Год заполнения' я ввожу текст '2 019'
	И из выпадающего списка "Периодичность" я выбираю по строке 'Месяц'
	И я нажимаю на кнопку 'Заполнить по периодичности'
	И Я закрываю окно 'Форма заполнения периодов'
	Тогда открылось окно 'Периоды планирования продаж'
	И Я закрываю окно 'Периоды планирования продаж'

Сценарий: 004.4. Tma. Проверка периодов планирования продаж

	И В командном интерфейсе я выбираю 'Маркетинг и продажи' 'Периоды планирования продаж'
	Тогда открылось окно 'Периоды планирования продаж'
	Тогда таблица "Список" стала равной:
		| 'Наименование'  | 'Периодичность' | 'Дата начала'        | 'Дата окончания'      |
		| '2019 Август'   | 'Месяц'         | '01.08.2019 0:00:00' | '31.08.2019 23:59:59' |
		| '2019 Апрель'   | 'Месяц'         | '01.04.2019 0:00:00' | '30.04.2019 23:59:59' |
		| '2019 Декабрь'  | 'Месяц'         | '01.12.2019 0:00:00' | '31.12.2019 23:59:59' |
		| '2019 Июль'     | 'Месяц'         | '01.07.2019 0:00:00' | '31.07.2019 23:59:59' |
		| '2019 Июнь'     | 'Месяц'         | '01.06.2019 0:00:00' | '30.06.2019 23:59:59' |
		| '2019 Май'      | 'Месяц'         | '01.05.2019 0:00:00' | '31.05.2019 23:59:59' |
		| '2019 Март'     | 'Месяц'         | '01.03.2019 0:00:00' | '31.03.2019 23:59:59' |
		| '2019 Ноябрь'   | 'Месяц'         | '01.11.2019 0:00:00' | '30.11.2019 23:59:59' |
		| '2019 Октябрь'  | 'Месяц'         | '01.10.2019 0:00:00' | '31.10.2019 23:59:59' |
		| '2019 Сентябрь' | 'Месяц'         | '01.09.2019 0:00:00' | '30.09.2019 23:59:59' |
		| '2019 Февраль'  | 'Месяц'         | '01.02.2019 0:00:00' | '28.02.2019 23:59:59' |
		| '2019 Январь'   | 'Месяц'         | '01.01.2019 0:00:00' | '31.01.2019 23:59:59' |
	И Я закрываю окно 'Периоды планирования продаж'

Сценарий: 004.5. Tma. Создание настроек планирования

	Дано я удаляю объекты "Справочники.к2НастройкиПланирования" без контроля ссылок
	И В командном интерфейсе я выбираю 'Маркетинг и продажи' 'Настройки планирования'
	Тогда открылось окно 'Настройки планирования'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Настройки планирования (создание)'
	И из выпадающего списка "Сценарий" я выбираю по строке 'Сценарий плана продаж (месяц)'
	И в поле 'Наименование' я ввожу текст 'Настройки планирования'
	И я перехожу к закладке "Отборы"
	И я нажимаю кнопку выбора у поля "Товарные категории"
	Тогда открылось окно 'Список значений'
	И Я добавляю в список значений значение "Плавленые сыры"
	И Я добавляю в список значений значение "Рассольные сыры"
	И я нажимаю на кнопку 'ОК'
	Тогда открылось окно 'Настройки планирования (создание) *'
	И я нажимаю кнопку выбора у поля "Группы планирования"
	Тогда открылось окно 'Список значений'
	И Я добавляю в список значений значение "Магнит"
	И Я добавляю в список значений значение "Лента"
	И я нажимаю на кнопку 'ОК'
	Тогда открылось окно 'Настройки планирования (создание) *'
	И я нажимаю на кнопку 'Записать'
	Тогда открылось окно 'Настройки планирования (Настройки планирования)'
	И я нажимаю на кнопку 'Закрыть'
	Тогда открылось окно 'Настройки планирования'

Сценарий: 004.6. Tma. Создание плана продаж

	Дано я удаляю объекты "Документы.к2ПланПродаж" без контроля ссылок
	И В командном интерфейсе я выбираю 'Маркетинг и продажи' 'План продаж'
	Тогда открылось окно 'План продаж'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'План продаж (создание)'
	И в поле 'от' я ввожу текст '10.01.2019  0:00:00'
	И из выпадающего списка "Период планирования" я выбираю по строке '2019 Июнь'
	И из выпадающего списка "Сценарий" я выбираю по строке 'Сценарий плана продаж (месяц)'
	И из выпадающего списка "Настройки планирования" я выбираю по строке 'Настройки планирования'
	И я перехожу к закладке "План"
	И я заполняю таблицу "ДанныеПлана" данными
		| 'Группа партнеров' | 'Июнь 2019'   | 'Товарная категория' |
		| 'Лента'            | '100 000,000' | 'Рассольные сыры'    |
		| 'Магнит'           | '80 000,000'  | 'Рассольные сыры'    |
		| 'Лента'            | '100 000,000' | 'Плавленые сыры'     |
		| 'Магнит'           | '80 000,000'  | 'Плавленые сыры'     |
	Тогда таблица "ДанныеПлана" стала равной:
		| 'Группа партнеров' | 'Июнь 2019'   | 'Товарная категория' | 'Количество'  |
		| 'Лента'            | '100 000,000' | 'Рассольные сыры'    | '100 000,000' |
		| 'Магнит'           | '80 000,000'  | 'Рассольные сыры'    | '80 000,000'  |
		| 'Лента'            | '100 000,000' | 'Плавленые сыры'     | '100 000,000' |
		| 'Магнит'           | '80 000,000'  | 'Плавленые сыры'     | '80 000,000'  |
	И я нажимаю на кнопку 'Провести'
	Тогда открылось окно 'План продаж * от *'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'План продаж * от *' в течение 20 секунд
	Тогда открылось окно 'План продаж'
	И Я закрываю окно 'План продаж'

Сценарий: 004.7. Tma. Завершение

	И Я закрыл все окна клиентского приложения