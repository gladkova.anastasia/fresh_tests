#language: ru

@tma

Функционал: 010. Tma. Создание скидки на основании маркетингового мероприятия

Сценарий: 010.1. Tma. Создание скидки на основании маркетингового мероприятия

	Дано Я подключаюсь под "Администратор"
	Дано я удаляю объекты "Документы.к2СкидкаНаценка" без контроля ссылок
	И В командном интерфейсе я выбираю 'Маркетинг и продажи' 'Маркетинговое мероприятие'
	Тогда открылось окно 'Маркетинговое мероприятие'
		И в таблице "Список" я перехожу к строке:
		| 'Дата'       | 'Организация' | 'Ответственный' | 'Профиль акции'   | 'Статус'     |
		| '18.06.2019' | 'Организация' | 'Администратор' | 'Профиль акции 1' | 'Утверждена' |
	И я нажимаю на кнопку 'Скидки (наценки)'
	Тогда открылось окно 'Создание скидок'
	И в таблице "ТаблицаКонтрагентов" я перехожу к строке:
		| 'Контрагент' |
		| 'Магнит'     |
	Тогда таблица "ТаблицаНоменклатуры" стала равной:
		| 'Номенклатура'  | 'Ед.' | 'Характеристика'    | 'Акционная' | 'Отпускная' | '%'      |
		| 'Брынза'        | 'кг'  | 'Фасованный 200 гр' | '100,000'   | '120,000'   | '16,670' |
		| 'Сливочный сыр' | 'кг'  | 'Фасованный 200 гр' | '90,000'    | '100,000'   | '10,000' |
	И в таблице "ТаблицаСоглашений" я нажимаю на кнопку 'Подобрать'
	Тогда открылось окно 'Подбор соглашений'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'           |
		| 'Соглашение для Магнита' |
	И в таблице "Список" я выбираю текущую строку
	И я нажимаю на кнопку 'Перенести в документ'
	Тогда открылось окно 'Создание скидок'
	Тогда таблица "ТаблицаСоглашений" стала равной:
		| 'Соглашение'             | 'До характеристик' |
		| 'Соглашение для Магнита' | 'Нет'              |
	И в таблице "ТаблицаКонтрагентов" я перехожу к строке:
		| 'Контрагент' |
		| 'Лента'      |
	Тогда таблица "ТаблицаНоменклатуры" стала равной:
		| 'Номенклатура'  | 'Ед.' | 'Характеристика'    | 'Акционная' | 'Отпускная' | '%'      |
		| 'Брынза'        | 'кг'  | 'Фасованный 200 гр' | '100,000'   | '120,000'   | '16,670' |
		| 'Сливочный сыр' | 'кг'  | 'Фасованный 200 гр' | '90,000'    | '100,000'   | '10,000' |
	И в таблице "ТаблицаСоглашений" я нажимаю на кнопку 'Подобрать'
	Тогда открылось окно 'Подбор соглашений'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'         |
		| 'Соглашение для Ленты' |
	И в таблице "Список" я выбираю текущую строку
	И я нажимаю на кнопку 'Перенести в документ'
	Тогда открылось окно 'Создание скидок'
	Тогда таблица "ТаблицаСоглашений" стала равной:
		| 'Соглашение'           | 'До характеристик' |
		| 'Соглашение для Ленты' | 'Нет'              |
	И я меняю значение переключателя 'Скидка' на 'Фиксированная цена'
	И я нажимаю на кнопку 'Оформить скидки'
	И Я закрываю окно 'Создание скидок'
	Тогда открылось окно 'Закрытие'
	И я нажимаю на кнопку 'Да'
	Тогда открылось окно 'Маркетинговое мероприятие'
	И Я закрываю окно 'Маркетинговое мероприятие'

Сценарий: 010.2. Tma. Проверка созданных скидок

	И В командном интерфейсе я выбираю 'Заказы' 'Скидки (наценки)'
	Тогда открылось окно 'Скидки (наценки)'
	И из выпадающего списка "Соглашение" я выбираю по строке 'Соглашение для Магнита'
	И я перехожу к следующему реквизиту
	И в таблице "Список" я перехожу к строке:
		| 'Действует по'       | 'Действует с'        | 'Ответственный' | 'Согласование' | 'Статус'   | 'Тип'                |
		| '22.06.2019 0:00:00' | '18.06.2019 0:00:00' | 'Администратор' | 'Согласован'   | 'Завершен' | 'Фиксированная цена' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Скидка (наценка) * от * (Завершен 22.06.2019)'
	И элемент формы "Действует" стал равен "18.06.2019 - 22.06.2019"
	И элемент формы "Согласование" стал равен "Согласован"
	И элемент формы с именем "Тип" стал равен "Фиксированная цена"
	Тогда таблица "Соглашения" стала равной:
		| 'Соглашение'             | 'Контрагент' | 'Вид цен'        | 'До характеристик' |
		| 'Соглашение для Магнита' | 'Магнит'     | 'Отпускная(ТМА)' | 'Нет'              |
	Тогда таблица "Товары" стала равной:
		| 'Номенклатура'  | 'Упаковка' | 'Цена, RUB' |
		| 'Брынза'        | 'кг'       | '100,00'    |
		| 'Сливочный сыр' | 'кг'       | '90,00'     |
	И Я закрываю окно 'Скидка (наценка) * от * (Завершен 22.06.2019)'
	Тогда открылось окно 'Скидки (наценки)'
	И я нажимаю кнопку очистить у поля "Соглашение"
	И из выпадающего списка "Соглашение" я выбираю по строке 'Соглашение для Ленты'
	И я перехожу к следующему реквизиту
	И в таблице "Список" я перехожу к строке:
		| 'Действует по'       | 'Действует с'        | 'Ответственный' | 'Согласование' | 'Статус'      | 'Тип'                |
		| '22.06.2019 0:00:00' | '18.06.2019 0:00:00' | 'Администратор' | 'Согласован'     | 'Завершен' | 'Фиксированная цена' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Скидка (наценка) * от * (Завершен 22.06.2019)'
	И элемент формы "Действует" стал равен "18.06.2019 - 22.06.2019"
	И элемент формы "Согласование" стал равен "Согласован"
	И элемент формы с именем "Тип" стал равен "Фиксированная цена"
	Тогда таблица "Соглашения" стала равной:
		| 'Соглашение'           | 'Контрагент' | 'Вид цен'        | 'До характеристик' |
		| 'Соглашение для Ленты' | 'Лента'      | 'Отпускная(ТМА)' | 'Нет'              |
	Тогда таблица "Товары" стала равной:
		| 'Номенклатура'  | 'Упаковка' | 'Цена, RUB' |
		| 'Брынза'        | 'кг'       | '100,00'    |
		| 'Сливочный сыр' | 'кг'       | '90,00'     |
	И Я закрываю окно 'Скидка (наценка) * от * (Завершен 22.06.2019)'
	Тогда открылось окно 'Скидки (наценки)'
	И я нажимаю кнопку очистить у поля "Соглашение"
	И Я закрываю окно 'Скидки (наценки)'

Сценарий: 010.3. Tma. Завершение

	И Я закрыл все окна клиентского приложения