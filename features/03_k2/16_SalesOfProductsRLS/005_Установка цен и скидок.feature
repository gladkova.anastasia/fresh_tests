
#language: ru

@SalesOfProductsRLS

Функционал: 005. SalesOfProductsRLS. Установка цен и фиксация скидок

Сценарий: 005.1. SalesOfProductsRLS. Установка цен

	Дано Я подключаюсь под "Ценообразование"
	Дано я устанавливаю текущему сеансу заголовок приложения "16.005. РЛС Реализация товаров"
	Дано я удаляю объекты "Документы.к2УстановкаЦенНоменклатуры" без контроля ссылок
	И В командном интерфейсе я выбираю 'Заказы' 'История изменения цен'
	Тогда открылось окно 'История изменения цен'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Установка цен (создание)'
	И Я добавляю вид цены "Базовый" в документ установка цен
	И Я добавляю вид цены "Базовый для зависимых" в документ установка цен
	И Я добавляю вид цены "Зависимый 1" в документ установка цен
	И Я добавляю вид цены "Зависимый 2" в документ установка цен
	И Я добавляю вид цены "Ашан" в документ установка цен
	И Я добавляю вид цены "Перекресток" в документ установка цен
	И Я добавляю вид цены "5ка" в документ установка цен
	И в поле 'от' я ввожу текст '01.08.2019'
	И в поле 'Комментарий' я ввожу текст 'Документ 1. Установка цен'
	И я нажимаю на кнопку 'Перейти к установке цен'
	И в таблице "ДеревоЦен" я нажимаю на кнопку 'Подобрать'
	Тогда открылось окно 'Подбор номенклатуры'
	И в таблице "СписокИерархия" я перехожу к строке:
		| 'Ссылка' |
		| 'Сыры'   |
	И Я подбираю номенклатуру "Сыр Адыгейский"
	И Я подбираю номенклатуру "Сыр Костромской"
	И Я подбираю номенклатуру "Сыр Пошехонский"
	И я нажимаю на кнопку "Перенести"
	Тогда открылось окно 'Установка цен (создание) *'
	И Я устанавливаю для номенклатуры "Сыр Адыгейский" и цены "ДеревоЦенНоваяЦена_4" цену "100,00"
	И Я устанавливаю для номенклатуры "Сыр Костромской" и цены "ДеревоЦенНоваяЦена_4" цену "50,00"
	И Я устанавливаю для номенклатуры "Сыр Пошехонский" и цены "ДеревоЦенНоваяЦена_4" цену "250,00"
	И Я устанавливаю для номенклатуры "Сыр Адыгейский" и цены "ДеревоЦенНоваяЦена_5" цену "100,00"
	И Я устанавливаю для номенклатуры "Сыр Костромской" и цены "ДеревоЦенНоваяЦена_5" цену "50,00"
	И Я устанавливаю для номенклатуры "Сыр Пошехонский" и цены "ДеревоЦенНоваяЦена_5" цену "250,00"
	И Я устанавливаю для номенклатуры "Сыр Адыгейский" и цены "ДеревоЦенНоваяЦена_8" цену "100,00"
	И Я устанавливаю для номенклатуры "Сыр Костромской" и цены "ДеревоЦенНоваяЦена_8" цену "50,00"
	И Я устанавливаю для номенклатуры "Сыр Пошехонский" и цены "ДеревоЦенНоваяЦена_8" цену "250,00"
	И Я устанавливаю для номенклатуры "Сыр Адыгейский" и цены "ДеревоЦенНоваяЦена_10" цену "200,00"
	И Я устанавливаю для номенклатуры "Сыр Костромской" и цены "ДеревоЦенНоваяЦена_10" цену "250,00"
	И Я устанавливаю для номенклатуры "Сыр Пошехонский" и цены "ДеревоЦенНоваяЦена_10" цену "350,00"
	И Я устанавливаю для номенклатуры "Сыр Адыгейский" и цены "ДеревоЦенНоваяЦена_9" цену "200,00"
	И Я устанавливаю для номенклатуры "Сыр Костромской" и цены "ДеревоЦенНоваяЦена_9" цену "250,00"
	И Я устанавливаю для номенклатуры "Сыр Пошехонский" и цены "ДеревоЦенНоваяЦена_9" цену "350,00"
	И в таблице "ДеревоЦен" я нажимаю на кнопку 'Добавить'
	Тогда открылось окно 'Выберите номенклатуру'
	И Я подбираю номенклатуру "Крылья куринные (подложка)"
	Тогда открылось окно 'Установка цен (создание) *'
	И Я устанавливаю для номенклатуры "Крылья куринные (подложка)" и цены "ДеревоЦенНоваяЦена_4" цену "200,00"
	И Я устанавливаю для номенклатуры "Крылья куринные (подложка)" и цены "ДеревоЦенНоваяЦена_5" цену "200,00"
	И Я устанавливаю для номенклатуры "Крылья куринные (подложка)" и цены "ДеревоЦенНоваяЦена_8" цену "200,00"
	И Я устанавливаю для номенклатуры "Крылья куринные (подложка)" и цены "ДеревоЦенНоваяЦена_9" цену "200,00"
	И Я устанавливаю для номенклатуры "Крылья куринные (подложка)" и цены "ДеревоЦенНоваяЦена_10" цену "200,00"
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Установка цен (создание) *' в течение 20 секунд
	Тогда открылось окно 'История изменения цен'
	И Я закрываю окно 'История изменения цен'

Сценарий: 005.2. SalesOfProductsRLS. Завершение

	И я закрываю сеанс TESTCLIENT