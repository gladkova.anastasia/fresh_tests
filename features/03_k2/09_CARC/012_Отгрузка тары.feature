﻿#language: ru

@carc

Функционал: 012. CARC. Отгрузка тары

Сценарий: 012.1. CARC. Создание плана отгрузки

	Дано Я подключаюсь под "Администратор"
	Дано я удаляю объекты "Документы.к2ПланОтгрузки" без контроля ссылок
	Дано я удаляю объекты "Документы.к2РаспоряжениеНаОтгрузку" без контроля ссылок
	И В командном интерфейсе я выбираю 'Склад и доставка' 'Планы отгрузки'
	Тогда открылось окно 'Планы отгрузки'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'План отгрузки (создание)'
	И в поле 'Дата отгрузки' я ввожу текст '24.08.2020'
	И из выпадающего списка "Склад" я выбираю по строке 'Склад с ячейкой тары'
	И из выпадающего списка "Статус" я выбираю точное значение 'Собрано'
	И я перехожу к закладке "Товары"
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" из выпадающего списка "Номенклатура" я выбираю по строке 'Творожный сырок'
	И в таблице "Товары" в поле 'Количество' я ввожу текст '10,000'
	И в таблице "Товары" я завершаю редактирование строки
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'План отгрузки (создание) *' в течение 20 секунд
	Тогда открылось окно 'Планы отгрузки'
	И Я закрываю окно 'Планы отгрузки'

Сценарий: 012.2. CARC. Отгрузка тары
	
	И В командном интерфейсе я выбираю 'Склад и доставка' 'Упаковочные листы'
	Тогда открылось окно 'Упаковочные листы'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Упаковочный лист (создание)'
	И из выпадающего списка "Организация" я выбираю по строке 'Организация'
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" из выпадающего списка "Номенклатура" я выбираю по строке 'Творожный сырок'
	И в таблице "Товары" в поле 'Количество' я ввожу текст '10,000'
	И в таблице "Товары" в поле 'Количество упаковок' я ввожу текст '10,000'
	И в таблице "Товары" я завершаю редактирование строки
	И я нажимаю на кнопку 'Провести'
	И я нажимаю на кнопку 'Паспорт паллеты'
	И я жду открытия окна 'Паллета *' в течение 20 секунд
	И я жду когда в табличном документе "ТекущаяПечатнаяФорма" заполнится ячейка "R28C6" в течение 20 секунд
	И я запоминаю значение ячейки табличного документа "ТекущаяПечатнаяФорма" "R28C6" в переменную "ШтрихКодТворожногоСырка"
	И Я закрываю окно 'Паллета *'
	Тогда открылось окно 'Паллета *'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Паллета *' в течение 20 секунд
	Тогда открылось окно 'Упаковочные листы'
	И я закрываю окно 'Упаковочные листы'
	И В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Меню учетных точек'
	Тогда открылось окно '[Администратор]: Меню учетных точек'
	И я нажимаю кнопку выбора у поля "УчетнаяТочка"
	Тогда открылось окно 'Учетные точки'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'         |
		| 'Склад с ячейкой тары' |
	И я нажимаю на кнопку с именем 'Выбрать'
	Тогда открылось окно '[Администратор]: Меню учетных точек'
	И я нажимаю на кнопку 'Отгрузка'
	И я нажимаю на кнопку 'Выбрать'
	И я перехожу к следующему реквизиту
	И в поле 'Штрихкод' я ввожу текст "$$ШтрихКодСинейКоробки$$"
	И я перехожу к следующему реквизиту
	И в поле 'Штрихкод' я ввожу текст "$$ШтрихКодПластиковогоПалета$$"
	И я перехожу к следующему реквизиту
	И в поле 'Штрихкод' я ввожу текст "$ШтрихКодТворожногоСырка$"
	И я перехожу к следующему реквизиту
	И я нажимаю на кнопку 'Завершить'
	И я нажимаю на кнопку 'КомандаОК'
	И Я закрываю окно ''
	И я нажимаю на кнопку 'КомандаДа'
	И Я закрываю окно '[Администратор]: Меню учетных точек'

Сценарий: 012.3. CARC. Проверка распоряжения на отгрузку

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Распоряжения на отгрузку'
	Тогда открылось окно 'Распоряжения на отгрузку'
	И в таблице "Список" я перехожу к строке:
		| 'Склад'                |
		| 'Склад с ячейкой тары' |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Распоряжение на отгрузку * от *'
	И элемент формы с именем "Склад" стал равен 'Склад с ячейкой тары'
	И таблица "Товары" стала равной:
		| 'Количество' | 'Ед.' | 'Номенклатура'      | 'Характеристика'    | 'Серия'             | 'В ед. хранения' | 'Ячейка'  |
		| '1,000'      | 'шт'  | 'Коробка синяя'     | '<не используются>' | '<не используются>' | '1,000, шт'      | '0010014' |
		| '1,000'      | 'шт'  | 'Пластиковый палет' | '<не используются>' | '<не используются>' | '1,000, шт'      | '0010014' |
		| '10,000'     | 'шт'  | 'Творожный сырок'   | '<не используются>' | '<не используются>' | '10,000, шт'     | '0010012' |
	И элемент формы с именем "СпособСоздания" стал равен 'Терминал сбора данных'
	И Я закрываю окно 'Распоряжение на отгрузку * от *'
	Тогда открылось окно 'Распоряжения на отгрузку'
	И Я закрываю окно 'Распоряжения на отгрузку'

Сценарий: 012.4. CARC. Завершение

	И Я закрыл все окна клиентского приложения