﻿#language: ru

@carc

Функционал: 010. CARC. Перемещение тары между складами

Сценарий: 010.1. CARC. Перемещение тары между складами

	Дано Я подключаюсь под "Администратор"
	Дано я удаляю объекты "Документы.к2РаспоряжениеНаПеремещение" без контроля ссылок
	И В командном интерфейсе я выбираю 'Склад и доставка' 'Упаковочные листы'
	Тогда открылось окно 'Упаковочные листы'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Упаковочный лист (создание)'
	И из выпадающего списка "Организация" я выбираю по строке 'Организация'
	И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
	И в таблице "Товары" из выпадающего списка "Номенклатура" я выбираю по строке 'Творожный сырок'
	И в таблице "Товары" в поле 'Количество' я ввожу текст '10,000'
	И в таблице "Товары" в поле 'Количество упаковок' я ввожу текст '10,000'
	И в таблице "Товары" я завершаю редактирование строки
	И я нажимаю на кнопку 'Провести'
	И я нажимаю на кнопку 'Паспорт паллеты'
	И я жду открытия окна 'Паллета *' в течение 20 секунд
	И я жду когда в табличном документе "ТекущаяПечатнаяФорма" заполнится ячейка "R28C6" в течение 20 секунд
	И я запоминаю значение ячейки табличного документа "ТекущаяПечатнаяФорма" "R28C6" в переменную "ШтрихКодТворожногоСырка"
	И Я закрываю окно 'Паллета *'
	Тогда открылось окно 'Паллета *'
	И я нажимаю на кнопку 'Провести и закрыть'
	И я жду закрытия окна 'Паллета *' в течение 20 секунд
	Тогда открылось окно 'Упаковочные листы'
	И я закрываю окно 'Упаковочные листы'
	И В командном интерфейсе я выбираю 'Нормативно-справочная информация' 'Меню учетных точек'
	Тогда открылось окно '[Администратор]: Меню учетных точек'
	И я нажимаю кнопку выбора у поля "УчетнаяТочка"
	Тогда открылось окно 'Учетные точки'
	И в таблице "Список" я перехожу к строке:
		| 'Наименование'         |
		| 'Склад с ячейкой тары' |
	И я нажимаю на кнопку с именем 'Выбрать'
	Тогда открылось окно '[Администратор]: Меню учетных точек'
	И я нажимаю на кнопку 'Перемещение между складами'
	И я нажимаю на кнопку 'Выбор склад получатель'
	И в таблице "Список" я перехожу к строке:
		| 'Ссылка'                 |
		| 'Склад с ячейкой тары 2' |
	И я нажимаю на кнопку 'Готово'
	И я нажимаю на кнопку 'Далее'
	И я нажимаю на кнопку 'Создать'
	И в поле 'Штрихкод' я ввожу текст "$$ШтрихКодСинейКоробки$$"
	И я перехожу к следующему реквизиту
	И в поле 'Штрихкод' я ввожу текст "$$ШтрихКодПластиковогоПалета$$"
	И я перехожу к следующему реквизиту
	И в поле 'Штрихкод' я ввожу текст "$ШтрихКодТворожногоСырка$"
	И я перехожу к следующему реквизиту
	И я нажимаю на кнопку 'Завершить'
	И я нажимаю на кнопку 'КомандаОК'
	И Я закрываю окно ''
	И я нажимаю на кнопку 'КомандаДа'
	И Я закрываю окно '[Администратор]: Меню учетных точек'

Сценарий: 010.2. CARC. Проверка распоряжения на перемещение на другой склад

	И В командном интерфейсе я выбираю 'Склад и доставка' 'Распоряжения на перемещение'
	Тогда открылось окно 'Распоряжения на перемещение'
	И в таблице "Список" я перехожу к строке:
		| 'Склад отправитель'    | 'Склад получатель'       | 'Способ создания'       | 'Статус'    | 'Тип перемещения' |
		| 'Склад с ячейкой тары' | 'Склад с ячейкой тары 2' | 'Терминал сбора данных' | 'Выполнено' | 'Перемещение'     |
	И в таблице "Список" я выбираю текущую строку
	Тогда открылось окно 'Распоряжение на перемещение * от *'
	И элемент формы с именем "СкладОтправитель" стал равен 'Склад с ячейкой тары'
	И элемент формы с именем "СкладПолучатель" стал равен 'Склад с ячейкой тары 2'
	И элемент формы с именем "ТипПеремещения" стал равен 'Перемещение'
	И в таблице "ТоварыПлан" количество строк "равно" 0
	И таблица "Товары" стала равной:
		| 'Количество' | 'Ед.' | 'Номенклатура'      | 'Характеристика'    | 'Серия'             | 'В ед. хранения' | 'Ячейка отправитель' | 'Ячейка получатель' |
		| '1,000'      | 'шт'  | 'Коробка синяя'     | '<не используются>' | '<не используются>' | '1,000, шт'      | '0010014'            | '0010024'           |
		| '1,000'      | 'шт'  | 'Пластиковый палет' | '<не используются>' | '<не используются>' | '1,000, шт'      | '0010014'            | '0010024'           |
		| '10,000'     | 'шт'  | 'Творожный сырок'   | '<не используются>' | '<не используются>' | '10,000, шт'     | '0010012'            | '0010021'           |
	И элемент формы с именем "СпособСоздания" стал равен 'Терминал сбора данных'
	И Я закрываю окно 'Распоряжение на перемещение * от *'
	Тогда открылось окно 'Распоряжения на перемещение'
	И Я закрываю окно 'Распоряжения на перемещение'

Сценарий: 010.3. CARC. Завершение

	И Я закрыл все окна клиентского приложения	