﻿#language: ru

@MovingProductsRLS

Функционал: 001. MovingProductsRLS. НСИ.

Сценарий: 001.1. SalesOfProductsRLS. Настройки исполнения заказов
	
	Дано Я подключаюсь под "Администратор"
	Дано я устанавливаю текущему сеансу заголовок приложения "17.001. РЛС Перемещения товаров"
	И В командном интерфейсе я выбираю 'Заказы' 'Настройки исполнения заказов'
	Тогда открылось окно 'Настройки исполнения заказов'
	И я устанавливаю флаг 'Использовать неавтоматизированные склады при оформлении накладных'
	И Я закрываю окно 'Настройки исполнения заказов'

Сценарий: 001.2. MovingProductsRLS. Загрузка НСИ

	Дано Я загружаю общую НСИ

Сценарий: 001.3. MovingProductsRLS. Создание валюты

	И Я создаю валюту RUB

Сценарий: 001.4. MovingProductsRLS. Завершение

	И Я закрыл все окна клиентского приложения