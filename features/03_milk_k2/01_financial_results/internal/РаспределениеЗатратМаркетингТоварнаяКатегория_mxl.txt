﻿														
Движения документа Распределение затрат *														
														
Регистр накопления "Доходы и расходы" (6)														
Стандартные реквизиты		Измерения									Ресурсы		Реквизиты	
Активность	Период	Организация	Подразделение	Контрагент	Заказ клиента	Направление деятельности	Номенклатура	Статья	Аналитика доходов	Аналитика затрат	Сумма доходов	Сумма расходов	Пояснение	Операция
Да	28.02.2021 *	ТД_Молочный мир	Маркетинг	ИП Смирнов	Заказ клиента * от 21.02.2021 [Создан]	Основное направление	Молоко 2,5 %	Маркетинг (товарная категория)		Молоко		18,21	Маркетинг (товарная категория)	Распределение затрат
Да	28.02.2021 *	ТД_Молочный мир	Маркетинг	ИП Смирнов	Заказ клиента * от 21.02.2021 [Создан]	Основное направление	Молоко 3,2 %	Маркетинг (товарная категория)		Молоко		20,05	Маркетинг (товарная категория)	Распределение затрат
Да	28.02.2021 *	ТД_Молочный мир	Маркетинг	Магнит	Заказ клиента * от 20.02.2021 [Создан]	Основное направление	Молоко 2,5 %	Маркетинг (товарная категория)		Молоко		8,20	Маркетинг (товарная категория)	Распределение затрат
Да	28.02.2021 *	ТД_Молочный мир	Маркетинг	Магнит	Заказ клиента * от 20.02.2021 [Создан]	Основное направление	Молоко 3,2 %	Маркетинг (товарная категория)		Молоко		18,94	Маркетинг (товарная категория)	Распределение затрат
Да	28.02.2021 *	ТД_Молочный мир	Маркетинг	Х5	Заказ клиента * от 20.02.2021 [Создан]	Основное направление	Молоко 2,5 %	Маркетинг (товарная категория)		Молоко		16,39	Маркетинг (товарная категория)	Распределение затрат
Да	28.02.2021 *	ТД_Молочный мир	Маркетинг	Х5	Заказ клиента * от 20.02.2021 [Создан]	Основное направление	Молоко 3,2 %	Маркетинг (товарная категория)		Молоко		18,21	Маркетинг (товарная категория)	Распределение затрат

Регистр накопления "Затраты" (1)										
Стандартные реквизиты			Измерения					Ресурсы	Реквизиты	
Вид движения	Активность	Период	Организация	Подразделение	Направление деятельности	Статья затрат	Аналитика затрат	Сумма	Пояснение	Операция
Расход	Да	28.02.2021 *	ТД_Молочный мир	Маркетинг	Основное направление	Маркетинг (товарная категория)	Молоко	100,00	Маркетинг (товарная категория)	Распределение затрат