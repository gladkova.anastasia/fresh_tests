#language: ru

@financial_results

Функционал: 009. financial_results. Отчет Доходы и расходы

Сценарий: 009.1. financial_results. Отчет Доходы и расходы

	Дано Я подключаюсь под "Администратор"
	Дано я устанавливаю текущему сеансу заголовок приложения "01.009. ФинРез"
	И В командном интерфейсе я выбираю 'Деньги' 'Отчеты'
	Тогда открылось окно 'Отчеты по финансам'
	И я нажимаю на гиперссылку 'Доходы и расходы'
	Тогда открылось окно 'Доходы и расходы'
	И Я в отчете устанавливаю период "01.02.2021" "28.02.2021"
	И из выпадающего списка с именем "КомпоновщикНастроекПользовательскиеНастройкиЭлемент1Значение" я выбираю по строке 'ТД_Молочный мир'
	И я нажимаю на кнопку с именем 'СформироватьОтчет'
	Тогда Табличный документ "ОтчетТабличныйДокумент" равен макету "ОтчетДоходыРасходыФевраль" по шаблону
	И Я закрываю окно 'Доходы и расходы'
	Тогда открылось окно 'Отчеты по финансам'
	И Я закрываю окно 'Отчеты по финансам'