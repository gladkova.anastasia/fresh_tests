#language: ru

@demo_base_milk
@financial_results

Функционал: 003. financial_results. Создание общих документов

Сценарий: 003.1. financial_results. Создание поступлений товаров и услуг 20.02.2021

	Дано Я подключаюсь под "Администратор"
	И Я начинаю создавать поступление товаров на дату "20.02.2021"
	И из выпадающего списка с именем "Холдинг" я выбираю по строке 'Дом молока'
	И из выпадающего списка с именем "Контрагент" я выбираю по строке 'Нижегородское молоко'
	И из выпадающего списка "Организация" я выбираю по строке 'ТД_Молочный мир'
	И из выпадающего списка "Склад" я выбираю по строке 'Склад готовой продукции'
	И из выпадающего списка "Статус" я выбираю точное значение 'Подтверждено'
	И в поле 'Номер вх.' я ввожу текст '1'
	И я перехожу к закладке "Факт"
	И я заполняю таблицу "Товары" данными
		| 'Номенклатура' | 'Количество' | 'Цена, RUB' |
		| 'Молоко 3,2 %' | '20,000'     | '34,00'     |
		| 'Молоко 2,5 %' | '15,000'     | '31,00'     |
		| 'Кефир'        | '30,000'     | '32,00'     |
		| 'Творог'       | '28,000'     | '85,00'     |
	И Я создаю в поступлении товаров для номенклатуры "Молоко 3,2 %" серию "000001" с датой выпуска "20.02.2021" и окончанием срока годности "06.03.2021"
	И Я создаю в поступлении товаров для номенклатуры "Молоко 2,5 %" серию "000001" с датой выпуска "20.02.2021" и окончанием срока годности "06.03.2021"
	И Я создаю в поступлении товаров для номенклатуры "Кефир" серию "000001" с датой выпуска "20.02.2021" и окончанием срока годности "06.03.2021"
	И Я создаю в поступлении товаров для номенклатуры "Творог" серию "000001" с датой выпуска "20.02.2021" и окончанием срока годности "06.03.2021"
	И Я заканчиваю создавать поступление товаров и услуг

Сценарий: 003.2. financial_results. Создание поступлений товаров и услуг 21.02.2021

	И Я начинаю создавать поступление товаров на дату "21.02.2021"
	И из выпадающего списка с именем "Холдинг" я выбираю по строке 'Дом молока'
	И из выпадающего списка с именем "Контрагент" я выбираю по строке 'Нижегородское молоко'
	И из выпадающего списка "Организация" я выбираю по строке 'ТД_Молочный мир'
	И из выпадающего списка "Склад" я выбираю по строке 'Склад готовой продукции'
	И из выпадающего списка "Статус" я выбираю точное значение 'Подтверждено'
	И в поле 'Номер вх.' я ввожу текст '1'
	И я перехожу к закладке "Факт"
	И я заполняю таблицу "Товары" данными
		| 'Номенклатура' | 'Количество' | 'Цена, RUB' |
		| 'Молоко 3,2 %' | '10,000'     | '36,00'     |
		| 'Молоко 2,5 %' | '10,000'     | '32,00'     |
	И Я создаю в поступлении товаров для номенклатуры "Молоко 3,2 %" серию "000001" с датой выпуска "21.02.2021" и окончанием срока годности "07.03.2021"
	И Я создаю в поступлении товаров для номенклатуры "Молоко 2,5 %" серию "000001" с датой выпуска "21.02.2021" и окончанием срока годности "07.03.2021"
	И Я заканчиваю создавать поступление товаров и услуг

Сценарий: 003.3. financial_results. Маркетинговое мероприятие

	И В командном интерфейсе я выбираю 'Маркетинг и продажи' 'Маркетинговое мероприятие'
	Тогда открылось окно 'Маркетинговое мероприятие'
	И я нажимаю на кнопку с именем 'ФормаСоздать'
	Тогда открылось окно 'Маркетинговое мероприятие (создание)'
	И из выпадающего списка с именем "Организация" я выбираю по строке 'ТД_Молочный мир'
	И из выпадающего списка с именем "Вид" я выбираю точное значение 'Отгрузка конечному клиенту'
	И из выпадающего списка с именем "Статус" я выбираю точное значение 'Утверждена'
	И в поле с именем 'ДатаНачалаДействияАкционныхЦен' я ввожу текст '15.02.2021'
	И в поле с именем 'ДатаОкончанияДействияАкционныхЦен' я ввожу текст '25.02.2021'
	И я заполняю таблицу "Номенклатура" данными
		| 'Номенклатура' |
		| 'Кефир'        |
	И я заполняю таблицу "Контрагенты" данными
		| 'Контрагент' |
		| 'ИП Смирнов' |
		| 'Магнит'     |
	И я нажимаю на кнопку с именем 'СформироватьНаименование'
	И я нажимаю на кнопку с именем 'ФормаПровестиИЗакрыть'
	И я жду закрытия окна 'Маркетинговое мероприятие (создание) *' в течение 20 секунд
	Тогда открылось окно 'Маркетинговое мероприятие'
	И Я закрываю окно 'Маркетинговое мероприятие'

Сценарий: 003.4. financial_results. Создание скидок

	* Создание скидок на основании маркетингового мероприятия
		И В командном интерфейсе я выбираю 'Маркетинг и продажи' 'Маркетинговое мероприятие'
		Тогда открылось окно 'Маркетинговое мероприятие'
		И в таблице "Список" я перехожу к строке:
			| 'Наименование'                                        |
			| 'Акция с 15.02.21 по 25.02.21 для ИП Смирнов; Магнит' |
		И я нажимаю на кнопку 'Скидки (наценки)'
		Тогда открылось окно 'Создание скидок'
		И в таблице "ТаблицаКонтрагентов" я перехожу к строке:
			| 'Контрагент' |
			| 'ИП Смирнов' |
		И в таблице "ТаблицаСоглашений" я нажимаю на кнопку с именем 'ТаблицаСоглашенийПодобратьСоглашение'
		Тогда открылось окно 'Подбор соглашений'
		И в таблице "Список" я выбираю текущую строку
		И я нажимаю на кнопку с именем 'ОК'
		Тогда открылось окно 'Создание скидок'
		И в таблице "ТаблицаКонтрагентов" я перехожу к строке:
			| 'Контрагент' |
			| 'Магнит'     |
		И в таблице "ТаблицаСоглашений" я нажимаю на кнопку с именем 'ТаблицаСоглашенийПодобратьСоглашение'
		Тогда открылось окно 'Подбор соглашений'
		И в таблице "Список" я выбираю текущую строку
		И я нажимаю на кнопку с именем 'ОК'
		Тогда открылось окно 'Создание скидок'
		И я нажимаю на кнопку с именем 'ОформитьСкидки'
		И Я закрываю окно 'Создание скидок'
		Тогда открылось окно 'Закрытие'
		И я нажимаю на кнопку 'Да'
		Тогда открылось окно 'Маркетинговое мероприятие'
		И Я закрываю окно 'Маркетинговое мероприятие'
	* Установка скидок
		И Я устанавливаю скидку 10% на Кефир
		И Я устанавливаю скидку 10% на Кефир

Сценарий: 003.5. financial_results. Создание заданий на доставку

	И Я создаю задание на доставку для ТС "АК234У Газель" на дату "20.02.2021"
	И Я создаю задание на доставку для ТС "АА777А Газель" на дату "21.02.2021"

Сценарий: 003.6. financial_results. Создание заказа в точку доставки Пятерочка 20.02.2021

	И Я начинаю создавать заказ клиента
	И в поле 'Дата' я ввожу текст '20.02.2021  0:00:00'
	И из выпадающего списка с именем "Организация" я выбираю по строке 'ТД_Молочный мир'
	И из выпадающего списка с именем "Холдинг" я выбираю по строке 'Х5'
	И из выпадающего списка с именем "Контрагент" я выбираю по строке 'Х5'
	И из выпадающего списка с именем "ТочкаДоставки" я выбираю по строке 'Пятерочка'
	И в поле 'Дата отгрузки' я ввожу текст '20.02.2021'
	И в поле 'Комментарий' я ввожу текст 'Заказ Пятерочка 20.02.2021'
	И я перехожу к следующему реквизиту
	И я перехожу к закладке с именем "СтраницаТовары"
	И я заполняю таблицу "Товары" данными
		| 'Количество' | 'Номенклатура' | 'Цена, RUB' |
		| '10,000'     | 'Молоко 3,2 %' | '50,00'     |
		| '10,000'     | 'Молоко 2,5 %' | '45,00'     |
		| '15,000'     | 'Кефир'        | '45,00'     |
		| '10,000'     | 'Творог'       | '110,00'    |
	И Я заканчиваю создавать заказ клиента

Сценарий: 003.7. financial_results. Создание заказа в точку доставки Магнит №1 20.02.2021

	И Я начинаю создавать заказ клиента
	И в поле 'Дата' я ввожу текст '20.02.2021  0:00:00'
	И из выпадающего списка с именем "Организация" я выбираю по строке 'ТД_Молочный мир'
	И из выпадающего списка с именем "Холдинг" я выбираю по строке 'Магнит'
	И из выпадающего списка с именем "Контрагент" я выбираю по строке 'Магнит'
	И из выпадающего списка с именем "ТочкаДоставки" я выбираю по строке 'Магнит №1'
	И в поле 'Дата отгрузки' я ввожу текст '20.02.2021'
	И в поле 'Комментарий' я ввожу текст 'Заказ Магнит 20.02.2021'
	И я перехожу к следующему реквизиту
	И я перехожу к закладке с именем "СтраницаТовары"
	И я заполняю таблицу "Товары" данными
		| 'Количество' | 'Номенклатура' | 'Цена, RUB' |
		| '10,000'     | 'Молоко 3,2 %' | '52,00'     |
		| '5,000'      | 'Молоко 2,5 %' | '45,00'     |
		| '10,000'     | 'Кефир'        | '50,00'     |
		| '10,000'     | 'Творог'       | '115,00'    |
	И Я заканчиваю создавать заказ клиента

Сценарий: 003.8. financial_results. Создание заказа в точку доставки ИП Смирнов 21.02.2021

	И Я начинаю создавать заказ клиента
	И в поле 'Дата' я ввожу текст '21.02.2021  0:00:00'
	И из выпадающего списка с именем "Организация" я выбираю по строке 'ТД_Молочный мир'
	И из выпадающего списка с именем "Холдинг" я выбираю по строке 'ИП Смирнов'
	И из выпадающего списка с именем "Контрагент" я выбираю по строке 'ИП Смирнов'
	И из выпадающего списка с именем "ТочкаДоставки" я выбираю по строке 'ИП Смирнов'
	И в поле 'Дата отгрузки' я ввожу текст '21.02.2021'
	И в поле 'Комментарий' я ввожу текст 'Заказ ИП Смирнов 21.02.2021'
	И я перехожу к следующему реквизиту
	И я перехожу к закладке с именем "СтраницаТовары"
	И я заполняю таблицу "Товары" данными
		| 'Количество' | 'Номенклатура' | 'Цена, RUB' |
		| '10,000'     | 'Молоко 3,2 %' | '55,00'     |
		| '10,000'     | 'Молоко 2,5 %' | '50,00'     |
		| '5,000'      | 'Кефир'        | '55,56'     |
		| '8,000'      | 'Творог'       | '120,00'    |
	И Я заканчиваю создавать заказ клиента

Сценарий: 003.9. financial_results. Создание реализации в точку доставки Пятерочка 20.02.2021

	И Я начинаю создавать реализацию товаров на основании заказа "Заказ Пятерочка 20.02.2021"
	И в поле с именем 'Дата' я ввожу текст '20.02.2021'
	И я нажимаю кнопку выбора у поля с именем "ЗаданиеНаДоставку"
	Тогда открылось окно 'Задания на доставку'
	И в таблице "Список" я перехожу к строке:
		| '(План) Начало рейса ' | '(План) Окончание рейса' | 'Водитель'     | 'Дата'       | 'Статус'     | 'ТС'            |
		| '20.02.2021 0:00:00'   | '20.02.2021 0:00:00'     | 'Любимов П.С.' | '20.02.2021' | 'Отправлено' | 'АК234У Газель' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Реализация товаров (создание) *'
	И я перехожу к закладке с именем "СтраницаТовары"
	И Я выбираю серию "000001 до 06.03.2021 0:00:00" для номенклатуры "Молоко 3,2 %"
	И Я выбираю серию "000001 до 06.03.2021 0:00:00" для номенклатуры "Молоко 2,5 %"
	И Я выбираю серию "000001 до 06.03.2021 0:00:00" для номенклатуры "Кефир"
	И Я выбираю серию "000001 до 06.03.2021 0:00:00" для номенклатуры "Творог"
	И Я заканчиваю создавать реализацию товаров на основании заказа
	
Сценарий: 003.10. financial_results. Создание реализации в точку доставки Магнит №1 20.02.2021

	И Я начинаю создавать реализацию товаров на основании заказа "Заказ Магнит 20.02.2021"
	И в поле с именем 'Дата' я ввожу текст '20.02.2021'
	И я нажимаю кнопку выбора у поля с именем "ЗаданиеНаДоставку"
	Тогда открылось окно 'Задания на доставку'
	И в таблице "Список" я перехожу к строке:
		| '(План) Начало рейса ' | '(План) Окончание рейса' | 'Водитель'     | 'Дата'       | 'Статус'     | 'ТС'            |
		| '20.02.2021 0:00:00'   | '20.02.2021 0:00:00'     | 'Любимов П.С.' | '20.02.2021' | 'Отправлено' | 'АК234У Газель' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Реализация товаров (создание) *'
	И я перехожу к закладке с именем "СтраницаТовары"
	И Я выбираю серию "000001 до 06.03.2021 0:00:00" для номенклатуры "Молоко 3,2 %"
	И Я выбираю серию "000001 до 06.03.2021 0:00:00" для номенклатуры "Молоко 2,5 %"
	И Я выбираю серию "000001 до 06.03.2021 0:00:00" для номенклатуры "Кефир"
	И Я выбираю серию "000001 до 06.03.2021 0:00:00" для номенклатуры "Творог"
	И Я заканчиваю создавать реализацию товаров на основании заказа

Сценарий: 003.11. financial_results. Создание реализации в точку доставки ИП Смирнов 21.02.2021

	И Я начинаю создавать реализацию товаров на основании заказа "Заказ ИП Смирнов 21.02.2021"
	И в поле с именем 'Дата' я ввожу текст '21.02.2021'
	И я нажимаю кнопку выбора у поля с именем "ЗаданиеНаДоставку"
	Тогда открылось окно 'Задания на доставку'
	И в таблице "Список" я перехожу к строке:
		| '(План) Начало рейса ' | '(План) Окончание рейса' | 'Водитель'     | 'Дата'       | 'Статус'     | 'ТС'            |
		| '21.02.2021 0:00:00'   | '21.02.2021 0:00:00'     | 'Сергеев Л.Н.' | '21.02.2021' | 'Отправлено' | 'АА777А Газель' |
	И я нажимаю на кнопку с именем 'ФормаВыбрать'
	Тогда открылось окно 'Реализация товаров (создание) *'
	И я перехожу к закладке с именем "СтраницаТовары"
	И Я выбираю серию "000001 до 07.03.2021 0:00:00" для номенклатуры "Молоко 3,2 %"
	И Я выбираю серию "000001 до 07.03.2021 0:00:00" для номенклатуры "Молоко 2,5 %"
	И Я выбираю серию "000001 до 06.03.2021 0:00:00" для номенклатуры "Кефир"
	И Я выбираю серию "000001 до 06.03.2021 0:00:00" для номенклатуры "Творог"
	И Я заканчиваю создавать реализацию товаров на основании заказа

Сценарий: 003.12. financial_results. Создание поступлений товаров и услуг 20.02.2021 - Аренда

	И Я начинаю создавать поступление товаров на дату "20.02.2021"
	И из выпадающего списка с именем "Холдинг" я выбираю по строке 'Арендодатель'
	И из выпадающего списка с именем "Контрагент" я выбираю по строке 'Арендодатель'
	И из выпадающего списка "Организация" я выбираю по строке 'ТД_Молочный мир'
	И из выпадающего списка "Склад" я выбираю по строке 'Склад готовой продукции'
	И из выпадающего списка "Статус" я выбираю точное значение 'Подтверждено'
	И в поле 'Номер вх.' я ввожу текст '1'
	И я перехожу к закладке "Факт"
	И я заполняю таблицу "Услуги" данными
		| 'Номенклатура' | 'Количество' | 'Цена, RUB' | 'Статья затрат' | 'Подразделение' | 'Направление деятельности' |
		| 'Аренда'       | '1,000'      | '400,00'    | 'Аренда'        | 'Склад'         | 'Основное направление'     |
	И Я заканчиваю создавать поступление товаров и услуг