#language: ru

@demo_base_milk
@mutual_settlements

Функционал: 004. mutual_settlements. Создание документов взаиморасчетов

Сценарий: 004.1. mutual_settlements. Создание поступления ДС для реализации товаров Лента

	Дано Я подключаюсь под "Администратор"
	И Я начинаю создавать поступление ДС на основании реализации товаров "Реализация Лента 18.03.2021"
	И в поле с именем 'Дата' я ввожу текст '24.03.2021  0:00:00'
	И в поле с именем 'ДатаПлатежа' я ввожу текст '24.03.2021'
	И Я заканчиваю создавать поступление ДС на основании реализации товаров

Сценарий: 004.2. mutual_settlements. Создание поступления ДС для реализации товаров Перекресток

	И Я начинаю создавать поступление ДС на основании реализации товаров "Реализация Перекресток 18.03.2021"
	И в поле с именем 'ДатаПлатежа' я ввожу текст '26.03.2021'
	И в поле с именем 'Дата' я ввожу текст '26.03.2021  0:00:00'
	И я перехожу к закладке с именем "СтраницаРасшифровкаПлатежа"
	И в таблице "РасшифровкаПлатежа" я перехожу к строке:
		| 'Объект расчета'                         | 'Основание'                              | 'Сумма'     |
		| 'Соглашение с Перекрестком с 01.01.2021' | 'Соглашение с Перекрестком с 01.01.2021' | '27 750,00' |
	И в таблице "РасшифровкаПлатежа" в поле с именем 'РасшифровкаПлатежаСумма' я ввожу текст '15 000,00'
	И в таблице "РасшифровкаПлатежа" я завершаю редактирование строки
	И Я заканчиваю создавать поступление ДС на основании реализации товаров

Сценарий: 004.3. mutual_settlements. Создание приходного кассового ордера для реализации товаров Перекресток

	И Я начинаю создавать приходный кассовый ордер на основании реализации товаров "Реализация Перекресток 18.03.2021"
	И из выпадающего списка с именем 'Касса' я выбираю по строке 'Основная касса Нижегородский МК'
	И в поле с именем 'Дата' я ввожу текст '27.03.2021  0:00:00'
	И в поле с именем 'ДатаПлатежа' я ввожу текст '27.03.2021  0:00:00'
	И я перехожу к закладке с именем "СтраницаРасшифровкаПлатежа"
	И в таблице "РасшифровкаПлатежа" я перехожу к строке:
		| 'Объект расчета'                         | 'Основание'                              | 'Сумма'     |
		| 'Соглашение с Перекрестком с 01.01.2021' | 'Соглашение с Перекрестком с 01.01.2021' | '27 750,00' |
	И в таблице "РасшифровкаПлатежа" в поле с именем 'РасшифровкаПлатежаСумма' я ввожу текст '12 750,00'
	И в таблице "РасшифровкаПлатежа" я завершаю редактирование строки
	И Я заканчиваю создавать приходный кассовый ордер на основании реализации товаров

Сценарий: 004.4. mutual_settlements. Создание приходного кассового ордера для реализации товаров Табрис

	И Я начинаю создавать приходный кассовый ордер на основании реализации товаров "Реализация Табрис 18.03.2021"
	И в поле с именем 'Дата' я ввожу текст '26.03.2021  0:00:00'
	И из выпадающего списка с именем 'Касса' я выбираю по строке 'Основная касса Нижегородский МК'
	И в поле с именем 'ДатаПлатежа' я ввожу текст '26.03.2021  0:00:00'
	И Я заканчиваю создавать приходный кассовый ордер на основании реализации товаров

Сценарий: 004.5. mutual_settlements. Возврат оплаты клиенту Перекресток

	И Я начинаю создавать расходный кассовый ордер с операцией "Возврат оплаты клиенту"
	* Заполнение вкладки Основное
		И в поле с именем 'Дата' я ввожу текст '29.03.2021  0:00:00'
		И из выпадающего списка с именем "Организация" я выбираю по строке 'Нижегородский МК'
		И из выпадающего списка с именем "Подразделение" я выбираю по строке 'Бухгалтерия'
		И из выпадающего списка с именем "Касса" я выбираю по строке 'Основная Касса Нижегородский МК'
		И из выпадающего списка с именем "Контрагент" я выбираю по строке 'Перекресток'
		И в поле с именем 'Сумма' я ввожу текст '1 000,00'
		И в поле с именем 'ДатаПлатежа' я ввожу текст '29.03.2021  0:00:00'
	* Заполнение расшифровки платежа
		И я перехожу к закладке с именем "СтраницаРасшифровкаПлатежа"
		И я заполняю таблицу "РасшифровкаПлатежа" данными
			| 'Основание'                              | 'Сумма'    |
			| 'Соглашение с Перекрестком с 01.01.2021' | '1 000,00' |
		И в таблице "РасшифровкаПлатежа" я нажимаю кнопку выбора у реквизита с именем "РасшифровкаПлатежаОбъектРасчета"
		И Я выбираю "Соглашение с Перекрестком с 01.01.2021" с типом "Соглашение с контрагентом"
		Тогда открылось окно 'Расходный кассовый ордер (создание) *'
		И в таблице "РасшифровкаПлатежа" я завершаю редактирование строки
	И Я заканчиваю создавать расходный кассовый ордер

Сценарий: 004.6. mutual_settlements. Взаимозачет задолженности Перекрестка на Ленту

	И Я начинаю создавать взаимозачет задолженностей
	И в поле с именем 'Дата' я ввожу текст '29.03.2021  0:00:00'
	И из выпадающего списка с именем "Дебитор" я выбираю по строке 'Перекресток'
	И из выпадающего списка с именем "Кредитор" я выбираю по строке 'Лента'
	И из выпадающего списка с именем "Организация" я выбираю по строке 'Нижегородский МК'
	И в поле с именем 'ДатаСписания' я ввожу текст '29.03.2021'
	И в поле с именем 'ДатаОприходования' я ввожу текст '29.03.2021'
	И в таблице "ДебиторскаяЗадолженность" я нажимаю на кнопку с именем 'ДебиторскаяЗадолженностьЗаполнитьПоОстаткам'
	И я перехожу к закладке с именем "СтраницаКредиторскаяЗадолженность"
	И я заполняю таблицу "КредиторскаяЗадолженность" данными
		| 'Оплатить до' | 'Основание'                        | 'Сумма'  | 'Валюта' |
		| '31.03.2021'  | 'Соглашение с Лентой с 01.01.2021' | '500,00' | 'RUB'    |
	И в таблице "КредиторскаяЗадолженность" я нажимаю кнопку выбора у реквизита с именем "КредиторскаяЗадолженностьОбъектРасчета"
	И Я выбираю "Соглашение с Лентой с 01.01.2021" с типом "Соглашение с контрагентом"
	Тогда открылось окно 'Взаимозачет задолженностей (создание) *'
	И в таблице "КредиторскаяЗадолженность" я завершаю редактирование строки
	И Я заканчиваю создавать взаимозачет задолженностей

Сценарий: 004.7. mutual_settlements. Списание задолженности Лента

	И Я начинаю создавать списание задолженности
	И из выпадающего списка с именем "Организация" я выбираю по строке 'Нижегородский МК'
	И из выпадающего списка с именем "Подразделение" я выбираю по строке 'Бухгалтерия'
	И из выпадающего списка с именем "Контрагент" я выбираю по строке 'Лента'
	И в поле с именем 'ДатаСписания' я ввожу текст '31.03.2021'
	И я перехожу к закладке с именем "СтраницаРасшифровкаСписания"
	И в таблице "Задолженность" я нажимаю на кнопку с именем 'ЗадолженностьЗаполнитьПоОстаткам'
	И Я заканчиваю создавать списание задолженности
	
Сценарий: 004.8. mutual_settlements. Сверка взаиморасчетов для Перекрестка

	И Я начинаю создавать сверку взаиморасчетов для организации "Нижегородский МК" с подразделением "Бухгалтерия" по "Перекресток"
	И в поле с именем 'Дата' я ввожу текст '31.03.2021  0:00:00'
	И я нажимаю кнопку выбора у поля с именем "ПериодДействия"
	И Я указываю в диалоге стандартного периода период с "01.03.2021" по "31.03.2021"
	Тогда открылось окно 'Сверка взаиморасчетов (создание) *'
	И в таблице "Взаиморасчеты" я нажимаю на кнопку с именем 'ВзаиморасчетыЗаполнитьЗаПериод'
	И Я заканчиваю создавать сверку взаиморасчетов

Сценарий: 004.9. mutual_settlements. Сверка взаиморасчетов для Ленты

	И Я начинаю создавать сверку взаиморасчетов для организации "Нижегородский МК" с подразделением "Бухгалтерия" по "Лента"
	И в поле с именем 'Дата' я ввожу текст '31.03.2021  0:00:00'
	И я нажимаю кнопку выбора у поля с именем "ПериодДействия"
	И Я указываю в диалоге стандартного периода период с "01.03.2021" по "31.03.2021"
	Тогда открылось окно 'Сверка взаиморасчетов (создание) *'
	И в таблице "Взаиморасчеты" я нажимаю на кнопку с именем 'ВзаиморасчетыЗаполнитьЗаПериод'
	И Я заканчиваю создавать сверку взаиморасчетов

Сценарий: 004.10. mutual_settlements. Сверка взаиморасчетов для Табрис

	И Я начинаю создавать сверку взаиморасчетов для организации "Нижегородский МК" с подразделением "Бухгалтерия" по "Табрис"
	И в поле с именем 'Дата' я ввожу текст '31.03.2021  0:00:00'
	И я нажимаю кнопку выбора у поля с именем "ПериодДействия"
	И Я указываю в диалоге стандартного периода период с "01.03.2021" по "31.03.2021"
	Тогда открылось окно 'Сверка взаиморасчетов (создание) *'
	И в таблице "Взаиморасчеты" я нажимаю на кнопку с именем 'ВзаиморасчетыЗаполнитьЗаПериод'
	И Я заканчиваю создавать сверку взаиморасчетов

Сценарий: 004.11. mutual_settlements. Создание приходного кассового ордера - прочий приход

	И Я начинаю создавать приходный кассовый ордер с операцией "Прочие приходы"
	И в поле с именем 'Дата' я ввожу текст '01.04.2021  0:00:00'
	И из выпадающего списка с именем "Организация" я выбираю по строке 'Нижегородский МК'
	И из выпадающего списка с именем "Подразделение" я выбираю по строке 'Бухгалтерия'
	И из выпадающего списка с именем "Касса" я выбираю по строке 'Основная Касса Нижегородский МК'
	И в поле с именем 'СуммаДокумента' я ввожу текст '7 000,00'
	И из выпадающего списка с именем "Валюта" я выбираю по строке 'RUB'
	И в поле с именем 'ДатаПлатежа' я ввожу текст '01.04.2021  0:00:00'
	И Я заканчиваю создавать приходный кассовый ордер

Сценарий: 004.12. mutual_settlements. Создание расходного кассового ордера - Прочий расход

	И Я начинаю создавать расходный кассовый ордер с операцией "Прочие расходы"
	И в поле с именем 'Дата' я ввожу текст '02.04.2021  0:00:00'
	И из выпадающего списка с именем "Организация" я выбираю по строке 'Нижегородский МК'
	И из выпадающего списка с именем "Подразделение" я выбираю по строке 'Бухгалтерия'
	И из выпадающего списка с именем "Касса" я выбираю по строке 'Основная Касса Нижегородский МК'
	И в поле с именем 'Сумма' я ввожу текст '1 000,00'
	И из выпадающего списка с именем "Валюта" я выбираю по строке 'RUB'
	И в поле с именем 'ДатаПлатежа' я ввожу текст '02.04.2021  0:00:00'
	И Я заканчиваю создавать расходный кассовый ордер

Сценарий: 004.13. mutual_settlements. Создание расходного кассового ордера - Инкассация в банк

	И Я начинаю создавать расходный кассовый ордер с операцией "Инкассация в банк"
	И в поле с именем 'Дата' я ввожу текст '03.04.2021  0:00:00'
	И из выпадающего списка с именем "Организация" я выбираю по строке 'Нижегородский МК'
	И из выпадающего списка с именем "Подразделение" я выбираю по строке 'Бухгалтерия'
	И из выпадающего списка с именем "Касса" я выбираю по строке 'Основная Касса Нижегородский МК'
	И из выпадающего списка с именем "БанковскийСчет" я выбираю по строке 'СберБанк'
	И в поле с именем 'Сумма' я ввожу текст '3 000,00'
	И из выпадающего списка с именем "Валюта" я выбираю по строке 'RUB'
	И в поле с именем 'ДатаПлатежа' я ввожу текст '03.04.2021  0:00:00'
	И Я заканчиваю создавать расходный кассовый ордер

Сценарий: 004.14. mutual_settlements. Создание расходного кассового ордера - Выдача подотчетнику

	И Я начинаю создавать расходный кассовый ордер с операцией "Выдача подотчетнику"
	И в поле с именем 'Дата' я ввожу текст '04.04.2021  0:00:00'
	И из выпадающего списка с именем "Организация" я выбираю по строке 'Нижегородский МК'
	И из выпадающего списка с именем "Подразделение" я выбираю по строке 'Бухгалтерия'
	И из выпадающего списка с именем "Касса" я выбираю по строке 'Основная Касса Нижегородский МК'
	И из выпадающего списка с именем "ПодотчетноеЛицо" я выбираю по строке 'Комаров И.О.'
	И в поле с именем 'Сумма' я ввожу текст '2 000,00'
	И из выпадающего списка с именем "Валюта" я выбираю по строке 'RUB'
	И в поле с именем 'ДатаПлатежа' я ввожу текст '04.04.2021  0:00:00'
	И Я заканчиваю создавать расходный кассовый ордер

Сценарий: 004.15. mutual_settlements. Создание приходного кассового ордера - Поступление от подотчетного лица

	И Я начинаю создавать приходный кассовый ордер с операцией "Поступление от подотчетного лица"
	И в поле с именем 'Дата' я ввожу текст '05.04.2021  0:00:00'
	И из выпадающего списка с именем "Организация" я выбираю по строке 'Нижегородский МК'
	И из выпадающего списка с именем "Подразделение" я выбираю по строке 'Бухгалтерия'
	И из выпадающего списка с именем "Касса" я выбираю по строке 'Основная Касса Нижегородский МК'
	И из выпадающего списка с именем "ПодотчетноеЛицо" я выбираю по строке 'Комаров И.О.'
	И в поле с именем 'СуммаДокумента' я ввожу текст '1 000,00'
	И из выпадающего списка с именем "Валюта" я выбираю по строке 'RUB'
	И в поле с именем 'ДатаПлатежа' я ввожу текст '05.04.2021  0:00:00'
	И Я заканчиваю создавать приходный кассовый ордер

Сценарий: 004.16. mutual_settlements. Создание листа кассовой книги на 26.03.2021	
	
	И Я начинаю создавать лист кассовой книги "Основная Касса Нижегородский МК" на "26.03.2021" для организации "Нижегородский МК" с подразделением "Бухгалтерия"
	И Я заканчиваю создавать лист кассовой книги

Сценарий: 004.17. mutual_settlements. Создание листа кассовой книги на 27.03.2021	
	
	И Я начинаю создавать лист кассовой книги "Основная Касса Нижегородский МК" на "27.03.2021" для организации "Нижегородский МК" с подразделением "Бухгалтерия"
	И Я заканчиваю создавать лист кассовой книги

Сценарий: 004.18. mutual_settlements. Создание листа кассовой книги на 29.03.2021	
	
	И Я начинаю создавать лист кассовой книги "Основная Касса Нижегородский МК" на "29.03.2021" для организации "Нижегородский МК" с подразделением "Бухгалтерия"
	И Я заканчиваю создавать лист кассовой книги

Сценарий: 004.19. mutual_settlements. Создание листа кассовой книги на 01.04.2021	
	
	И Я начинаю создавать лист кассовой книги "Основная Касса Нижегородский МК" на "01.04.2021" для организации "Нижегородский МК" с подразделением "Бухгалтерия"
	И Я заканчиваю создавать лист кассовой книги

Сценарий: 004.20. mutual_settlements. Создание листа кассовой книги на 02.04.2021	
	
	И Я начинаю создавать лист кассовой книги "Основная Касса Нижегородский МК" на "02.04.2021" для организации "Нижегородский МК" с подразделением "Бухгалтерия"
	И Я заканчиваю создавать лист кассовой книги

Сценарий: 004.21. mutual_settlements. Создание листа кассовой книги на 03.04.2021	
	
	И Я начинаю создавать лист кассовой книги "Основная Касса Нижегородский МК" на "03.04.2021" для организации "Нижегородский МК" с подразделением "Бухгалтерия"
	И Я заканчиваю создавать лист кассовой книги

Сценарий: 004.22. mutual_settlements. Создание листа кассовой книги на 04.04.2021	
	
	И Я начинаю создавать лист кассовой книги "Основная Касса Нижегородский МК" на "04.04.2021" для организации "Нижегородский МК" с подразделением "Бухгалтерия"
	И Я заканчиваю создавать лист кассовой книги

Сценарий: 004.23. mutual_settlements. Создание листа кассовой книги на 05.04.2021	
	
	И Я начинаю создавать лист кассовой книги "Основная Касса Нижегородский МК" на "05.04.2021" для организации "Нижегородский МК" с подразделением "Бухгалтерия"
	И Я заканчиваю создавать лист кассовой книги

Сценарий: 004.24. mutual_settlements. Создание инвентаризации ДС - 10.04.2021

	И Я начинаю создавать инвентаризацию ДС на "10.04.2021" для организации "Нижегородский МК" с подразделением "Бухгалтерия"
	И в таблице "ДенежныеСредства" я перехожу к строке:
		| 'Валюта' | 'Касса'                           | 'Сумма по учету' | 'Сумма расхождение' |
		| 'RUB'    | 'Основная Касса Нижегородский МК' | '54 800,00'      | '54 800,00'         |
	И в таблице "ДенежныеСредства" в поле с именем 'ДенежныеСредстваСуммаПоФакту' я ввожу текст '55 200,00'
	И в таблице "ДенежныеСредства" я завершаю редактирование строки
	И в поле с именем 'Комментарий' я ввожу текст 'Инвентаризация 10.04'
	И Я заканчиваю создавать инвентаризацию ДС

Сценарий: 004.25. mutual_settlements. Создание приходного кассового по инвентаризации ДС - 10.04.2021

	И Я начинаю создавать приходный кассовый ордер по инвентаризации ДС "Инвентаризация 10.04" на "10.04.2021"
	И Я заканчиваю создавать приходный кассовый ордер по инвентаризации ДС

Сценарий: 004.26. mutual_settlements. Создание инвентаризации ДС - 15.04.2021

	И Я начинаю создавать инвентаризацию ДС на "15.04.2021" для организации "Нижегородский МК" с подразделением "Бухгалтерия"
	И в таблице "ДенежныеСредства" я перехожу к строке:
		| 'Валюта' | 'Касса'                           | 'Сумма по учету' | 'Сумма расхождение' |
		| 'RUB'    | 'Основная Касса Нижегородский МК' | '55 200,00'      | '55 200,00'         |
	И в таблице "ДенежныеСредства" в поле с именем 'ДенежныеСредстваСуммаПоФакту' я ввожу текст '53 800,00'
	И в таблице "ДенежныеСредства" я завершаю редактирование строки
	И в поле с именем 'Комментарий' я ввожу текст 'Инвентаризация 15.04'
	И Я заканчиваю создавать инвентаризацию ДС

Сценарий: 004.27. mutual_settlements. Создание расходного кассового по инвентаризации ДС - 15.04.2021

	И Я начинаю создавать расходный кассовый ордер по инвентаризации ДС "Инвентаризация 15.04" на "15.04.2021"
	И Я заканчиваю создавать расходный кассовый ордер по инвентаризации ДС