#language: ru

@demo_base_milk
@mutual_settlements

Функционал: 002. mutual_settlements. Создание НСИ Взаиморасчетов

Сценарий: 002.1. mutual_settlements. Создание группы взаиморасчетов

	Дано Я подключаюсь под "Администратор"
	И Я создаю группу взаиморасчетов "Молоко"

Сценарий: 002.2. mutual_settlements. Заполенение группы взаиморасчетов в товарной категории

	И Я заполняю группу взаиморасчетов в товарной категории "Молоко"

Сценарий: 002.3. mutual_settlements. Создание графиков оплаты

	* Перекресток
		И Я начинаю создавать график оплаты "Перекресток" с формой оплаты "Любая"
		* Заполнение основного графика
			И Я добавляю вариант оплаты "Предоплата" с моментом определения "На дату накладной" со сдвигом "2" и процентом "40,00"
			И Я добавляю вариант оплаты "Кредит" с моментом определения "На дату отгрузки" со сдвигом "3" и процентом "60,00"
		* Заполнение уточняющего графика оплаты
			И я разворачиваю группу с именем "Группа_Уточнения"
			И Я добавляю уточнение варианта оплаты "Предоплата" для "Молоко" с моментом определения "На дату заказа" со сдвигом "4" и процентом "20,00"
			И Я добавляю уточнение варианта оплаты "Кредит" для "Молоко" с моментом определения "На дату доставки" со сдвигом "6" и процентом "80,00"
		* Установка календаря
			И я меняю значение переключателя с именем 'РежимУчетаОтсрочки' на 'по календарным дням'
		И я заканчиваю создавать график оплаты
	* Табрис
		И Я начинаю создавать график оплаты "Табрис" с формой оплаты "Наличными"
		* Заполнение основного графика
			И Я добавляю вариант оплаты "Предоплата" с моментом определения "На дату заказа" со сдвигом "3" и процентом "50,00"
			И Я добавляю вариант оплаты "Кредит" с моментом определения "На дату доставки" со сдвигом "4" и процентом "50,00"
		* Заполнение уточняющего графика оплаты
			И я разворачиваю группу с именем "Группа_Уточнения"
			И Я добавляю уточнение варианта оплаты "Предоплата" для "Молоко" с моментом определения "На дату накладной" со сдвигом "2" и процентом "30,00"
			И Я добавляю уточнение варианта оплаты "Кредит" для "Молоко" с моментом определения "На дату отгрузки" со сдвигом "2" и процентом "70,00"
		* Установка календаря
			И я меняю значение переключателя с именем 'РежимУчетаОтсрочки' на 'по рабочему календарю'
			И из выпадающего списка с именем "Календарь" я выбираю по строке 'Российская Федерация'
		И я заканчиваю создавать график оплаты
	* Лента
		И Я начинаю создавать график оплаты "Лента" с формой оплаты "Безналичными"
		* Заполнение основного графика
			И Я добавляю вариант оплаты "Предоплата" с моментом определения "На дату накладной" со сдвигом "1" и процентом "10,00"
			И Я добавляю вариант оплаты "Кредит" с моментом определения "На дату отгрузки" со сдвигом "3" и процентом "90,00"
		* Заполнение уточняющего графика оплаты
			И я разворачиваю группу с именем "Группа_Уточнения"
			И Я добавляю уточнение варианта оплаты "Предоплата" для "Молоко" с моментом определения "На дату накладной" со сдвигом "2" и процентом "50,00"
			И Я добавляю уточнение варианта оплаты "Кредит" для "Молоко" с моментом определения "На дату отгрузки" со сдвигом "3" и процентом "50,00"
		* Установка календаря
			И я меняю значение переключателя с именем 'РежимУчетаОтсрочки' на 'по календарным дням'
		И я заканчиваю создавать график оплаты

Сценарий: 002.4. mutual_settlements. Создание соглашений

	И Я создаю соглашение "Соглашение с Перекрестком с 01.01.2021" с покупателем "Перекресток" и видом цены "Цена отгрузки для Перекрестка" для организации "Нижегородский МК" с графиком оплаты "Перекресток" и порядком расчетов "По соглашениям"
	И Я создаю соглашение "Соглашение с Лентой с 01.01.2021" с покупателем "Лента" и видом цены "Цена отгрузки для Ленты" для организации "Нижегородский МК" с графиком оплаты "Лента" и порядком расчетов "По соглашениям"
	И Я создаю соглашение "Соглашение с Табрис с 01.01.2021" с покупателем "Табрис" и видом цены "Цена отгрузки для Табрис" для организации "Нижегородский МК" с графиком оплаты "Табрис" и порядком расчетов "По накладным"

Сценарий: 002.5. mutual_settlements. Создание касс

	И Я создаю кассу "Основная Касса Нижегородский МК" для организации "Нижегородский МК" и подразделения "Бухгалтерия" с валютой "RUB"
