@chcp 65001>nul

@echo Чтение настроек

:: Путь к корню проекта. Следует менять только если вы понимаете что тут написано
@set RUNNER_WORKSPACE=%~dp0\..

:: Путь к обработке VA
@set RUNNER_PATHVANESSA=E:\GIT_Repo\github\vanessa-automation\vanessa-automation.epf

:: Настройки раннера
@set RUNNER_SETTINGS_DEFAULT=%RUNNER_WORKSPACE%\.local\env.json

:: Настройки VA, которые будут применены при открытии
@set VBParams=%RUNNER_WORKSPACE%\.local\VA.json

:: Настройки подключения к хранилищу для скриптов, которые cf получают из хранилища
@set STORAGE_NAME="\\Dellg5\хранилища\wms"
@set STORAGE_USER="tests"

:: путь к cf для скриптов, которые cf получают из файла
@set SRC_CF=D:\к2\1cv8.cf

:: Путь к публичной ссылке на яндекс.диске где лежит 1cv8.cf
@set YDISK_PUBLIC_HREF=https://yadi.sk/d/sssssssss
:: Каталог для сохранения папки с яндекс.диска
@set YDISK_DIR=%RUNNER_WORKSPACE%\build\
:: Путь к cf из загруженного с яднекс.диска
@set YDISK_CF=%YDISK_DIR%\1cv8.cf

:: Абсолютный путь к DT, из которого будет разворачиваться база для выполнения тестов
@set INIT_DT=%RUNNER_WORKSPACE%\BaseWithLicenseAndUser.dt

:: Абсолютный путь к DT, в который будет сохранятся база после тестов
@set RESULT_DT=%RUNNER_WORKSPACE%\build\demo.dt

:: Абсолютный путь к DT, из которого будет разворачиваться база для наполнения демо базы
@set MODEL_DT=%RUNNER_WORKSPACE%\build\model.dt

